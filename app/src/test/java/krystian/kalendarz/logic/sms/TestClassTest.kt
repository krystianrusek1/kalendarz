package krystian.kalendarz.logic.sms

import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TestClassTest {

    @Mock
    lateinit var testclass2: Testclass2

    lateinit var sun: TestClass

    @ExperimentalCoroutinesApi
    val testDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        sun = TestClass(testclass2)
    }

    @Test
    fun test() {

        runBlocking {
            sun.triggerAll()
            //delay(3000)

            verify(testclass2).sendString("text1")
            verify(testclass2).sendString("text2")
        }
    }
}