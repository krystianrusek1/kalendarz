package krystian.kalendarz.logic.sms

import android.telephony.SmsManager
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.work.WorkManager
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import krystian.kalendarz.aplication.AppLiveCycle
import krystian.kalendarz.aplication.PendingIntentFactory
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.data.sms.SendSMSRepository
import krystian.kalendarz.logic.sms.SyncSmsManagerImpl.Companion.listDatesOfTry
import krystian.kalendarz.service.authenticator.AppAccountManager
import org.hamcrest.CoreMatchers.*
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import org.hamcrest.CoreMatchers.`is` as coreMatchersIs

/**
 * Tests [SyncSmsManagerImpl]
 **/
@RunWith(MockitoJUnitRunner::class)
class SyncSmsManagerImplTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var appAccountManager: AppAccountManager

    @Mock
    lateinit var sendSMSRepository: SendSMSRepository

    @Mock
    lateinit var pendingIntentFactory: PendingIntentFactory

    @Mock
    lateinit var smsManager: SmsManager

    @Mock
    lateinit var workManager: WorkManager

    @Mock
    lateinit var appLiveCycle: AppLiveCycle

    val smsListLiveData = MutableLiveData<List<SmsEntity>>()

    lateinit var sun: SyncSmsManagerImpl

    @ExperimentalCoroutinesApi
    val testDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        whenever(sendSMSRepository.getSmsList()).thenReturn(smsListLiveData)
        sun = SyncSmsManagerImpl(appAccountManager, sendSMSRepository, pendingIntentFactory, smsManager, workManager,appLiveCycle)
    }

    @Test
    fun refreshReadyToSentSms_smsReadToSendList() {

        runBlocking {
            val smsEntity1 = createSms(1, "phonenumer1", "test1", Date())
            val smsEntity2 = createSms(2, "phonenumer2", "test2", Date())
            val listSms = arrayListOf(smsEntity1, smsEntity2)

            whenever(sendSMSRepository.getSmsReadyToSendList(any(), any())).thenReturn(listSms)

            val arrayList1 = arrayListOf(smsEntity1.text)
            val arrayList2 = arrayListOf(smsEntity2.text)
            whenever(smsManager.divideMessage(smsEntity1.text)).thenReturn(arrayList1)
            whenever(smsManager.divideMessage(smsEntity2.text)).thenReturn(arrayList2)

            sun.refreshReadyToSentSms()

            verify(smsManager).sendMultipartTextMessage(
                eq(smsEntity1.phoneNumber),
                isNull(),
                eq(arrayList1),
                anyOrNull(),
                anyOrNull()
            )
            verify(smsManager).sendMultipartTextMessage(
                eq(smsEntity2.phoneNumber),
                isNull(),
                eq(arrayList2),
                anyOrNull(),
                anyOrNull()
            )

            assertThat(smsEntity1.isPending, `is`(true))
            assertThat(smsEntity1.lastTryDate, any(Date::class.java))
            assertThat(smsEntity1.numberOfTry, `is`(1))
            assertThat(smsEntity1.nextDateOfTry.time, `is`(listDatesOfTry[0] + smsEntity1.lastTryDate!!.time))
        }
    }

    @Test
    fun onSmsSent_Success() {
        runBlocking {
            val smsEntity1 = createSms(1, "phonenumer1", "test1", Date())
            val smsEntity2 = createSms(1, "phonenumer2", "test2", Date())
            smsEntity2.isPending = true

            whenever(sendSMSRepository.getSms(smsEntity1.id!!)).thenReturn(smsEntity2)

            sun.onSmsSent(smsEntity1, null)

            assertThat(smsEntity2.isDelivered, `is`(false))
            assertThat(smsEntity2.isSent, `is`(true))
            assertThat(smsEntity2.sentDate, any(Date::class.java))
            assertThat(smsEntity2.isDelivered, `is`(false))
            assertThat(smsEntity2.deliveredDate, `is`(nullValue()))
            verify(sendSMSRepository).addOrUpdateSms(smsEntity2)
        }
    }

    @Test
    fun onSmsSent_Error() {
        runBlocking {
            val smsEntity1 = createSms(1, "phonenumer1", "test1", Date())
            val smsEntity2 = createSms(1, "phonenumer2", "test2", Date())
            smsEntity2.isPending = true

            whenever(sendSMSRepository.getSms(smsEntity1.id!!)).thenReturn(smsEntity2)

            sun.onSmsSent(smsEntity1, 1)

            assertThat(smsEntity2.isPending, `is`(false))
            assertThat(smsEntity2.isSent, `is`(false))
            assertThat(smsEntity2.sentDate, `is`(nullValue()))
            assertThat(smsEntity2.isDelivered, `is`(false))
            assertThat(smsEntity2.deliveredDate, `is`(nullValue()))
            assertThat(smsEntity2.lasDateErrorCode, any(Date::class.java))
            assertThat(smsEntity2.lasErrorCode, `is`(1))
            verify(sendSMSRepository).addOrUpdateSms(smsEntity2)
        }
    }

    @Test
    fun onSmsDelivered_Success() {
        runBlocking {
            val smsEntity1 = createSms(1, "phonenumer1", "test1", Date())
            val smsEntity2 = createSms(1, "phonenumer2", "test2", Date())

            whenever(sendSMSRepository.getSms(smsEntity1.id!!)).thenReturn(smsEntity2)

            sun.onSmsDelivered(smsEntity1, null)

            assertThat(smsEntity2.isDelivered, `is`(true))
            assertThat(smsEntity2.deliveredDate, any(Date::class.java))
            verify(sendSMSRepository).addOrUpdateSms(smsEntity2)
        }
    }

    @Test
    fun onSmsDelivered_Error() {
        runBlocking {
            val smsEntity1 = createSms(1, "phonenumer1", "test1", Date())
            val smsEntity2 = createSms(1, "phonenumer2", "test2", Date())

            whenever(sendSMSRepository.getSms(smsEntity1.id!!)).thenReturn(smsEntity2)

            sun.onSmsDelivered(smsEntity1, 1)

            assertThat(smsEntity2.isDelivered, `is`(false))
            assertThat(smsEntity2.deliveredDate, `is`(nullValue()))
            assertThat(smsEntity2.lasErrorCode, `is`(1))
            assertThat(smsEntity2.lasDateErrorCode, any(Date::class.java))
            verify(sendSMSRepository).addOrUpdateSms(smsEntity2)
        }
    }

    private fun createSms(id: Int, phoneNumber: String, text: String, date: Date): SmsEntity {
        return SmsEntity(
            id,
            phoneNumber,
            text,
            date,
            false,
            null,
            date,
            0,
            false,
            null,
            false,
            null,
            null,
            null
        )
    }
}