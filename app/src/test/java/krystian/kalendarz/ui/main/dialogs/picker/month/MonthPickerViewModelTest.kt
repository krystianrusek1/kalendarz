package krystian.kalendarz.ui.main.dialogs.picker.month

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class MonthPickerViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var resources: Resources

    lateinit var suit: MonthPickerViewModel

    @Before
    fun setUp() {
        DateFormatUnit.setResources(resources)
        suit = MonthPickerViewModel()
    }

    @Test
    fun onItemClicked_showPreviousMonth() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onItemClicked(
            DayCalendarItem(
                calendar = calendar,
                isBeforePeriod = true
            )
        )

        Assert.assertThat(suit.showPreviousMonth.value, Is.`is`(true))
        Assert.assertThat(suit.getSelectedDate().value, Is.`is`(calendar.time))
    }

    @Test
    fun onItemClicked_showNextMonth() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onItemClicked(
            DayCalendarItem(
                calendar = calendar,
                isAfterPeriod = true
            )
        )

        Assert.assertThat(suit.showNextMonth.value, Is.`is`(true))
        Assert.assertThat(suit.getSelectedDate().value, Is.`is`(calendar.time))
    }
}