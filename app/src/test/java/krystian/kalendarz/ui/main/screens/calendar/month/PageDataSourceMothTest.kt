package krystian.kalendarz.ui.main.screens.calendar.month

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PageKeyedDataSource
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.R
import krystian.kalendarz.YearMonthDayMatcher
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

/**
 * Tests [PageDataSourceMoth]
 **/
@RunWith(MockitoJUnitRunner::class)
class PageDataSourceMothTest {

    companion object {

        const val ID_CALENDAR1 = 1L

        const val ID_CALENDAR2 = 2L

        const val ID_CALENDAR3 = 3L

        const val COLOR1 = 1
        const val COLOR2 = 2
        const val COLOR3 = 3
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var suit: DataSourceMoth.PageDataSourceMoth

    @Mock
    private lateinit var calendarRepository: CalendarRepository

    @Mock
    private lateinit var resources: Resources

    @Before
    fun setUp() {
        DateFormatUnit.setResources(resources)
    }

    @Test
    fun loadInitial_countryCanada_august2019() {
        runBlocking {
            Locale.setDefault(Locale.CANADA)
            CalendarHelper.firstDayOfWeek = null

            val calendar = Calendar.getInstance()
            calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)
            suit = DataSourceMoth.PageDataSourceMoth(6, calendar.time)

            whenever(resources.getString(R.string.august)).thenReturn("august")

            val loadParams: PageKeyedDataSource.LoadInitialParams<Date> =
                mock(PageKeyedDataSource.LoadInitialParams::class.java)
                        as PageKeyedDataSource.LoadInitialParams<Date>
            val callback: PageKeyedDataSource.LoadInitialCallback<Date, CalendarPeriodModel> =
                mock(PageKeyedDataSource.LoadInitialCallback::class.java)
                        as PageKeyedDataSource.LoadInitialCallback<Date, CalendarPeriodModel>
            val captorCalendarPeriodList: ArgumentCaptor<List<CalendarPeriodModel>> =
                ArgumentCaptor.forClass(List::class.java)
                        as ArgumentCaptor<List<CalendarPeriodModel>>
            val dateBefore = ArgumentCaptor.forClass(Date::class.java)
            val dateAfter = ArgumentCaptor.forClass(Date::class.java)

            suit.loadInitial(
                loadParams,
                callback
            )
            verify(callback).onResult(captorCalendarPeriodList.capture(), dateBefore.capture(), dateAfter.capture())
            val calendarPeriodList: List<CalendarPeriodModel> = captorCalendarPeriodList.value

            Assert.assertThat(calendarPeriodList[0].monthName, Is.`is`("august 2019"))
            Assert.assertThat(calendarPeriodList[0].dayList.size, Is.`is`(42))

            Assert.assertThat(calendarPeriodList[0].dayList.size, Is.`is`(42))

            Assert.assertThat(calendarPeriodList[0].dayList[0].calendar.get(Calendar.DAY_OF_MONTH), Is.`is`(28))
            Assert.assertThat(
                calendarPeriodList[0].dayList[calendarPeriodList[0].dayList.size - 1].calendar.get(Calendar.DAY_OF_MONTH),
                Is.`is`(7)
            )

            Assert.assertThat(calendarPeriodList[0].dayList[6].calendar.get(Calendar.DAY_OF_MONTH), Is.`is`(3))
            Assert.assertThat(calendarPeriodList[0].dayList[6].calendar.get(Calendar.MONTH), Is.`is`(Calendar.AUGUST))
            Assert.assertThat(calendarPeriodList[0].dayList[6].isOutOfPeriod, Is.`is`(false))

            Assert.assertThat(calendarPeriodList[0].dayList[0].isOutOfPeriod, Is.`is`(true))
            Assert.assertThat(calendarPeriodList[0].dayList[0].isBeforePeriod, Is.`is`(true))
            Assert.assertThat(calendarPeriodList[0].dayList[40].isAfterPeriod, Is.`is`(true))

            calendar.set(2019, Calendar.SEPTEMBER, 10, 0, 0, 0)
            Assert.assertThat(dateAfter.value, YearMonthDayMatcher(calendar.time))
            calendar.set(2019, Calendar.JULY, 10, 0, 0, 0)
            Assert.assertThat(dateBefore.value, YearMonthDayMatcher(calendar.time))
        }
    }

    fun createColorEvent(idCalendar: Long, color: Int): ColorEvent {
        return ColorEvent(idCalendar, color = color)
    }
}