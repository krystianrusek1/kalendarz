package krystian.kalendarz.ui.main.screens.calendar.week

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PageKeyedDataSource
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.R
import krystian.kalendarz.YearMonthDayMatcher
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

/**
 * Tests [PageDataSourceWeek]
 **/
@RunWith(MockitoJUnitRunner::class)
class DataSourceWeekTest {

    companion object {

        const val ID_CALENDAR1 = 1L

        const val ID_CALENDAR2 = 2L

        const val ID_CALENDAR3 = 3L

        const val COLOR1 = 1
        const val COLOR2 = 2
        const val COLOR3 = 3
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var suit: DataSourceWeek.PageDataSourceWeek

    @Mock
    private lateinit var calendarRepository: CalendarRepository

    @Mock
    private lateinit var resources: Resources

    @Before
    fun setUp() {
        DateFormatUnit.setResources(resources)
    }

    @Test
    fun loadInitial_countryCanada_august2019() {
        runBlocking {
            Locale.setDefault(Locale.CANADA)
            CalendarHelper.firstDayOfWeek = null

            val calendar = Calendar.getInstance()
            calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)
            suit = DataSourceWeek.PageDataSourceWeek(1, calendar.time)

            whenever(resources.getString(R.string.august)).thenReturn("august")

            val loadParams: PageKeyedDataSource.LoadInitialParams<Date> =
                Mockito.mock(PageKeyedDataSource.LoadInitialParams::class.java)
                        as PageKeyedDataSource.LoadInitialParams<Date>
            val callback: PageKeyedDataSource.LoadInitialCallback<Date, CalendarPeriodModel> =
                Mockito.mock(PageKeyedDataSource.LoadInitialCallback::class.java)
                        as PageKeyedDataSource.LoadInitialCallback<Date, CalendarPeriodModel>
            val captorCalendarPeriodList: ArgumentCaptor<List<CalendarPeriodModel>> =
                ArgumentCaptor.forClass(List::class.java)
                        as ArgumentCaptor<List<CalendarPeriodModel>>
            val dateBefore = ArgumentCaptor.forClass(Date::class.java)
            val dateAfter = ArgumentCaptor.forClass(Date::class.java)

            suit.loadInitial(
                loadParams,
                callback
            )
            verify(callback).onResult(captorCalendarPeriodList.capture(), dateBefore.capture(), dateAfter.capture())
            val calendarPeriodList: List<CalendarPeriodModel> = captorCalendarPeriodList.value

            Assert.assertThat(calendarPeriodList[0].monthName, Is.`is`("august 2019"))
            Assert.assertThat(calendarPeriodList[0].dayList.size, Is.`is`(7))

            Assert.assertThat(calendarPeriodList[0].dayList[0].calendar.get(Calendar.DAY_OF_MONTH), Is.`is`(4))
            Assert.assertThat(
                calendarPeriodList[0].dayList[calendarPeriodList[0].dayList.size - 1].calendar.get(Calendar.DAY_OF_MONTH),
                Is.`is`(10)
            )

            Assert.assertThat(calendarPeriodList[0].dayList[4].calendar.get(Calendar.DAY_OF_MONTH), Is.`is`(8))
            Assert.assertThat(calendarPeriodList[0].dayList[4].calendar.get(Calendar.MONTH), Is.`is`(Calendar.AUGUST))
            Assert.assertThat(calendarPeriodList[0].dayList[4].isOutOfPeriod, Is.`is`(false))

            Assert.assertThat(calendarPeriodList[0].dayList[0].isOutOfPeriod, Is.`is`(false))
            Assert.assertThat(calendarPeriodList[0].dayList[0].isBeforePeriod, Is.`is`(false))
            Assert.assertThat(calendarPeriodList[0].dayList[6].isAfterPeriod, Is.`is`(false))

            calendar.set(2019, Calendar.AUGUST, 17, 0, 0, 0)
            Assert.assertThat(dateAfter.value, YearMonthDayMatcher(calendar.time))
            calendar.set(2019, Calendar.AUGUST, 3, 0, 0, 0)
            Assert.assertThat(dateBefore.value, YearMonthDayMatcher(calendar.time))
        }
    }

    fun createColorEvent(idCalendar: Long, color: Int): ColorEvent {
        return ColorEvent(idCalendar, color = color)
    }
}