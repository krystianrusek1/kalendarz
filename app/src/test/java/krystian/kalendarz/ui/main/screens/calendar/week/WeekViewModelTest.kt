package krystian.kalendarz.ui.main.screens.calendar.week

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class WeekViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var resources: Resources

    @Mock
    lateinit var calendarRepository: CalendarRepository

    lateinit var suit: WeekViewModel

    val visibleDate = MutableLiveData<Date>()

    @Mock
    lateinit var appNavigator: MainNavigator

    @ExperimentalCoroutinesApi
    val testDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        DateFormatUnit.setResources(resources)
        whenever(calendarRepository.getVisibleDate()).doReturn(visibleDate)
        suit = WeekViewModel(appNavigator, calendarRepository, resources)
    }


    @Test
    fun showPreviousWeek() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val calendar = CalendarHelper.getInstance()
        calendar.set(2019, Calendar.NOVEMBER, 9, 0, 0, 0)

        val listItem = createWeeksItems(calendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)

        calendar.set(2019, Calendar.NOVEMBER, 3, 0, 0, 0)
        suit.onDayListChanged(DayAdapter.DayModel(calendar.time, MutableLiveData()), true)


        Assert.assertThat(suit.showPreviousWeek.value, Is.`is`(true))
    }

    @Test
    fun showNextWeek() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val calendar = CalendarHelper.getInstance()
        calendar.set(2019, Calendar.NOVEMBER, 9, 0, 0, 0)

        val listItem = createWeeksItems(calendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)

        calendar.set(2019, Calendar.NOVEMBER, 11, 0, 0, 0)
        suit.onDayListChanged(DayAdapter.DayModel(calendar.time, MutableLiveData()), true)


        Assert.assertThat(suit.showNextWeek.value, Is.`is`(true))
    }

    @Test
    fun showOnTopList_resultNull() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val dayCalendar = CalendarHelper.getInstance()
        dayCalendar.set(2019, Calendar.NOVEMBER, 15, 0, 0, 0)
        suit.onDayListChanged(DayAdapter.DayModel(dayCalendar.time, MutableLiveData()), true)


        val weekCalendar = CalendarHelper.getInstance()
        weekCalendar.set(2019, Calendar.NOVEMBER, 13, 0, 0, 0)
        val listItem = createWeeksItems(weekCalendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)


        Assert.assertNull(suit.dayListScrollTo.value)
    }

    @Test
    fun rebuildList_resultNextWeek_7DaysAfter() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val dayCalendar = CalendarHelper.getInstance()
        dayCalendar.set(2019, Calendar.NOVEMBER, 5, 0, 0, 0)
        suit.onDayListChanged(DayAdapter.DayModel(dayCalendar.time, MutableLiveData()), true)

        val weekCalendar = CalendarHelper.getInstance()
        weekCalendar.set(2019, Calendar.NOVEMBER, 13, 0, 0, 0)
        val listItem = createWeeksItems(weekCalendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)

        dayCalendar.add(Calendar.DATE, 7)
        suit.dayItems.observeForever { }
        val dataSourceDay = suit.dayItems.value!!.dataSource as DataSourceDay.PageDataSourceDay
        Assert.assertThat(dataSourceDay.startDate, Is.`is`(dayCalendar.time))
        Assert.assertThat(suit.refreshDayItem.value, Is.`is`(true))
    }

    @Test
    fun rebuildList_resultNextWeek_14DaysAfter() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val dayCalendar = CalendarHelper.getInstance()
        dayCalendar.set(2019, Calendar.NOVEMBER, 5, 0, 15, 0)
        suit.onDayListChanged(DayAdapter.DayModel(dayCalendar.time, MutableLiveData()), true)

        val weekCalendar = CalendarHelper.getInstance()
        weekCalendar.set(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val listItem = createWeeksItems(weekCalendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)

        dayCalendar.add(Calendar.DATE, 14)
        suit.dayItems.observeForever { }
        val dataSourceDay = suit.dayItems.value!!.dataSource as DataSourceDay.PageDataSourceDay
        Assert.assertThat(dataSourceDay.startDate, Is.`is`(dayCalendar.time))
        Assert.assertThat(suit.refreshDayItem.value, Is.`is`(true))
    }

    @Test
    fun rebuildList_resultPreviousWeek_7Days() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val dayCalendar = CalendarHelper.getInstance()
        dayCalendar.set(2019, Calendar.NOVEMBER, 22, 0, 0, 0)
        suit.onDayListChanged(DayAdapter.DayModel(dayCalendar.time, MutableLiveData()), true)

        val weekCalendar = CalendarHelper.getInstance()
        weekCalendar.set(2019, Calendar.NOVEMBER, 11, 0, 0, 0)
        val listItem = createWeeksItems(weekCalendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)

        dayCalendar.add(Calendar.DATE, -7)
        suit.dayItems.observeForever { }
        val dataSourceDay = suit.dayItems.value!!.dataSource as DataSourceDay.PageDataSourceDay
        Assert.assertThat(dataSourceDay.startDate, Is.`is`(dayCalendar.time))
        Assert.assertThat(suit.refreshDayItem.value, Is.`is`(true))
    }

    @Test
    fun rebuildList_resultPreviousWeek_14Days() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val dayCalendar = CalendarHelper.getInstance()
        dayCalendar.set(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        suit.onDayListChanged(DayAdapter.DayModel(dayCalendar.time, MutableLiveData()), true)

        val weekCalendar = CalendarHelper.getInstance()
        weekCalendar.set(2019, Calendar.NOVEMBER, 4, 0, 0, 0)
        val listItem = createWeeksItems(weekCalendar.time)
        suit.onWeekListChanged(CalendarPeriodModel("", listItem), true)

        dayCalendar.add(Calendar.DATE, -14)
        suit.dayItems.observeForever { }
        val dataSourceDay = suit.dayItems.value!!.dataSource as DataSourceDay.PageDataSourceDay
        Assert.assertThat(dataSourceDay.startDate, Is.`is`(dayCalendar.time))
        Assert.assertThat(suit.refreshDayItem.value, Is.`is`(true))
    }

    @Test
    fun onItemDayInserted_NoScroll() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        runBlocking {
            suit.onItemDayInserted()

            delay(200)

            Assert.assertNull(suit.dayListScrollTo.value)
        }
    }

    @Test
    fun onItemDayInserted_Scroll() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        runBlocking {
            val date = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 4, 0, 0, 0)
            visibleDate.value = date
            suit.onItemDayInserted()
            delay(200)

            Assert.assertThat(suit.dayListScrollTo.value, Is.`is`(date))
        }
    }

    private fun createWeeksItems(showDate: Date): List<DayCalendarItem> {
        val list = ArrayList<DayCalendarItem>()
        val calendar = CalendarHelper.getInstance()
        calendar.time = showDate

        CalendarHelper.generateWeeksFrom(1, showDate).forEach {
            val item = DayCalendarItem()
            item.calendar = it
            item.isPresentDay = CalendarHelper.equalsDayMothYear(item.calendar, CalendarHelper.getInstance())
            list.add(item)
        }

        return list
    }
}