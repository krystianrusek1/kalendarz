package krystian.kalendarz.ui.main.view.daydetails

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.R
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class TransformationEventModelTest {

    companion object {
        private const val EVENT1 = "EVENT1"
        private const val EVENT2 = "EVENT2"
        private const val EVENT3 = "EVENT3"
        private const val EVENT4 = "EVENT4"
        private const val EVENT5 = "EVENT5"
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var resources: Resources

    @Mock
    lateinit var calendarRepository: CalendarRepository

    lateinit var suit: TransformationDayEventModel

    var selectedDate = MutableLiveData<Date>()

    @Before
    fun setUp() {
        DateFormatUnit.setResources(resources)
        suit = TransformationDayEventModel(calendarRepository, resources)
    }

    @Test
    fun getEventsByLiveDate() {
        Locale.setDefault(Locale.FRENCH)

        whenever(resources.getString(R.string.screen_day_list_allDay)).thenReturn("all day")

        selectedDate.value = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10)

        val eventList = ArrayList<EventEntity>()
        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT1,
                startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 5),
                endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 15)
            )
        )
        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT2,
                startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 1, 1),
                endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 15)
            )
        )
        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT3,
                startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 6),
                endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 1, 1)
            )
        )

        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT4,
                startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 5, 5),
                endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 10, 0)
            )
        )

        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT5,
                allDays = true,
                startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 5, 5),
                endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 10, 0)
            )
        )

        whenever(
            calendarRepository.fetchEventListRecursiveByLiveDate(
                CalendarHelper.prepareStartDate(selectedDate.value!!),
                CalendarHelper.prepareEndDate(selectedDate.value!!)
            )
        ).thenReturn(MutableLiveData<List<EventEntity>>(eventList))


        val liveEvents = suit.getEvents(selectedDate)
        liveEvents.observeForever { }
        val dayEvents = liveEvents.value

        Assert.assertThat(dayEvents!![0].eventName, Is.`is`("all day $EVENT1"))
        Assert.assertThat(dayEvents!![1].eventName, Is.`is`("01:01 - ... $EVENT2"))
        Assert.assertThat(dayEvents!![2].eventName, Is.`is`("... - 01:01 $EVENT3"))
        Assert.assertThat(dayEvents!![3].eventName, Is.`is`("05:05 - 10:00 $EVENT4"))
        Assert.assertThat(dayEvents!![4].eventName, Is.`is`("all day $EVENT5"))
    }

    @Test
    fun getEventsByCoroutines() {
        runBlocking() {

            Locale.setDefault(Locale.FRENCH)

            whenever(resources.getString(R.string.screen_day_list_allDay)).thenReturn("all day")

            val selectedDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10)

            val eventList = ArrayList<EventEntity>()
            eventList.add(
                createEventEntity(
                    calendarId = 0,
                    title = EVENT1,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 5),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 15)
                )
            )
            eventList.add(
                createEventEntity(
                    calendarId = 0,
                    title = EVENT2,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 1, 1),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 15)
                )
            )
            eventList.add(
                createEventEntity(
                    calendarId = 0,
                    title = EVENT3,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 6),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 1, 1)
                )
            )

            eventList.add(
                createEventEntity(
                    calendarId = 0,
                    title = EVENT4,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 5, 5),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 10, 0)
                )
            )

            eventList.add(
                createEventEntity(
                    calendarId = 0,
                    title = EVENT5,
                    allDays = true,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 5, 5),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.AUGUST, 10, 10, 0)
                )
            )

            whenever(
                calendarRepository.fetchEventListRecursiveByCoroutines(
                    CalendarHelper.prepareStartDate(selectedDate),
                    CalendarHelper.prepareEndDate(selectedDate)
                )
            ).thenReturn(eventList)


            val dayEvents = suit.getEvents(selectedDate)

            Assert.assertThat(dayEvents!![0].eventName, Is.`is`("all day $EVENT1"))
            Assert.assertThat(dayEvents!![1].eventName, Is.`is`("01:01 - ... $EVENT2"))
            Assert.assertThat(dayEvents!![2].eventName, Is.`is`("... - 01:01 $EVENT3"))
            Assert.assertThat(dayEvents!![3].eventName, Is.`is`("05:05 - 10:00 $EVENT4"))
            Assert.assertThat(dayEvents!![4].eventName, Is.`is`("all day $EVENT5"))
        }
    }

    fun createEventEntity(
        calendarId: Long,
        title: String = "",
        startDate: Date,
        endDate: Date,
        description: String = "",
        allDays: Boolean = false,
        timeZone: TimeZone = TimeZone.getDefault()
    ): EventEntity {
        return EventEntity.Builder(calendarId)
            .withTitle(title)
            .withStartDate(startDate)
            .withEndDate(endDate)
            .withAllDays(allDays)
            .withTimeZone(timeZone)
            .withDescription(description)
            .build()
    }
}