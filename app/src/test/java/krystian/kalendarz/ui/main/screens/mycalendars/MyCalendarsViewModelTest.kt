package krystian.kalendarz.ui.main.screens.mycalendars

import android.content.res.Resources
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.whenever
import krystian.kalendarz.R
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MyCalendarsViewModelTest {

    companion object {
        private const val DISPLAY_NAME1 = "DISPLAY_NAME1"
        private const val DISPLAY_NAME2 = "DISPLAY_NAME2"
        private const val DISPLAY_NAME3 = "DISPLAY_NAME3"
        private const val DISPLAY_NAME4 = "DISPLAY_NAME4"
        private const val DISPLAY_NAME5 = "DISPLAY_NAME5"

        private const val ACCOUNT_NAME1 = "ACCOUNT_NAME1"
        private const val ACCOUNT_NAME2 = "ACCOUNT_NAME2"
        private const val ACCOUNT_NAME3 = "ACCOUNT_NAME3"
        private const val ACCOUNT_NAME4 = "ACCOUNT_NAME4"
        private const val ACCOUNT_NAME5 = "ACCOUNT_NAME5"

        private const val LOCAL_STRING = "LOCAL_STRING"
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()


    lateinit var suit: MyCalendarsViewModel

    @Mock
    private lateinit var resources: Resources

    @Mock
    private lateinit var mainNavigator: MainNavigator

    @Mock
    lateinit var calendarRepository: CalendarRepository

    @Before
    fun setUp() {

        whenever(resources.getString(R.string.local_calendar)).thenReturn(LOCAL_STRING)
        DateFormatUnit.setResources(resources)
        suit = MyCalendarsViewModel(mainNavigator, calendarRepository, resources)
    }

    @Test
    fun getCalendarList() {

        val calendarList = ArrayList<CalendarEntity>()
        calendarList.add(
            createCalendarEntity(
                displayName = DISPLAY_NAME1,
                accountName = ACCOUNT_NAME1
            )
        )
        calendarList.add(
            createCalendarEntity(
                displayName = DISPLAY_NAME2,
                accountName = ACCOUNT_NAME1
            )
        )
        calendarList.add(
            createCalendarEntity(
                displayName = DISPLAY_NAME3,
                accountName = ACCOUNT_NAME2,
                accountType = CalendarEntity.AccountType.LOCAL
            )
        )
        calendarList.add(
            createCalendarEntity(
                displayName = DISPLAY_NAME3,
                accountName = ACCOUNT_NAME1
            )
        )
        calendarList.add(
            createCalendarEntity(
                displayName = DISPLAY_NAME4,
                accountName = ACCOUNT_NAME3
            )
        )

        val liveDateCalendarList = MutableLiveData<List<CalendarEntity>>()
        liveDateCalendarList.value = calendarList
            whenever(calendarRepository.getCalendarList()).thenReturn(liveDateCalendarList)


        val liveDateList = suit.getCalendarList()
        liveDateList.observeForever { }
        val list = liveDateList.value

        Assert.assertThat((list!![0] as HeaderModel).title, Is.`is`(ACCOUNT_NAME1))
        Assert.assertThat((list!![1] as EntryModel).title, Is.`is`(DISPLAY_NAME1))
        Assert.assertThat((list!![2] as EntryModel).title, Is.`is`(DISPLAY_NAME2))
        Assert.assertThat((list!![3] as EntryModel).title, Is.`is`(DISPLAY_NAME3))

        Assert.assertThat((list!![4] as HeaderModel).title, Is.`is`(ACCOUNT_NAME3))
        Assert.assertThat((list!![5] as EntryModel).title, Is.`is`(DISPLAY_NAME4))

        Assert.assertThat((list!![6] as HeaderModel).title, Is.`is`(LOCAL_STRING))
        Assert.assertThat((list!![7] as EntryModel).title, Is.`is`(DISPLAY_NAME3))
        Assert.assertThat((list!![8] as AddEntryModel).calendarAccountNme, Is.`is`(ACCOUNT_NAME2))
    }

    fun createCalendarEntity(
        displayName: String? = null,
        accountName: String = CalendarEntity.ACCOUNT_LOCAL_NAME,
        @ColorInt color: Int = Color.TRANSPARENT,
        visible: Boolean = true,
        accountType: CalendarEntity.AccountType = CalendarEntity.AccountType.OTHER
    ): CalendarEntity {
        return CalendarEntity.Builder().withDisplayName(displayName)
            .withAccountName(accountName)
            .withColor(color)
            .withVisible(visible)
            .withAccountType(accountType)
            .build()
    }
}