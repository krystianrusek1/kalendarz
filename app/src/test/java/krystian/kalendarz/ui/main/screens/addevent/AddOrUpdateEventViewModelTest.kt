package krystian.kalendarz.ui.main.screens.addevent

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import krystian.kalendarz.R
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.contact.ContactRepository
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.data.phone.PhoneNumberRepository
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.util.CalendarHelper
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class AddOrUpdateEventViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainNavigator: MainNavigator

    @Mock
    private lateinit var calendarRepository: CalendarRepository

    @Mock
    private lateinit var phoneNumberRepository: PhoneNumberRepository

    @Mock
    private lateinit var contactRepository: ContactRepository

    private val calendarEntity =
        CalendarEntity.Builder().withId(0).withAccountName("AccountName").build()

    lateinit var suit: AddOrUpdateEventViewModel

    @ExperimentalCoroutinesApi
    val testDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Locale.setDefault(Locale.UK)
        Dispatchers.setMain(testDispatcher)

        runBlockingTest {
            whenever(calendarRepository.getLastChosenCalendar()).thenReturn(calendarEntity)
            suit =
                AddOrUpdateEventViewModel(mainNavigator, calendarRepository, phoneNumberRepository, contactRepository)
        }
    }

    @Test
    fun onDatesChanged_theSameDates_isValid() {
        suit.onProposalDateChanged(null)

        val calendar = CalendarHelper.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onDatesChanged(calendar.time, calendar.time)

        Assert.assertThat(suit.startTimeLiveDate.value, Is.`is`("00:00"))
        Assert.assertThat(suit.startDayLiveDate.value, Is.`is`("10/08/19"))

        Assert.assertThat(suit.endTimeLiveDate.value, Is.`is`("00:00"))
        Assert.assertThat(suit.endDayLiveDate.value, Is.`is`("10/08/19"))
    }

    @Test
    fun onDatesChanged_theDifferentDates_isValid() {
        val calendarStart = CalendarHelper.getInstance()
        calendarStart.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        val calendarEnd = CalendarHelper.getInstance()
        calendarEnd.set(2019, Calendar.AUGUST, 20, 0, 0, 0)

        suit.onDatesChanged(calendarStart.time, calendarEnd.time)

        Assert.assertThat(suit.startTimeLiveDate.value, Is.`is`("00:00"))
        Assert.assertThat(suit.startDayLiveDate.value, Is.`is`("10/08/19"))

        Assert.assertThat(suit.endTimeLiveDate.value, Is.`is`("00:00"))
        Assert.assertThat(suit.endDayLiveDate.value, Is.`is`("20/08/19"))
    }

    @Test
    fun onDatesChanged_theDifferentDates_isNotValid() {
        val calendarStart = CalendarHelper.getInstance()
        calendarStart.set(2019, Calendar.AUGUST, 10, 0, 0, 1)

        val calendarEnd = CalendarHelper.getInstance()
        calendarEnd.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onDatesChanged(calendarStart.time, calendarEnd.time)

        Assert.assertNull(suit.startTimeLiveDate.value)
        Assert.assertNull(suit.startDayLiveDate.value)

        Assert.assertNull(suit.endTimeLiveDate.value)
        Assert.assertNull(suit.endDayLiveDate.value)

        verify(mainNavigator).showToast(R.string.screen_addEvent_wrongDates_toast)
    }

    @Test
    fun onProposalDateChanged() {
        val calendar = CalendarHelper.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onProposalDateChanged(calendar.time)

        Assert.assertThat(suit.startTimeLiveDate.value, Is.`is`("10:00"))
        Assert.assertThat(suit.startDayLiveDate.value, Is.`is`("10/08/19"))

        Assert.assertThat(suit.endTimeLiveDate.value, Is.`is`("11:00"))
        Assert.assertThat(suit.endDayLiveDate.value, Is.`is`("10/08/19"))
    }

    @Test
    fun onAddEventClicked_AllDayIsFalse() {
        val calendarStart = CalendarHelper.getInstance()
        calendarStart.set(2019, Calendar.AUGUST, 10, 9, 20, 0)

        val calendarEnd = CalendarHelper.getInstance()
        calendarEnd.set(2019, Calendar.AUGUST, 10, 10, 5, 0)

        suit.onDatesChanged(calendarStart.time, calendarEnd.time)
        suit.onAddEventClicked()

        val captorEventEntity = argumentCaptor<EventEntity>()
        runBlocking {
            verify(calendarRepository).insertOrUpdateEvent(captorEventEntity.capture())
        }

        val addedEventEntity = captorEventEntity.firstValue

        Assert.assertThat(addedEventEntity.startDate, Is.`is`(calendarStart.time))
        Assert.assertThat(addedEventEntity.endDate, Is.`is`(calendarEnd.time))
        Assert.assertThat(addedEventEntity.allDays, Is.`is`(false))

    }

    @Test
    fun onAddEventClicked_AllDayIsTrue() {
        val calendarStart = CalendarHelper.getInstance()
        calendarStart.set(2019, Calendar.AUGUST, 3, 9, 20, 0)

        val calendarEnd = CalendarHelper.getInstance()
        calendarEnd.set(2019, Calendar.AUGUST, 10, 10, 5, 0)

        suit.onDatesChanged(calendarStart.time, calendarEnd.time)
        suit.onAllDayTextClicked()
        suit.onAddEventClicked()

        val captorEventEntity = argumentCaptor<EventEntity>()
        runBlocking {
            verify(calendarRepository).insertOrUpdateEvent(captorEventEntity.capture())
        }

        val addedEventEntity = captorEventEntity.firstValue

        Assert.assertThat(addedEventEntity.allDays, Is.`is`(true))
        Assert.assertThat(addedEventEntity.startDate, Is.`is`(CalendarHelper.prepareStartDate(calendarStart.time)))
        Assert.assertThat(addedEventEntity.endDate, Is.`is`(CalendarHelper.prepareEndDate(calendarEnd.time)))

    }

    @Test
    fun onEventChanged() {
        runBlockingTest {
            val calendarEntity = CalendarEntity.Builder().withId(10).build()

            whenever(calendarRepository.getAllCalendar(calendarEntity.id!!)).thenReturn(calendarEntity)

            val eventBuilder = EventEntity.Builder(calendarEntity.id!!)
            val calendarStart = CalendarHelper.getInstance()
            calendarStart.set(2019, Calendar.AUGUST, 3, 9, 20, 0)
            eventBuilder.startDate = calendarStart.time

            val calendarEnd = CalendarHelper.getInstance()
            calendarEnd.set(2019, Calendar.AUGUST, 10, 10, 5, 0)
            eventBuilder.endDate = calendarEnd.time

            suit.onProposalDateChanged(calendarEnd.time)
            suit.onEventChanged(eventBuilder.build())
            suit.onAddEventClicked()

            val captorEventEntity = argumentCaptor<EventEntity>()

            verify(calendarRepository).insertOrUpdateEvent(captorEventEntity.capture())
            val addedEventEntity = captorEventEntity.firstValue

            Assert.assertThat(addedEventEntity.allDays, Is.`is`(false))
            Assert.assertThat(addedEventEntity.startDate, Is.`is`(calendarStart.time))
            Assert.assertThat(addedEventEntity.endDate, Is.`is`(calendarEnd.time))

            Assert.assertThat(suit.startDayLiveDate.value, Is.`is`("03/08/19"))
            Assert.assertThat(suit.startTimeLiveDate.value, Is.`is`("09:20"))

            Assert.assertThat(suit.endDayLiveDate.value, Is.`is`("10/08/19"))
            Assert.assertThat(suit.endTimeLiveDate.value, Is.`is`("10:05"))

            Assert.assertThat(suit.allDayCheckedLiveDate.value, Is.`is`(false))
            Assert.assertThat(suit.showEndStartTimeLiveDate.value, Is.`is`(false))

            delay(100)
            Assert.assertThat(suit.chooseCalendarEntityLiveDate.value, Is.`is`(calendarEntity))
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun onEventChanged_allDay() {
        runBlockingTest {
            val calendarEntity = CalendarEntity.Builder().withId(10).build()

            whenever(calendarRepository.getAllCalendar(calendarEntity.id!!)).thenReturn(calendarEntity)

            val eventBuilder = EventEntity.Builder(calendarEntity.id!!)
            val calendarStart = CalendarHelper.getInstance()
            calendarStart.set(2019, Calendar.AUGUST, 3, 9, 20, 0)
            eventBuilder.startDate = calendarStart.time

            val calendarEnd = CalendarHelper.getInstance()
            calendarEnd.set(2019, Calendar.AUGUST, 10, 10, 5, 0)
            eventBuilder.endDate = calendarEnd.time

            eventBuilder.allDays = true

            suit.onProposalDateChanged(calendarEnd.time)
            suit.onEventChanged(eventBuilder.build())
            suit.onAddEventClicked()

            val captorEventEntity = argumentCaptor<EventEntity>()

            verify(calendarRepository).insertOrUpdateEvent(captorEventEntity.capture())
            val addedEventEntity = captorEventEntity.firstValue

            Assert.assertThat(addedEventEntity.allDays, Is.`is`(true))
            Assert.assertThat(addedEventEntity.startDate, Is.`is`(CalendarHelper.prepareStartDate(calendarStart.time)))
            Assert.assertThat(addedEventEntity.endDate, Is.`is`(CalendarHelper.prepareEndDate(calendarEnd.time)))

            Assert.assertThat(suit.startDayLiveDate.value, Is.`is`("03/08/19"))
            Assert.assertThat(suit.startTimeLiveDate.value, Is.`is`("09:20"))

            Assert.assertThat(suit.endDayLiveDate.value, Is.`is`("10/08/19"))
            Assert.assertThat(suit.endTimeLiveDate.value, Is.`is`("10:05"))

            Assert.assertThat(suit.allDayCheckedLiveDate.value, Is.`is`(true))
            Assert.assertThat(suit.showEndStartTimeLiveDate.value, Is.`is`(true))
            Assert.assertThat(suit.chooseCalendarEntityLiveDate.value, Is.`is`(calendarEntity))
        }
    }
}