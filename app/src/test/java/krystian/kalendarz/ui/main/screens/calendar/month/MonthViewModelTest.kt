package krystian.kalendarz.ui.main.screens.calendar.month

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.whenever
import krystian.kalendarz.R
import krystian.kalendarz.YearMonthDayMatcher
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.main.screens.calendar.CalendarNavigator
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.CalendarHelper.prepareDate
import krystian.kalendarz.util.DateFormatUnit
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

import java.util.*

@RunWith(MockitoJUnitRunner::class)
class MonthViewModelTest {

    companion object {
        private const val EVENT1 = "EVENT1"
        private const val EVENT2 = "EVENT2"
        private const val EVENT3 = "EVENT3"
        private const val EVENT4 = "EVENT4"
        private const val EVENT5 = "EVENT5"
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var resources: Resources

    @Mock
    private lateinit var calendarNavigator: CalendarNavigator

    @Mock
    lateinit var calendarRepository: CalendarRepository

    @Mock
    lateinit var appNavigator: MainNavigator

    var selectedDate = MutableLiveData<Date>()
    var visibleDate = MutableLiveData<Date>()

    lateinit var suit: MonthViewModel

    @Before
    fun setUp() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        DateFormatUnit.setResources(resources)
        whenever(calendarRepository.getSelectedDate()).thenReturn(selectedDate)
        whenever(calendarRepository.getVisibleDate()).thenReturn(visibleDate)
        suit = MonthViewModel(appNavigator, calendarNavigator, calendarRepository, resources)
    }

    @Test
    fun onItemClicked_showPreviousMonth() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = CalendarHelper.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onItemClicked(
            DayCalendarItem(
                calendar = calendar,
                isBeforePeriod = true
            )
        )

        Assert.assertThat(suit.showPreviousMonth.value, Is.`is`(true))
        Assert.assertThat(suit.getSelectedDate().value, Is.`is`(calendar.time))
    }

    @Test
    fun onItemClicked_showNextMonth() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = CalendarHelper.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        suit.onItemClicked(
            DayCalendarItem(
                calendar = calendar,
                isAfterPeriod = true
            )
        )

        Assert.assertThat(suit.showNextMonth.value, Is.`is`(true))
        Assert.assertThat(suit.getSelectedDate().value, Is.`is`(calendar.time))
    }

    @Test
    fun getEvents() {
        Locale.setDefault(Locale.FRENCH)

        whenever(resources.getString(R.string.screen_day_list_allDay)).thenReturn("all day")

        visibleDate.value = prepareDate(2019, Calendar.AUGUST, 10)

        val eventList = ArrayList<EventEntity>()
        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT1,
                startDate = prepareDate(2019, Calendar.AUGUST, 5),
                endDate = prepareDate(2019, Calendar.AUGUST, 15)
            )
        )
        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT2,
                startDate = prepareDate(2019, Calendar.AUGUST, 10, 1, 1),
                endDate = prepareDate(2019, Calendar.AUGUST, 15)
            )
        )
        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT3,
                startDate = prepareDate(2019, Calendar.AUGUST, 6),
                endDate = prepareDate(2019, Calendar.AUGUST, 10, 1, 1)
            )
        )

        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT4,
                startDate = prepareDate(2019, Calendar.AUGUST, 10, 5, 5),
                endDate = prepareDate(2019, Calendar.AUGUST, 10, 10, 0)
            )
        )

        eventList.add(
            createEventEntity(
                calendarId = 0,
                title = EVENT5,
                allDays = true,
                startDate = prepareDate(2019, Calendar.AUGUST, 10, 5, 5),
                endDate = prepareDate(2019, Calendar.AUGUST, 10, 10, 0)
            )
        )

        whenever(
            calendarRepository.fetchEventListRecursiveByLiveDate(
                CalendarHelper.prepareStartDate(visibleDate.value!!),
                CalendarHelper.prepareEndDate(visibleDate.value!!)
            )
        ).thenReturn(MutableLiveData<List<EventEntity>>(eventList))


        val liveEvents = suit.getDayEvents()
        liveEvents.observeForever { }
        val dayModel = liveEvents.value

        Assert.assertThat(dayModel!!.listEvent[0].eventName, Is.`is`("all day $EVENT1"))
        Assert.assertThat(dayModel!!.listEvent[1].eventName, Is.`is`("01:01 - ... $EVENT2"))
        Assert.assertThat(dayModel!!.listEvent[2].eventName, Is.`is`("... - 01:01 $EVENT3"))
        Assert.assertThat(dayModel!!.listEvent[3].eventName, Is.`is`("05:05 - 10:00 $EVENT4"))
        Assert.assertThat(dayModel!!.listEvent[4].eventName, Is.`is`("all day $EVENT5"))
    }

    @Test
    fun onMonthListChanged_nextMonthVisible() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        selectedDate.value = prepareDate(2019, Calendar.APRIL, 31)
        visibleDate.value = prepareDate(2019, Calendar.AUGUST, 10)

        val listItem = createMonthItems(prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 0, 0))
        suit.onMonthListChanged(
            CalendarPeriodModel("", listItem)
        )

        Assert.assertThat(
            suit.getVisibleDate().value,
            YearMonthDayMatcher(prepareDate(2019, Calendar.SEPTEMBER, 10, 0, 0, 0))
        )
    }

    @Test
    fun onMonthListChanged_sameMonth() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        selectedDate.value = prepareDate(2019, Calendar.SEPTEMBER, 17)
        visibleDate.value = prepareDate(2019, Calendar.AUGUST, 10)

        val listItem = createMonthItems(prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 0, 0))
        suit.onMonthListChanged(
            CalendarPeriodModel("", listItem)
        )

        Assert.assertThat(
            suit.getVisibleDate().value,
            YearMonthDayMatcher(prepareDate(2019, Calendar.SEPTEMBER, 17, 0, 0, 0))
        )
    }

    @Test
    fun onMonthListChanged_nextMonthVisible_Bound() {
        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        selectedDate.value = prepareDate(2019, Calendar.APRIL, 31)
        visibleDate.value = prepareDate(2019, Calendar.JULY, 31)

        val listItem = createMonthItems(prepareDate(2019, Calendar.SEPTEMBER, 5, 0, 0, 0))
        suit.onMonthListChanged(
            CalendarPeriodModel("", listItem)
        )

        Assert.assertThat(
            suit.getVisibleDate().value,
            YearMonthDayMatcher(prepareDate(2019, Calendar.SEPTEMBER, 30, 0, 0, 0))
        )
    }

    private fun createMonthItems(showDate: Date): List<DayCalendarItem> {
        val list = ArrayList<DayCalendarItem>()
        val calendar = CalendarHelper.getInstance()
        calendar.time = showDate

        CalendarHelper.generateWeeksForMonthFrom(6, showDate).forEach {
            val item = DayCalendarItem()
            item.calendar = it
            item.isPresentDay = CalendarHelper.equalsDayMothYear(item.calendar, CalendarHelper.getInstance())
            list.add(item)
        }

        return list
    }

    fun createEventEntity(
        calendarId: Long,
        title: String = "",
        startDate: Date,
        endDate: Date,
        description: String = "",
        allDays: Boolean = false,
        timeZone: TimeZone = TimeZone.getDefault()
    ): EventEntity {
        return EventEntity.Builder(calendarId)
            .withTitle(title)
            .withStartDate(startDate)
            .withEndDate(endDate)
            .withAllDays(allDays)
            .withTimeZone(timeZone)
            .withDescription(description)
            .build()
    }
}