package krystian.kalendarz.util

import android.content.res.Resources
import com.nhaarman.mockitokotlin2.whenever
import krystian.kalendarz.R
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

/**
[DateFormatUnit]
 */
@RunWith(MockitoJUnitRunner::class)
class DateFormatUnitTest {

    @Mock
    lateinit var resources: Resources

    @Before
    fun setUp() {
        DateFormatUnit.setResources(resources)
    }

    @Test
    fun getMothName_august() {
        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.AUGUST, 2, 0, 0, 0)

        whenever(resources.getString(R.string.august)).thenReturn("august")
        Assert.assertThat(DateFormatUnit.getMothName(calendar), Is.`is`("august"))
    }

    @Test
    fun getMothName_april() {
        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.APRIL, 2, 0, 0, 0)

        whenever(resources.getString(R.string.april)).thenReturn("april")
        Assert.assertThat(DateFormatUnit.getMothName(calendar), Is.`is`("april"))
    }

    @Test
    fun formatMonthYear_april2019() {
        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.APRIL, 2, 0, 0, 0)

        whenever(resources.getString(R.string.april)).thenReturn("april")
        Assert.assertThat(DateFormatUnit.formatMonthYear(calendar.time), Is.`is`("april 2019"))
    }

    @Test
    fun formatMonthYear_february2020() {
        val calendar = Calendar.getInstance()
        calendar.set(2020, Calendar.FEBRUARY, 2, 0, 0, 0)

        whenever(resources.getString(R.string.february)).thenReturn("february")
        Assert.assertThat(DateFormatUnit.formatMonthYear(calendar.time), Is.`is`("february 2020"))
    }

    @Test
    fun formatMonthYear_aprilAndMay2020() {
        val calendar1 = Calendar.getInstance()
        calendar1.set(2020, Calendar.APRIL, 2, 0, 0, 0)

        val calendar2 = Calendar.getInstance()
        calendar2.set(2020, Calendar.MAY, 2, 0, 0, 0)

        whenever(resources.getString(R.string.april)).thenReturn("april")
        whenever(resources.getString(R.string.may)).thenReturn("may")
        Assert.assertThat(
            DateFormatUnit.format2MonthYear(calendar1.time, calendar2.time),
            Is.`is`("april/may 2020")
        )
    }

    @Test
    fun formatMonthYear_decemberAndJanuary2020() {
        val calendar1 = Calendar.getInstance()
        calendar1.set(2020, Calendar.DECEMBER, 2, 0, 0, 0)

        val calendar2 = Calendar.getInstance()
        calendar2.set(2021, Calendar.JANUARY, 2, 0, 0, 0)

        whenever(resources.getString(R.string.december)).thenReturn("december")
        whenever(resources.getString(R.string.january)).thenReturn("january")
        Assert.assertThat(
            DateFormatUnit.format2MonthYear(calendar1.time, calendar2.time),
            Is.`is`("december 2020/january 2021")
        )
    }

    @Test
    fun formatMonthYear_decemberAndDecember2020() {
        val calendar1 = Calendar.getInstance()
        calendar1.set(2020, Calendar.DECEMBER, 2, 0, 0, 0)

        val calendar2 = Calendar.getInstance()
        calendar2.set(2020, Calendar.DECEMBER, 2, 0, 0, 0)

        whenever(resources.getString(R.string.december)).thenReturn("december")
        Assert.assertThat(
            DateFormatUnit.format2MonthYear(calendar1.time, calendar2.time),
            Is.`is`("december 2020")
        )
    }

}