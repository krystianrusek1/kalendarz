package krystian.kalendarz.util

import krystian.kalendarz.YearMonthDayMatcher
import org.hamcrest.core.Is
import org.junit.Assert.*
import org.junit.Test
import java.util.*

/**
[CalendarHelper]
 */
class CalendarHelperTest {

    @Test
    fun generateWeeksForMonthFrom_august19_firstMonday() {

        Locale.setDefault(Locale.FRENCH)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.AUGUST, 2, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(29))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(8))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(4))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.AUGUST))
    }

    @Test
    fun generateWeeksForMonthFrom_august19_firstSunday() {

        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(28))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(7))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(3))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.AUGUST))
    }

    @Test
    fun generateWeeksForMonthFrom_august19_firstSunday_OneWeek() {

        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.AUGUST, 10, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(1, calendar.time)

        assertThat(list.size, Is.`is`(7))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(28))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(3))

        assertThat(list[5].get(Calendar.DAY_OF_MONTH), Is.`is`(2))
        assertThat(list[5].get(Calendar.MONTH), Is.`is`(Calendar.AUGUST))
    }

    @Test
    fun generateWeeksForMonthFrom_february20_firstSunday() {

        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2020, Calendar.FEBRUARY, 12, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(26))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(7))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(1))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.FEBRUARY))
    }

    @Test
    fun generateWeeksForMonthFrom_september19_firstSunday() {

        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.SEPTEMBER, 2, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(1))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(12))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(7))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.SEPTEMBER))
    }

    @Test
    fun generateWeeksForMonthFrom_December19_firstMonday() {

        Locale.setDefault(Locale.FRENCH)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.DECEMBER, 1, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(25))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(5))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(1))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.DECEMBER))
    }

    @Test
    fun generateWeeksForMonthFrom_July19_firstMonday() {

        Locale.setDefault(Locale.FRENCH)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.JULY, 1, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(1))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(11))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(7))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.JULY))
    }

    @Test
    fun generateWeeksForMonthFrom_October19_firstMonday() {

        Locale.setDefault(Locale.FRENCH)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.OCTOBER, 1, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(30))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(10))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(6))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.OCTOBER))
    }


    @Test
    fun generateWeeksForMonthFrom_March20_firstMonday() {

        Locale.setDefault(Locale.FRENCH)
        CalendarHelper.firstDayOfWeek = null

        val calendar = Calendar.getInstance()
        calendar.set(2020, Calendar.MARCH, 12, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(24))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(5))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(1))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.MARCH))
    }

    @Test
    fun generateWeeksForMonthFrom_november19_firstSaturday() {

        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.SATURDAY

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.NOVEMBER, 17, 0, 0, 0)

        val list = CalendarHelper.generateWeeksForMonthFrom(6, calendar.time)

        assertThat(list.size, Is.`is`(42))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(26))
        assertThat(list[list.size - 1].get(Calendar.DAY_OF_MONTH), Is.`is`(6))

        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(1))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.NOVEMBER))
    }

    @Test
    fun generateWeeksFrom_november19_firstSaturday_1Week() {

        Locale.setDefault(Locale.CANADA)
        CalendarHelper.firstDayOfWeek = Calendar.SATURDAY

        val calendar = Calendar.getInstance()
        calendar.set(2019, Calendar.NOVEMBER, 20, 0, 0, 0)

        val list = CalendarHelper.generateWeeksFrom(1, calendar.time)

        assertThat(list.size, Is.`is`(7))

        assertThat(list[0].get(Calendar.DAY_OF_MONTH), Is.`is`(16))
        assertThat(list[6].get(Calendar.DAY_OF_MONTH), Is.`is`(22))
        assertThat(list[6].get(Calendar.MONTH), Is.`is`(Calendar.NOVEMBER))
    }

    @Test
    fun generateNamesOfDayWeek_firstMonday() {

        Locale.setDefault(Locale.US)
        CalendarHelper.firstDayOfWeek = Calendar.MONDAY

        val list = CalendarHelper.generateNamesOfDayWeek()

        assertThat(list[0], Is.`is`("Mon"))
        assertThat(list[1], Is.`is`("Tue"))
        assertThat(list[2], Is.`is`("Wed"))
        assertThat(list[3], Is.`is`("Thu"))
        assertThat(list[4], Is.`is`("Fri"))
        assertThat(list[5], Is.`is`("Sat"))
        assertThat(list[6], Is.`is`("Sun"))
    }

    @Test
    fun generateNamesOfDayWeek_firstSunday() {

        Locale.setDefault(Locale.US)
        CalendarHelper.firstDayOfWeek = null

        val list = CalendarHelper.generateNamesOfDayWeek()

        assertThat(list.size, Is.`is`(7))

        assertThat(list[0], Is.`is`("Sun"))
        assertThat(list[1], Is.`is`("Mon"))
        assertThat(list[2], Is.`is`("Tue"))
        assertThat(list[3], Is.`is`("Wed"))
        assertThat(list[4], Is.`is`("Thu"))
        assertThat(list[5], Is.`is`("Fri"))
        assertThat(list[6], Is.`is`("Sat"))
    }

    @Test
    fun generateNamesOfDayWeek_firstSaturday() {

        Locale.setDefault(Locale.US)
        CalendarHelper.firstDayOfWeek = Calendar.SATURDAY

        val list = CalendarHelper.generateNamesOfDayWeek()

        assertThat(list.size, Is.`is`(7))

        assertThat(list[0], Is.`is`("Sat"))
        assertThat(list[1], Is.`is`("Sun"))
        assertThat(list[2], Is.`is`("Mon"))
        assertThat(list[3], Is.`is`("Tue"))
        assertThat(list[4], Is.`is`("Wed"))
        assertThat(list[5], Is.`is`("Thu"))
        assertThat(list[6], Is.`is`("Fri"))
    }

    @Test
    fun equalsDayMothYear_True() {

        val calendar1 = Calendar.getInstance()
        calendar1.set(2019, Calendar.NOVEMBER, 17, 0, 0, 0)
        val calendar2 = Calendar.getInstance()
        calendar2.set(2019, Calendar.NOVEMBER, 17, 0, 0, 0)

        assertTrue(CalendarHelper.equalsDayMothYear(calendar1, calendar2))
    }

    @Test
    fun equalsDayMothYear_False_Day() {

        val calendar1 = Calendar.getInstance()
        calendar1.set(2019, Calendar.NOVEMBER, 17, 0, 0, 0)
        val calendar2 = Calendar.getInstance()
        calendar2.set(2019, Calendar.NOVEMBER, 20, 0, 0, 0)

        assertFalse(CalendarHelper.equalsDayMothYear(calendar1, calendar2))
    }

    @Test
    fun equalsDayMothYear_False_Moth() {

        val calendar1 = Calendar.getInstance()
        calendar1.set(2019, Calendar.NOVEMBER, 20, 0, 0, 0)
        val calendar2 = Calendar.getInstance()
        calendar2.set(2019, Calendar.FEBRUARY, 20, 0, 0, 0)

        assertFalse(CalendarHelper.equalsDayMothYear(calendar1, calendar2))
    }

    @Test
    fun equalsDayMothYear_False_Year() {

        val calendar1 = Calendar.getInstance()
        calendar1.set(300, Calendar.NOVEMBER, 20, 0, 0, 0)
        val calendar2 = Calendar.getInstance()
        calendar2.set(301, Calendar.NOVEMBER, 20, 0, 0, 0)

        assertFalse(CalendarHelper.equalsDayMothYear(calendar1, calendar2))
    }

    @Test
    fun equalsDayMothYear_False_1null() {
        val calendar1 = Calendar.getInstance()
        calendar1.set(300, Calendar.NOVEMBER, 20, 0, 0, 0)

        assertFalse(CalendarHelper.equalsDayMothYear(calendar1, null))
    }

    @Test
    fun equalsDayMothYear_False_2null() {
        assertFalse(CalendarHelper.equalsDayMothYear(null, null))
    }

    @Test
    fun getDateBetweenForDayOfWeek_Saturday() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 1, 0, 0, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 22, 0, 0, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }

    @Test
    fun getDateBetweenForDayOfWeek_StartFromFridayBefore() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 1, 0, 0, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 22, 0, 0, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }

    @Test
    fun getDateBetweenForDayOfWeek_StartFromSundayBefore() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.OCTOBER, 20, 0, 0, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }

    @Test
    fun getDateBetweenForDayOfWeek_StartFromMondayBefore() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.OCTOBER, 14, 0, 0, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }

    @Test
    fun getDateBetweenForDayOfWeek_StartFromFridayAfter() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 29, 0, 0, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 22, 0, 0, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }

    @Test
    fun getDateBetweenForDayOfWeek_StartFromSundayAfter() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.DECEMBER, 8, 1, 0, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 1, 0, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }

    @Test
    fun getDateBetweenForDayOfWeek_StartFromMondayAfter() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        val from = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 0, 0)
        val to = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 24, 0, 0, 0)

        val dayOfWeek = CalendarHelper.prepareDate(2019, Calendar.DECEMBER, 30, 0, 15, 0)

        val result = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 18, 0, 15, 0)
        assertThat(
            CalendarHelper.getDateBetweenForDayOfweek(from, to, dayOfWeek), Is.`is`(result)
        )
    }


    @Test
    fun setDayToMonth_NextMonth_Bound() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        CalendarHelper.firstDayOfWeek = null

        val day = CalendarHelper.prepareDate(2019, Calendar.JULY, 31, 1, 0, 0)
        val month = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 5, 0, 0, 0)

        assertThat(
            CalendarHelper.setDayToMonth(day, month),
            YearMonthDayMatcher(CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 30, 0, 0, 0))
        )
    }

    @Test
    fun setDayToMonth_PreviousMonth_Bound() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        CalendarHelper.firstDayOfWeek = null

        val day = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 1, 0, 0, 0)
        val month = CalendarHelper.prepareDate(2019, Calendar.JULY, 5, 0, 0, 0)

        assertThat(
            CalendarHelper.setDayToMonth(day, month),
            Is.`is`(CalendarHelper.prepareDate(2019, Calendar.JULY, 1, 0, 0, 0))
        )
    }

    @Test
    fun setDayToMonth_DefenceYer() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        CalendarHelper.firstDayOfWeek = null

        val day = CalendarHelper.prepareDate(2009, Calendar.JUNE, 20, 0, 0, 0)
        val month = CalendarHelper.prepareDate(2019, Calendar.JULY, 5, 0, 0, 0)

        assertThat(
            CalendarHelper.setDayToMonth(day, month),
            YearMonthDayMatcher(CalendarHelper.prepareDate(2019, Calendar.JULY, 20, 0, 0, 0))
        )
    }

    @Test
    fun setDayToMonth_someMonth() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

        CalendarHelper.firstDayOfWeek = null

        val day = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 10, 0, 0, 0)
        val month = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 5, 0, 0, 0)

        assertThat(
            CalendarHelper.setDayToMonth(day, month),
            Is.`is`(CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 10, 0, 0, 0))
        )
    }

}