package krystian.kalendarz.util

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager


object KeyboardUtil {

    fun hiddenKeyboard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }
}
