package krystian.kalendarz.util

import android.content.Context
import io.michaelrocks.libphonenumber.android.NumberParseException
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import io.michaelrocks.libphonenumber.android.Phonenumber
import java.util.*

object AppPhoneNumberUtil {

    private lateinit var phoneNumberUtil: PhoneNumberUtil

    fun init(appContext: Context) {
        phoneNumberUtil = PhoneNumberUtil.createInstance(appContext)
    }

    fun formatNumber(phoneNumber: String): String {
        return phoneNumberUtil.format(getPhoneNumber(phoneNumber),PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
    }

    private fun getPhoneNumber(phoneNumber: String): Phonenumber.PhoneNumber {
        return try {
            phoneNumberUtil.parse(phoneNumber, Locale.getDefault().country)
        } catch (e: NumberParseException) {
            null
        } ?: return Phonenumber.PhoneNumber()
    }

    fun clearPhoneNumber(phoneNumber: String): String {
        return phoneNumber.replace(Regex("[^0-9.-\\\\#\\\\+]"), "")
    }
}