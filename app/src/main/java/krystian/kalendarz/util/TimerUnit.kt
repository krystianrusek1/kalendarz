package krystian.kalendarz.util

class TimerUnit {

    private val LOG = Loggger(this.javaClass)

    private var nanoTime = System.nanoTime()

    private var title: String

    constructor() {
        this.title = ""
    }

    constructor(title: String) {
        this.title = title
    }

    fun start(): Unit {
        nanoTime = System.nanoTime()
    }

    fun getPeriodOfTimeNano(): Long {
        return System.nanoTime() - nanoTime
    }

    fun getPeriodOfTimeMili(): Long {
        return (System.nanoTime() - nanoTime) / 1000000
    }

    fun logNano() {
        LOG.d("$title + ${getPeriodOfTimeNano()} ")
    }

    fun logNano(text:String) {
        LOG.d(title + " " + text + " "+ getPeriodOfTimeNano() )
    }

    fun logMili() {
        LOG.d("$title + ${getPeriodOfTimeMili()} ")
    }

    fun logMili(text:String) {
        LOG.d(title + " " + text + " "+ getPeriodOfTimeMili() )
    }
}