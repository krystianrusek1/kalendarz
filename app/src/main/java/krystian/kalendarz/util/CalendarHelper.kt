package krystian.kalendarz.util

import java.util.*
import kotlin.collections.ArrayList

object CalendarHelper {

    const val ONE_MINUTE_MILLISECONDS = 60 * 1000L
    const val ONE_HOUR_MILLISECONDS = 60 * ONE_MINUTE_MILLISECONDS
    const val ONE_DAY_MILLISECONDS = 24 * ONE_HOUR_MILLISECONDS
    const val SEVEN_DAY_MILLISECONDS = 7 * ONE_DAY_MILLISECONDS

    /**
     * @see [Calendar.SUNDAY]
     * @see [Calendar.MONDAY]
     * @see [Calendar.TUESDAY]
     * @see [Calendar.WEDNESDAY]
     * @see [Calendar.THURSDAY]
     * @see [Calendar.SATURDAY]
     * @see [Calendar.SUNDAY]
     *
     * If null, then it is taken form [Calendar]
     */
    var firstDayOfWeek: Int? = Calendar.MONDAY

    fun getInstance(): Calendar {
        val cal = Calendar.getInstance()
        if (firstDayOfWeek != null) {
            cal.firstDayOfWeek = firstDayOfWeek as Int
        }
        return cal
    }

    fun getInstance(zone: TimeZone): Calendar {
        val cal = Calendar.getInstance(zone)
        if (firstDayOfWeek != null) {
            cal.firstDayOfWeek = firstDayOfWeek as Int
        }
        return cal
    }

    fun generateWeeksForMonthFrom(numberOfWeeks: Int, date: Date): List<Calendar> {
        val calendar = getInstance()
        calendar.time = date
        calendar.set(Calendar.DAY_OF_MONTH, 1)

        return generateWeeksFrom(numberOfWeeks, calendar)
    }

    fun generateWeeksFrom(numberOfWeeks: Int, date: Date): List<Calendar> {
        val calendar = getInstance()
        calendar.time = date
        return generateWeeksFrom(numberOfWeeks, calendar)
    }

    fun generateWeeksFrom(numberOfWeeks: Int, calendar: Calendar): List<Calendar> {
        val firstDateOfWeek = findFirstDateOfWeek(calendar)
        val list = ArrayList<Calendar>()
        for (i in 1..(numberOfWeeks * 7)) {
            list.add(firstDateOfWeek.clone() as Calendar)
            firstDateOfWeek.add(Calendar.DATE, 1)
        }

        return list
    }

    fun generateNamesOfDayWeek(): List<String> {
        val list = ArrayList<String>()
        val calendar = getInstance()

        calendar.set(Calendar.DAY_OF_WEEK, calendar.firstDayOfWeek)
        for (i in 0..6) {
            list.add(
                calendar.getDisplayName(
                    Calendar.DAY_OF_WEEK,
                    Calendar.SHORT,
                    Locale.getDefault()
                )
            )
            calendar.add(Calendar.DATE, 1)
        }

        return list
    }

    fun findFirstDateOfWeek(calendar: Calendar): Calendar {
        val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        var diff = calendar.firstDayOfWeek - dayOfWeek

        if (diff > 0) {
            diff -= 7
        }
        calendar.add(Calendar.DATE, diff)

        return calendar
    }

    fun equalsDayMothYear(calendar1: Calendar?, calendar2: Calendar?): Boolean {
        if (calendar1 == null || calendar2 == null) {
            return false
        }

        var isEquals = true
        if (calendar1.get(Calendar.DAY_OF_YEAR) != calendar2.get(Calendar.DAY_OF_YEAR)) {
            isEquals = false
        }

        if (calendar1.get(Calendar.YEAR) != calendar2.get(Calendar.YEAR)) {
            isEquals = false
        }

        return isEquals
    }

    fun prepareStartDate(date: Date): Date {
        val calendar = getInstance()
        calendar.time = date
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        return calendar.time
    }

    fun prepareEndDate(date: Date): Date {
        val calendar = getInstance()
        calendar.time = date
        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 59)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        return calendar.time
    }

    fun prepareCalendar(
        year: Int, month: Int, date: Int
    ): Calendar {
        val calendar = getInstance()
        calendar.set(year, month, date)
        return calendar
    }

    fun prepareDate(
        year: Int, month: Int, date: Int
    ): Date {
        return prepareDate(year, month, date, 0, 0, 0)
    }

    fun prepareDate(
        year: Int, month: Int, date: Int, hourOfDay: Int, minute: Int
    ): Date {
        return prepareDate(year, month, date, hourOfDay, minute, 0)
    }

    fun prepareDate(
        year: Int, month: Int, date: Int, hourOfDay: Int, minute: Int, second: Int
    ): Date {
        val calendar = getInstance()
        calendar.set(year, month, date, hourOfDay, minute, second)
        return calendar.time
    }

    fun isAfterDay(firstDate: Date, lastDate: Date): Boolean {
        return prepareStartDate(firstDate).after(prepareStartDate(lastDate))
    }

    fun isBeforeDay(firstDate: Date, lastDate: Date): Boolean {
        return prepareStartDate(firstDate).before(prepareStartDate(lastDate))
    }

    fun getDateBetweenForDayOfweek(from: Date, to: Date, dayOfWeek: Date): Date {
        val from = prepareStartDate(from)
        val to = prepareEndDate(to)

        return when {
            dayOfWeek.time <= from.time -> {
                val difTimeMillis = from.time - dayOfWeek.time
                val numberOfWeeks = (difTimeMillis / SEVEN_DAY_MILLISECONDS) + 1
                Date(dayOfWeek.time + numberOfWeeks * SEVEN_DAY_MILLISECONDS)
            }
            to.time <= dayOfWeek.time -> {
                val difTimeMillis = dayOfWeek.time - to.time
                val numberOfWeeks = (difTimeMillis / SEVEN_DAY_MILLISECONDS) + 1
                Date(dayOfWeek.time - numberOfWeeks * SEVEN_DAY_MILLISECONDS)
            }
            else -> dayOfWeek
        }
    }

    fun setDayToMonth(day: Date, month: Date): Date {
        val calendarDay = getInstance()
        calendarDay.time = day
        val calendarMonth = getInstance()
        calendarMonth.time = month

        val min = calendarMonth.getMinimum(Calendar.DAY_OF_MONTH)
        val max = calendarMonth.getActualMaximum(Calendar.DAY_OF_MONTH)
        var dayInt = calendarDay[Calendar.DAY_OF_MONTH]

        if (min > dayInt) {
            dayInt = min
        }

        if (max < dayInt) {
            dayInt = max
        }

        calendarMonth[Calendar.DAY_OF_MONTH] = dayInt
        return calendarMonth.time
    }

    fun areSameMonth(month1: Date, month2: Date): Boolean {

        val calendarMonth1 = getInstance()
        calendarMonth1.time = month1

        val calendarMonth2 = getInstance()
        calendarMonth2.time = month2

        return ((calendarMonth1[Calendar.YEAR] == calendarMonth2[Calendar.YEAR]) &&
                (calendarMonth1[Calendar.MONTH] == calendarMonth2[Calendar.MONTH]))
    }

    fun getDeferenceNumberOfDay(date1: Date, date2: Date): Int {
        val difTimeMillis = date1.time - date2.time
        return ((difTimeMillis / ONE_DAY_MILLISECONDS)).toInt()
    }
}