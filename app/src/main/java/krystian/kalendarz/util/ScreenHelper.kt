package krystian.kalendarz.util

import android.content.res.Resources

object ScreenHelper {

    val HEIGHT_PIXELS_SCREEN = Resources.getSystem().displayMetrics.heightPixels

    val HEIGHT_PIXELS_66_PERCENT_SCREEN = (HEIGHT_PIXELS_SCREEN * (2f/3f)).toInt()
    val HEIGHT_PIXELS_33_PERCENT_SCREEN = (HEIGHT_PIXELS_SCREEN * (1f/3f)).toInt()
}