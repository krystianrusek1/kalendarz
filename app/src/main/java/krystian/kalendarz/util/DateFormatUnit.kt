package krystian.kalendarz.util

import android.annotation.SuppressLint
import android.content.res.Resources
import krystian.kalendarz.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object DateFormatUnit {

    private lateinit var resources: Resources

    fun setResources(resources: Resources) {
        this.resources = resources
    }

    @SuppressLint("SimpleDateFormat")
    fun formatDay(date: Date): String {
        val simpleDateFormatDate = SimpleDateFormat("yyyy-MM-dd")
        return simpleDateFormatDate.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun formatTime(date: Date): String {
        val simpleDateFormatTime = SimpleDateFormat("HH:mm:ss")
        return simpleDateFormatTime.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun formatDate(date: Date): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return simpleDateFormat.format(date)
    }

    fun formatMonthYear(date: Date): String {
        val calendar = CalendarHelper.getInstance()
        calendar.time = date
        val mothName = getMothName(calendar)
        val yearName = calendar.get(Calendar.YEAR)

        return "$mothName $yearName"
    }

    fun format2MonthYear(date1: Date, date2: Date): String {
        val calendar1 = CalendarHelper.getInstance()
        calendar1.time = date1
        val mothName1 = getMothName(calendar1)
        val year1 = calendar1.get(Calendar.YEAR)

        val calendar2 = CalendarHelper.getInstance()
        calendar2.time = date2
        val mothName2 = getMothName(calendar2)
        val year2 = calendar2.get(Calendar.YEAR)

        return when {
            mothName1 == mothName2 -> formatMonthYear(date1)
            year1 == year2 -> "$mothName1/$mothName2 $year1"
            else -> "$mothName1 $year1/$mothName2 $year2"
        }
    }


    fun getMothName(calendar: Calendar): String {
        return when (calendar.get(Calendar.MONTH)) {
            Calendar.JANUARY -> resources.getString(R.string.january)
            Calendar.FEBRUARY -> resources.getString(R.string.february)
            Calendar.MARCH -> resources.getString(R.string.march)
            Calendar.APRIL -> resources.getString(R.string.april)
            Calendar.MAY -> resources.getString(R.string.may)
            Calendar.JUNE -> resources.getString(R.string.june)
            Calendar.JULY -> resources.getString(R.string.july)
            Calendar.AUGUST -> resources.getString(R.string.august)
            Calendar.SEPTEMBER -> resources.getString(R.string.september)
            Calendar.OCTOBER -> resources.getString(R.string.october)
            Calendar.NOVEMBER -> resources.getString(R.string.november)
            Calendar.DECEMBER -> resources.getString(R.string.december)
            else -> ""
        }
    }

    fun formatDayShort(date: Date): String {
        return DateFormat.getDateInstance(DateFormat.SHORT).format(date)
    }

    fun formatDayLong(date: Date): String {
        return DateFormat.getDateInstance(DateFormat.LONG).format(date)
    }

    fun formatTimeShort(date: Date): String {
        return DateFormat.getTimeInstance(DateFormat.SHORT).format(date)
    }
}