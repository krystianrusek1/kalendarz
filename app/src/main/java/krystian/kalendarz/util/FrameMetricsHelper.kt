package krystian.kalendarz.util

import android.app.Activity
import android.os.Build
import android.os.Handler
import android.view.FrameMetrics
import android.view.Window
import androidx.annotation.RequiresApi
import krystian.kalendarz.ui.main.activity.MainActivity

@RequiresApi(Build.VERSION_CODES.N)
class FrameMetricsHelper : Window.OnFrameMetricsAvailableListener {

    internal val LOG = Loggger(this.javaClass)

    companion object{
        var MILI:Double = Math.pow(10.0,6.0)
    }

    private lateinit var title: String

    fun addOnFrameMetrics(activity: Activity, title: String) {
        this.title = title
        activity.window.addOnFrameMetricsAvailableListener(this, Handler())

    }

    fun removeOnFrameMetrics(activity: Activity) {

        activity.window.removeOnFrameMetricsAvailableListener(this)

    }


    override fun onFrameMetricsAvailable(
        window: Window?,
        metrics: FrameMetrics?,
        dropCountSinceLastInvocation: Int
    ) {


        LOG.d("start : $title")
        LOG.d("ANIMATION_DURATION: " + metrics!!.getMetric(FrameMetrics.ANIMATION_DURATION)/MILI)
        LOG.d("ANIMATION_DURATION: " + metrics!!.getMetric(FrameMetrics.ANIMATION_DURATION)/MILI)
        LOG.d("COMMAND_ISSUE_DURATION: " + metrics.getMetric(FrameMetrics.COMMAND_ISSUE_DURATION)/MILI)
        LOG.d("DRAW_DURATION: " + metrics.getMetric(FrameMetrics.DRAW_DURATION)/MILI)
        LOG.d("FIRST_DRAW_FRAME: " + metrics.getMetric(FrameMetrics.FIRST_DRAW_FRAME)/MILI)
        LOG.d("INPUT_HANDLING_DURATION: " + metrics.getMetric(FrameMetrics.INPUT_HANDLING_DURATION)/MILI)
        LOG.d("LAYOUT_MEASURE_DURATION: " + metrics.getMetric(FrameMetrics.LAYOUT_MEASURE_DURATION)/MILI)
        LOG.d("SWAP_BUFFERS_DURATION: " + metrics.getMetric(FrameMetrics.SWAP_BUFFERS_DURATION)/MILI)
        LOG.d("SYNC_DURATION: " + metrics.getMetric(FrameMetrics.SYNC_DURATION)/MILI)
        val total = metrics.getMetric(FrameMetrics.TOTAL_DURATION)/MILI
        if (total > 300.0){
            LOG.e("TOTAL_DURATION: $total")
        } else {
            LOG.d("TOTAL_DURATION: $total")
        }
        LOG.d("UNKNOWN_DELAY_DURATION: " + metrics.getMetric(FrameMetrics.UNKNOWN_DELAY_DURATION)/MILI)
        LOG.d("stop : $title")

    }
}