package krystian.kalendarz.util

import android.util.Log

class Loggger {

    val tag: String

    constructor(javaClass: Class<*>) {
        this.tag = javaClass.name
    }

    constructor(tag: String) {
        this.tag = tag
    }

    fun d(text: String, vararg args: Any?) {
        Log.d(tag, String.format(text, args))
    }

    fun i(text: String, vararg args: Any?) {
        Log.i(tag, String.format(text, args))
    }

    fun w(text: String) {
        Log.w(tag, text)
    }

    fun e(text: String, throwable: Throwable) {
        Log.e(tag, text, throwable)
    }

    fun e(text: String) {
        Log.e(tag, text)
    }
}