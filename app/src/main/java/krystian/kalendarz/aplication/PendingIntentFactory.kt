package krystian.kalendarz.aplication

import android.app.PendingIntent
import android.content.Context
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.service.sms.DeliverySmsBroadcastReceiver
import krystian.kalendarz.service.sms.SentSmsBroadcastReceiver
import javax.inject.Singleton

@Singleton
class PendingIntentFactory(private val context: Context) {

    fun createSentSmsBroadcastReceiver(sms: SmsEntity): PendingIntent {
        return SentSmsBroadcastReceiver.newPendingIntent(context, sms)
    }

    fun createDeliverySmsBroadcastReceiver(sms: SmsEntity): PendingIntent {
        return DeliverySmsBroadcastReceiver.newPendingIntent(context, sms)
    }
}