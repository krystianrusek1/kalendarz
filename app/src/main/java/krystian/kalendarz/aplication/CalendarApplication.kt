package krystian.kalendarz.aplication

import android.app.Application
import androidx.work.Configuration
import krystian.kalendarz.logic.sms.SyncSmsManager
import krystian.kalendarz.ui.tools.gesturedecector.GestureDetectorHelper
import krystian.kalendarz.util.AppPhoneNumberUtil
import krystian.kalendarz.util.DateFormatUnit
import krystian.kalendarz.util.KeyboardUtil
import javax.inject.Inject

class CalendarApplication : Application(), Configuration.Provider {

    lateinit var applicationComponent: ApplicationComponent

    @Inject
    lateinit var syncSmsManager: SyncSmsManager

    override fun onCreate() {
        super.onCreate()
        inject()
        init()
        GestureDetectorHelper.setup(this)
        AppPhoneNumberUtil.init(this)
    }

    private fun inject() {
        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()

        applicationComponent.inject(this)
    }

    private fun init() {
        DateFormatUnit.setResources(resources)
    }

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .setMinimumLoggingLevel(android.util.Log.INFO)
            .build()
    }

}
