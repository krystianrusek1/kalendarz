package krystian.kalendarz.aplication

import android.accounts.AccountManager
import android.content.Context
import android.content.res.Resources
import android.telephony.SmsManager
import androidx.room.Room
import androidx.work.WorkManager
import dagger.Module
import dagger.Provides
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.calendar.CalendarRepositoryImpl
import krystian.kalendarz.data.contact.ContactRepository
import krystian.kalendarz.data.contact.ContactRepositoryImpl
import krystian.kalendarz.data.database.AppDatabase
import krystian.kalendarz.data.externalcalendar.ExternalDataBase
import krystian.kalendarz.data.externalcalendar.ExternalDataBaseImpl
import krystian.kalendarz.data.phone.PhoneNumberRepository
import krystian.kalendarz.data.phone.PhoneNumberRepositoryImpl
import krystian.kalendarz.data.sms.SendSMSRepository
import krystian.kalendarz.data.sms.SendSMSRepositoryImpl
import krystian.kalendarz.data.storage.LocalStorage
import krystian.kalendarz.data.storage.LocalStorageImpl
import krystian.kalendarz.logic.sms.SyncSmsManager
import krystian.kalendarz.logic.sms.SyncSmsManagerImpl
import krystian.kalendarz.permissions.PermissionManager
import krystian.kalendarz.permissions.PermissionManagerImpl
import krystian.kalendarz.service.authenticator.AppAccountManager
import krystian.kalendarz.service.authenticator.AppAccountManagerImpl
import javax.inject.Singleton

@Module
class ApplicationModule(val context: Context) {

    @Provides
    @Singleton
    fun providesFakeDatabase(): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, AppDatabase.DATA_BASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun providesSendSMSRepository(sendSMSRepositoryImpl: SendSMSRepositoryImpl): SendSMSRepository {
        return sendSMSRepositoryImpl
    }

    @Provides
    @Singleton
    fun providesContactRepository(contactRepository: ContactRepositoryImpl): ContactRepository {
        return contactRepository
    }

    @Provides
    @Singleton
    fun providesCalendarRepository(calendarRepository: CalendarRepositoryImpl): CalendarRepository {
        return calendarRepository
    }

    @Provides
    @Singleton
    fun providesPhoneNumberRepository(phoneNumberRepository: PhoneNumberRepositoryImpl): PhoneNumberRepository {
        return phoneNumberRepository
    }

    @Provides
    @Singleton
    fun providesExternalDataBase(externalDataBase: ExternalDataBaseImpl): ExternalDataBase {
        return externalDataBase
    }

    @Provides
    @Singleton
    fun providesAccountManager(): AccountManager {
        return context.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager
    }

    @Provides
    @Singleton
    fun providesSyncAdapterManager(syncSmsManager: SyncSmsManagerImpl): SyncSmsManager {
        return syncSmsManager
    }

    @Provides
    @Singleton
    fun providesAppAccountManager(appAccountManager: AppAccountManagerImpl): AppAccountManager {
        return appAccountManager
    }

    @Provides
    @Singleton
    fun providesLocalStorage(localStorage: LocalStorageImpl): LocalStorage {
        return localStorage
    }

    @Provides
    @Singleton
    fun providesPermissionManager(permissionManager: PermissionManagerImpl): PermissionManager {
        return permissionManager
    }

    @Provides
    @Singleton
    fun providesApplicationContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun providesSmsManager(): SmsManager {
        return SmsManager.getDefault()
    }

    @Provides
    @Singleton
    fun providesPendingIntentFactory(): PendingIntentFactory {
        return PendingIntentFactory(context)
    }

    @Provides
    @Singleton
    fun providesWorkManager(): WorkManager {
        return WorkManager.getInstance(context)
    }

    @Provides
    @Singleton
    fun providesAppLiveCycle(): AppLiveCycle {
        return AppLiveCycle()
    }

    @Provides
    @Singleton
    fun resources(): Resources {
        return context.resources
    }
}