package krystian.kalendarz.aplication

import javax.inject.Singleton

@Singleton
class AppLiveCycle {

    interface AppLiveCycleListener {
        fun onResume()
        fun onPause()
    }

    private val listenerList = ArrayList<AppLiveCycleListener>()

    fun addListener(listener: AppLiveCycleListener) {
        if (!listenerList.contains(listener)) {
            listenerList.add(listener)
        }
    }

    fun removeListener(listener: AppLiveCycleListener) {
        listenerList.remove(listener)
    }

    fun triggerResume() {
        listenerList.forEach { it.onResume() }
    }

    fun triggerPause() {
        listenerList.forEach { it.onPause() }
    }
}