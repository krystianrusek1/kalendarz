package krystian.kalendarz.aplication

import android.accounts.AccountManager
import android.content.Context
import android.content.res.Resources
import android.telephony.SmsManager
import androidx.work.WorkManager
import dagger.Component
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.contact.ContactRepository
import krystian.kalendarz.data.database.AppDatabase
import krystian.kalendarz.data.externalcalendar.ExternalDataBase
import krystian.kalendarz.data.phone.PhoneNumberRepository
import krystian.kalendarz.data.sms.SendSMSRepository
import krystian.kalendarz.data.storage.LocalStorage
import krystian.kalendarz.logic.sms.SyncSmsManager
import krystian.kalendarz.permissions.PermissionManager
import krystian.kalendarz.service.authenticator.AppAccountManager
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun database(): AppDatabase

    fun sendSMSRepository(): SendSMSRepository

    fun calendarRepository(): CalendarRepository

    fun phoneNumberRepository(): PhoneNumberRepository

    fun contactRepository(): ContactRepository

    fun accountManager(): AccountManager

    fun syncAdapterManager(): SyncSmsManager

    fun appAccountManager(): AppAccountManager

    fun externalDataBase(): ExternalDataBase

    fun permissionManager(): PermissionManager

    fun localStorage(): LocalStorage

    fun applicationContext(): Context

    fun smsManager(): SmsManager

    fun pendingIntentFactory(): PendingIntentFactory

    fun workManager(): WorkManager

    fun appLiveCycle(): AppLiveCycle

    fun resources(): Resources

    fun inject(calendarApplication: CalendarApplication)

    @Component.Builder
    interface Builder {

        fun applicationModule(applicationModule: ApplicationModule): Builder

        fun build(): ApplicationComponent
    }
}