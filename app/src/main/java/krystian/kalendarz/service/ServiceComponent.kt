package krystian.kalendarz.service

import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.data.database.AppDatabase
import krystian.kalendarz.logic.sms.SyncSmsManager

@ServiceScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ServiceModule::class]
)
interface ServiceComponent {

    fun database(): AppDatabase

    fun syncSmsManager(): SyncSmsManager

    @Component.Builder
    interface Builder {
        fun build(): ServiceComponent

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}