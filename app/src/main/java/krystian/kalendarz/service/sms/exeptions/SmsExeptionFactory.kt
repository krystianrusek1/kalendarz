package krystian.kalendarz.service.sms.exeptions

import android.telephony.SmsManager

object SmsExeptionFactory {
    fun create(message: String, errorCode: Int): Exception? = when (errorCode) {
        SmsManager.RESULT_ERROR_GENERIC_FAILURE -> GenericFalureSmsExeption(message)
        SmsManager.RESULT_ERROR_NULL_PDU -> NullPduSmsExeption(message)
        SmsManager.RESULT_ERROR_RADIO_OFF -> RadioOffSmsExeption(message)
        else -> null
    }
}