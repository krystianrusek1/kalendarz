package krystian.kalendarz.service.sms

import android.accounts.Account
import android.content.*
import android.os.Bundle
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.service.DaggerServiceComponent
import krystian.kalendarz.service.ServiceComponent
import krystian.kalendarz.util.Loggger

class SyncAdapter @JvmOverloads constructor(
    context: Context,
    autoInitialize: Boolean,
    allowParallelSyncs: Boolean = false,
    val contentResolver: ContentResolver = context.contentResolver
) : AbstractThreadedSyncAdapter(context, autoInitialize, allowParallelSyncs) {

    private val LOG = Loggger(this.javaClass)

    override fun onPerformSync(
        account: Account?,
        extras: Bundle?,
        authority: String?,
        provider: ContentProviderClient?,
        syncResult: SyncResult?
    ) {
        LOG.d("onPerformSync ")
        val syncSmsManager = inject().syncSmsManager()
        //inject().syncExternalCalendarManager().onSync()

        //syncSmsManager.onPerformSync()

    }

    private fun inject(): ServiceComponent {
        return DaggerServiceComponent.builder().applicationComponent(getApplicationComponent()).build()
    }

    private fun getApplicationComponent(): ApplicationComponent {
        return (context.applicationContext as CalendarApplication).applicationComponent
    }

}
