package krystian.kalendarz.service.sms

import android.app.PendingIntent
import android.app.PendingIntent.FLAG_UPDATE_CURRENT
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.logic.sms.SyncSmsManagerImpl
import krystian.kalendarz.service.DaggerServiceComponent
import krystian.kalendarz.service.ServiceComponent
import krystian.kalendarz.service.sms.exeptions.SmsExeptionFactory
import krystian.kalendarz.util.Loggger


class DeliverySmsBroadcastReceiver : BroadcastReceiver() {

    internal val LOG = Loggger(this.javaClass)

    companion object {
        const val KEY_SMS = "KEY_SMS"
        fun newPendingIntent(context: Context, sms: SmsEntity): PendingIntent {
            val intent = Intent(context, DeliverySmsBroadcastReceiver::class.java)
            intent.putExtra(KEY_SMS, sms)
            return PendingIntent.getBroadcast(
                context,
                sms.id!!,
                intent,
                FLAG_UPDATE_CURRENT
            )
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        LOG.d("onReceive resultCode: $resultCode")

        val exception = SmsExeptionFactory.create("The message has not delivered", resultCode)
        var errorCode: Int? = null
        if (exception != null) {
            LOG.e("onReceive resultCode: $resultCode", exception)
            errorCode = resultCode
        }

        if (context != null) {
            val sms = intent?.extras?.getSerializable(SentSmsBroadcastReceiver.KEY_SMS)

            if (sms != null) {
                handleSms(context, sms as SmsEntity, errorCode)
            } else {
                LOG.e("Can not find the message in the intent")
            }
        }
    }

    private fun handleSms(context: Context, sms: SmsEntity, errorCode: Int?) {
        GlobalScope.launch {
            getSyncSmsManagerImpl(context).onSmsDelivered(sms, errorCode)
        }
    }

    private fun getSyncSmsManagerImpl(context: Context): SyncSmsManagerImpl {
        return inject(context).syncSmsManager() as SyncSmsManagerImpl
    }

    private fun inject(context: Context): ServiceComponent {
        return DaggerServiceComponent.builder().applicationComponent(getApplicationComponent(context)).build()
    }

    private fun getApplicationComponent(context: Context): ApplicationComponent {
        return (context.applicationContext as CalendarApplication).applicationComponent
    }

}