package krystian.kalendarz.service.authenticator

import android.accounts.Account
import android.accounts.AccountManager
import krystian.kalendarz.service.authenticator.AppAccountManager.Companion.ACCOUNT
import krystian.kalendarz.service.authenticator.AppAccountManager.Companion.ACCOUNT_TYPE
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppAccountManagerImpl @Inject constructor(private val accountManager: AccountManager) : AppAccountManager {

    override fun getAccount(): Account {
        return Account(ACCOUNT, ACCOUNT_TYPE).also { newAccount ->
            accountManager.addAccountExplicitly(newAccount, null, null)
        }
    }


}