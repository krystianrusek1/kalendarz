package krystian.kalendarz.service.authenticator

import android.app.Service
import android.content.Intent
import android.os.IBinder

class AuthenticatorService : Service() {

    private lateinit var mAuthenticator: Authenticator

    override fun onBind(intent: Intent?): IBinder = mAuthenticator.iBinder
}