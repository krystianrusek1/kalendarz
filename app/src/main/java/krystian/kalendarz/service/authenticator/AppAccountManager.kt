package krystian.kalendarz.service.authenticator

import android.accounts.Account

interface AppAccountManager {

    companion object {
        const val ACCOUNT_TYPE = "com.krystian.calendar.datasync"
        const val ACCOUNT = "calendar"
        const val AUTHORITY = "com.calendar.android.datasync.provider"
    }

    fun getAccount(): Account
}