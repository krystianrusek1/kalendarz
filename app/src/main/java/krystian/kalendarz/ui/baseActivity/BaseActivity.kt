package krystian.kalendarz.ui.baseActivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.ui.viewmodel.BaseViewModel
import krystian.kalendarz.util.Loggger
import javax.inject.Inject

abstract class BaseActivity<T : BaseViewModel>(private var viewModelClass: Class<T>) : AppCompatActivity() {

    companion object {
        private const val KEY_HAS_SAVE_INSTANCE_STATE = "KEY_HAS_SAVE_INSTANCE_STATE"
    }

    internal val LOG = Loggger(this.javaClass)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState?.putBoolean(KEY_HAS_SAVE_INSTANCE_STATE, true)
        super.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onStop() {
        viewModel.onStop()
        super.onStop()
    }

    abstract fun goBack()

    protected fun getApplicationComponent(): ApplicationComponent {
        return (applicationContext as CalendarApplication).applicationComponent
    }

    private fun inject() {
        injectDagger()
        initViewModel(viewModelFactory)
    }

    private fun initViewModel(viewModelFactory: ViewModelProvider.Factory) {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClass)
    }

    protected abstract fun injectDagger()
}