package krystian.kalendarz.ui.baseActivity

import javax.inject.Scope

@Scope
annotation class ActivityScope {
}