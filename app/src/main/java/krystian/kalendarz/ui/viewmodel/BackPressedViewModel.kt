package krystian.kalendarz.ui.viewmodel

import krystian.kalendarz.ui.basefragment.BackPressed

abstract class BackPressedViewModel : BaseViewModel(), BackPressed {

    override fun onBackPressed(): Boolean {
        return false
    }
}