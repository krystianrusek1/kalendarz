package krystian.kalendarz.ui.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import krystian.kalendarz.ui.basefragment.BackPressed
import krystian.kalendarz.util.Loggger

abstract class BaseViewModel : ViewModel() {

    internal val LOG = Loggger(this.javaClass)

    @CallSuper
    open fun onStart() {
        LOG.d("onStart")
    }

    @CallSuper
    open fun onStop() {
        LOG.d("onStop")
    }

    @CallSuper
    override fun onCleared() {
        LOG.d("onCleared")
        super.onCleared()
    }
}