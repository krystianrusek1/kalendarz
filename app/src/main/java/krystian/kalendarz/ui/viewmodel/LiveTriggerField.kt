package krystian.kalendarz.ui.viewmodel

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 * Class is wrapper on [MutableLiveData]. Class keep only one last [Observer].
 *
 * @param <T> The type of data hold by this instance
 */
class LiveTriggerField<T> : LifecycleObserver {
    private var liveData = MutableLiveData<T>()

    private var observer: Observer<in T>? = null

    private var hasExecuted = false

    var value: T?
        get() = liveData.value
        set(value) {
            hasExecuted = false
            liveData.value = value
        }

    @MainThread
    fun setObserver(owner: LifecycleOwner, observer: Observer<in T>) {
        this.observer = observer
        liveData.observe(owner, Observer { value ->
            if (!hasExecuted) {
                observer.onChanged(value)
                hasExecuted = true
            }
        })
    }
}