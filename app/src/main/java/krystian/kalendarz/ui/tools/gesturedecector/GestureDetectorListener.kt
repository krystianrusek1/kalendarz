package krystian.kalendarz.ui.tools.gesturedecector

import android.view.GestureDetector
import android.view.MotionEvent

abstract class GestureDetectorListener : GestureDetector.OnGestureListener {
    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {

    }
}