package krystian.kalendarz.ui.tools.gesturedecector

import android.content.Context
import android.view.MotionEvent
import android.view.ViewConfiguration
import krystian.kalendarz.util.Loggger

object GestureDetectorHelper {

    private val LOG = Loggger(this.javaClass)

    private var SCALED_TOUCH_SLOP: Int = 0

    private var SCALED_TOUCH_SLOP_X2: Int = 0

    private lateinit var viewConfiguration: ViewConfiguration

    fun setup(context: Context) {
        this.viewConfiguration = ViewConfiguration.get(context)
        SCALED_TOUCH_SLOP = viewConfiguration.scaledTouchSlop
        SCALED_TOUCH_SLOP_X2 = SCALED_TOUCH_SLOP * 2
    }

    fun isRightToLeftSwipe(e1: MotionEvent, e2: MotionEvent): Boolean {
        val distanceY = Math.abs(e2.y - e1.y)
        if (e1.x - e2.x > SCALED_TOUCH_SLOP && distanceY < SCALED_TOUCH_SLOP_X2) {
            return true
        }

        return false
    }

    fun isLeftToRightSwipe(e1: MotionEvent, e2: MotionEvent): Boolean {
        val distanceY = Math.abs(e2.y - e1.y)
        if (e2.x - e1.x > SCALED_TOUCH_SLOP && distanceY < SCALED_TOUCH_SLOP_X2) {
            return true
        }

        return false
    }

    fun isStartFromRightEdge(e1: MotionEvent, e2: MotionEvent): Boolean {
        if (e1.x <= SCALED_TOUCH_SLOP) {
            return true
        }

        return false
    }
}