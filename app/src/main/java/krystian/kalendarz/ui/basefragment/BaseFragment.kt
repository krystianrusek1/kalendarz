package krystian.kalendarz.ui.basefragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.BaseViewModel
import krystian.kalendarz.util.Loggger

@FragmentScope
abstract class BaseFragment<T : BackPressedViewModel>(private var viewModelClass: Class<T>) : Fragment(), BackPressed {

    companion object {
        private const val KEY_HAS_SAVE_INSTANCE_STATE = "KEY_HAS_SAVE_INSTANCE_STATE"
    }

    protected val LOG = Loggger(this.javaClass)

    protected lateinit var viewModel: T private set

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(KEY_HAS_SAVE_INSTANCE_STATE, true)
        super.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onStop() {
        viewModel.onStop()
        super.onStop()
    }

    protected abstract fun goBack()

    protected fun getApplicationComponent(): ApplicationComponent {
        return (context!!.applicationContext as CalendarApplication).applicationComponent
    }

    private fun inject() {
        initViewModel(injectDagger().viewModelFactory())
    }

    protected abstract fun injectDagger(): BaseFragmentComponent

    protected open fun getKeyViewModel(): String? {
        return null
    }

    private fun initViewModel(viewModelFactory: ViewModelProvider.Factory) {
        val key = getKeyViewModel()
        viewModel = if (key == null) {
            ViewModelProviders.of(this, viewModelFactory).get(viewModelClass)
        } else {
            ViewModelProviders.of(this, viewModelFactory).get(key, viewModelClass)
        }
    }

    override fun onBackPressed(): Boolean {
        return viewModel.onBackPressed()
    }
}
