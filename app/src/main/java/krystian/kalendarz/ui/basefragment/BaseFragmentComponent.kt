package krystian.kalendarz.ui.basefragment

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.ui.main.fragment.MainFragmentModule
import krystian.kalendarz.ui.viewmodel.ViewModelFactoryModule


interface BaseFragmentComponent {

    fun viewModelFactory(): ViewModelProvider.Factory
}