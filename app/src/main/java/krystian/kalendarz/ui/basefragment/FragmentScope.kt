package krystian.kalendarz.ui.basefragment

import javax.inject.Scope

@Scope
annotation class FragmentScope {
}