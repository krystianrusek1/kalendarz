package krystian.kalendarz.ui.basefragment

interface BackPressed {

    fun onBackPressed(): Boolean

}