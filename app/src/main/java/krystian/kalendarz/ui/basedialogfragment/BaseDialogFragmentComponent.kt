package krystian.kalendarz.ui.basedialogfragment

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.ui.main.fragment.MainFragmentModule
import krystian.kalendarz.ui.viewmodel.ViewModelFactoryModule


interface BaseDialogFragmentComponent {

    fun viewModelFactory(): ViewModelProvider.Factory
}