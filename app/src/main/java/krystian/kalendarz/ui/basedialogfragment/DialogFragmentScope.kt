package krystian.kalendarz.ui.basedialogfragment

import javax.inject.Scope

@Scope
annotation class DialogFragmentScope {
}