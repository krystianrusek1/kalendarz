package krystian.kalendarz.ui.basedialogfragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.ui.viewmodel.BaseViewModel

abstract class BaseDialogFragment<T : BaseViewModel>(private var viewModelClass: Class<T>) : DialogFragment() {

    protected lateinit var viewModel: T private set

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)
    }

    private fun inject() {
        initViewModel(injectDagger().viewModelFactory())
    }

    protected abstract fun injectDagger(): BaseDialogFragmentComponent

    protected open fun getKeyViewModel(): String? {
        return null
    }

    private fun initViewModel(viewModelFactory: ViewModelProvider.Factory) {
        val key = getKeyViewModel()
        viewModel = if (key == null) {
            ViewModelProviders.of(this, viewModelFactory).get(viewModelClass)
        } else {
            ViewModelProviders.of(this, viewModelFactory).get(key, viewModelClass)
        }
    }

    protected fun getApplicationComponent(): ApplicationComponent {
        return (context!!.applicationContext as CalendarApplication).applicationComponent
    }
}