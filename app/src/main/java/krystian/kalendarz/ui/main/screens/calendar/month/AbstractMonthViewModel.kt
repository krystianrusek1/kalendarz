package krystian.kalendarz.ui.main.screens.calendar.month

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.LiveTriggerField
import java.util.*

abstract class AbstractMonthViewModel : BackPressedViewModel() {

    companion object {
        internal const val NUMBER_OF_WEEKS = 6
    }

    protected lateinit var dataSourceMoth: DataSourceMoth

    lateinit var calendarItems: LiveData<PagedList<CalendarPeriodModel>>

    val showNextMonth = LiveTriggerField<Boolean>()

    val showPreviousMonth = LiveTriggerField<Boolean>()

    open fun onShowDateChange(showDate: Date) {
        dataSourceMoth = createDataSourceMoth(showDate)
        calendarItems = LivePagedListBuilder(dataSourceMoth, 6).build()
    }

    fun onItemClicked(item: DayCalendarItem) {
        getSelectedDate().value = item.calendar.time
        if (item.isBeforePeriod) {
            showPreviousMonth.value = true
        } else if (item.isAfterPeriod) {
            showNextMonth.value = true
        }
    }

    open fun onMonthListChanged(calendarPeriodModel: CalendarPeriodModel?) {

    }

    abstract fun createDataSourceMoth(showDate: Date): DataSourceMoth

    abstract fun getSelectedDate(): MutableLiveData<Date>
}