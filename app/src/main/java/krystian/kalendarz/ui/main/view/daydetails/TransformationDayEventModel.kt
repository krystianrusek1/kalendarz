package krystian.kalendarz.ui.main.view.daydetails

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import krystian.kalendarz.R
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import java.util.*

class TransformationDayEventModel(
    private val calendarRepository: CalendarRepository,
    private val resources: Resources
) {

    fun getEvents(day: MutableLiveData<Date>): LiveData<List<EventAdapter.EventModel>> {
        return Transformations.switchMap(day) { itDay ->
            prepareDayEventModel(fetchEventList(itDay), itDay)
        }
    }

    fun getEventsByLiveDate(day: Date): LiveData<List<EventAdapter.EventModel>> {
        return prepareDayEventModel(fetchEventList(day), day)
    }

    suspend fun getEvents(day: Date): List<EventAdapter.EventModel> {
        return prepareDayEventModel(fetchEventListByCoroutines(day), day)
    }

    private fun fetchEventList(date: Date): LiveData<List<EventEntity>> {
        return calendarRepository.fetchEventListRecursiveByLiveDate(
            CalendarHelper.prepareStartDate(date),
            CalendarHelper.prepareEndDate(date)
        )
    }

    private suspend fun fetchEventListByCoroutines(date: Date): List<EventEntity> {
        return calendarRepository.fetchEventListRecursiveByCoroutines(
            CalendarHelper.prepareStartDate(date),
            CalendarHelper.prepareEndDate(date)
        )
    }

    private fun prepareDayEventModel(
        liveEventEntity: LiveData<List<EventEntity>>,
        date: Date
    ): LiveData<List<EventAdapter.EventModel>> {
        return Transformations.map(liveEventEntity) { listEvent ->
            prepareDayEventModel(listEvent, date)
        }
    }

    private fun prepareDayEventModel(listEvent: List<EventEntity>, date: Date): List<EventAdapter.EventModel> {
        val dayEvent = ArrayList<EventAdapter.EventModel>()
        listEvent.forEach {
            dayEvent.add(createDayEvent(it, date))
        }
        return dayEvent
    }

    private fun createDayEvent(
        event: EventEntity,
        selectedDate: Date
    ): EventAdapter.EventModel {
        val isStartDate = isEventAtTheStartDate(event, selectedDate)
        val isEndDate = isEventAtTheEndDate(event, selectedDate)

        val eventName = if (event.allDays) {
            "${resources.getString(R.string.screen_day_list_allDay)} ${event.title}"
        } else if (isStartDate && isEndDate) {
            "${DateFormatUnit.formatTimeShort(event.startDate)} - ${DateFormatUnit.formatTimeShort(event.endDate)} ${event.title}"
        } else if (isStartDate) {
            "${DateFormatUnit.formatTimeShort(event.startDate)} - ... ${event.title}"
        } else if (isEndDate) {
            "... - ${DateFormatUnit.formatTimeShort(event.endDate)} ${event.title}"
        } else {
            "${resources.getString(R.string.screen_day_list_allDay)} ${event.title}"
        }

        return EventAdapter.EventModel(event.id ?: -1, eventName, event.color)
    }

    private fun isEventAtTheStartDate(
        eventEntity: EventEntity,
        selectedDate: Date
    ): Boolean {
        return CalendarHelper.prepareStartDate(eventEntity.startDate) == CalendarHelper.prepareStartDate(
            selectedDate
        )
    }

    private fun isEventAtTheEndDate(
        eventEntity: EventEntity,
        selectedDate: Date
    ): Boolean {
        return CalendarHelper.prepareEndDate(eventEntity.endDate) == CalendarHelper.prepareEndDate(
            selectedDate
        )
    }
}