package krystian.kalendarz.ui.main.fragment

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import krystian.kalendarz.ui.main.activity.MainViewModel
import krystian.kalendarz.ui.main.dialogs.picker.month.MonthPickerViewModel
import krystian.kalendarz.ui.main.screens.addevent.AddOrUpdateEventViewModel
import krystian.kalendarz.ui.main.screens.calendar.CalendarViewModel
import krystian.kalendarz.ui.main.screens.calendar.month.MonthViewModel
import krystian.kalendarz.ui.main.screens.calendar.week.WeekViewModel
import krystian.kalendarz.ui.main.screens.mycalendars.MyCalendarsViewModel
import krystian.kalendarz.ui.main.screens.testSms.TestSmsViewModel
import krystian.kalendarz.ui.viewmodel.ViewModelKey

@Module
interface MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun provideTestSmsViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TestSmsViewModel::class)
    fun bindTestSmsViewModel(viewModel: TestSmsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MonthViewModel::class)
    fun bindMonthViewModel(viewModel: MonthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WeekViewModel::class)
    fun bindWeekViewModel(viewModel: WeekViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CalendarViewModel::class)
    fun bindCalendarViewModel(viewModel: CalendarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddOrUpdateEventViewModel::class)
    fun bindAddEventViewModel(viewModelOrUpdate: AddOrUpdateEventViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MonthPickerViewModel::class)
    fun bindMonthCalendarPickerViewModel(viewModel: MonthPickerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyCalendarsViewModel::class)
    fun bindMyCalendarsViewModel(viewModel: MyCalendarsViewModel): ViewModel
}