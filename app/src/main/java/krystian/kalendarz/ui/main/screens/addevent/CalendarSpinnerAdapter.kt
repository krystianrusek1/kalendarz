package krystian.kalendarz.ui.main.screens.addevent

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.INVALID_POSITION
import android.widget.ArrayAdapter
import android.widget.TextView
import krystian.kalendarz.R
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.ui.main.view.CircleView


class CalendarSpinnerAdapter(context: Context) :
    ArrayAdapter<CalendarEntity>(context, 0) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.add_event_spinner_calendar_item, parent, false)
        }
        val entity = getItem(position)
        entity?.let {
            view!!.findViewById<CircleView>(R.id.circleTypeCalendar).color = it.color
            view.findViewById<TextView>(R.id.nameTypeCalendar).text = it.displayName
        }

        return view!!
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.add_event_spinner_calendar_dropdown, parent, false)
        }
        val entity = getItem(position)
        entity?.let {
            view!!.findViewById<CircleView>(R.id.circleTypeCalendar).color = it.color
            view.findViewById<TextView>(R.id.nameTypeCalendar).text = it.displayName
        }

        return view!!
    }

    override fun getPosition(item: CalendarEntity?): Int {
        if (item == null) {
            return INVALID_POSITION
        }

        for (i in 0 until count) {
            val adapterItem = getItem(i)
            if (adapterItem?.id == item.id) {
                return i
            }
        }
        return INVALID_POSITION
    }
}