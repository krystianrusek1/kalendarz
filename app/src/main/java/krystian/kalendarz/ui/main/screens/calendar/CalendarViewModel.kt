package krystian.kalendarz.ui.main.screens.calendar

import androidx.lifecycle.MutableLiveData
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import java.util.*
import javax.inject.Inject

class CalendarViewModel @Inject constructor(
    private val appNavigator: MainNavigator,
    private val calendarNavigator: CalendarNavigator,
    private val calendarRepository: CalendarRepository
) :
    BackPressedViewModel() {

    val hasMonthVisible = MutableLiveData<Boolean>()

    fun onViewCreated() {
        showMonthScreen()
    }

    fun onAddEventClicked() {
        calendarRepository.getSelectedDate().value?.let {
            appNavigator.showAddOrUpdateEventScreen(getVisibleDate())
        }
    }

    fun onActionMonthClicked() {
        showMonthScreen()
    }

    fun onActionWeekClicked() {
        showWeekScreen()
    }

    fun onMyCalendarsClicked() {
        appNavigator.showMyCalendarsScreen()
    }

    private fun showMonthScreen() {
        calendarNavigator.showMonthScreen(getVisibleDate())
        hasMonthVisible.value = true
    }

    private fun showWeekScreen() {
        calendarNavigator.showWeekScreen(getVisibleDate())
        hasMonthVisible.value = false
    }

    private fun getSelectedDate(): Date {
        return calendarRepository.getSelectedDate().value ?: return Date()
    }

    private fun getVisibleDate(): Date {
        return calendarRepository.getVisibleDate().value ?: return Date()
    }
}