package krystian.kalendarz.ui.main.screens.addevent.phonenumber

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import krystian.kalendarz.data.PhoneNumber

class PhoneNumberListView : LinearLayout {

    interface PhonesListListener {
        fun onPhonesListChanged(list: List<PhoneNumber>)
        fun onAddPhoneClicked()
    }

    var phonesListListener: PhonesListListener? = null

    var blockedUpdateEmpyView = false

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {

        addNewPhoneNumberView(null)
    }

    fun setPhonesList(list: List<PhoneNumber>) {
        blockedUpdateEmpyView = true
        removeAllViews()
        list.forEach { phone ->
            addNewPhoneNumberView(phone)
        }
        addNewPhoneNumberView(null)
        blockedUpdateEmpyView = false
    }

    fun addPhones(phone: PhoneNumber) {
        val view = getChildAt(childCount - 1)
        if (view != null && view is PhoneNumberRowView) {
            view.setPhoneNumber(phone)
        } else {
            addNewPhoneNumberView(phone)
        }
        updateEmptyViews()
    }

    private fun addNewPhoneNumberView(phoneNumber: PhoneNumber?) {
        val view = PhoneNumberRowView(
            context
        )

        view.layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        addView(view)

        view.phoneNumberChangeListener = object : PhoneNumberRowView.PhoneNumberChangeListener {
            override fun onChanged(phoneNumber: PhoneNumber) {
                updateEmptyViews()
                phonesListListener?.onPhonesListChanged(getListPhones())
            }
        }

        view.phoneNumberRowViewListener = object : PhoneNumberRowView.PhoneNumberRowViewListener {
            override fun onAddPhoneClicked() {
                phonesListListener?.onAddPhoneClicked()
            }

            override fun onDeleteClicked() {
                val position = indexOfChild(view)
                removeViewAt(position)
                updateVisibilityIcons(position - 1)
                updateVisibilityIcons(position)
                updateVisibilityIcons(position + 1)
                phonesListListener?.onPhonesListChanged(getListPhones())
            }
        }

        phoneNumber?.let { view.setPhoneNumber(it) }

        updateVisibilityIcons(childCount - 1)
        updateVisibilityIcons(childCount - 2)
        updateVisibilityIcons(childCount - 3)
    }

    private fun removeLastView() {
        removeViewAt(childCount - 1)
        updateVisibilityIcons(childCount - 1)
        updateVisibilityIcons(childCount - 2)
    }

    private fun updateEmptyViews() {
        if (!blockedUpdateEmpyView) {
            var lastView = getChildAt(childCount - 1)
            if (lastView is PhoneNumberRowView) {
                if (lastView.getPhoneNumber().isNotEmpty()) {
                    addNewPhoneNumberView(null)
                }
            }

            lastView = getChildAt(childCount - 2)
            if (lastView is PhoneNumberRowView) {
                if (lastView.getPhoneNumber().isEmpty()) {
                    removeLastView()
                }
            }
        }
    }

    private fun updateVisibilityIcons(position: Int) {
        val lastView = getChildAt(position)
        if (lastView is PhoneNumberRowView) {
            if (childCount - 1 == position) {
                lastView.setAddContactVisible(View.VISIBLE)
                lastView.setDeleteVisible(View.GONE)
            } else {
                lastView.setAddContactVisible(View.GONE)
                lastView.setDeleteVisible(View.VISIBLE)
            }
        }
    }

    private fun getListPhones(): List<PhoneNumber> {
        val phones = ArrayList<PhoneNumber>()
        for (i in 0 until childCount) {
            val phoneRow = getChildAt(i)
            if (phoneRow is PhoneNumberRowView) {
                val number = phoneRow.getPhoneNumber()
                if (number.isNotEmpty()) {
                    phones.add(phoneRow.getPhoneNumber())
                }
            }
        }
        return phones
    }
}