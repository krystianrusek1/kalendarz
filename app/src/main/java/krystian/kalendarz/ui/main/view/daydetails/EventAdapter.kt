package krystian.kalendarz.ui.main.view.daydetails

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.ui.main.view.CircleView

class EventAdapter :
    ListAdapter<EventAdapter.EventModel, EventAdapter.ViewHolder>(DIFF_CALLBACK) {

    interface EventAdapterListener {
        fun onEventClicked(eventModel: EventModel)
    }

    var eventAdapterListener: EventAdapterListener? = null

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<EventModel>() {
            override fun areItemsTheSame(oldItem: EventModel, newItem: EventModel) =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: EventModel, newItem: EventModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    data class EventModel(val eventId: Long, val eventName: String, @ColorInt val color: Int)

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        lateinit var eventName: TextView
        lateinit var circleColor: CircleView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.event_adapter, parent, false)
        val holder = ViewHolder(view)
        holder.eventName = view.findViewById(R.id.eventName)
        holder.circleColor = view.findViewById(R.id.circleColor)
        return holder
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = getItem(position)
        holder.eventName.text = event.eventName
        holder.circleColor.color = event.color

        holder.view.setOnClickListener {
            eventAdapterListener?.onEventClicked(event)
        }
    }
}