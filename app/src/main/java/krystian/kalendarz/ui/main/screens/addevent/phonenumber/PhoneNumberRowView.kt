package krystian.kalendarz.ui.main.screens.addevent.phonenumber

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import krystian.kalendarz.R
import krystian.kalendarz.data.PhoneNumber
import krystian.kalendarz.util.AppPhoneNumberUtil

class PhoneNumberRowView : LinearLayout {

    private val phoneNumberView: EditText
    private val addContactView: View
    private val deleteView: View

    interface PhoneNumberChangeListener {
        fun onChanged(phoneNumber: PhoneNumber)
    }

    var phoneNumberChangeListener: PhoneNumberChangeListener? = null

    interface PhoneNumberRowViewListener {
        fun onDeleteClicked()
        fun onAddPhoneClicked()
    }

    var phoneNumberRowViewListener: PhoneNumberRowViewListener? = null

    private val textWatcher = object : TextWatcher {

        override fun afterTextChanged(s: Editable?) {
            phoneNumberChangeListener?.onChanged(PhoneNumber(s.toString()))
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {

        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.add_event_phone_number_entity, this, true)

        phoneNumberView = findViewById(R.id.phoneNumber)
        addContactView = findViewById(R.id.addContact)
        deleteView = findViewById(R.id.delete)

        deleteView.setOnClickListener { phoneNumberRowViewListener?.onDeleteClicked() }
        addContactView.setOnClickListener { phoneNumberRowViewListener?.onAddPhoneClicked() }
    }

    fun setPhoneNumber(phoneNumber: PhoneNumber) {
        phoneNumberView.setText(phoneNumber.phoneNumber)
    }

    fun getPhoneNumber(): PhoneNumber {
        return PhoneNumber(phoneNumberView.text?.toString() ?: "")
    }

    fun setAddContactVisible(visibility: Int) {
        addContactView.visibility = visibility
    }

    fun setDeleteVisible(visibility: Int) {
        deleteView.visibility = visibility
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        phoneNumberView.addTextChangedListener(textWatcher)
    }

    override fun onDetachedFromWindow() {
        phoneNumberView.removeTextChangedListener(textWatcher)
        super.onDetachedFromWindow()
    }
}