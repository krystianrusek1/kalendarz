package krystian.kalendarz.ui.main.screens.calendar.month

import android.os.Bundle
import android.view.*
import android.widget.RelativeLayout
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.FlingAnimation
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import krystian.kalendarz.R
import krystian.kalendarz.ui.basefragment.BaseFragmentComponent
import krystian.kalendarz.ui.main.activity.MainActivity
import krystian.kalendarz.ui.main.activity.MainActivityComponent
import krystian.kalendarz.ui.main.fragment.DaggerMainFragmentComponent
import krystian.kalendarz.ui.main.view.daydetails.DayEventsView
import krystian.kalendarz.ui.main.view.daydetails.EventAdapter
import krystian.kalendarz.util.ScreenHelper.HEIGHT_PIXELS_33_PERCENT_SCREEN
import krystian.kalendarz.util.ScreenHelper.HEIGHT_PIXELS_66_PERCENT_SCREEN
import java.util.*


class MonthFragment :
    AbstractMonthFragment<MonthViewModel>(MonthViewModel::class.java) {

    companion object {

        /**
         * Screen shows the moth calendar based on the [showDate]
         */
        fun newInstance(showDate: Date): MonthFragment {
            val fragment = MonthFragment()
            val bundle = Bundle()
            fragment.arguments = obtainParameters(bundle, showDate)
            return fragment
        }
    }

    enum class DayEventsState {
        DOWN,
        GOING_TO_UP,
        GOING_TO_DOWN,
        UP
    }

    private var firstPositionOfDayEvents = 0f

    private var dayEventsState: DayEventsState = DayEventsState.DOWN

    @BindView(R.id.dayEvents)
    @JvmField
    var dayDayEvents: DayEventsView? = null

    @BindView(R.id.grayLayout)
    @JvmField
    var grayLayout: View? = null

    @BindView(R.id.layout)
    @JvmField
    var layout: View? = null

    private val dayEventsOnItemTouchListener = object : RecyclerView.OnItemTouchListener {
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            if (dayEventsState != DayEventsState.UP) {
                onTouchEvent(e)
                gestureDetector.onTouchEvent(e)
            }
            return dayEventsState != DayEventsState.UP
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
            if (dayEventsState != DayEventsState.UP) {
                onTouchEvent(e)
            }
        }
    }

    private val monthListOnItemTouchListener = object : RecyclerView.OnItemTouchListener {
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            if (dayEventsState != DayEventsState.DOWN) {
                onTouchEvent(e)
                gestureDetector.onTouchEvent(e)
            }
            return dayEventsState != DayEventsState.DOWN
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
            if (dayEventsState != DayEventsState.DOWN) {
                onTouchEvent(e)
                gestureDetector.onTouchEvent(e)
            }
        }
    }

    private lateinit var dayEventsFlingAnimation: FlingAnimation

    private lateinit var gestureDetector: GestureDetector

    private val onAnimationUpdateListener =
        DynamicAnimation.OnAnimationUpdateListener { animation, value, velocity ->
            if (getLocationPositionYonScreen(dayDayEvents!!) < HEIGHT_PIXELS_33_PERCENT_SCREEN && velocity <= 0) {
                dayEventsFlingAnimation.cancel()
                dayEventsState = DayEventsState.UP
            }

            if (getLocationPositionYonScreen(dayDayEvents!!) > firstPositionOfDayEvents && velocity >= 0) {
                dayEventsFlingAnimation.cancel()
                dayEventsState = DayEventsState.DOWN
            }

            onDayEventsPositionChange()
        }

    private val gestureListener = object : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent?): Boolean {
            dayEventsFlingAnimation.cancel()
            return true
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean {

            val newY = dayDayEvents!!.y - distanceY

            var isUpdated = true
            if (getLocationPositionYonScreen(dayDayEvents!!) < HEIGHT_PIXELS_33_PERCENT_SCREEN && distanceY >= 0) {
                isUpdated = false
            } else if (getLocationPositionYonScreen(dayDayEvents!!) > firstPositionOfDayEvents && distanceY <= 0) {
                isUpdated = false
            }

            if (isUpdated) {
                dayDayEvents!!.y = newY
            }

            onDayEventsPositionChange()

            return true
        }
    }

    override fun isCircleVisible(): Boolean {
        return true
    }

    override fun injectDagger(): BaseFragmentComponent {
        return DaggerMainFragmentComponent
            .builder()
            .mainActivityComponent(getMainActivityComponent())
            .build()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.month_calendar_fragment, container, false)
    }

    override fun onCreateMonthList(view: View): RecyclerView {
        return view.findViewById(R.id.monthList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        grayLayout!!.alpha = 0f

        dayEventsFlingAnimation = FlingAnimation(dayDayEvents, DynamicAnimation.Y).apply {
            friction = 0.001f
        }

        dayEventsFlingAnimation.addUpdateListener(onAnimationUpdateListener)

        dayDayEvents!!.layoutParams.apply {
            width = RelativeLayout.LayoutParams.MATCH_PARENT
            height = HEIGHT_PIXELS_66_PERCENT_SCREEN
        }

        gestureDetector = GestureDetector(context, gestureListener)

        layout!!.setOnTouchListener { _, motionEvent ->
            onTouchEvent(motionEvent)
            gestureDetector.onTouchEvent(motionEvent)
        }

        viewModel.getDayEvents().observe(this, Observer { dayModel ->
            if (dayModel != null) {
                dayDayEvents!!.setEvents(dayModel.listEvent)
                dayDayEvents!!.updateDayName(dayModel.day)
            }
        })

        viewModel.getVisibleDate().observe(this, Observer { date ->
            monthAdapter.visibleDate = date
        })

        dayDayEvents!!.setEventAdapterListener(object : EventAdapter.EventAdapterListener {
            override fun onEventClicked(eventModel: EventAdapter.EventModel) {
                viewModel.onEventModelClicked(eventModel)
            }
        })
    }

    override fun onDestroy() {
        dayEventsFlingAnimation.removeUpdateListener(onAnimationUpdateListener)
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        monthList.addOnItemTouchListener(monthListOnItemTouchListener)
        dayDayEvents!!.addOnItemTouchListener(dayEventsOnItemTouchListener)
    }

    override fun onPause() {
        monthList.removeOnItemTouchListener(monthListOnItemTouchListener)
        dayDayEvents!!.removeOnItemTouchListener(dayEventsOnItemTouchListener)
        super.onPause()
    }

    protected fun getMainActivityComponent(): MainActivityComponent {
        return (requireActivity() as MainActivity).mainActivityComponent
    }

    override fun goBack() {
        (requireActivity() as MainActivity).goBack()
    }

    private fun onTouchEvent(motionEvent: MotionEvent) {
        if (motionEvent.action == MotionEvent.ACTION_UP) {
            if (dayEventsState == DayEventsState.GOING_TO_UP) {
                dayEventsFlingAnimation.setStartVelocity(-1200f)
                dayEventsFlingAnimation.start()
            } else if (dayEventsState == DayEventsState.GOING_TO_DOWN) {
                dayEventsFlingAnimation.setStartVelocity(1200f)
                dayEventsFlingAnimation.start()
            }
        } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
            if (firstPositionOfDayEvents == 0f) {
                firstPositionOfDayEvents = getLocationPositionYonScreen(dayDayEvents!!).toFloat()
            }

            if (dayEventsState == DayEventsState.DOWN) {
                dayEventsState = DayEventsState.GOING_TO_UP
            } else if (dayEventsState == DayEventsState.UP) {
                dayEventsState = DayEventsState.GOING_TO_DOWN
            } else if (dayEventsState == DayEventsState.GOING_TO_UP) {
                dayEventsState = DayEventsState.GOING_TO_DOWN
            } else if (dayEventsState == DayEventsState.GOING_TO_DOWN) {
                dayEventsState = DayEventsState.GOING_TO_UP
            }
        }
    }

    private fun onDayEventsPositionChange() {
        val percentOfDown =
            (getLocationPositionYonScreen(dayDayEvents!!) - HEIGHT_PIXELS_33_PERCENT_SCREEN) /
                    (firstPositionOfDayEvents - HEIGHT_PIXELS_33_PERCENT_SCREEN)

        val percentOfUp = 1 - percentOfDown

        grayLayout!!.alpha = (percentOfUp - 0.1f)
    }

    private fun getLocationPositionYonScreen(view: View): Int {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        return location[1]
    }
}

