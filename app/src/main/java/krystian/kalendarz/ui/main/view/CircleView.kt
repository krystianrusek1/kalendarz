package krystian.kalendarz.ui.main.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.util.AttributeSet
import android.widget.ImageView
import androidx.annotation.ColorInt
import krystian.kalendarz.R


class CircleView : ImageView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {
        if (isInEditMode) {
            setCircle(Color.BLUE)
        }
    }

    @ColorInt
    var color: Int = Color.BLACK
        set(value) {
            setCircle(value)
            field = value
        }

    private fun setCircle(@ColorInt color: Int) {
        val drawable = resources.getDrawable(R.drawable.full_circle)
        when (drawable) {
            is ShapeDrawable -> drawable.paint.color = color
            is GradientDrawable -> drawable.setColor(color)
            is ColorDrawable -> drawable.color = color
        }
        setImageDrawable(drawable)
    }
}
