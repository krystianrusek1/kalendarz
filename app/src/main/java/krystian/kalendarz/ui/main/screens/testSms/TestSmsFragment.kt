package krystian.kalendarz.ui.main.screens.testSms

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.OnClick
import krystian.kalendarz.R
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.ui.main.fragment.AbstractMainFragment
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import java.util.*


class TestSmsFragment : AbstractMainFragment<TestSmsViewModel>(TestSmsViewModel::class.java) {

    companion object {
        fun newInstance(): TestSmsFragment {
            return TestSmsFragment()
        }
    }


    @BindView(R.id.smsList)
    @JvmField
    var recyclerView: RecyclerView? = null

    @BindView(R.id.description)
    @JvmField
    var description: TextView? = null

    @BindView(R.id.dateSend)
    @JvmField
    var dateSend: TextView? = null

    @BindView(R.id.timeSend)
    @JvmField
    var timeSend: TextView? = null

    private lateinit var viewAdapter: SmsAdapter

    private val prepareCalendar = CalendarHelper.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.test_sms_fragment, container, false)

        rootView.findViewById<CheckBox>(R.id.isSentCheckBox).setOnCheckedChangeListener { _, isChecked ->
            viewModel.isChecked = isChecked
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()

        observeViewModel()
        updateDate(prepareCalendar)
    }

    private fun initRecyclerView() {
        val viewManager = LinearLayoutManager(context)

        viewAdapter = SmsAdapter()

        recyclerView?.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

    }

    @OnClick(R.id.fab)
    internal fun addButtonClickedSms() {
        viewModel.addSms(
            SmsEntity.create(
                "+48669010008",
                description?.text.toString() + " termin wyslania: " + DateFormatUnit.formatDate(prepareCalendar.time),
                prepareCalendar.time
            )
        )
    }

    @SuppressLint("SimpleDateFormat")
    private fun updateDate(calendar: Calendar) {

        dateSend?.text = DateFormatUnit.formatDay(calendar.time)
        timeSend?.text = DateFormatUnit.formatTime(calendar.time)
    }

    @OnClick(R.id.dateSend)
    internal fun showDatePicker() {
        val listener =
            DatePickerDialog.OnDateSetListener { datePicker: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
                prepareCalendar.set(Calendar.YEAR, year)
                prepareCalendar.set(Calendar.MONTH, month)
                prepareCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDate(prepareCalendar)
            }
        val datePickerDialog = context?.let {
            DatePickerDialog(
                it,
                listener,
                prepareCalendar.get(Calendar.YEAR),
                prepareCalendar.get(Calendar.MONTH),
                prepareCalendar.get(Calendar.DAY_OF_MONTH)
            )
        }
        datePickerDialog?.setCancelable(false)
        datePickerDialog?.show()
    }

    @OnClick(R.id.timeSend)
    internal fun showTimePicker() {
        val listener = TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
            prepareCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
            prepareCalendar.set(Calendar.MINUTE, minute)
            updateDate(prepareCalendar)
        }
        val timePickerDialog = TimePickerDialog(
            context,
            listener,
            prepareCalendar.get(Calendar.HOUR_OF_DAY),
            prepareCalendar.get(Calendar.MINUTE),
            true
        )
        timePickerDialog.setCancelable(false)
        timePickerDialog.show()
    }

    private fun observeViewModel() {
        viewModel.getSmsList()
            .observe(this, Observer { smsList ->
                if (smsList != null) {
                    viewAdapter.setData(smsList)
                }
            })
    }
}
