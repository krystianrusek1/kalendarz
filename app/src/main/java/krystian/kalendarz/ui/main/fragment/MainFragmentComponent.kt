package krystian.kalendarz.ui.main.fragment

import dagger.Component
import krystian.kalendarz.ui.basefragment.BaseFragmentComponent
import krystian.kalendarz.ui.basefragment.FragmentScope
import krystian.kalendarz.ui.main.activity.MainActivityComponent
import krystian.kalendarz.ui.main.screens.calendar.CalendarFragment
import krystian.kalendarz.ui.main.screens.calendar.CalendarNavigator

@FragmentScope
@Component(
    dependencies = [MainActivityComponent::class],
    modules = [MainFragmentModule::class]
)
interface MainFragmentComponent : BaseFragmentComponent {

    fun calendarNavigator(): CalendarNavigator

    fun inject(calendarFragment: CalendarFragment)

    @Component.Builder
    interface Builder {
        fun build(): MainFragmentComponent

        fun mainFragmentModule(mainFragmentModule: MainFragmentModule): Builder

        fun mainActivityComponent(mainActivityComponent: MainActivityComponent): Builder
    }
}