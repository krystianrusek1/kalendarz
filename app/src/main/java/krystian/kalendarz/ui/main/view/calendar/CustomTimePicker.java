package krystian.kalendarz.ui.main.view.calendar;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TimePicker;

import java.lang.reflect.Field;

public class CustomTimePicker extends TimePicker {
    public CustomTimePicker(Context context) {
        super(context);
    }

    public CustomTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomTimePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setData(int minutes, int hour) {
        fixValues();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setHour(hour);
            setMinute(minutes);
        } else {
            setCurrentHour(hour);
            setCurrentMinute(minutes);
        }
    }

    private void fixValues() {
        // bug is not reproducible in APIs 24 and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) return;
        try {
            int hour, minute;
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                hour = this.getHour();
                minute = this.getMinute();
            } else {
                hour = this.getCurrentHour();
                minute = this.getCurrentMinute();
            }

            Field mDelegateField = this.getClass().getDeclaredField("mDelegate");
            mDelegateField.setAccessible(true);
            Class<?> clazz;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                clazz = Class.forName("android.widget.TimePickerClockDelegate");
            } else {
                clazz = Class.forName("android.widget.TimePickerSpinnerDelegate");
            }
            Field mInitialHourOfDayField = clazz.getDeclaredField("mInitialHourOfDay");
            Field mInitialMinuteField = clazz.getDeclaredField("mInitialMinute");
            mInitialHourOfDayField.setAccessible(true);
            mInitialMinuteField.setAccessible(true);
            mInitialHourOfDayField.setInt(mDelegateField.get(this), hour);
            mInitialMinuteField.setInt(mDelegateField.get(this), minute);
        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
