package krystian.kalendarz.ui.main.activity

import krystian.kalendarz.permissions.PermissionManager
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val mainNavigator: MainNavigator,
    private val permissionManager: PermissionManager
) :
    BackPressedViewModel() {

    fun onViewCreated() {
    }

    fun onCalendarPermissionGranted() {
        permissionManager.triggerPermissionChanged()
    }
}