package krystian.kalendarz.ui.main.view.calendar

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import krystian.kalendarz.R
import krystian.kalendarz.util.Loggger


class NumberDayView : FrameLayout {

    private val LOG = Loggger(this.javaClass)

    enum class Circle {
        FULL, STORKE, NONE
    }

    data class ColorDots(
        @ColorInt var color1: Int? = null,
        @ColorInt var color2: Int? = null,
        @ColorInt var color3: Int? = null
    )

    private val numberOfDay: TextView
    private val dotsSection: LinearLayout
    private val dots1: View
    private val dots2: View
    private val dots3: View

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {

        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.day_number_view, this, true)

        numberOfDay = findViewById(R.id.numberOfDay)
        dotsSection = findViewById(R.id.dotsSection)
        dots1 = findViewById(R.id.dots1)
        dots1.visibility = View.GONE
        dots2 = findViewById(R.id.dots2)
        dots2.visibility = View.GONE
        dots3 = findViewById(R.id.dots3)
        dots3.visibility = View.GONE
    }

    fun setCircle(circle: Circle) {

        when (circle) {
            Circle.FULL -> numberOfDay.setBackgroundResource(R.drawable.circle_color_accent_full)
            Circle.STORKE -> numberOfDay.setBackgroundResource(R.drawable.circle_color_accent_storke)
            else -> numberOfDay.background = null
        }
    }

    fun setGrayNumber(enabled: Boolean) {
        numberOfDay.isEnabled = !enabled
    }

    fun setText(text: String) {
        numberOfDay.text = text
    }

    fun setTypeface(tf: Typeface) {
        numberOfDay.typeface = tf
    }

    fun getTypeface(): Typeface {
        return numberOfDay.typeface
    }

    fun setDots(colorList: ColorDots) {
        setDot(dots1, colorList.color1)
        setDot(dots2, colorList.color2)
        setDot(dots3, colorList.color3)
    }

    fun setDots1(@ColorInt color: Int?) {
        setDot(dots1, color)
    }

    fun setDots2(@ColorInt color: Int?) {
        setDot(dots2, color)
    }

    fun setDots3(@ColorInt color: Int?) {
        setDot(dots3, color)
    }

    private fun setDot(viewDot: View, @ColorInt color: Int?) {
        if (color != null) {
            viewDot.background = createOval(color)
            viewDot.visibility = View.VISIBLE
        } else {
            viewDot.visibility = View.GONE
        }
    }

    private fun createOval(@ColorInt color: Int): ShapeDrawable {
        val drawable = ShapeDrawable(OvalShape())
        drawable.paint.color = color
        return drawable
    }
}