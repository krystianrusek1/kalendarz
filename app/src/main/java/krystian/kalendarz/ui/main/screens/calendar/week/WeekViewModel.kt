package krystian.kalendarz.ui.main.screens.calendar.week

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.*
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.ui.main.view.daydetails.EventAdapter
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.LiveTriggerField
import krystian.kalendarz.util.CalendarHelper
import java.util.*
import javax.inject.Inject

class WeekViewModel @Inject constructor(
    private val appNavigator: MainNavigator,
    private val calendarRepository: CalendarRepository,
    private val resources: Resources
) : BackPressedViewModel() {

    companion object {
        internal const val NUMBER_OF_WEEKS = 1
    }

    lateinit var calendarItems: LiveData<PagedList<CalendarPeriodModel>>

    lateinit var dayItems: LiveData<PagedList<DayAdapter.DayModel>>

    val showNextWeek = LiveTriggerField<Boolean>()

    val showPreviousWeek = LiveTriggerField<Boolean>()

    val refreshDayItem = LiveTriggerField<Boolean>()

    val dayListScrollTo = LiveTriggerField<Date>()

    private var lastPeriodModelViable: CalendarPeriodModel? = null

    private var lastDayModelViable: DayAdapter.DayModel? = null

    var dayListScrollToJob: Job? = null

    var dayListIsInitialing = true

    fun onShowDateChange(showDate: Date) {
        val dataSourceWeek = DataSourceWeek(NUMBER_OF_WEEKS, showDate)
        calendarItems = LivePagedListBuilder(dataSourceWeek, 6).build()

        dayItems = createLivePagedListBuilder(showDate)
    }

    fun onWeekListChanged(calendarPeriodModel: CalendarPeriodModel, isScrollStopped: Boolean) {
        this.lastPeriodModelViable = calendarPeriodModel

        if (isScrollStopped) {
            this.lastPeriodModelViable?.let { periodModel ->
                lastDayModelViable?.let { dayModel ->
                    if (CalendarHelper.isBeforeDay(dayModel.day, periodModel.dayList[0].calendar.time)) {
                        val visibleDate: Date? = getVisibleDate().value

                        val setDate = if (visibleDate == null) {
                            periodModel.dayList[0].calendar.time
                        } else {
                            CalendarHelper.getDateBetweenForDayOfweek(
                                periodModel.dayList[0].calendar.time,
                                periodModel.dayList[periodModel.dayList.size - 1].calendar.time,
                                visibleDate
                            )
                        }
                        getVisibleDate().value = setDate
                        rebuildDayList(setDate)
                    } else if (CalendarHelper.isAfterDay(
                            dayModel.day,
                            periodModel.dayList[periodModel.dayList.size - 1].calendar.time
                        )
                    ) {
                        val visibleDate: Date? = getVisibleDate().value

                        val setDate = if (visibleDate == null) {
                            periodModel.dayList[0].calendar.time
                        } else {
                            CalendarHelper.getDateBetweenForDayOfweek(
                                periodModel.dayList[0].calendar.time,
                                periodModel.dayList[6].calendar.time,
                                visibleDate
                            )
                        }
                        getVisibleDate().value = setDate
                        rebuildDayList(setDate)
                    }
                }
            }
        }
    }

    fun onDayListChanged(dayModel: DayAdapter.DayModel?, isScrolledByUser: Boolean) {
        this.lastDayModelViable = dayModel

        if (isScrolledByUser) {
            calendarRepository.getVisibleDate().value = dayModel?.day
            lastPeriodModelViable?.let { periodModel ->
                dayModel?.let {
                    if (CalendarHelper.isBeforeDay(dayModel.day, periodModel.dayList[0].calendar.time)) {
                        showPreviousWeek.value = true
                    } else if (CalendarHelper.isAfterDay(dayModel.day, periodModel.dayList[6].calendar.time)) {
                        showNextWeek.value = true
                    }
                }
            }
        }
    }

    fun onDayItemClickedOnWeekList(item: DayCalendarItem) {
        rebuildDayList(item.calendar.time)
        calendarRepository.getSelectedDate().value = item.calendar.time
    }

    fun getSelectedDate(): MutableLiveData<Date> {
        return calendarRepository.getSelectedDate()
    }

    fun getVisibleDate(): MutableLiveData<Date> {
        return calendarRepository.getVisibleDate()
    }

    fun onItemDayInserted() {
        dayListScrollToJob?.cancel()
        if (dayListIsInitialing) {
            dayListScrollToJob = GlobalScope.launch {
                delay(100)
                withContext(Dispatchers.Main) {
                    dayListIsInitialing = false
                    dayListScrollTo.value = getVisibleDate().value
                }
            }
        }
    }

    private fun rebuildDayList(startDate: Date) {
        dayItems = createLivePagedListBuilder(startDate)
        dayListIsInitialing = true
        refreshDayItem.value = true
    }

    private fun createLivePagedListBuilder(firstDate: Date): LiveData<PagedList<DayAdapter.DayModel>> {
        val dataSourceDay = DataSourceDay(firstDate, calendarRepository, resources)
        return LivePagedListBuilder(
            dataSourceDay, PagedList
                .Config.Builder()
                .setPageSize(20)
                .setInitialLoadSizeHint(20)
                .build()
        ).build()
    }

    fun onEventModelClicked(eventModel: EventAdapter.EventModel) {
        GlobalScope.launch {
            val event = calendarRepository.getEvent(eventModel.eventId)
            event?.let { appNavigator.showAddOrUpdateEventScreen(it) }
        }
    }
}