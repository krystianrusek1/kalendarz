package krystian.kalendarz.ui.main.dialogs.picker.month

import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.ui.basedialogfragment.BaseDialogFragmentComponent
import krystian.kalendarz.ui.basefragment.BaseFragmentComponent
import krystian.kalendarz.ui.basefragment.FragmentScope
import krystian.kalendarz.ui.main.dialogs.picker.PickerDialogFragmentComponent
import krystian.kalendarz.ui.main.dialogs.picker.PickerDialogFragmentModule
import krystian.kalendarz.ui.main.dialogs.picker.PickerDialogViewModelModule
import krystian.kalendarz.ui.viewmodel.ViewModelFactoryModule

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewModelFactoryModule::class, PickerDialogViewModelModule::class]
)
interface MonthPickerFragmentComponent :BaseFragmentComponent{

    fun inject(monthPickerFragment: MonthPickerFragment)

    @Component.Builder
    interface Builder {
        fun build(): MonthPickerFragmentComponent

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}