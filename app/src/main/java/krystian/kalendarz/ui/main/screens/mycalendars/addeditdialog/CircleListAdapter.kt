package krystian.kalendarz.ui.main.screens.mycalendars.addeditdialog

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.ui.main.view.TickCircleCheckBoxView


class CircleListAdapter(context: Context) : RecyclerView.Adapter<CircleListAdapter.ViewHolder>() {

    var circleListAdapterListener: CircleListAdapterListener? = null

    private val colorList = ArrayList<Int>()

    private var colorSelected: Int? = null

    init {
        context.resources.getStringArray(R.array.calendarColors).forEach {
            colorList.add(Color.parseColor(it))
        }
        colorSelected = colorList[0]
    }

    override fun getItemCount(): Int {
        return colorList.size
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val checkBox: TickCircleCheckBoxView = view.findViewById(R.id.checkbox)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.add_edit_dialog_circle_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val color = colorList[position]
        holder.checkBox.setColor(color)
        colorSelected?.let { selected ->
            holder.checkBox.isChecked = color == selected
        }

        holder.checkBox.setOnClickListener {
            setColor(color)
            circleListAdapterListener?.onColorChanged(color)
        }
    }

    fun setColor(@ColorInt color: Int) {
        colorSelected = color
        notifyDataSetChanged()
    }

    interface CircleListAdapterListener {
        fun onColorChanged(@ColorInt color: Int)
    }
}