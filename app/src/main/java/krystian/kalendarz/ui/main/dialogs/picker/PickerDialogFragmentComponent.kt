package krystian.kalendarz.ui.main.dialogs.picker

import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.ui.basedialogfragment.BaseDialogFragmentComponent
import krystian.kalendarz.ui.basedialogfragment.DialogFragmentScope
import krystian.kalendarz.ui.viewmodel.ViewModelFactoryModule

@DialogFragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [PickerDialogFragmentModule::class, ViewModelFactoryModule::class, PickerDialogViewModelModule::class]
)
interface PickerDialogFragmentComponent : BaseDialogFragmentComponent {

    fun inject(cateTimePickerFragment: DateTimePickerFragment)

    @Component.Builder
    interface Builder {
        fun build(): PickerDialogFragmentComponent

        fun pickerDialogFragmentModule(pickerDialogFragmentModule: PickerDialogFragmentModule): Builder

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}