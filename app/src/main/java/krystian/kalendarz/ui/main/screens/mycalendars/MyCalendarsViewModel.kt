package krystian.kalendarz.ui.main.screens.mycalendars

import android.content.res.Resources
import android.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import krystian.kalendarz.R
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.LiveTriggerField
import javax.inject.Inject

class MyCalendarsViewModel @Inject constructor(
    private val appNavigator: MainNavigator,
    private val calendarRepository: CalendarRepository,
    private val resources: Resources
) : BackPressedViewModel() {

    val showAddEditDialog = LiveTriggerField<CalendarEntity>()

    fun goBack() {
        appNavigator.goBack()
    }

    fun getCalendarList(): LiveData<List<MyCalendarModel>> {
        return Transformations.map(calendarRepository.getCalendarList()) { list ->
            transformCalendarList(list)
        }
    }

    fun onItemCheckboxChecked(isChecked: Boolean, entryModel: EntryModel) {
        GlobalScope.launch(Dispatchers.IO) {
            val entity = calendarRepository.getAllCalendar(entryModel.calendarId)
            if (entity != null) {
                calendarRepository.insertOrUpdateCalendar(entity.copyBuilder().withVisible(isChecked).build())
            }
        }
    }

    fun onItemClicked(entryModel: EntryModel) {
        GlobalScope.launch(Dispatchers.IO) {
            val calendarEntity = calendarRepository.getAllCalendar(entryModel.calendarId)
            if (calendarEntity != null) {
                if (calendarEntity.accountType == CalendarEntity.AccountType.LOCAL) {
                    GlobalScope.launch(Dispatchers.Main) {
                        showAddEditDialog.value = calendarEntity
                    }
                }
            }
        }
    }

    fun onAddItemClicked(addEntryModel: AddEntryModel) {
        val colorString = resources.getStringArray(R.array.calendarColors)[0]
        showAddEditDialog.value = CalendarEntity.Builder()
            .withAccountName(addEntryModel.calendarAccountNme)
            .withColor(Color.parseColor(colorString)).build()
    }

    private fun transformCalendarList(calendarList: List<CalendarEntity>): List<MyCalendarModel> {
        val myCalendarList = ArrayList<MyCalendarModel>()

        sortCalendarListByTypeAndAccount(calendarList).forEach {
            myCalendarList.addAll(createMyCalendarList(it))
        }

        return myCalendarList
    }

    private fun sortCalendarListByTypeAndAccount(calendarList: List<CalendarEntity>): ArrayList<ArrayList<CalendarEntity>> {
        val localList = ArrayList<CalendarEntity>()
        val sortedByAccount = HashMap<String, ArrayList<CalendarEntity>>()
        calendarList.forEach { entry ->
            if (entry.accountType == CalendarEntity.AccountType.LOCAL) {
                localList.add(entry)
            } else {
                var entryList = sortedByAccount[entry.accountName]
                if (entryList == null) {
                    entryList = ArrayList()
                    sortedByAccount[entry.accountName] = entryList
                }
                entryList.add(entry)
            }
        }
        val resultList = ArrayList<ArrayList<CalendarEntity>>()
        resultList.addAll(sortedByAccount.values)
        resultList.add(localList)

        return resultList
    }

    private fun createMyCalendarList(list: ArrayList<CalendarEntity>): List<MyCalendarModel> {
        if (list.size == 0) {
            return emptyList()
        }
        val myCalendarList = ArrayList<MyCalendarModel>()
        myCalendarList.add(HeaderModel(getAccountName(list[0])))
        list.forEach {
            myCalendarList.add(EntryModel(it.id ?: -1, it.displayName ?: "", it.color, it.visible))
        }
        if (list[0].accountType == CalendarEntity.AccountType.LOCAL) {
            myCalendarList.add(AddEntryModel(list[0].accountName))
        }
        return myCalendarList
    }

    private fun getAccountName(calendarEntity: CalendarEntity): String {
        return if (calendarEntity.accountType == CalendarEntity.AccountType.LOCAL) {
            resources.getString(R.string.local_calendar)
        } else {
            calendarEntity.accountName
        }
    }

    fun onCalendarChange(calendarEntity: CalendarEntity) {
        GlobalScope.launch(Dispatchers.IO) {
            calendarRepository.insertOrUpdateCalendar(calendarEntity)
        }
    }

    fun onDelete(calendarEntity: CalendarEntity) {
        GlobalScope.launch(Dispatchers.IO) {
            calendarRepository.deleteCalendar(calendarEntity)
        }
    }
}