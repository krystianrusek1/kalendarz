package krystian.kalendarz.ui.main.screens.mycalendars

internal data class HeaderModel(val title: String) : MyCalendarModel {
    override fun getViewType(): Int {
        return MyCalendarsAdapter.HEADER_VIEW_TYPE
    }
}