package krystian.kalendarz.ui.main.screens.testSms

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.data.sms.SendSMSRepository
import krystian.kalendarz.logic.sms.SyncSmsManager
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.BaseViewModel
import javax.inject.Inject

class TestSmsViewModel @Inject constructor(
    private val sendSMSRepository: SendSMSRepository,
    private val syncSmsManager: SyncSmsManager
) :
    BackPressedViewModel() {

    var isChecked = false
        set(value) {
            field = value
            refreshList()
        }

    private val listSms = MutableLiveData<List<SmsEntity>>()

    private val observerSms = Observer<List<SmsEntity>> { listEntry: List<SmsEntity>? ->
        listSms.value = listEntry
    }

    private var refreshListSms = sendSMSRepository.getListOrderByDispatchDate(isChecked)

    fun addSms(sms: SmsEntity) {
        sendSMSRepository.addOrUpdateSms(sms)
    }

    fun getSmsList(): LiveData<List<SmsEntity>> {
        refreshList()
        return listSms
    }

    private fun refreshList() {
        refreshListSms.removeObserver(observerSms)
        refreshListSms = sendSMSRepository.getListOrderByDispatchDate(isChecked)
        refreshListSms.observeForever(observerSms)
    }
}