package krystian.kalendarz.ui.main.screens.calendar.week

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import butterknife.BindView
import krystian.kalendarz.R
import krystian.kalendarz.ui.main.fragment.AbstractMainFragment
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodAdapter
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.ui.main.view.calendar.CalendarViewListener
import krystian.kalendarz.ui.main.view.daydetails.EventAdapter
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.CenterLayoutManager
import krystian.kalendarz.util.TopLayoutManager
import java.util.*


class WeekFragment : AbstractMainFragment<WeekViewModel>(WeekViewModel::class.java) {

    companion object {
        private const val KEY_SHOW_DATE = "KEY_SHOW_DATE"
        /**
         * Screen shows the moth calendar based on the [showDate]
         */
        fun newInstance(showDate: Date): WeekFragment {
            val fragment = WeekFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_SHOW_DATE, showDate)
            fragment.arguments = bundle
            return fragment
        }
    }

    @BindView(R.id.weekList)
    @JvmField
    var weekList: RecyclerView? = null

    @BindView(R.id.dayList)
    @JvmField
    var dayList: RecyclerView? = null

    private lateinit var weekAdapter: CalendarPeriodAdapter

    private lateinit var dayAdapter: DayAdapter

    private val onWeekScrollListener = object : RecyclerView.OnScrollListener() {

        private var isStabilizing: Boolean = true

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            val currentItem = getCurrentItemOnWeekList()

            if (newState == RecyclerView.SCROLL_STATE_IDLE && !isStabilizing) {
                isStabilizing = true
                currentItem?.let { item ->
                    recyclerView.smoothScrollToPosition(item)
                    weekAdapter.getCalendarPeriodModel(item)
                        ?.let { model -> viewModel.onWeekListChanged(model, true) }
                }
            } else if (newState == SCROLL_STATE_DRAGGING) {
                isStabilizing = false
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val currentItem = getCurrentItemOnWeekList()

            currentItem?.let { item ->
                weekAdapter.getCalendarPeriodModel(item)
                    ?.let { model -> viewModel.onWeekListChanged(model, false) }
            }

        }
    }

    private val onDayScrollListener = object : RecyclerView.OnScrollListener() {

        var isScrolledByUser = false

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

            if (newState == SCROLL_STATE_DRAGGING) {
                isScrolledByUser = true
            }

            if (newState == SCROLL_STATE_IDLE) {
                isScrolledByUser = false
            }

            val dayModel = getFirstFullVisibleItemOnDayList()
            dayModel?.let { viewModel.onDayListChanged(it, isScrolledByUser) }

        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val dayModel = getFirstFullVisibleItemOnDayList()
            dayModel?.let { viewModel.onDayListChanged(it, isScrolledByUser) }

        }
    }

    private val datAdapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            viewModel.onItemDayInserted()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.week_calendar_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val showDate = getShowDateArgument()

        if (showDate != null) {
            viewModel.onShowDateChange(showDate)
        }

        initWeekList()
        initDayList()
    }

    override fun onStart() {
        weekList?.addOnScrollListener(onWeekScrollListener)
        dayList?.addOnScrollListener(onDayScrollListener)

        dayAdapter.registerAdapterDataObserver(datAdapterDataObserver)
        super.onStart()
    }

    override fun onStop() {
        weekList?.removeOnScrollListener(onWeekScrollListener)
        dayList?.removeOnScrollListener(onDayScrollListener)

        dayAdapter.unregisterAdapterDataObserver(datAdapterDataObserver)
        super.onStop()
    }

    private fun initWeekList() {
        weekList?.let {
            weekAdapter = CalendarPeriodAdapter(true, WeekViewModel.NUMBER_OF_WEEKS)
            it.setHasFixedSize(true)
            it.adapter = weekAdapter
            val layoutManager = CenterLayoutManager(context)
            layoutManager.orientation = LinearLayoutManager.HORIZONTAL
            it.layoutManager = layoutManager
        }

        weekAdapter.calendarViewListener = object :
            CalendarViewListener {
            override fun onItemClicked(dayCalendarItem: DayCalendarItem) {
                viewModel.onDayItemClickedOnWeekList(dayCalendarItem)
            }
        }

        viewModel.calendarItems.observe(this, Observer { calendarItems ->
            weekAdapter.submitList(calendarItems)
        })

        viewModel.getSelectedDate().observe(this, Observer { date ->
            weekAdapter.selectedDate = date
        })

        viewModel.getVisibleDate().observe(this, Observer { date ->
            weekAdapter.visibleDate = date
        })

        viewModel.showNextWeek.setObserver(this, Observer {
            getCurrentItemOnWeekList()?.plus(1)
                ?.let { currentItem -> weekList!!.smoothScrollToPosition(currentItem) }
        })

        viewModel.showPreviousWeek.setObserver(this, Observer {
            getCurrentItemOnWeekList()?.minus(1)
                ?.let { currentItem -> weekList!!.smoothScrollToPosition(currentItem) }
        })
    }

    private fun initDayList() {
        dayList?.let {
            dayAdapter = DayAdapter()
            it.adapter = dayAdapter
            val layoutManager = TopLayoutManager(context)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
        }

        viewModel.refreshDayItem.setObserver(this, Observer {
            refreshDayItem()
        })

        refreshDayItem()

        viewModel.dayListScrollTo.setObserver(this, Observer { scrollToDate ->
            if (scrollToDate != null) {
                getPositionFirstVisibleItemOnDayList()?.let { position ->
                    val dayVisible = dayAdapter.getDayModel(position)
                    dayVisible?.let {
                        val diff = CalendarHelper.getDeferenceNumberOfDay(scrollToDate, dayVisible.day)
                        dayList?.smoothScrollToPosition(position + diff)
                    }
                }
            }
        })

        dayAdapter!!.eventListener = object : DayAdapter.EventListener {
            override fun onEventClocked(eventModel: EventAdapter.EventModel) {
                viewModel.onEventModelClicked(eventModel)
            }
        }
    }

    private fun refreshDayItem() {
        viewModel.dayItems.observe(this, Observer { dayItems ->
            LOG.d("dayItems.observe")
            dayAdapter.submitList(dayItems)
        })
    }

    private fun getShowDateArgument(): Date? {
        return arguments?.getSerializable(KEY_SHOW_DATE) as Date
    }

    private fun getCurrentItemOnWeekList(): Int? {
        weekList?.let { weekList ->
            val snapHelper = LinearSnapHelper()
            val view = snapHelper.findSnapView(weekList.layoutManager)
            var position = RecyclerView.NO_POSITION
            view?.let { position = weekList.getChildAdapterPosition(it) }

            if (position == RecyclerView.NO_POSITION) {
                val firstPos =
                    (weekList.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                val lastPos =
                    (weekList.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                position = Math.abs(lastPos - firstPos) / 2 + firstPos
            }
            return position
        }
        return null
    }

    private fun getFirstFullVisibleItemOnDayList(): DayAdapter.DayModel? {
        return getPositionFirstVisibleItemOnDayList()?.let { position ->
            dayAdapter.getDayModel(position + 1)
        }
    }

    private fun getFirstVisibleItemOnDayList(): DayAdapter.DayModel? {
        return getPositionFirstVisibleItemOnDayList()?.let { position ->
            dayAdapter.getDayModel(position)
        }
    }

    private fun getPositionFirstVisibleItemOnDayList(): Int? {
        dayList?.let { dayList ->
            val itemPosition = (dayList.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
            if (itemPosition != RecyclerView.NO_POSITION) {
                return itemPosition
            }
            return null
        }
        return null
    }
}