package krystian.kalendarz.ui.main.screens.calendar.month

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.ui.tools.gesturedecector.GestureDetectorHelper
import krystian.kalendarz.ui.tools.gesturedecector.GestureDetectorListener

class GestureSwipes(context: Context) {

    interface GestureSwipesListener {
        fun onLeftToRightSwipe()
        fun onRiftToLeftSwipe()
    }

    interface Filter {
        fun onCatchAction(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean
    }

    var gestureSwipesListener: GestureSwipesListener? = null

    var filter: Filter? = null

    private val views = HashSet<View>()

    private val onItemTouchListener = object : RecyclerView.OnItemTouchListener {
        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            return gestureDetector.onTouchEvent(e)
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }

        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        }
    }

    private val gestureDetectorListener = object : GestureDetectorListener() {
        override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
            filter?.let {
                if (it.onCatchAction(e1, e2, velocityX, velocityY)) {
                    return false
                }
            }

            if (e1 == null || e2 == null) {
                return false
            }
            if (GestureDetectorHelper.isLeftToRightSwipe(e1, e2)) {
                gestureSwipesListener?.onLeftToRightSwipe()
                return true
            }

            if (GestureDetectorHelper.isRightToLeftSwipe(e1, e2)) {
                gestureSwipesListener?.onRiftToLeftSwipe()
                return true
            }
            return false
        }
    }

    val gestureDetector: GestureDetector = GestureDetector(context, gestureDetectorListener)

    fun addedView(view: View?) {
        if (view == null) {
            return
        }

        if (view is RecyclerView) {
            view.addOnItemTouchListener(onItemTouchListener)
        } else {
            view.setOnTouchListener { _, event -> gestureDetector.onTouchEvent(event) }
        }

        view.let { views.add(it) }
    }

    fun removeView(view: View?) {
        if (view == null) {
            return
        }


        if (view is RecyclerView) {
            view.removeOnItemTouchListener(onItemTouchListener)
        } else {
            view.setOnTouchListener(null)
        }
        view.let { views.remove(it) }
    }
}