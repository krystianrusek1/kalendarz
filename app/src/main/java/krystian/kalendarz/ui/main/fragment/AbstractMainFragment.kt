package krystian.kalendarz.ui.main.fragment

import krystian.kalendarz.ui.basefragment.BaseFragment
import krystian.kalendarz.ui.basefragment.BaseFragmentComponent
import krystian.kalendarz.ui.main.activity.MainActivity
import krystian.kalendarz.ui.main.activity.MainActivityComponent
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel

abstract class AbstractMainFragment<T : BackPressedViewModel>(
    viewModelClass: Class<T>
) : BaseFragment<T>(viewModelClass) {

    override fun injectDagger(): BaseFragmentComponent {
        return DaggerMainFragmentComponent
            .builder()
            .mainActivityComponent(getMainActivityComponent())
            .build()
    }

    protected fun getMainActivityComponent(): MainActivityComponent {
        return (requireActivity() as MainActivity).mainActivityComponent
    }

    override fun goBack() {
        (requireActivity() as MainActivity).goBack()
    }
}