package krystian.kalendarz.ui.main.screens.addevent

import java.util.*

interface DateTimePickerScreenListener  {

    fun onDatesChanged(startDate: Date, endDate: Date)
}