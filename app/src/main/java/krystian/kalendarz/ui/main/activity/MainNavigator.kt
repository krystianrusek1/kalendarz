package krystian.kalendarz.ui.main.activity

import android.widget.Toast
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.ui.main.screens.addevent.AddOrUpdateEventFragment
import krystian.kalendarz.ui.main.screens.mycalendars.MyCalendarsFragment
import krystian.kalendarz.ui.main.screens.testSms.TestSmsFragment
import krystian.kalendarz.util.Loggger
import java.util.*

object MainNavigator : LifecycleObserver {

    internal val LOG = Loggger(this.javaClass)

    internal var mainActivity: MainActivity? = null
        set(value) {
            LOG.d("setMinActivity")
            value?.lifecycle?.addObserver(this)
            field = value
        }

    fun showTestSmsScreen() {
        showFragment(TestSmsFragment.newInstance())
    }

    fun showAddOrUpdateEventScreen(proposalDate: Date) {
        showFragment(AddOrUpdateEventFragment.newInstance(proposalDate))
    }

    fun showAddOrUpdateEventScreen(eventEntity: EventEntity) {
        showFragment(AddOrUpdateEventFragment.newInstance(eventEntity))
    }

    fun showMyCalendarsScreen() {
        showFragment(MyCalendarsFragment.newInstance())
    }

    fun goBack() {
        mainActivity?.goBack()
    }

    fun showToast(@StringRes resId: Int) {
        mainActivity?.let { activity ->
            Toast.makeText(activity, resId, Toast.LENGTH_LONG).show()
        }

    }

    internal fun showFragment(
        fragment: Fragment, @AnimatorRes @AnimRes enter: Int?,
        @AnimatorRes @AnimRes exit: Int?, @AnimatorRes @AnimRes popEnter: Int?,
        @AnimatorRes @AnimRes popExit: Int
    ) {
        showFragment(fragment, null, enter, exit, popEnter, popExit)
    }

    internal fun showFragment(fragment: Fragment) {
        showFragment(fragment, null, null, null, null, null)
    }

    internal fun showFragment(
        fragment: Fragment, tag: String?, @AnimatorRes @AnimRes enter: Int?,
        @AnimatorRes @AnimRes exit: Int?, @AnimatorRes @AnimRes popEnter: Int?,
        @AnimatorRes @AnimRes popExit: Int?
    ) {
        mainActivity?.showFragment(fragment, tag, enter, exit, popEnter, popExit)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        LOG.d("onDestroy")
        mainActivity?.lifecycle?.removeObserver(this)
        mainActivity = null
    }
}
