package krystian.kalendarz.ui.main.screens.calendar.week

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.ui.main.view.daydetails.TransformationDayEventModel
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.Loggger
import java.util.*

class DataSourceDay(
    private val startDate: Date,
    private val calendarRepository: CalendarRepository,
    private val resources: Resources
) :
    DataSource.Factory<Date, DayAdapter.DayModel>() {

    private val LOG = Loggger(this.javaClass)

    private val sourceLiveData = MutableLiveData<PageDataSourceDay>()

    private var pageDataSourceMoth: PageDataSourceDay? = null

    override fun create(): PageDataSourceDay {
        pageDataSourceMoth = PageDataSourceDay(
            startDate,
            calendarRepository,
            resources
        )
        sourceLiveData.postValue(pageDataSourceMoth)
        return pageDataSourceMoth!!
    }

    class PageDataSourceDay(
        val startDate: Date,
        private val calendarRepository: CalendarRepository,
        private val resources: Resources
    ) :
        PageKeyedDataSource<Date, DayAdapter.DayModel>() {

        private val LOG = Loggger(this.javaClass)

        override fun loadInitial(
            params: LoadInitialParams<Date>,
            callback: LoadInitialCallback<Date, DayAdapter.DayModel>
        ) {
            runBlocking {
                val monthModel = createDayModel(startDate)
                callback.onResult(monthModel, previousDate(startDate), nextDate(startDate))
            }
        }

        override fun loadAfter(params: LoadParams<Date>, callback: LoadCallback<Date, DayAdapter.DayModel>) {
            runBlocking {
                val monthModel = createDayModel(params.key)
                callback.onResult(monthModel, nextDate(params.key))
            }
        }

        override fun loadBefore(params: LoadParams<Date>, callback: LoadCallback<Date, DayAdapter.DayModel>) {
            runBlocking {
                val monthModel = createDayModel(params.key)
                callback.onResult(monthModel, previousDate(params.key))
            }
        }

        private fun nextDate(date: Date): Date {
            val calendar = CalendarHelper.getInstance()
            calendar.time = date
            calendar.add(Calendar.DATE, 1)
            return calendar.time
        }

        private fun previousDate(date: Date): Date {
            val calendar = CalendarHelper.getInstance()
            calendar.time = date
            calendar.add(Calendar.DATE, -1)
            return calendar.time
        }

        private fun createDayModel(date: Date): List<DayAdapter.DayModel> {
            val transformation = TransformationDayEventModel(calendarRepository, resources)
            val listEvents = transformation.getEventsByLiveDate(date)
            val dayModel = DayAdapter.DayModel(date, listEvents)

            return listOf(dayModel)
        }
    }
}