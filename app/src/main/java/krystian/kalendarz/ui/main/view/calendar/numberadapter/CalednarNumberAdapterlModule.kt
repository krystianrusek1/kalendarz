package krystian.kalendarz.ui.main.view.calendar.numberadapter

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import krystian.kalendarz.ui.main.dialogs.picker.month.MonthPickerViewModel
import krystian.kalendarz.ui.viewmodel.ViewModelKey

@Module
interface CalednarNumberAdapterlModule {

    @Binds
    @IntoMap
    @ViewModelKey(CalendarNumberAdapterViewModel::class)
    fun bindCalendarNumberAdapterViewModel(viewModel: CalendarNumberAdapterViewModel): ViewModel
}