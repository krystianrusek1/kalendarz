package krystian.kalendarz.ui.main.screens.mycalendars

interface MyCalendarModel {

    fun getViewType(): Int
}