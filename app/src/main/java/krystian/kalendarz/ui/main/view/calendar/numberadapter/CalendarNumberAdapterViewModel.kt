package krystian.kalendarz.ui.main.view.calendar.numberadapter

import androidx.lifecycle.LiveData
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.calendar.ColorsDiffModel
import krystian.kalendarz.data.calendar.YearMonthDay
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.ui.viewmodel.BaseViewModel
import java.util.*
import javax.inject.Inject

class CalendarNumberAdapterViewModel @Inject constructor(private val calendarRepository: CalendarRepository) :
    BaseViewModel() {

    fun getColorsDots(from: YearMonthDay, to: YearMonthDay): LiveData<ColorsDiffModel> {
        return calendarRepository.getColors(from, to, 3)
    }

    fun getColorsPerDayHasMap(): HashMap<YearMonthDay, List<ColorEvent>> {
        return calendarRepository.getColorsPerDayHasMap()
    }

    fun getColorsPerDayHasMapLiveData(): LiveData<YearMonthDay>  {
        return calendarRepository.getColorsPerDayHasMapLiveData()
    }
}