package krystian.kalendarz.ui.main.screens.calendar.month

import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import krystian.kalendarz.R
import krystian.kalendarz.ui.basefragment.BaseFragment
import krystian.kalendarz.ui.main.screens.calendar.month.AbstractMonthViewModel.Companion.NUMBER_OF_WEEKS
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodAdapter
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.ui.main.view.calendar.CalendarViewListener
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.CenterLayoutManager
import java.util.*


abstract class AbstractMonthFragment<T : AbstractMonthViewModel>(
    viewModelClass: Class<T>
) : BaseFragment<T>(viewModelClass) {

    companion object {

        fun obtainParameters(bundle: Bundle, showDate: Date): Bundle {
            bundle.putSerializable(KEY_SHOW_DATE, showDate)
            return bundle
        }

        private const val KEY_SHOW_DATE = "KEY_SHOW_DATE"

    }

    lateinit var monthList: RecyclerView

    protected lateinit var monthAdapter: CalendarPeriodAdapter

    private val onScrollListener = object : RecyclerView.OnScrollListener() {

        private var isStabilizing: Boolean = true

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            if (newState == SCROLL_STATE_IDLE && !isStabilizing) {
                isStabilizing = true
                getCurrentItem()?.let { item ->
                    recyclerView.smoothScrollToPosition(item)
                    monthAdapter.getCalendarPeriodModel(item)
                        ?.let { model -> viewModel.onMonthListChanged(model) }
                }

            } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                isStabilizing = false
            }
        }
    }

    abstract fun onCreateMonthList(view: View): RecyclerView

    @IdRes
    protected fun getContainerId(): Int {
        return R.id.container
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        monthList = onCreateMonthList(view)

        val showDate = getShowDateArgument()

        if (showDate != null) {
            viewModel.onShowDateChange(showDate)
        }

        initMonthList()
        observeViewModel()

    }

    override fun onStart() {
        monthList.addOnScrollListener(onScrollListener)
        super.onStart()
    }

    override fun onStop() {
        monthList.removeOnScrollListener(onScrollListener)
        super.onStop()
    }

    protected fun initMonthList() {
        monthList.let {
            monthAdapter = CalendarPeriodAdapter(isCircleVisible(), NUMBER_OF_WEEKS)
            it.setHasFixedSize(true)
            it.adapter = monthAdapter
            val layoutManager = CenterLayoutManager(context)
            layoutManager.orientation = LinearLayoutManager.HORIZONTAL
            it.layoutManager = layoutManager
        }

        monthAdapter.calendarViewListener = object :
            CalendarViewListener {
            override fun onItemClicked(dayCalendarItem: DayCalendarItem) {
                viewModel.onItemClicked(dayCalendarItem)
            }
        }
    }

    abstract fun isCircleVisible(): Boolean

    protected fun observeViewModel() {
        viewModel.calendarItems.observe(this, Observer { calendarItems ->
            monthAdapter.submitList(calendarItems)
        })
        viewModel.getSelectedDate().observe(this, Observer { date ->
            monthAdapter.selectedDate = date
        })

        viewModel.showNextMonth.setObserver(this, Observer {
            getCurrentItem()?.plus(1)
                ?.let { currentItem -> monthList.smoothScrollToPosition(currentItem) }
        })

        viewModel.showPreviousMonth.setObserver(this, Observer {
            getCurrentItem()?.minus(1)
                ?.let { currentItem -> monthList.smoothScrollToPosition(currentItem) }
        })
    }

    private fun getShowDateArgument(): Date? {
        return arguments?.getSerializable(KEY_SHOW_DATE) as Date
    }

    private fun getCurrentItem(): Int? {
        monthList.let { monthList ->
            val snapHelper = LinearSnapHelper()
            val view = snapHelper.findSnapView(monthList.layoutManager)
            var position = RecyclerView.NO_POSITION
            view?.let { position = monthList.getChildAdapterPosition(it) }

            if (position == RecyclerView.NO_POSITION) {
                val firstPos =
                    (monthList.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                val lastPos =
                    (monthList.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                position = Math.abs(lastPos - firstPos) / 2 + firstPos
            }
            return position
        }
        return null
    }

    override fun getKeyViewModel(): String? {
        val date = getShowDateArgument()
        val calendar = CalendarHelper.getInstance()
        calendar.time = date

        return this.javaClass.name + calendar[Calendar.YEAR] + calendar[Calendar.MONTH]
    }
}
