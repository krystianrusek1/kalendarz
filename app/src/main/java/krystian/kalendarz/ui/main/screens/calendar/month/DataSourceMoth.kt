package krystian.kalendarz.ui.main.screens.calendar.month

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import krystian.kalendarz.util.Loggger
import java.util.*

class DataSourceMoth(
    private val numberOfWeeks: Int,
    private val startDate: Date
) :
    DataSource.Factory<Date, CalendarPeriodModel>() {

    private val LOG = Loggger(this.javaClass)

    private val sourceLiveData = MutableLiveData<PageDataSourceMoth>()

    private var pageDataSourceMoth: PageDataSourceMoth? = null

    override fun create(): PageDataSourceMoth {
        pageDataSourceMoth = PageDataSourceMoth(
            numberOfWeeks,
            startDate
        )
        sourceLiveData.postValue(pageDataSourceMoth)
        return pageDataSourceMoth!!
    }

    class PageDataSourceMoth(
        private val numberOfWeeks: Int,
        private val startDate: Date
    ) :
        PageKeyedDataSource<Date, CalendarPeriodModel>() {

        private val LOG = Loggger(this.javaClass)

        override fun loadInitial(
            params: LoadInitialParams<Date>,
            callback: LoadInitialCallback<Date, CalendarPeriodModel>
        ) {
            runBlocking {
                val monthModel = createMonthModel(startDate)
                callback.onResult(monthModel, previousDate(startDate), nextDate(startDate))
            }
        }

        override fun loadAfter(params: LoadParams<Date>, callback: LoadCallback<Date, CalendarPeriodModel>) {
            runBlocking {
                val monthModel = createMonthModel(params.key)
                callback.onResult(monthModel, nextDate(params.key))
            }
        }

        override fun loadBefore(params: LoadParams<Date>, callback: LoadCallback<Date, CalendarPeriodModel>) {
            runBlocking {
                val monthModel = createMonthModel(params.key)
                callback.onResult(monthModel, previousDate(params.key))
            }
        }

        private fun nextDate(date: Date): Date {
            val calendar = CalendarHelper.getInstance()
            calendar.time = date
            calendar.add(Calendar.MONTH, 1)
            return calendar.time
        }

        private fun previousDate(date: Date): Date {
            val calendar = CalendarHelper.getInstance()
            calendar.time = date
            calendar.add(Calendar.MONTH, -1)
            return calendar.time
        }

        private fun createMonthModel(date: Date): List<CalendarPeriodModel> {
            val name = DateFormatUnit.formatMonthYear(date)
            val list = createCalendarItems(date)
            val monthModel = CalendarPeriodModel(name, list)

            return listOf(monthModel)
        }

        private fun createCalendarItems(showDate: Date): List<DayCalendarItem> {
            val list = ArrayList<DayCalendarItem>()
            val calendar = CalendarHelper.getInstance()
            calendar.time = showDate
            val month = calendar.get(Calendar.MONTH)

            CalendarHelper.generateWeeksForMonthFrom(numberOfWeeks, showDate).forEach {
                val item = DayCalendarItem()
                item.calendar = it
                item.isOutOfPeriod = item.calendar.get(Calendar.MONTH) != month
                if (item.isOutOfPeriod) {
                    if (showDate.after(item.calendar.time)) {
                        item.isBeforePeriod = true
                    } else {
                        item.isAfterPeriod = true
                    }
                }
                item.isPresentDay = CalendarHelper.equalsDayMothYear(item.calendar, CalendarHelper.getInstance())
                list.add(item)
            }

            return list
        }
    }
}