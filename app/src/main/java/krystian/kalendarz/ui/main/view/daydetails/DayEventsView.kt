package krystian.kalendarz.ui.main.view.daydetails

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.util.DateFormatUnit
import java.util.*

class DayEventsView : LinearLayout {

    private val dayNameView: TextView
    private val noEventsView: TextView
    private val eventsView: RecyclerView

    private val eventAdapter: EventAdapter

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {

        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.day_events_view, this, true)

        dayNameView = findViewById(R.id.dayName)
        noEventsView = findViewById(R.id.noEvents)
        eventsView = findViewById(R.id.events)

        eventAdapter = EventAdapter()
        initEvent()
    }

    fun setEvents(events: List<EventAdapter.EventModel>) {
        if (events.isEmpty()) {
            noEventsView.visibility = View.VISIBLE
            eventsView.visibility = View.GONE
        } else {
            noEventsView.visibility = View.GONE
            eventsView.visibility = View.VISIBLE
            eventAdapter.submitList(events)
        }
    }

    fun updateDayName(date: Date) {
        dayNameView.text = DateFormatUnit.formatDayLong(date)
    }

    fun addOnItemTouchListener(listener: RecyclerView.OnItemTouchListener) {
        eventsView.addOnItemTouchListener(listener)
    }

    fun removeOnItemTouchListener(listener: RecyclerView.OnItemTouchListener) {
        eventsView.removeOnItemTouchListener(listener)
    }

    fun setEventAdapterListener(eventAdapterListener: EventAdapter.EventAdapterListener?) {
        eventAdapter.eventAdapterListener = eventAdapterListener
    }

    private fun initEvent() {
        eventsView.apply {
            adapter = eventAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }
}