package krystian.kalendarz.ui.main.screens.mycalendars

data class AddEntryModel(val calendarAccountNme: String) : MyCalendarModel {
    override fun getViewType(): Int {
        return MyCalendarsAdapter.NEW_ENTRY_VIEW_TYPE
    }
}