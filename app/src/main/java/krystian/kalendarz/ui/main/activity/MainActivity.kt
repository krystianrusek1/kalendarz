package krystian.kalendarz.ui.main.activity

import android.Manifest
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import krystian.kalendarz.R
import krystian.kalendarz.aplication.AppLiveCycle
import krystian.kalendarz.ui.baseActivity.BaseActivity
import krystian.kalendarz.ui.basefragment.BackPressed
import krystian.kalendarz.ui.main.screens.calendar.CalendarFragment
import krystian.kalendarz.permissions.PermissionHelper
import javax.inject.Inject

class MainActivity : BaseActivity<MainViewModel>(MainViewModel::class.java),
    FragmentManager.OnBackStackChangedListener {

    companion object {
        const val MY_PERMISSIONS_REQUEST = 0
    }

    @Inject
    lateinit var appLiveCycle: AppLiveCycle

    lateinit var mainActivityComponent: MainActivityComponent

    @Inject
    lateinit var mainNavigator: MainNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.addOnBackStackChangedListener(this)

        permissionCheck()
        setContentView(R.layout.activity_main)
        mainNavigator.mainActivity = this

        window.statusBarColor = resources.getColor(R.color.statusBar)

        viewModel.onViewCreated()
    }

    override fun onBackStackChanged() {
        LOG.d("onBackStackChanged ${supportFragmentManager.backStackEntryCount}")
        if (supportFragmentManager.backStackEntryCount == 0) {
            getCalendarFragment()?.setVisibility(View.VISIBLE)
        } else if (supportFragmentManager.backStackEntryCount > 0) {
            getCalendarFragment()?.setVisibility(View.GONE)
        }
    }

    private fun getCalendarFragment(): CalendarFragment? {
        val fragment = supportFragmentManager.findFragmentById(R.id.calendarFragment)
        return if (fragment != null) {
            fragment as CalendarFragment
        } else {
            null
        }
    }

    override fun onDestroy() {
        supportFragmentManager.removeOnBackStackChangedListener(this)
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        appLiveCycle.triggerResume()
    }

    override fun onPause() {
        appLiveCycle.triggerPause()
        super.onPause()
    }

    override fun injectDagger() {
        mainActivityComponent = DaggerMainActivityComponent.builder()
            .mainActivityModule(MainActivityModule(this))
            .applicationComponent(getApplicationComponent())
            .build()

        mainActivityComponent.inject(this)
    }

    override fun onBackPressed() {
        goBack()
    }

    override fun goBack() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        var isBackPressedHandle = false
        if (fragment is BackPressed) {
            isBackPressedHandle = (fragment as BackPressed).onBackPressed()
        }

        if (!isBackPressedHandle) {
            super.onBackPressed()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return super.onTouchEvent(event)
    }

    private fun permissionCheck() {
        PermissionHelper.permissionCheck(
            this,
            MY_PERMISSIONS_REQUEST,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (PermissionHelper.isCalendarGranted(this) && requestCode == MY_PERMISSIONS_REQUEST) {
            LOG.i("Calendar permission granted")
            viewModel.onCalendarPermissionGranted()
        }
    }

    internal fun showFragment(
        fragment: Fragment, tag: String?, @AnimatorRes @AnimRes enter: Int?,
        @AnimatorRes @AnimRes exit: Int?, @AnimatorRes @AnimRes popEnter: Int?,
        @AnimatorRes @AnimRes popExit: Int?
    ) {
        val transaction = supportFragmentManager.beginTransaction().apply {
            if (enter != null && exit != null && popEnter != null && popExit != null) {
                setCustomAnimations(
                    enter, exit, popEnter, popExit
                )
            } else if (enter != null && exit != null) {
                setCustomAnimations(
                    enter, exit
                )
            }
            replace(R.id.fragment_container, fragment, tag)
            addToBackStack(null)
        }

        transaction.commitAllowingStateLoss()

    }
}
