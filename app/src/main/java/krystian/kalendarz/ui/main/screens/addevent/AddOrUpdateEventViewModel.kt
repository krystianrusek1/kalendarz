package krystian.kalendarz.ui.main.screens.addevent

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.R
import krystian.kalendarz.data.PhoneNumber
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.data.contact.ContactRepository
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.data.phone.PhoneNumberRepository
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.LiveTriggerField
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import java.util.*
import javax.inject.Inject

class AddOrUpdateEventViewModel @Inject constructor(
    private val appNavigator: MainNavigator,
    private val calendarRepository: CalendarRepository,
    private val phoneNumberRepository: PhoneNumberRepository,
    private val contactRepository: ContactRepository
) : BackPressedViewModel(), DateTimePickerScreenListener {

    private lateinit var eventBuilder: EventEntity.Builder

    private var phonesList: List<PhoneNumber> = emptyList()

    var phonesLiveDate: LiveData<List<PhoneNumber>>? = null

    var addPhoneLiveDate = MutableLiveData<PhoneNumber>()

    var titleLiveData = MutableLiveData<String>()

    val showEndStartTimeLiveDate = MutableLiveData<Boolean>()
    val allDayCheckedLiveDate = MutableLiveData<Boolean>()

    val startDayLiveDate = MutableLiveData<String>()
    val endDayLiveDate = MutableLiveData<String>()

    val startTimeLiveDate = MutableLiveData<String>()
    val endTimeLiveDate = MutableLiveData<String>()

    val chooseCalendarEntityLiveDate = MutableLiveData<CalendarEntity>()

    val calendarListLiveDate = MediatorLiveData<CalendarList>()

    var showPickerDateDialog = LiveTriggerField<ShowPickerDateDialogData>()

    var hideKeyboard = LiveTriggerField<Boolean>()

    data class CalendarList(val list: List<CalendarEntity>, val selected: CalendarEntity?)

    data class ShowPickerDateDialogData(
        val isStartDateFirst: Boolean,
        val startDate: Date,
        val endDate: Date,
        val showTime: Boolean
    )

    init {
        runBlocking {
            chooseCalendarEntityLiveDate.value = calendarRepository.getLastChosenCalendar()
            eventBuilder = EventEntity.Builder(chooseCalendarEntityLiveDate.value?.id ?: -1)
        }
        val liveDataList = calendarRepository.getOnlyVisibleCalendarList()

        calendarListLiveDate.addSource(chooseCalendarEntityLiveDate) { selected ->
            if (selected != null) {
                val list = liveDataList.value ?: emptyList()
                calendarListLiveDate.value = CalendarList(list, selected)
                eventBuilder.withCalendarId(selected.id ?: -1)
            }
        }

        calendarListLiveDate.addSource(liveDataList) { listValue ->
            val list = listValue ?: emptyList()
            calendarListLiveDate.value = CalendarList(list, chooseCalendarEntityLiveDate.value)
        }
    }

    fun onEventChanged(eventEntity: EventEntity) {
        eventBuilder = eventEntity.copyBuilder()
        updateEvent(eventBuilder)
    }

    fun onProposalDateChanged(proposalDate: Date?) {
        eventBuilder = EventEntity.Builder(chooseCalendarEntityLiveDate.value?.id ?: -1)
        proposalDate?.let {
            val calendar = CalendarHelper.getInstance()
            calendar.time = it
            calendar.set(Calendar.HOUR_OF_DAY, 10)
            calendar.set(Calendar.MINUTE, 0)
            eventBuilder.startDate = calendar.time

            calendar.set(Calendar.HOUR_OF_DAY, 11)
            eventBuilder.endDate = calendar.time
        }
        updateEvent(eventBuilder)
    }

    fun onConcatSelected(contactUri: Uri) {
        GlobalScope.launch {
            val list = contactRepository.getContactPhone(contactUri)
            if (list.isNotEmpty()) {
                GlobalScope.launch(Dispatchers.Main) {
                    addPhoneLiveDate.value = list[0].phoneNumber
                }
            }
        }
    }

    private fun updateEvent(eventBuilder: EventEntity.Builder) {
        titleLiveData.value = eventBuilder.title
        triggerStartDateChange(eventBuilder.startDate)
        triggerEndDateChange(eventBuilder.endDate)
        allDayCheckedLiveDate.value = eventBuilder.allDays
        showEndStartTimeLiveDate.value = eventBuilder.allDays
        GlobalScope.launch {
            val calendarEntity = calendarRepository.getAllCalendar(eventBuilder.calendarId)
            GlobalScope.launch(Dispatchers.Main) {
                chooseCalendarEntityLiveDate.value = calendarEntity
                calendarEntity?.let { calendarRepository.saveLastChosenCalendar(it) }
            }
        }
        val eventId = eventBuilder.id

        phonesLiveDate = if (eventId != null) {
            phoneNumberRepository.getPhonesList(eventId)
        } else {
            null
        }
    }

    override fun onDatesChanged(startDate: Date, endDate: Date) {
        if (startDate.after(endDate)) {
            appNavigator.showToast(R.string.screen_addEvent_wrongDates_toast)
        } else {
            onStartDateChange(startDate)
            onEndDateChange(endDate)
        }
    }

    fun onStartDateChange(date: Date) {
        eventBuilder.withStartDate(date)
        triggerStartDateChange(date)
    }

    private fun triggerStartDateChange(date: Date) {
        startDayLiveDate.value = DateFormatUnit.formatDayShort(date)
        startTimeLiveDate.value = DateFormatUnit.formatTimeShort(date)
    }

    fun onEndDateChange(date: Date) {
        eventBuilder.withEndDate(date)
        triggerEndDateChange(date)
    }

    private fun triggerEndDateChange(date: Date) {
        endDayLiveDate.value = DateFormatUnit.formatDayShort(date)
        endTimeLiveDate.value = DateFormatUnit.formatTimeShort(date)
    }

    fun onAllDayTextClicked() {
        eventBuilder.allDays = !eventBuilder.allDays
        allDayCheckedLiveDate.value = eventBuilder.allDays
        showEndStartTimeLiveDate.value = eventBuilder.allDays
    }

    fun onAllDayCheckedChanged(isChecked: Boolean) {
        eventBuilder.allDays = isChecked
        showEndStartTimeLiveDate.value = eventBuilder.allDays
        allDayCheckedLiveDate.value = eventBuilder.allDays
    }

    fun onStartDateRowClicked() {
        showPickerDateDialog.value =
            ShowPickerDateDialogData(true, eventBuilder.startDate, eventBuilder.endDate, !eventBuilder.allDays)
    }

    fun onEndDateRowClicked() {
        showPickerDateDialog.value =
            ShowPickerDateDialogData(false, eventBuilder.startDate, eventBuilder.endDate, !eventBuilder.allDays)
    }

    fun onTitleChange(text: String) {
        eventBuilder.withTitle(text)
    }

    fun onAddEventClicked() {
        addEvent(eventBuilder)

        appNavigator.goBack()
    }

    fun onCalendarSelected(calendarEntity: CalendarEntity) {
        eventBuilder.withCalendarId(calendarEntity.id ?: -1)
        chooseCalendarEntityLiveDate.value = calendarEntity
        calendarRepository.saveLastChosenCalendar(calendarEntity)
    }

    private fun addEvent(
        eventBuilder: EventEntity.Builder
    ) {

        hideKeyboard.value = true

        eventBuilder.startDate = if (eventBuilder.allDays) {
            CalendarHelper.prepareStartDate(eventBuilder.startDate)
        } else {
            eventBuilder.startDate
        }

        eventBuilder.endDate = if (eventBuilder.allDays) {
            CalendarHelper.prepareEndDate(eventBuilder.endDate)
        } else {
            eventBuilder.endDate
        }

        GlobalScope.launch {
            val eventId = calendarRepository.insertOrUpdateEvent(eventBuilder.build())
            phoneNumberRepository.updatePhonesList(eventId, phonesList)
        }
    }

    fun onPhonesListChanged(list: List<PhoneNumber>) {
        this.phonesList = list
    }

    fun goBack() {
        hideKeyboard.value = true
        appNavigator.goBack()
    }
}