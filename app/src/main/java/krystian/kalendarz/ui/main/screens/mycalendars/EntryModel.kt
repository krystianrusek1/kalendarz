package krystian.kalendarz.ui.main.screens.mycalendars

import androidx.annotation.ColorInt

data class EntryModel(
    val calendarId: Long,
    val title: String, @ColorInt val color: Int,
    val isChecked: Boolean
) :
    MyCalendarModel {
    override fun getViewType(): Int {
        return MyCalendarsAdapter.ENTRY_VIEW_TYPE
    }
}