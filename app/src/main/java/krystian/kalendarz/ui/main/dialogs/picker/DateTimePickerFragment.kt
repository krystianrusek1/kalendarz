package krystian.kalendarz.ui.main.dialogs.picker

import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.ScrollView
import android.widget.TimePicker
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import butterknife.BindView
import butterknife.OnCheckedChanged
import butterknife.OnClick
import krystian.kalendarz.R
import krystian.kalendarz.ui.basedialogfragment.BaseDialogFragment
import krystian.kalendarz.ui.basedialogfragment.BaseDialogFragmentComponent
import krystian.kalendarz.ui.basedialogfragment.DialogFragmentScope
import krystian.kalendarz.ui.main.dialogs.picker.month.MonthPickerFragment
import krystian.kalendarz.ui.main.view.calendar.CustomTimePicker
import java.util.*


@DialogFragmentScope
class DateTimePickerFragment : BaseDialogFragment<DateTimePickerViewModel>(
    DateTimePickerViewModel::class.java
), SelectedDateChange {

    companion object {

        fun newInstance(
            isStartDateFirst: Boolean,
            startDate: Date,
            endDate: Date,
            showTime: Boolean
        ): DateTimePickerFragment {
            val fragment = DateTimePickerFragment()
            val bundle = Bundle()
            bundle.putBoolean(KEY_START_DATE_FIRST, isStartDateFirst)
            bundle.putSerializable(KEY_START_DATE, startDate)
            bundle.putSerializable(KEY_END_DATE, endDate)
            bundle.putBoolean(KEY_SHOW_TIME, showTime)
            fragment.arguments = bundle
            return fragment
        }

        private const val KEY_START_DATE_FIRST = "KEY_START_DATE_FIRST"
        private const val KEY_START_DATE = "KEY_START_DATE"
        private const val KEY_END_DATE = "KEY_END_DATE"
        private const val KEY_SHOW_TIME = "KEY_SHOW_TIME"
    }

    interface DatesChangeListener {

        fun onDatesChanged(startDate: Date, endDate: Date)
    }

    @BindView(R.id.toolbar)
    @JvmField
    var toolbar: Toolbar? = null

    @BindView(R.id.startDateRadio)
    @JvmField
    var startDateRadio: RadioButton? = null

    @BindView(R.id.endDateRadio)
    @JvmField
    var endDateRadio: RadioButton? = null

    @BindView(R.id.timePicker)
    @JvmField
    var timePicker: CustomTimePicker? = null

    @BindView(R.id.scrollView)
    @JvmField
    var scrollView: ScrollView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.AppTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.date_time_picker_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observerViewModel()

        toolbar?.setNavigationOnClickListener { viewModel.goBack() }
        timePicker!!.setIs24HourView(
            DateFormat.is24HourFormat(activity)
        )

        timePicker!!.setOnTimeChangedListener { timePicker: TimePicker, hourOfDay: Int, minute: Int ->
            viewModel.onTimeChanged(hourOfDay, minute)
        }

        val isStartDateFirst = arguments?.getBoolean(KEY_START_DATE_FIRST)
        val startDate = arguments?.getSerializable(KEY_START_DATE)
        val endDate = arguments?.getSerializable(KEY_END_DATE)
        val showTime = arguments?.getBoolean(KEY_SHOW_TIME)

        if (isStartDateFirst != null && startDate is Date && endDate is Date && showTime != null) {
            viewModel.onCreateStartEndDate(isStartDateFirst, startDate, endDate, showTime)
            arguments?.clear()
        }
    }

    override fun injectDagger(): BaseDialogFragmentComponent {
        val fragmentComponent = DaggerPickerDialogFragmentComponent
            .builder()
            .applicationComponent(getApplicationComponent())
            .pickerDialogFragmentModule(PickerDialogFragmentModule())
            .build()
        fragmentComponent.inject(this)

        return fragmentComponent
    }

    override fun onSelectedDateChange(date: Date) {
        viewModel.onDayChanged(date)
    }

    @OnCheckedChanged(R.id.startDateRadio)
    fun startDateRadioCheckedChanged(button: CompoundButton, checked: Boolean) {
        if (button.isPressed) {
            viewModel.onDateCheckedChanged(checked)
        }
    }

    @OnCheckedChanged(R.id.endDateRadio)
    fun endDateRadioCheckedChanged(button: CompoundButton, checked: Boolean) {
        if (button.isPressed) {
            viewModel.onDateCheckedChanged(!checked)
        }
    }

    @OnClick(R.id.done)
    fun onDoneClicked() {
        viewModel.onDoneClicked()
    }

    private fun observerViewModel() {
        viewModel.isStartDateChecked.observe(this, Observer { isStartDateChecked ->
            if (isStartDateChecked) {
                startDateRadio!!.isChecked = true
            } else {
                endDateRadio!!.isChecked = true
            }
        })

        viewModel.pickerTime.observe(this, Observer { calendar ->
            timePicker!!.setData(calendar.get(Calendar.MINUTE), calendar.get(Calendar.HOUR_OF_DAY))
        })

        viewModel.pickerTimeVisiblity.observe(this, Observer { isVisible ->
            if (isVisible) {
                timePicker?.visibility = VISIBLE
            } else {
                timePicker?.visibility = GONE
            }
        })

        viewModel.showPickerMoth.setObserver(this, Observer { date ->
            val fragment = childFragmentManager.findFragmentById(R.id.pickerMonth_container)
            if (fragment != null && fragment is MonthPickerFragment) {
                fragment.onDatesChanged(date)
            } else {
                showFragment(MonthPickerFragment.newInstance(date))
            }
        })

        viewModel.onChangeDate.observe(this, Observer { data ->

            if (targetFragment is DatesChangeListener) {
                (targetFragment as DatesChangeListener).onDatesChanged(data.startDate, data.endDate)
            }
        })

        viewModel.close.setObserver(this, Observer {
            dismiss()
        })
    }


    fun showFragment(fragment: Fragment) {
        showFragmentWithAnimations(fragment, null, null)
    }

    fun showFragmentWithAnimations(
        fragment: Fragment, @AnimatorRes @AnimRes enter: Int?,
        @AnimatorRes @AnimRes exit: Int?
    ) {
        val transaction = childFragmentManager.beginTransaction().apply {
            if (enter != null && exit != null) {
                setCustomAnimations(
                    enter, exit
                )
            }
            replace(R.id.pickerMonth_container, fragment)
            addToBackStack(null)
        }

        transaction.commit()
    }
}