package krystian.kalendarz.ui.main.activity

import dagger.Module
import dagger.Provides
import krystian.kalendarz.ui.baseActivity.ActivityScope
import krystian.kalendarz.ui.main.screens.calendar.CalendarNavigator

@Module
class MainActivityModule(val mainActivity: MainActivity) {

    @Provides
    @ActivityScope
    fun providesMainNavigator(): MainNavigator {
        return MainNavigator
    }

    @Provides
    @ActivityScope
    fun providesCalendarNavigator(): CalendarNavigator {
        return CalendarNavigator
    }
}