package krystian.kalendarz.ui.main.dialogs.picker.month

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.ui.basefragment.BaseFragmentComponent
import krystian.kalendarz.ui.main.dialogs.picker.SelectedDateChange
import krystian.kalendarz.ui.main.screens.calendar.month.AbstractMonthFragment
import java.util.*

class MonthPickerFragment :
    AbstractMonthFragment<MonthPickerViewModel>(
        MonthPickerViewModel::class.java
    ) {

    companion object {

        /**
         * Screen shows the moth calendar based on the [showDate]
         */
        fun newInstance(showDate: Date): MonthPickerFragment {
            val fragment = MonthPickerFragment()
            val bundle = Bundle()
            fragment.arguments = obtainParameters(bundle, showDate)
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.month_picker_calendar_fragment, container, false)
    }

    override fun onCreateMonthList(view: View): RecyclerView {
        return view.findViewById(R.id.monthList)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getSelectedDate().observe(this, androidx.lifecycle.Observer { date ->
            if (parentFragment is SelectedDateChange) {
                (parentFragment as SelectedDateChange).onSelectedDateChange(date)
            }
        })
    }

    override fun goBack() {

    }

    override fun isCircleVisible(): Boolean {
        return false
    }

    fun onDatesChanged(showDate: Date) {
        viewModel.onShowDateChange(showDate)
        initMonthList()
        observeViewModel()
    }

    override fun injectDagger(): BaseFragmentComponent {
        val fragmentComponent = DaggerMonthPickerFragmentComponent
            .builder()
            .applicationComponent(getApplicationComponent())
            .build()
        fragmentComponent.inject(this)

        return fragmentComponent
    }
}
