package krystian.kalendarz.ui.main.screens.calendar.period

data class CalendarPeriodModel(val monthName: String, val dayList: List<DayCalendarItem>) {
}