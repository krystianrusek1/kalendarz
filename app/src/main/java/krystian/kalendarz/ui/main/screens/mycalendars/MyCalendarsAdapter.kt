package krystian.kalendarz.ui.main.screens.mycalendars

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.ui.main.view.CircleCheckBoxView

class MyCalendarsAdapter : ListAdapter<MyCalendarModel, MyCalendarsAdapter.ViewHolder>(DIFF_CALLBACK) {

    var myCalendarsAdapterListener: MyCalendarsAdapterListener? = null

    companion object {
        const val HEADER_VIEW_TYPE = 0
        const val ENTRY_VIEW_TYPE = 1
        const val NEW_ENTRY_VIEW_TYPE = 2

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MyCalendarModel>() {
            override fun areItemsTheSame(oldItem: MyCalendarModel, newItem: MyCalendarModel): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: MyCalendarModel, newItem: MyCalendarModel): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }

    inner class HeaderViewHolder(val view: View) : ViewHolder(view) {
        val header: TextView = view.findViewById(R.id.header)

        internal fun onBindViewHolder(holder: HeaderViewHolder, headerModel: HeaderModel) {
            holder.header.text = headerModel.title
        }
    }

    inner class EntryViewHolder(val view: View) : ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.title)
        val editSection: View = view.findViewById(R.id.editSection)
        val circleCheckBox = view.findViewById<CircleCheckBoxView>(R.id.checkBoxCircle)

        internal fun onBindViewHolder(holder: EntryViewHolder, entryModel: EntryModel) {
            holder.title.text = entryModel.title
            circleCheckBox.setChecked(entryModel.isChecked, entryModel.color)
            circleCheckBox.setOnClickListener {
                circleCheckBox.updateChecked(!circleCheckBox.isChecked)
                myCalendarsAdapterListener?.onItemCheckboxChecked(circleCheckBox.isChecked, entryModel)
            }
            editSection.setOnClickListener {
                myCalendarsAdapterListener?.onItemClicked(entryModel)
            }
        }
    }

    inner class NewEntryViewHolder(val view: View) : ViewHolder(view) {
        val addCalendar: View = view.findViewById(R.id.add_calendar)

        internal fun onBindViewHolder(holder: NewEntryViewHolder, addEntryModel: AddEntryModel) {
            addCalendar.setOnClickListener {
                myCalendarsAdapterListener?.onAddItemClicked(addEntryModel)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).getViewType()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            HEADER_VIEW_TYPE -> (holder as HeaderViewHolder).onBindViewHolder(holder, getItem(position) as HeaderModel)
            ENTRY_VIEW_TYPE -> (holder as EntryViewHolder).onBindViewHolder(holder, getItem(position) as EntryModel)
            NEW_ENTRY_VIEW_TYPE -> (holder as NewEntryViewHolder).onBindViewHolder(
                holder,
                getItem(position) as AddEntryModel
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            HEADER_VIEW_TYPE -> {
                return HeaderViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.my_calendars_list_header,
                        parent,
                        false
                    )
                )
            }
            ENTRY_VIEW_TYPE -> {
                return EntryViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.my_calendars_list_entry,
                        parent,
                        false
                    )
                )
            }
            NEW_ENTRY_VIEW_TYPE -> {
                return NewEntryViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.my_calendars_list_new_entry,
                        parent,
                        false
                    )
                )
            }
            else -> throw NullPointerException("wrong viewType $viewType")
        }
    }

    interface MyCalendarsAdapterListener {
        fun onItemCheckboxChecked(isChecked: Boolean, entryModel: EntryModel)
        fun onItemClicked(entryModel: EntryModel)
        fun onAddItemClicked(addEntryModel: AddEntryModel)
    }
}
