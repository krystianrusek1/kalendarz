package krystian.kalendarz.ui.main.screens.addevent


import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import butterknife.BindView
import butterknife.OnCheckedChanged
import butterknife.OnClick
import butterknife.OnTextChanged
import krystian.kalendarz.R
import krystian.kalendarz.data.PhoneNumber
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.ui.main.dialogs.picker.DateTimePickerFragment
import krystian.kalendarz.ui.main.fragment.AbstractMainFragment
import krystian.kalendarz.ui.main.screens.addevent.phonenumber.PhoneNumberListView
import krystian.kalendarz.util.KeyboardUtil
import java.util.*
import android.content.Intent
import android.provider.ContactsContract


class AddOrUpdateEventFragment : AbstractMainFragment<AddOrUpdateEventViewModel>(AddOrUpdateEventViewModel::class.java),
    DateTimePickerFragment.DatesChangeListener {

    companion object {

        fun newInstance(proposalDate: Date): AddOrUpdateEventFragment {
            val fragment = AddOrUpdateEventFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_PROPOSAL_DATE, proposalDate)
            fragment.arguments = bundle
            return fragment
        }

        fun newInstance(eventEntity: EventEntity): AddOrUpdateEventFragment {
            val fragment = AddOrUpdateEventFragment()
            val bundle = Bundle()
            bundle.putSerializable(KEY_EVENT_ENTITY, eventEntity)
            fragment.arguments = bundle
            return fragment
        }

        private const val KEY_PROPOSAL_DATE = "KEY_PROPOSAL_DATE"

        private const val KEY_EVENT_ENTITY = "KEY_EVENT_ENTITY"

        private const val PICKER_DIALOG = "PICKER_DIALOG"

        private const val PICKER_DIALOG_CODE = 0

        private const val KEY_SELECT_PHONE_NUMBER = 0
    }

    @BindView(R.id.toolbar)
    @JvmField
    var toolbar: Toolbar? = null

    @BindView(R.id.title)
    @JvmField
    var titleView: EditText? = null

    @BindView(R.id.allDayCheckBox)
    @JvmField
    var allDayCheckBox: AppCompatCheckBox? = null

    @BindView(R.id.startDay)
    @JvmField
    var startDay: TextView? = null

    @BindView(R.id.endDay)
    @JvmField
    var endDay: TextView? = null

    @BindView(R.id.startTime)
    @JvmField
    var startTime: TextView? = null

    @BindView(R.id.endTime)
    @JvmField
    var endTime: TextView? = null

    @BindView(R.id.calendarSpinner)
    @JvmField
    var calendarSpinner: Spinner? = null

    @BindView(R.id.phoneList)
    @JvmField
    var phoneNumberListView: PhoneNumberListView? = null

    private lateinit var calendarAdapter: CalendarSpinnerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.add_event_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerViewModel()

        val eventEntity = arguments?.getSerializable(KEY_EVENT_ENTITY)
        if (eventEntity != null) {
            viewModel.onEventChanged(eventEntity as EventEntity)
            arguments?.clear()

        } else {
            val proposalDate = arguments?.getSerializable(KEY_PROPOSAL_DATE)
            if (proposalDate != null) {
                viewModel.onProposalDateChanged(proposalDate as Date)
                arguments?.clear()
            }
        }

        toolbar?.setNavigationOnClickListener { viewModel.goBack() }
        initCalendarSpinner()
        initPhoneNumberListView()
    }

    private fun initCalendarSpinner() {
        calendarAdapter = CalendarSpinnerAdapter(context!!)
        calendarSpinner!!.adapter = calendarAdapter

        viewModel.calendarListLiveDate.observe(this, Observer { list ->

            calendarAdapter.clear()
            calendarAdapter.addAll(list.list)

            if (list.selected != null) {
                val position = calendarAdapter.getPosition(list.selected)
                calendarSpinner!!.setSelection(position)
            }
        })

        calendarSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val calendar = calendarAdapter.getItem(position)
                if (calendar != null) {
                    viewModel.onCalendarSelected(calendar)
                }
            }
        }
    }

    private fun initPhoneNumberListView() {
        phoneNumberListView!!.phonesListListener = object : PhoneNumberListView.PhonesListListener {
            override fun onAddPhoneClicked() {
                openSelectNumberPhoneScreen()
            }

            override fun onPhonesListChanged(list: List<PhoneNumber>) {
                viewModel.onPhonesListChanged(list)
            }
        }

        viewModel.phonesLiveDate?.observe(this, Observer { list ->
            if (list != null) {
                phoneNumberListView!!.setPhonesList(list)
            }
        })

        viewModel.addPhoneLiveDate?.observe(this, Observer { phone ->
            if (phone != null) {
                phoneNumberListView!!.addPhones(phone)
            }
        })
    }

    private fun openSelectNumberPhoneScreen(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(intent, KEY_SELECT_PHONE_NUMBER)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KEY_SELECT_PHONE_NUMBER && resultCode == RESULT_OK) {
            val contactUri = data?.data ?: return
            viewModel.onConcatSelected(contactUri)
        }
    }

    @OnCheckedChanged(R.id.allDayCheckBox)
    fun onAllDayCheckedChanged(button: CompoundButton, checked: Boolean) {
        if (button.isPressed) {
            viewModel.onAllDayCheckedChanged(checked)
        }
    }

    @OnClick(R.id.allDayText)
    fun onAllDayClicked() {
        viewModel.onAllDayTextClicked()
    }

    @OnClick(R.id.startDateRow)
    fun onStartDateRowClicked() {
        viewModel.onStartDateRowClicked()
    }

    @OnClick(R.id.endDateRow)
    fun onEndDateRowClicked() {
        viewModel.onEndDateRowClicked()
    }

    @OnClick(R.id.done)
    fun onAddEventClicked() {
        viewModel.onAddEventClicked()
    }

    @OnTextChanged(R.id.title)
    fun onTitleChange(text: CharSequence) {
        viewModel.onTitleChange(text.toString())
    }

    override fun onDatesChanged(startDate: Date, endDate: Date) {
        viewModel.onDatesChanged(startDate, endDate)
    }

    private fun observerViewModel() {
        viewModel.showEndStartTimeLiveDate.observe(this, Observer { isChecked ->
            if (isChecked) {
                startTime!!.visibility = View.INVISIBLE
                endTime!!.visibility = View.INVISIBLE
            } else {
                startTime!!.visibility = View.VISIBLE
                endTime!!.visibility = View.VISIBLE
            }
        })

        viewModel.titleLiveData.observe(this, Observer { title ->
            titleView!!.setText(title.toString())
        })

        viewModel.allDayCheckedLiveDate.observe(this, Observer { isChecked ->
            allDayCheckBox!!.isChecked = isChecked
        })

        viewModel.startDayLiveDate.observe(this, Observer { day -> startDay!!.text = day })
        viewModel.endDayLiveDate.observe(this, Observer { day -> endDay!!.text = day })

        viewModel.startTimeLiveDate.observe(this, Observer { time -> startTime!!.text = time })
        viewModel.endTimeLiveDate.observe(this, Observer { time -> endTime!!.text = time })

        viewModel.showPickerDateDialog.setObserver(this, Observer { data ->
            showPickerDialog(data)
        })


        viewModel.hideKeyboard.setObserver(this, Observer { view?.let { it1 -> KeyboardUtil.hiddenKeyboard(it1) } })
    }

    private fun showPickerDialog(data: AddOrUpdateEventViewModel.ShowPickerDateDialogData) {
        val dialog =
            DateTimePickerFragment.newInstance(data.isStartDateFirst, data.startDate, data.endDate, data.showTime)
        dialog.setTargetFragment(this, PICKER_DIALOG_CODE)
        fragmentManager?.let { dialog.show(it, PICKER_DIALOG) }
    }
}