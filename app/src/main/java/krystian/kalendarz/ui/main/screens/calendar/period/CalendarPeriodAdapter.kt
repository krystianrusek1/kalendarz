package krystian.kalendarz.ui.main.screens.calendar.period

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.ui.main.view.calendar.CalendarPeriodView
import krystian.kalendarz.ui.main.view.calendar.CalendarViewListener
import krystian.kalendarz.util.Loggger
import java.util.*

class CalendarPeriodAdapter(private val isCircleVisible: Boolean, private val numberOfWeeks: Int) :
    PagedListAdapter<CalendarPeriodModel, CalendarPeriodAdapter.HolderView>(
        DIFF_CALLBACK
    ) {

    internal val LOG = Loggger(this.javaClass)

    var calendarViewListener: CalendarViewListener? = null

    var selectedDate: Date? = null
        set(value) {
            notifyDataSetChanged()
            field = value
        }

    var visibleDate: Date? = null
        set(value) {
            notifyDataSetChanged()
            field = value
        }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CalendarPeriodModel>() {
            override fun areItemsTheSame(oldItem: CalendarPeriodModel, newItem: CalendarPeriodModel) =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: CalendarPeriodModel, newItem: CalendarPeriodModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    class HolderView(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var monthYearName: TextView
        lateinit var calendarPeriod: CalendarPeriodView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderView {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.calendar_period_adapter, parent, false)

        val viewHolder = HolderView(view)

        viewHolder.monthYearName = view.findViewById(R.id.monthYearName)
        viewHolder.calendarPeriod = view.findViewById(R.id.calendarPeriod)
        viewHolder.calendarPeriod.init(isCircleVisible, numberOfWeeks)

        return viewHolder
    }

    override fun onBindViewHolder(holder: HolderView, position: Int) {
        val calendarPeriodModel: CalendarPeriodModel? = getItem(position)

        holder.calendarPeriod.setData(calendarPeriodModel!!.dayList)
        selectedDate?.let { holder.calendarPeriod.setSelectedDay(it) }
        visibleDate?.let { holder.calendarPeriod.setVisibleDay(it) }

        holder.calendarPeriod.calendarViewListener = calendarViewListener
        holder.monthYearName.text = calendarPeriodModel.monthName
    }

    fun getCalendarPeriodModel(position: Int): CalendarPeriodModel? {
        if (itemCount > position) {
            return super.getItem(position)
        }
        return null
    }
}