package krystian.kalendarz.ui.main.dialogs.picker

import java.util.*

interface SelectedDateChange {

    fun onSelectedDateChange(date: Date)
}