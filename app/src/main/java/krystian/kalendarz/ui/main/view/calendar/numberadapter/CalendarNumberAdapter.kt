package krystian.kalendarz.ui.main.view.calendar.numberadapter

import android.content.Context
import android.graphics.Typeface
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import kotlinx.coroutines.*
import krystian.kalendarz.R
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.data.calendar.ColorsDiffModel
import krystian.kalendarz.data.calendar.YearMonthDay
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.ui.main.view.calendar.CalendarPeriodView
import krystian.kalendarz.ui.main.view.calendar.NumberDayView
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.Loggger
import java.util.*
import kotlin.collections.HashMap

/*
* The class is responsible for loading the [DayCalendarItem] to views.
* The dayList have to sorted in order date.
* */
class CalendarNumberAdapter(
    private val isCircleVisible:Boolean,
    private val calendarPeriodView: CalendarPeriodView,
    private var dayList: List<DayCalendarItem>
) {

    internal val LOG = Loggger(this.javaClass)

    val context: Context = calendarPeriodView.context

    val viewModel: CalendarNumberAdapterViewModel

    private val positionYearMonthDay = HashMap<YearMonthDay, Int>()

    init {
        val component = injectDagger(context)
        viewModel = initViewModel(context, component.viewModelFactory())
        setDateList(dayList)

        if(isCircleVisible) {
            viewModel.getColorsPerDayHasMapLiveData().observe(context as LifecycleOwner, Observer { yearMonthDay ->
                positionYearMonthDay[yearMonthDay]?.let { notifyItemChanged(it) }
            })
        }
    }

    private var selectedPosition: Int? = null

    private var visiblePosition: Int? = null

    // The observer is needed to inform about period of time which is triggered.
    // The solution contains the debounceColorJob, then the app can not rely on response from colorObserver.
    // To notify the colors changes is used the viewModel.getColorsPerDayHasMapLiveData().
    private var colorObserver: Observer<ColorsDiffModel> = Observer { colorsDiffModel -> }

    private var debounceColorJob: Job? = null

    private var colorLiveDate: LiveData<ColorsDiffModel>? = null

    fun onCreate(numberDayView: NumberDayView) {

    }

    fun onBind(numberDayView: NumberDayView, position: Int) {
        val item = getItem(position)

        numberDayView.setTag(R.id.TAG_LAST_TEM, item)

        updateNumberOfDay(numberDayView, item, position)

        val circle = if (position == selectedPosition && !item.isOutOfPeriod) {
            NumberDayView.Circle.FULL
        } else if (position == visiblePosition && !item.isOutOfPeriod) {
            NumberDayView.Circle.STORKE
        } else {
            NumberDayView.Circle.NONE
        }
        numberDayView.setCircle(circle)

        if(isCircleVisible) {
            val yearMonthDay = YearMonthDay.build(item.calendar)
            val listColors = viewModel.getColorsPerDayHasMap()[yearMonthDay]
            if (listColors != null) {
                updateDots(numberDayView, listColors, yearMonthDay)
            } else {
                updateDots(numberDayView, emptyList(), yearMonthDay)
            }
            positionYearMonthDay[yearMonthDay] = position
        }
    }

    private fun getItem(position: Int): DayCalendarItem {
        return dayList[position]
    }

    private fun updateNumberOfDay(
        numberDayView: NumberDayView,
        item: DayCalendarItem,
        position: Int
    ) {
        numberDayView.setText(item.calendar.get(Calendar.DAY_OF_MONTH).toString())
        numberDayView.setGrayNumber(item.isOutOfPeriod)

        if (item.isPresentDay || (position == selectedPosition && !item.isOutOfPeriod)) {
            numberDayView.setTypeface(Typeface.create(numberDayView.getTypeface(), Typeface.BOLD))
        } else {
            numberDayView.setTypeface(Typeface.create(numberDayView.getTypeface(), Typeface.NORMAL))
        }
    }

    private fun updateDots(numberDayView: NumberDayView, colorList: List<ColorEvent>, yearMonthDay: YearMonthDay) {
        val item = getLastItem(numberDayView)
        if (item.isOutOfPeriod) {
            if (colorList.isNotEmpty()) {
                numberDayView.setDots1(context.resources.getColor(R.color.colorGrayLight))
            } else {
                numberDayView.setDots1(null)
            }
            if (colorList.size > 1) {
                numberDayView.setDots2(context.resources.getColor(R.color.colorGrayLight))
            } else {
                numberDayView.setDots2(null)
            }
            if (colorList.size > 2) {
                numberDayView.setDots3(context.resources.getColor(R.color.colorGrayLight))
            } else {
                numberDayView.setDots3(null)
            }
        } else {
            if (colorList.isNotEmpty()) {
                numberDayView.setDots1(colorList[0].color)
            } else {
                numberDayView.setDots1(null)
            }
            if (colorList.size > 1) {
                numberDayView.setDots2(colorList[1].color)
            } else {
                numberDayView.setDots2(null)
            }
            if (colorList.size > 2) {
                numberDayView.setDots3(colorList[2].color)
            } else {
                numberDayView.setDots3(null)
            }
        }
    }

    private fun getLastItem(numberDayView: NumberDayView): DayCalendarItem {
        return numberDayView.getTag(R.id.TAG_LAST_TEM) as DayCalendarItem
    }

    fun onBeforeReloadList(){

    }

    fun setDateList(dayList: List<DayCalendarItem>) {
        this.dayList = dayList
    }

    fun onFinishedReloadList() {
        if (dayList.isNotEmpty()) {
            debounceColorObserver(dayList)
        }
    }

    private fun debounceColorObserver(dayList: List<DayCalendarItem>) {
        if(isCircleVisible) {
            debounceColorJob?.cancel()
            debounceColorJob = GlobalScope.launch {
                delay(500)
                runBlocking(Dispatchers.Main) {
                    LOG.d("debounceColorObserver $dayList")
                    colorLiveDate?.removeObserver(colorObserver)
                }
                colorLiveDate = viewModel.getColorsDots(
                    YearMonthDay.build(dayList[0].calendar),
                    YearMonthDay.build(dayList[dayList.size - 1].calendar)
                )
                withContext(Dispatchers.Main) {
                    colorLiveDate!!.observe(context as LifecycleOwner, colorObserver)
                }
            }
        }
    }

    fun setSelectedDay(selectedDate: Date) {

        val selectedCalendar = CalendarHelper.getInstance()
        selectedCalendar.time = selectedDate


        val oldPosition = selectedPosition
        var position: Int? = null

        loop@ for (i in dayList.indices) {
            if (CalendarHelper.equalsDayMothYear(dayList[i].calendar, selectedCalendar)) {
                position = i
                break@loop
            }
        }

        selectedPosition = position

        oldPosition?.let { notifyItemChanged(it) }
        selectedPosition?.let { notifyItemChanged(it) }

    }

    fun setVisibleDay(visibleDate: Date) {
        val visibleCalendar = CalendarHelper.getInstance()
        visibleCalendar.time = visibleDate


        val oldPosition = visiblePosition
        var position: Int? = null

        loop@ for (i in dayList.indices) {
            if (CalendarHelper.equalsDayMothYear(dayList[i].calendar, visibleCalendar)) {
                position = i
                break@loop
            }
        }

        visiblePosition = position

        oldPosition?.let { notifyItemChanged(it) }
        visiblePosition?.let { notifyItemChanged(it) }

    }

    private fun notifyItemChanged(position: Int) {
        calendarPeriodView.notifyItemChanged(position)
    }

    private fun notifyAllItems() {
        calendarPeriodView.notifyAllItems()
    }

    private fun injectDagger(context: Context): CalendarNumberAdapterComponent {
        return DaggerCalendarNumberAdapterComponent
            .builder()
            .applicationComponent(getApplicationComponent(context))
            .build()
    }

    private fun getApplicationComponent(context: Context): ApplicationComponent {
        return (context.applicationContext as CalendarApplication).applicationComponent
    }

    private fun initViewModel(
        context: Context,
        viewModelFactory: ViewModelProvider.Factory
    ): CalendarNumberAdapterViewModel {
        return ViewModelProviders.of(context as FragmentActivity, viewModelFactory)
            .get(CalendarNumberAdapterViewModel::class.java)
    }
}