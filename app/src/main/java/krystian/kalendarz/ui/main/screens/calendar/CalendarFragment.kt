package krystian.kalendarz.ui.main.screens.calendar

import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.AnimRes
import androidx.annotation.AnimatorRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import butterknife.BindView
import butterknife.OnClick
import com.google.android.material.navigation.NavigationView
import krystian.kalendarz.R
import krystian.kalendarz.ui.basefragment.BaseFragmentComponent
import krystian.kalendarz.ui.main.activity.MainActivity
import krystian.kalendarz.ui.main.fragment.AbstractMainFragment
import krystian.kalendarz.ui.main.fragment.DaggerMainFragmentComponent
import krystian.kalendarz.ui.span.CustomTypeFaceSpan
import javax.inject.Inject


class CalendarFragment : AbstractMainFragment<CalendarViewModel>(CalendarViewModel::class.java),
    NavigationView.OnNavigationItemSelectedListener {

    companion object {
        fun newInstance(): CalendarFragment {
            return CalendarFragment()
        }
    }

    @BindView(R.id.drawerLayout)
    @JvmField
    var drawerLayout: DrawerLayout? = null

    @BindView(R.id.core)
    @JvmField
    var core: LinearLayout? = null

    @BindView(R.id.toolbar)
    @JvmField
    var toolbar: Toolbar? = null

    @BindView(R.id.navView)
    @JvmField
    var navigationView: NavigationView? = null

    @BindView(R.id.action_month)
    @JvmField
    var actionMonth: TextView? = null

    @BindView(R.id.action_week)
    @JvmField
    var actionWeek: TextView? = null

    @Inject
    lateinit var calendarNavigator: CalendarNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun injectDagger(): BaseFragmentComponent {
        val fragmentComponent = DaggerMainFragmentComponent
            .builder()
            .mainActivityComponent(getMainActivityComponent())
            .build()
        fragmentComponent.inject(this)

        return fragmentComponent
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.calendar_fragment_with_drawer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calendarNavigator.calendarFragment = this

        (activity as MainActivity).setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            activity, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout!!.addDrawerListener(toggle)
        toggle.syncState()

        navigationView!!.setNavigationItemSelectedListener(this)

        viewModel.onViewCreated()

        initObservers()
    }

    private fun initObservers() {
        viewModel.hasMonthVisible.observe(this, Observer { isVisible ->
            if (isVisible) {
                actionMonth!!.setTypeface(actionMonth!!.typeface, Typeface.BOLD)
                actionMonth!!.isSelected = true
                actionWeek!!.setTypeface(actionWeek!!.typeface, Typeface.NORMAL)
                actionWeek!!.isSelected = false
            } else {
                actionMonth!!.setTypeface(actionMonth!!.typeface, Typeface.NORMAL)
                actionMonth!!.isSelected = false
                actionWeek!!.setTypeface(actionWeek!!.typeface, Typeface.BOLD)
                actionWeek!!.isSelected = true
            }
        })
    }

    override fun onBackPressed(): Boolean {
        return if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
            true
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_tools -> {

            }
            R.id.nav_my_calendars -> {
                viewModel.onMyCalendarsClicked()
            }
        }
        drawerLayout?.closeDrawer(GravityCompat.START)
        return true
    }

    @OnClick(R.id.addEvent)
    fun onAddEventClicked() {
        viewModel.onAddEventClicked()
    }

    @OnClick(R.id.action_month)
    fun onActionMonthClicked() {
        viewModel.onActionMonthClicked()
    }

    @OnClick(R.id.action_week)
    fun onActionWeekClicked() {
        viewModel.onActionWeekClicked()
    }

    fun setVisibility(visibility: Int) {
        core?.visibility = visibility
    }

    fun showFragment(fragment: Fragment) {
        showFragmentWithAnimations(fragment, null, null)
    }

    fun showFragmentWithAnimations(
        fragment: Fragment, @AnimatorRes @AnimRes enter: Int?,
        @AnimatorRes @AnimRes exit: Int?
    ) {
        val transaction = childFragmentManager.beginTransaction().apply {
            if (enter != null && exit != null) {
                setCustomAnimations(
                    enter, exit
                )
            }
            replace(R.id.calendar_container, fragment)
            addToBackStack(null)
        }

        transaction.commit()
    }

    private fun applyMenuItemFont(menuItem: MenuItem, typeface: Typeface) {
        val newTitle = SpannableString(menuItem.title)
        newTitle.setSpan(CustomTypeFaceSpan(typeface), 0, newTitle.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        menuItem.title = newTitle
    }
}