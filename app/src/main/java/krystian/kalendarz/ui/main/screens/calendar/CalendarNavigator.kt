package krystian.kalendarz.ui.main.screens.calendar

import android.annotation.SuppressLint
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import krystian.kalendarz.ui.main.screens.calendar.month.MonthFragment
import krystian.kalendarz.ui.main.screens.calendar.week.WeekFragment
import krystian.kalendarz.util.Loggger
import java.util.*

object CalendarNavigator : LifecycleObserver {

    internal val LOG = Loggger(this.javaClass)

    @SuppressLint("StaticFieldLeak")
    var calendarFragment: CalendarFragment? = null
        set(value) {
            LOG.d("setCalendarFragment")
            value?.lifecycle?.addObserver(this)
            field = value
        }

    fun showMonthScreen(showDate: Date) {
        calendarFragment?.showFragment(MonthFragment.newInstance(showDate))
    }

    fun showWeekScreen(showDate: Date) {
        calendarFragment?.showFragment(WeekFragment.newInstance(showDate))
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        LOG.d("onDestroy")
        calendarFragment?.lifecycle?.removeObserver(this)
        calendarFragment = null
    }
}