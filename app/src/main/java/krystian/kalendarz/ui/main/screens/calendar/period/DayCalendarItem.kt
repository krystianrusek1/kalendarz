package krystian.kalendarz.ui.main.screens.calendar.period

import androidx.lifecycle.LiveData
import krystian.kalendarz.ui.main.view.calendar.NumberDayView
import krystian.kalendarz.util.CalendarHelper
import java.util.*

data class DayCalendarItem(
    var calendar: Calendar = CalendarHelper.getInstance(),
    var isOutOfPeriod: Boolean = false,
    var isBeforePeriod: Boolean = false,
    var isAfterPeriod: Boolean = false,
    var isPresentDay: Boolean = false
)