package krystian.kalendarz.ui.main.dialogs.picker

import androidx.lifecycle.MutableLiveData
import krystian.kalendarz.ui.viewmodel.BackPressedViewModel
import krystian.kalendarz.ui.viewmodel.LiveTriggerField
import krystian.kalendarz.util.CalendarHelper
import java.util.*
import javax.inject.Inject

class DateTimePickerViewModel @Inject constructor() : BackPressedViewModel() {

    val isStartDateChecked = MutableLiveData<Boolean>()

    var startCalendar = CalendarHelper.getInstance()

    var endCalendar = CalendarHelper.getInstance()

    var pickerTime = MutableLiveData<Calendar>()

    var pickerTimeVisiblity = MutableLiveData<Boolean>()

    var showPickerMoth = LiveTriggerField<Date>()

    var onChangeDate = MutableLiveData<OnChangeDateModel>()

    var close = LiveTriggerField<Void>()

    data class OnChangeDateModel(val startDate: Date, val endDate: Date)

    fun onCreateStartEndDate(isStartDateFirst: Boolean, startDate: Date, endDate: Date, showTime: Boolean) {
        this.startCalendar.time = startDate
        this.endCalendar.time = endDate
        isStartDateChecked.value = isStartDateFirst
        pickerTimeVisiblity.value = showTime
        onDateCheckedChanged(isStartDateFirst)
    }

    fun onDateCheckedChanged(isStartDate: Boolean) {
        isStartDateChecked.value = isStartDate
        val calendar = getSelectedCalendar()
        pickerTime.value = calendar
        showPickerMoth.value = calendar.time
    }

    fun onDayChanged(date: Date) {
        val calendar = CalendarHelper.getInstance()
        calendar.time = date
        val selectedDate = getSelectedCalendar()
        selectedDate.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE))
    }

    fun onTimeChanged(hourOfDay: Int, minute: Int) {
        val selectedDate = getSelectedCalendar()
        selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay)
        selectedDate.set(Calendar.MINUTE, minute)
    }

    fun onDoneClicked() {
        onChangeDate.value = OnChangeDateModel(startCalendar.time, endCalendar.time)
        close.value = null
    }

    fun goBack() {
        close.value = null
    }

    private fun getSelectedCalendar(): Calendar {
        return if (isStartDateChecked.value == null) {
            CalendarHelper.getInstance()
        } else {
            if (isStartDateChecked.value!!) {
                startCalendar
            } else {
                endCalendar
            }
        }
    }
}