package krystian.kalendarz.ui.main.screens.mycalendars

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import krystian.kalendarz.R
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.ui.main.fragment.AbstractMainFragment
import krystian.kalendarz.ui.main.screens.mycalendars.addeditdialog.AddEditCalendarDialog

class MyCalendarsFragment :
    AbstractMainFragment<MyCalendarsViewModel>(MyCalendarsViewModel::class.java),
    AddEditCalendarDialog.AddEditCalendarDialogListener {

    companion object {

        fun newInstance(): MyCalendarsFragment {
            return MyCalendarsFragment()
        }
    }

    @BindView(R.id.toolbar)
    @JvmField
    var toolbar: Toolbar? = null

    @BindView(R.id.listCalendars)
    @JvmField
    var listCalendars: RecyclerView? = null

    private lateinit var adapter: MyCalendarsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.my_calendars_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar?.setNavigationOnClickListener { viewModel.goBack() }

        initListCalendars()
        viewModel.showAddEditDialog.setObserver(this, Observer { entry ->
            if (entry != null) {
                showAddEditCalendarDialog(entry)
            }
        })
    }

    private fun initListCalendars() {
        listCalendars?.let {
            adapter = MyCalendarsAdapter()
            it.adapter = adapter
            val layoutManager = LinearLayoutManager(context)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            it.layoutManager = layoutManager
        }

        adapter.myCalendarsAdapterListener = object : MyCalendarsAdapter.MyCalendarsAdapterListener {
            override fun onItemCheckboxChecked(isChecked: Boolean, entryModel: EntryModel) {
                viewModel.onItemCheckboxChecked(isChecked, entryModel)
            }

            override fun onItemClicked(entryModel: EntryModel) {
                viewModel.onItemClicked(entryModel)
            }

            override fun onAddItemClicked(addEntryModel: AddEntryModel) {
                viewModel.onAddItemClicked(addEntryModel)
            }

        }

        viewModel.getCalendarList().observe(this, Observer { list ->
            adapter.submitList(list)
        })
    }

    private fun showAddEditCalendarDialog(calendarEntity: CalendarEntity) {
        val fragment = AddEditCalendarDialog.newInstance(calendarEntity)
        fragmentManager?.let {
            fragment.setTargetFragment(this, 0)
            fragment.show(it, null)
        }
    }

    override fun onCalendarChange(calendarEntity: CalendarEntity) {
        viewModel.onCalendarChange(calendarEntity)
    }

    override fun onDelete(calendarEntity: CalendarEntity) {
        viewModel.onDelete(calendarEntity)
    }
}