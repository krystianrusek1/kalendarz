package krystian.kalendarz.ui.main.screens.mycalendars.addeditdialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity

class AddEditCalendarDialog : DialogFragment() {

    companion object {
        fun newInstance(calendarEntity: CalendarEntity): AddEditCalendarDialog {
            val args = Bundle()
            args.putSerializable(CALENDAR_KEY, calendarEntity)

            val fragment = AddEditCalendarDialog()
            fragment.arguments = args
            return fragment
        }

        private const val CALENDAR_KEY = "CALENDAR_KEY"
    }

    private lateinit var calendarBuilder: CalendarEntity.Builder

    private lateinit var colorList: RecyclerView
    private lateinit var colorAdapter: CircleListAdapter

    private lateinit var deleteView: View
    private lateinit var addView: View
    private lateinit var title: EditText

    private val titleChange = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            calendarBuilder.withDisplayName(s.toString())
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.add_edit_calendar_fragment, container, true)
        deleteView = view.findViewById(R.id.delete)
        addView = view.findViewById(R.id.add)
        colorList = view.findViewById(R.id.colorList)
        title = view.findViewById(R.id.title)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initColorList()

        title.addTextChangedListener(titleChange)

        deleteView.setOnClickListener {
            if (targetFragment is AddEditCalendarDialogListener) {
                (targetFragment as AddEditCalendarDialogListener).onDelete(calendarBuilder.build())
            }
            dismiss()
        }

        addView.setOnClickListener {
            if (targetFragment is AddEditCalendarDialogListener) {
                (targetFragment as AddEditCalendarDialogListener).onCalendarChange(calendarBuilder.build())
            }
            dismiss()
        }

        val calendarEntityFormArgs = arguments?.getSerializable(CALENDAR_KEY)
        val calendarEntity = if (calendarEntityFormArgs == null) {
            CalendarEntity.Builder().withAccountName(CalendarEntity.ACCOUNT_LOCAL_NAME).build()
        } else {
            calendarEntityFormArgs as CalendarEntity
        }

        calendarBuilder = calendarEntity.copyBuilder()
        updateCalendar(calendarEntity)
    }

    override fun onDestroy() {
        super.onDestroy()
        title.removeTextChangedListener(titleChange)
    }

    private fun initColorList() {
        colorAdapter = CircleListAdapter(context!!)
        colorAdapter.circleListAdapterListener = object : CircleListAdapter.CircleListAdapterListener {
            override fun onColorChanged(color: Int) {
                calendarBuilder.withColor(color)
            }
        }

        colorList.adapter = colorAdapter
        colorList.layoutManager = GridLayoutManager(context, 3)
    }

    private fun updateCalendar(calendarEntity: CalendarEntity) {
        title.setText(calendarEntity.displayName)
        colorAdapter.setColor(calendarEntity.color)
        deleteView.visibility = if (calendarEntity.id == null) View.GONE else View.VISIBLE
    }

    interface AddEditCalendarDialogListener {
        fun onCalendarChange(calendarEntity: CalendarEntity)
        fun onDelete(calendarEntity: CalendarEntity)
    }
}