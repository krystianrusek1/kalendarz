package krystian.kalendarz.ui.main.view.calendar

import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem

interface CalendarViewListener {
    fun onItemClicked(dayCalendarItem: DayCalendarItem)
}