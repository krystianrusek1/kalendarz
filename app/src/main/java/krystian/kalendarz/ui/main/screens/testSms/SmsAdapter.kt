package krystian.kalendarz.ui.main.screens.testSms

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.util.DateFormatUnit
import java.util.*


class SmsAdapter : RecyclerView.Adapter<SmsAdapter.ViewHolder>() {

    private var smsList: List<SmsEntity> = emptyList()

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val text = view.findViewById<TextView>(R.id.text)
        val tryDate = view.findViewById<TextView>(R.id.tryDate)
        val numberOfTry = view.findViewById<TextView>(R.id.numberOfTry)
        val nextDateOfTry = view.findViewById<TextView>(R.id.nextDateOfTry)
        val dispatchDate = view.findViewById<TextView>(R.id.dispatchDate)
        val sentDate = view.findViewById<TextView>(R.id.sentDate)
        val deliveredDate = view.findViewById<TextView>(R.id.deliveredDate)
        val lastErrorDate = view.findViewById<TextView>(R.id.lastErrorDate)
        val lastErrorCode = view.findViewById<TextView>(R.id.lastErrorCode)
        val isPending = view.findViewById<ProgressBar>(R.id.isPending)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.sms_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val smsEntity = smsList[position]

        holder.text.text = smsEntity.text
        holder.tryDate.text = formatDate(smsEntity.lastTryDate)
        holder.numberOfTry.text = smsEntity.numberOfTry.toString()
        holder.nextDateOfTry.text = formatDate(smsEntity.nextDateOfTry)
        holder.dispatchDate.text = formatDate(smsEntity.dispatchDate)
        holder.sentDate.text = formatDate(smsEntity.sentDate)
        holder.deliveredDate.text = formatDate(smsEntity.deliveredDate)
        holder.lastErrorDate.text = formatDate(smsEntity.lasDateErrorCode)
        holder.lastErrorCode.text = formatErrorCode(smsEntity.lasErrorCode)

        updateProgress(holder, smsEntity)
        updatedBackground(holder, smsEntity)
    }

    private fun updateProgress(holder: ViewHolder, smsEntity: SmsEntity) {
        val visible = if (smsEntity.isPending) View.VISIBLE else View.GONE

        if (holder.isPending.visibility != visible) {
            holder.isPending.visibility = visible
        }
    }

    fun setData(smsList: List<SmsEntity>) {
        this.smsList = smsList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return smsList.size
    }

    @SuppressLint("SimpleDateFormat")
    private fun formatDate(date: Date?): String {
        if (date == null) {
            return "-------"
        } else {
            return DateFormatUnit.formatDate(date)
        }
    }

    private fun formatErrorCode(code: Int?): String {
        if (code == null) {
            return "-------"
        } else {
            return code.toString()
        }
    }

    private fun updatedBackground(holder: ViewHolder, smsEntity: SmsEntity) {
        val nowDate = Date()
        if (smsEntity.dispatchDate.before(nowDate) && !smsEntity.isSent) {
            holder.view.setBackgroundColor(holder.view.context.resources.getColor(R.color.colorOrange))
        } else {
            holder.view.setBackgroundColor(holder.view.context.resources.getColor(R.color.colorTransparent))
        }
    }
}