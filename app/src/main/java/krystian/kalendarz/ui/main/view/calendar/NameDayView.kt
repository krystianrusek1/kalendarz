package krystian.kalendarz.ui.main.view.calendar

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import krystian.kalendarz.R
import krystian.kalendarz.util.Loggger

class NameDayView : LinearLayout {

    private val LOG = Loggger(this.javaClass)

    private val dayOfWeekName: TextView

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {

        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.day_of_week_name, this, true)

        dayOfWeekName = findViewById(R.id.dayOfWeekName)
    }


    fun setText(text: String) {
        dayOfWeekName.text = text
    }

}