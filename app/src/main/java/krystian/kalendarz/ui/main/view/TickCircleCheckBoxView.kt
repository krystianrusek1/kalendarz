package krystian.kalendarz.ui.main.view

import android.content.Context
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import krystian.kalendarz.R

class TickCircleCheckBoxView : FrameLayout {

    private lateinit var circle: View
    private lateinit var tick: View

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {
        val inflater = LayoutInflater.from(context)

        inflater.inflate(R.layout.tick_circle_check_box_view, this, true)

        circle = findViewById(R.id.circle)
        tick = findViewById(R.id.tick)
    }

    var isChecked: Boolean = false
        set(value) {
            field = value
            tick.visibility = if (value) View.VISIBLE else View.GONE
        }

    fun setColor(@ColorInt color: Int) {
        circle.background = createOval(color)
    }

    private fun createOval(@ColorInt color: Int): ShapeDrawable {
        val drawable = ShapeDrawable(OvalShape())
        drawable.paint.color = color
        return drawable
    }
}