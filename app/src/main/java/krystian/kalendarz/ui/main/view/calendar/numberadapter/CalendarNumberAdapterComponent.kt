package krystian.kalendarz.ui.main.view.calendar.numberadapter

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.ui.baseAdapter.AdapterScope
import krystian.kalendarz.ui.viewmodel.ViewModelFactoryModule

@AdapterScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ViewModelFactoryModule::class, CalednarNumberAdapterlModule::class]
)
interface CalendarNumberAdapterComponent {

    fun viewModelFactory(): ViewModelProvider.Factory

    @Component.Builder
    interface Builder {
        fun build(): CalendarNumberAdapterComponent

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}