package krystian.kalendarz.ui.main.view.calendar

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.ui.main.view.calendar.numberadapter.CalendarNumberAdapter
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.Loggger
import java.util.*
import kotlin.collections.ArrayList


class CalendarPeriodView : LinearLayout {

    private val LOG = Loggger(this.javaClass)

    var calendarViewListener: CalendarViewListener? = null

    private var adapter: CalendarNumberAdapter? = null

    private lateinit var nameWeekView: NameWeekView

    private val dayViews = ArrayList<NumberDayView>()

    private val attrs: AttributeSet?
    private val defStyleAttr: Int
    private val defStyleRes: Int

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0
    )

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {

        this.attrs = attrs
        this.defStyleAttr = defStyleAttr
        this.defStyleRes = defStyleRes

        if (isInEditMode) {
            init(true, 6)
        }

    }

    fun init(isCircleVisible: Boolean, numberOfWeeks: Int) {

        adapter = CalendarNumberAdapter(isCircleVisible, this, emptyList())

        val dayNames = CalendarHelper.generateNamesOfDayWeek()
        nameWeekView = NameWeekView(
            context,
            attrs,
            defStyleAttr,
            defStyleRes,
            dayNames
        )
        nameWeekView.layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        nameWeekView.orientation = HORIZONTAL
        addView(nameWeekView)

        for (i in 0 until numberOfWeeks) {
            val view =
                NumberWeekView(
                    context,
                    attrs,
                    defStyleAttr,
                    defStyleRes,
                    adapter
                )
            view.layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            view.orientation = HORIZONTAL
            dayViews.addAll(view.dayViews)
            addView(view)
        }
    }

    fun setData(dayList: List<DayCalendarItem>) {
        if (dayViews.size != dayList.size) {
            if (dayViews.size == 0) {
                throw NullPointerException(" Please use the init(numberOfWeeks: Int)")
            }

            throw ArrayIndexOutOfBoundsException(
                String.format(
                    "length is not the same dayList:%1s dayViews:%2s",
                    dayList.size,
                    dayViews.size
                )
            )
        }

        adapter?.let { adapter ->
            adapter.onBeforeReloadList()
            adapter.setDateList(dayList)
            dayViews.forEachIndexed { index, view ->
                view.setOnClickListener { calendarViewListener?.onItemClicked(dayList[index]) }
                adapter.onBind(view, index)
            }
            adapter.onFinishedReloadList()
        }
    }

    fun setSelectedDay(selectedDate: Date) {
        adapter?.setSelectedDay(selectedDate)
    }

    fun setVisibleDay(visibleDate: Date) {
        adapter?.setVisibleDay(visibleDate)
    }

    fun notifyItemChanged(position: Int) {
        adapter?.onBind(dayViews[position], position)
    }

    fun notifyAllItems() {
        adapter?.let { adapter ->
            dayViews.forEachIndexed { index, view ->
                adapter.onBind(view, index)
            }
        }
    }
}