package krystian.kalendarz.ui.main.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.util.AttributeSet
import android.widget.ImageView
import androidx.annotation.ColorInt
import krystian.kalendarz.R


class CircleCheckBoxView : ImageView {

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes){
        if (isInEditMode){
            setFullCircle(Color.BLUE)
        }
    }

    @ColorInt
    private var color: Int = Color.BLACK

    var isChecked = false
        private set(value) {
            field = value
        }

    fun setChecked(isChecked: Boolean, @ColorInt color: Int) {
        this.isChecked = isChecked
        this.color = color
        updateCircle(isChecked, color)
    }

    fun updateChecked(isChecked: Boolean) {
        this.isChecked = isChecked
        updateCircle(isChecked, color)
    }

    private fun updateCircle(isChecked: Boolean, @ColorInt color: Int) {
        if (isChecked) {
            setFullCircle(color)
        } else {
            setStokeCircle(color)
        }
    }

    private fun setFullCircle(@ColorInt color: Int) {
        val drawable = resources.getDrawable(R.drawable.full_circle)
        when (drawable) {
            is ShapeDrawable -> drawable.paint.color = color
            is GradientDrawable -> drawable.setColor(color)
            is ColorDrawable -> drawable.color = color
        }
        setImageDrawable(drawable)
    }

    private fun setStokeCircle(@ColorInt color: Int) {
        val drawable = resources.getDrawable(R.drawable.storke_circle)
        when (drawable) {
            is GradientDrawable -> drawable.setStroke(
                resources.getDimension(R.dimen.stroke_circle_width).toInt(),
                color
            )
        }
        setImageDrawable(drawable)
    }

}
