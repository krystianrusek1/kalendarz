package krystian.kalendarz.ui.main.view.calendar

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout
import krystian.kalendarz.ui.main.view.calendar.numberadapter.CalendarNumberAdapter
import krystian.kalendarz.util.Loggger
import java.util.*


class NumberWeekView : LinearLayout {

    private val LOG = Loggger(this.javaClass)

    val dayViews = ArrayList<NumberDayView>()

    private var calendarNumberAdapter: CalendarNumberAdapter? = null

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(
        context,
        attrs,
        defStyleAttr,
        0, null
    )

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int,
        calendarNumberAdapter: CalendarNumberAdapter?
    ) : super(context, attrs, defStyleAttr) {

        this.calendarNumberAdapter = calendarNumberAdapter

        for (i in 0 until 7) {
            val view = NumberDayView(
                context,
                attrs,
                defStyleAttr,
                defStyleRes
            )
            view.layoutParams = LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f)
            dayViews.add(view)
            addView(view)
            calendarNumberAdapter?.onCreate(view)
        }
    }

}