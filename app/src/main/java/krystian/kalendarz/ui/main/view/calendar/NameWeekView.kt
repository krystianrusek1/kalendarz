package krystian.kalendarz.ui.main.view.calendar

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout
import krystian.kalendarz.util.Loggger


class NameWeekView : LinearLayout {

    private val LOG = Loggger(this.javaClass)

    val nameViews = ArrayList<NameDayView>()

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int)
            : this(context, attrs, defStyleAttr, defStyleRes, ArrayList<String>())

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int,
        namesOfWeek: List<String>
    ) : super(context, attrs, defStyleAttr, defStyleRes) {

        for (i in 0..6) {
            val view = NameDayView(
                context,
                attrs,
                defStyleAttr,
                defStyleRes
            )
            view.layoutParams = LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f)
            if (namesOfWeek.size > i) {
                view.setText(namesOfWeek[i])
            }
            nameViews.add(view)
            addView(view)
        }
    }

}