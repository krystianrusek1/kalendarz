package krystian.kalendarz.ui.main.dialogs.picker

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import krystian.kalendarz.ui.main.dialogs.picker.month.MonthPickerViewModel
import krystian.kalendarz.ui.viewmodel.ViewModelKey

@Module
interface PickerDialogViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(DateTimePickerViewModel::class)
    fun bindDateTimePickerViewModel(viewModel: DateTimePickerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MonthPickerViewModel::class)
    fun bindMonthCalendarPickerViewModel(viewModel: MonthPickerViewModel): ViewModel
}