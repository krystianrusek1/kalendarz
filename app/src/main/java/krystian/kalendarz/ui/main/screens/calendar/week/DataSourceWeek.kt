package krystian.kalendarz.ui.main.screens.calendar.week

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.screens.calendar.period.DayCalendarItem
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.DateFormatUnit
import krystian.kalendarz.util.Loggger
import java.util.*

class DataSourceWeek(
    private val numberOfWeeks: Int,
    private val startDate: Date
) :
    DataSource.Factory<Date, CalendarPeriodModel>() {

    private val LOG = Loggger(this.javaClass)

    private val sourceLiveData = MutableLiveData<PageDataSourceWeek>()

    private var pageDataSourceMoth: PageDataSourceWeek? = null

    override fun create(): PageDataSourceWeek {
        pageDataSourceMoth = PageDataSourceWeek(
            numberOfWeeks,
            startDate
        )
        sourceLiveData.postValue(pageDataSourceMoth)
        return pageDataSourceMoth!!
    }

    class PageDataSourceWeek(
        private val numberOfWeeks: Int,
        private val startDate: Date
    ) :
        PageKeyedDataSource<Date, CalendarPeriodModel>() {

        private val LOG = Loggger(this.javaClass)

        override fun loadInitial(
            params: LoadInitialParams<Date>,
            callback: LoadInitialCallback<Date, CalendarPeriodModel>
        ) {
            runBlocking {
                val monthModel = createMonthModel(startDate)
                callback.onResult(monthModel, previousDate(startDate), nextDate(startDate))
            }
        }

        override fun loadAfter(params: LoadParams<Date>, callback: LoadCallback<Date, CalendarPeriodModel>) {
            runBlocking {
                val monthModel = createMonthModel(params.key)
                callback.onResult(monthModel, nextDate(params.key))
            }
        }

        override fun loadBefore(params: LoadParams<Date>, callback: LoadCallback<Date, CalendarPeriodModel>) {
            runBlocking {
                val monthModel = createMonthModel(params.key)
                callback.onResult(monthModel, previousDate(params.key))
            }
        }

        private fun nextDate(date: Date): Date {
            val calendar = CalendarHelper.getInstance()
            calendar.time = date
            calendar.add(Calendar.WEEK_OF_YEAR, 1)
            return calendar.time
        }

        private fun previousDate(date: Date): Date {
            val calendar = CalendarHelper.getInstance()
            calendar.time = date
            calendar.add(Calendar.WEEK_OF_YEAR, -1)
            return calendar.time
        }

        private fun createMonthModel(date: Date): List<CalendarPeriodModel> {
            val list = createCalendarItems(date)
            val name = DateFormatUnit.format2MonthYear(list[0].calendar.time, list[list.size - 1].calendar.time)
            val monthModel = CalendarPeriodModel(name, list)

            return listOf(monthModel)
        }

        private fun createCalendarItems(showDate: Date): List<DayCalendarItem> {
            val list = ArrayList<DayCalendarItem>()
            val calendar = CalendarHelper.getInstance()
            calendar.time = showDate

            CalendarHelper.generateWeeksFrom(numberOfWeeks, showDate).forEach {
                val item = DayCalendarItem()
                item.calendar = it
                item.isPresentDay = CalendarHelper.equalsDayMothYear(item.calendar, CalendarHelper.getInstance())
                list.add(item)
            }

            return list
        }
    }
}