package krystian.kalendarz.ui.main.dialogs.picker.month

import androidx.lifecycle.MutableLiveData
import krystian.kalendarz.ui.main.screens.calendar.month.AbstractMonthViewModel
import krystian.kalendarz.ui.main.screens.calendar.month.DataSourceMoth
import java.util.*
import javax.inject.Inject

class MonthPickerViewModel @Inject constructor() :
    AbstractMonthViewModel() {

    override fun createDataSourceMoth(showDate: Date): DataSourceMoth {
        return DataSourceMoth(NUMBER_OF_WEEKS, showDate)
    }

    private val selectedDate = MutableLiveData<Date>()

    override fun onShowDateChange(showDate: Date) {
        super.onShowDateChange(showDate)
        selectedDate.value = showDate
    }

    override fun getSelectedDate(): MutableLiveData<Date> {
        return selectedDate
    }

}