package krystian.kalendarz.ui.main.screens.calendar.month

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import krystian.kalendarz.data.calendar.CalendarRepository
import krystian.kalendarz.ui.main.activity.MainNavigator
import krystian.kalendarz.ui.main.screens.calendar.CalendarNavigator
import krystian.kalendarz.ui.main.screens.calendar.period.CalendarPeriodModel
import krystian.kalendarz.ui.main.view.daydetails.EventAdapter
import krystian.kalendarz.ui.main.view.daydetails.TransformationDayEventModel
import krystian.kalendarz.util.CalendarHelper
import java.util.*
import javax.inject.Inject

class MonthViewModel @Inject constructor(
    private val appNavigator: MainNavigator,
    private val calendarNavigator: CalendarNavigator,
    private val calendarRepository: CalendarRepository,
    private val resources: Resources
) :
    AbstractMonthViewModel() {

    private val transformationDayEventModel = TransformationDayEventModel(calendarRepository, resources)

    data class DayModel(val day: Date, val listEvent: List<EventAdapter.EventModel>)

    override fun createDataSourceMoth(showDate: Date): DataSourceMoth {
        return DataSourceMoth(
            NUMBER_OF_WEEKS,
            showDate
        )
    }

    override fun getSelectedDate(): MutableLiveData<Date> {
        return calendarRepository.getSelectedDate()
    }

    override fun onMonthListChanged(calendarPeriodModel: CalendarPeriodModel?) {
        calendarPeriodModel?.let { calendarPeriod ->
            calendarRepository.getVisibleDate().value?.let { visibleDate ->
                val dayVisible = CalendarHelper.setDayToMonth(visibleDate, calendarPeriod.dayList[10].calendar.time)
                getSelectedDate().value?.let { selectedDay ->
                    if (CalendarHelper.areSameMonth(dayVisible, selectedDay)) {
                        getVisibleDate().value = selectedDay
                    } else {
                        getVisibleDate().value = dayVisible
                    }
                }
            }
        }
    }

    fun getVisibleDate(): MutableLiveData<Date> {
        return calendarRepository.getVisibleDate()
    }

    fun getDayEvents(): LiveData<DayModel> {
        return Transformations.map(getEvents(getVisibleDate())) { eventList ->
            getVisibleDate().value?.let {
                DayModel(it, eventList)
            }
        }
    }

    private fun getEvents(day: MutableLiveData<Date>): LiveData<List<EventAdapter.EventModel>> {
        return transformationDayEventModel.getEvents(day)
    }

    fun onEventModelClicked(eventModel: EventAdapter.EventModel) {
        GlobalScope.launch {
            val event = calendarRepository.getEvent(eventModel.eventId)
            event?.let { appNavigator.showAddOrUpdateEventScreen(it) }
        }
    }

}