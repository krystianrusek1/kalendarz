package krystian.kalendarz.ui.main.activity

import androidx.lifecycle.ViewModelProvider
import dagger.Component
import krystian.kalendarz.aplication.AppLiveCycle
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.ui.baseActivity.ActivityScope
import krystian.kalendarz.ui.main.fragment.MainViewModelModule
import krystian.kalendarz.ui.main.screens.calendar.CalendarNavigator
import krystian.kalendarz.ui.viewmodel.ViewModelFactoryModule

@ActivityScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [MainActivityModule::class, ViewModelFactoryModule::class, MainViewModelModule::class]
)
interface MainActivityComponent {

    fun viewModelProvider(): ViewModelProvider.Factory

    fun appLiveCycle(): AppLiveCycle

    fun mainNavigator(): MainNavigator

    fun calendarNavigator(): CalendarNavigator

    fun inject(mainActivity: MainActivity)

    @Component.Builder
    interface Builder {
        fun build(): MainActivityComponent

        fun mainActivityModule(mainActivityModule: MainActivityModule): Builder

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}