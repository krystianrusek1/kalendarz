package krystian.kalendarz.ui.main.screens.calendar.week

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import krystian.kalendarz.R
import krystian.kalendarz.ui.main.view.daydetails.DayEventsView
import krystian.kalendarz.ui.main.view.daydetails.EventAdapter
import java.util.*

class DayAdapter() :
    PagedListAdapter<DayAdapter.DayModel, DayAdapter.HolderView>(
        DIFF_CALLBACK
    ) {

    interface EventListener {
        fun onEventClocked(eventModel: EventAdapter.EventModel)
    }

    var eventListener: EventListener? = null

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<DayModel>() {
            override fun areItemsTheSame(oldItem: DayModel, newItem: DayModel) =
                oldItem == newItem

            override fun areContentsTheSame(
                oldItem: DayModel, newItem: DayModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    data class DayModel(val day: Date, val events: LiveData<List<EventAdapter.EventModel>>)

    inner class HolderView(val dayEventsView: DayEventsView) : RecyclerView.ViewHolder(dayEventsView) {
        var listEventsLive: LiveData<List<EventAdapter.EventModel>>? = null
        val listEventsObserver = Observer<List<EventAdapter.EventModel>> { events -> dayEventsView.setEvents(events) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderView {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.week_day_adapter, parent, false) as DayEventsView
        return HolderView(view)
    }

    override fun onBindViewHolder(holder: HolderView, position: Int) {
        getItem(position)?.let {
            holder.dayEventsView.updateDayName(it.day)

            holder.dayEventsView.setEvents(emptyList())

            if (holder.listEventsLive != null) {
                holder.listEventsLive!!.removeObserver(holder.listEventsObserver)
                holder.dayEventsView.setEvents(emptyList())
            }

            holder.listEventsLive = it.events
            val context = holder.dayEventsView.context
            if (context is LifecycleOwner) {
                holder.listEventsLive?.observe(context, holder.listEventsObserver)
            }

            holder.dayEventsView.setEventAdapterListener(object :EventAdapter.EventAdapterListener{
                override fun onEventClicked(eventModel: EventAdapter.EventModel) {
                    eventListener?.onEventClocked(eventModel)
                }
            })
        }
    }

    fun getDayModel(position: Int): DayModel? {
        if (itemCount > position) {
            return super.getItem(position)
        }
        return null
    }
}