package krystian.kalendarz.logic.sms

import kotlinx.coroutines.delay

class TestClass(private val testclass2: Testclass2){

    var ifChange  = false

    suspend fun triggerAll(){
        trigger1()
        trigger2()
    }

    suspend fun trigger1(){
        val string = getData1()
        testclass2.sendString(string)
    }

    suspend fun trigger2(){
        val string = getData2()
        testclass2.sendString(string)
        ifChange = true
    }

    suspend fun getData1():String{
        delay(5000)
        if(ifChange){
            return "ttttt"
        } else{
            return "text1"
        }
    }

    suspend fun getData2():String{
        delay(5)
        return "text2"
    }
}

class Testclass2{

    fun sendString(string: String){

    }
}