package krystian.kalendarz.logic.sms

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import krystian.kalendarz.aplication.ApplicationComponent
import krystian.kalendarz.aplication.CalendarApplication
import krystian.kalendarz.util.Loggger

@WorkerScope
class RefreshWorker(context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters) {
    private val LOG = Loggger(this.javaClass)
    override fun doWork(): Result {
        LOG.d("trigger requestSync from RefreshWorker")
        getSyncSmsManagerImpl(applicationContext).onPerformSync()
        return Result.success()
    }

    private fun getSyncSmsManagerImpl(context: Context): SyncSmsManagerImpl {
        return inject(context).syncSmsManager() as SyncSmsManagerImpl
    }

    private fun inject(context: Context): WorkerComponent {
        return DaggerWorkerComponent.builder().applicationComponent(getApplicationComponent(context)).build()
    }

    private fun getApplicationComponent(context: Context): ApplicationComponent {
        return (context.applicationContext as CalendarApplication).applicationComponent
    }
}