package krystian.kalendarz.logic.sms

interface SyncSmsManager {

    fun onPerformSync()
}