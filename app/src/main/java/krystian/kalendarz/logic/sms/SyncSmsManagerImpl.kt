package krystian.kalendarz.logic.sms

import android.app.PendingIntent
import android.content.ContentResolver
import android.os.Bundle
import android.telephony.SmsManager
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import krystian.kalendarz.aplication.AppLiveCycle
import krystian.kalendarz.aplication.PendingIntentFactory
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.data.sms.SendSMSRepository
import krystian.kalendarz.service.authenticator.AppAccountManager
import krystian.kalendarz.util.Loggger
import org.joda.time.Minutes
import org.joda.time.Seconds
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

const val SECONDS_PER_MINUTE = 60L
const val SYNC_INTERVAL_IN_MINUTES = 60L
const val SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE

@Singleton
class SyncSmsManagerImpl @Inject constructor(
    private val appAccountManager: AppAccountManager,
    private val sendSMSRepository: SendSMSRepository,
    private val pendingIntentFactory: PendingIntentFactory,
    private val smsManager: SmsManager,
    private val workManager: WorkManager,
    private val appLiveCycle: AppLiveCycle
) : SyncSmsManager, AppLiveCycle.AppLiveCycleListener {

    private val mutex = Mutex()

    companion object {
        /**
         * Contains the dates of next try send if first time mobile phone can not send sms.
         * Date is added to [SmsEntity.dispatchDate], according to the try.
         */
        val listDatesOfTry = arrayListOf(
            Seconds.seconds(10).toStandardDuration().millis,
            Seconds.seconds(15).toStandardDuration().millis,
            Seconds.seconds(15).toStandardDuration().millis,
            Seconds.seconds(15).toStandardDuration().millis,
            Seconds.seconds(15).toStandardDuration().millis,
            Minutes.minutes(1).toStandardDuration().millis,
            Minutes.minutes(2).toStandardDuration().millis,
            Minutes.minutes(3).toStandardDuration().millis,
            Minutes.minutes(5).toStandardDuration().millis,
            Minutes.minutes(8).toStandardDuration().millis,
            Minutes.minutes(13).toStandardDuration().millis,
            Minutes.minutes(27).toStandardDuration().millis,
            Minutes.minutes(60).toStandardDuration().millis,
            Minutes.minutes(120).toStandardDuration().millis
        )
    }

    internal val LOG = Loggger(this.javaClass)

    init {
        LOG.d("init")
        ContentResolver.addPeriodicSync(
            appAccountManager.getAccount(),
            AppAccountManager.AUTHORITY,
            Bundle.EMPTY,
            SYNC_INTERVAL
        )

        ContentResolver.requestSync(
            appAccountManager.getAccount(),
            AppAccountManager.AUTHORITY,
            Bundle()
        )

        sendSMSRepository.getSmsList()
            .observeForever { sms ->
                LOG.d("trigger requestSync from sendSMSRepository")
                onPerformSync()
            }

        appLiveCycle.addListener(this)
    }

    override fun onResume() {
        LOG.d("onResume")
        onPerformSync()
    }

    override fun onPause() {

    }

    override fun onPerformSync() {
        LOG.d("onPerformSync")

        GlobalScope.launch {
            mutex.withLock {
                LOG.d("onPerformSync sycnhronized")
                refreshReadyToSentSms()
                refreshScheduleList()
            }
        }

    }

    suspend fun onSmsSent(sms: SmsEntity, errorCode: Int?) {
        val smsToReplace = sendSMSRepository.getSms(sms.id!!)
        if (smsToReplace != null) {

            smsToReplace.isPending = false
            if (errorCode == null) {
                smsToReplace.isSent = true
                smsToReplace.sentDate = Date()
            } else {
                smsToReplace.lasDateErrorCode = Date()
                smsToReplace.lasErrorCode = errorCode
            }
            LOG.d("onSmsSent $smsToReplace")
            sendSMSRepository.addOrUpdateSms(smsToReplace)
        } else {
            LOG.w("Message id:${sms.id} can not be save as sent, because It does not exist in database")
        }
    }

    suspend fun onSmsDelivered(sms: SmsEntity, errorCode: Int?) {
        val smsToReplace = sendSMSRepository.getSms(sms.id!!)
        if (smsToReplace != null) {

            if (errorCode == null) {
                smsToReplace.isDelivered = true
                smsToReplace.deliveredDate = Date()
            } else {
                smsToReplace.lasDateErrorCode = Date()
                smsToReplace.lasErrorCode = errorCode
            }
            LOG.d("onSmsDelivered $smsToReplace")
            sendSMSRepository.addOrUpdateSms(smsToReplace)
        } else {
            LOG.w("Message id:${sms.id} can not be save as delivered, because It does not exist in database")
        }
    }

    /**
     * Methods trigger send sms-es which has not sent up now.
     */
    internal suspend fun refreshReadyToSentSms() {
        LOG.d("refreshReadyToSentSms")
        sendSMSRepository.getSmsReadyToSendList(Date(), listDatesOfTry.size).forEach {
            sendMessage(it)
        }
    }

    private fun calculateNextTry(sms: SmsEntity): Date {
        val numberOfTray = sms.numberOfTry - 1

        var nextDateTry = Date(sms.nextDateOfTry.time)
        if (sms.lastTryDate != null) {
            nextDateTry = Date(sms.lastTryDate!!.time)
        }

        if (listDatesOfTry.size > numberOfTray && numberOfTray > -1) {
            val defData = listDatesOfTry[numberOfTray]
            nextDateTry = Date(nextDateTry.time + defData)
        }


        LOG.d("calculateNextTry id: ${sms.id} Date: ${nextDateTry} ")
        return nextDateTry
    }

    private suspend fun sendMessage(sms: SmsEntity) {
        sms.isPending = true
        sms.lastTryDate = Date()
        sms.numberOfTry = sms.numberOfTry + 1
        sms.nextDateOfTry = calculateNextTry(sms)
        sendSMSRepository.addOrUpdateSmsBlocking(sms)

        val textList = smsManager.divideMessage(sms.text)
        val sentIntentList = ArrayList<PendingIntent>()
        val deliveryIntentList = ArrayList<PendingIntent>()

        for (i in 0 until textList.size) {
            sentIntentList.add(pendingIntentFactory.createSentSmsBroadcastReceiver(sms))
            deliveryIntentList.add(pendingIntentFactory.createDeliverySmsBroadcastReceiver(sms))
        }

        LOG.d("sendMessage $sms")
        smsManager.sendMultipartTextMessage(sms.phoneNumber, null, textList, sentIntentList, deliveryIntentList)
    }

    internal suspend fun refreshScheduleList() {
        LOG.d("refreshScheduleList")
        sendSMSRepository.getSmsToScheduleList(Date(), listDatesOfTry.size).forEach {
            scheduleSms(it)
        }
    }

    private fun scheduleSms(sms: SmsEntity) {
        LOG.d("scheduleSms $sms")
        val defDuration = sms.nextDateOfTry.time - Date().time + 1000
        LOG.d("scheduleSms defDuration = $defDuration")

        val workRequest = OneTimeWorkRequestBuilder<RefreshWorker>()
            .setInitialDelay(defDuration, TimeUnit.MILLISECONDS)
            .addTag("sms ${sms.id}")
            .build()
        workManager.enqueueUniqueWork("sms ${sms.id}", ExistingWorkPolicy.KEEP, workRequest)
    }
}