package krystian.kalendarz.logic.sms

import dagger.Component
import krystian.kalendarz.aplication.ApplicationComponent

@WorkerScope
@Component(
    dependencies = [ApplicationComponent::class]
)
interface WorkerComponent {

    fun syncSmsManager(): SyncSmsManager

    @Component.Builder
    interface Builder {
        fun build(): WorkerComponent

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder
    }
}