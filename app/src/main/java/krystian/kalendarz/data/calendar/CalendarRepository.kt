package krystian.kalendarz.data.calendar

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import krystian.kalendarz.data.externalcalendar.attendee.AttendeeEntity
import krystian.kalendarz.data.externalcalendar.attendee.AttendeeLiveDate
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import java.util.*

interface CalendarRepository {

    fun getVisibleDate(): MutableLiveData<Date>

    fun getSelectedDate(): MutableLiveData<Date>

    suspend fun getLastChosenCalendar(): CalendarEntity?

    fun saveLastChosenCalendar(calendarEntity: CalendarEntity)

    suspend fun getAllCalendar(accountName: String): List<CalendarEntity>

    suspend fun getAllCalendar(calendarId: Long): CalendarEntity?

    suspend fun insertOrUpdateCalendar(calendarEntity: CalendarEntity): Long

    suspend fun insertOrUpdateEvent(eventEntity: EventEntity): Long

    fun getCalendarList(): LiveData<List<CalendarEntity>>

    fun getOnlyVisibleCalendarList(): LiveData<List<CalendarEntity>>

    suspend fun getCalendarListByCoroutines(): List<CalendarEntity>

    fun getEventList(): LiveData<List<EventEntity>>

    suspend fun getEvent(eventId: Long): EventEntity?

    fun getEventListByDescription(description: String): LiveData<List<EventEntity>>

    fun fetchEventListRecursiveByLiveDate(fromDate: Date, toDate: Date): LiveData<List<EventEntity>>

    suspend fun fetchEventListRecursiveByCoroutines(fromDate: Date, toDate: Date): List<EventEntity>

    fun fetchColorGroupedByCalendarIdByLiveDate(fromDate: Date, toDate: Date, limit: Int): LiveData<List<ColorEvent>>

    suspend fun fetchColorGroupedByCalendarIdByCoroutines(fromDate: Date, toDate: Date, limit: Int): List<ColorEvent>

    fun getColors(
        from: YearMonthDay,
        to: YearMonthDay,
        limit: Int
    ): LiveData<ColorsDiffModel>

    fun getColorsPerDayHasMap(): HashMap<YearMonthDay, List<ColorEvent>>

    fun getColorsPerDayHasMapLiveData(): LiveData<YearMonthDay>

    suspend fun deleteEvent(id: Long)

    suspend fun deleteEvent(eventEntity: EventEntity)

    suspend fun deleteCalendar(id: Long)

    suspend fun deleteCalendar(calendarEntity: CalendarEntity)

    suspend fun insertOrUpdateAttendee(attendeeEntity: AttendeeEntity): Long

    fun getAttendeeListLiveDate(eventEntityId: Long): AttendeeLiveDate

    suspend fun deleteAttendee(attendeeEntity: AttendeeEntity)

    suspend fun deleteAttendeeByEventId(eventId: Long)

}