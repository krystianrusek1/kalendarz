package krystian.kalendarz.data.calendar

data class ColorsDiffModel(val periodTimeColorsModel: PeriodTimeColorsModel, val listDiff: List<YearMonthDay>) {
}