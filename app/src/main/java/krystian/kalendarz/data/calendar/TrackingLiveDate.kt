package krystian.kalendarz.data.calendar

import androidx.lifecycle.MutableLiveData

class TrackingLiveDate<T>(private val container: ActiveLiveDateContainer<T>) : MutableLiveData<T>() {

    var activeObserver: ActiveObserver? = null
        set(value) {
            field = value
            if (this.hasActiveObservers()) {
                activeObserver?.onActive()
            }
        }

    override fun onActive() {
        super.onActive()
        container.onActive(this)
        activeObserver?.onActive()
    }

    override fun onInactive() {
        super.onInactive()
        container.onInactive(this)
    }

    interface ActiveObserver {
        fun onActive()
    }
}