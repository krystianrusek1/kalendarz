package krystian.kalendarz.data.calendar

import androidx.lifecycle.MutableLiveData
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class ActiveLiveDateContainer<T> {

    val liveDateActive: MutableSet<MutableLiveData<T>> = Collections.newSetFromMap(
        ConcurrentHashMap()
    )

    fun create(): TrackingLiveDate<T> {
        return TrackingLiveDate(this)
    }

    internal fun onActive(liveData: TrackingLiveDate<T>) {
        liveDateActive.add(liveData)
    }

    internal fun onInactive(liveData: TrackingLiveDate<T>) {
        liveDateActive.remove(liveData)
    }
}