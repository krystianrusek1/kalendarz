package krystian.kalendarz.data.calendar

import krystian.kalendarz.util.CalendarHelper
import java.util.*

data class YearMonthDay(val year: Int, val month: Int, val day: Int) {

    fun toDate(): Date {
        return CalendarHelper.prepareDate(year, month, day)
    }

    fun toCalendar(): Calendar {
        val calendar = CalendarHelper.getInstance()
        calendar.set(year, month, day)
        return calendar
    }

    companion object {
        fun build(calendar: Calendar): YearMonthDay {
            return YearMonthDay(calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DATE])
        }
    }
}