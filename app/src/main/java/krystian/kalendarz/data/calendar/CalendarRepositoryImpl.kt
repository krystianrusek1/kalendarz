package krystian.kalendarz.data.calendar

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.data.externalcalendar.ExternalDataBase
import krystian.kalendarz.data.externalcalendar.attendee.AttendeeEntity
import krystian.kalendarz.data.externalcalendar.attendee.AttendeeLiveDate
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.data.externalcalendar.event.ColorEventMapper
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.data.externalcalendar.event.ExternalEventDataBaseListener
import krystian.kalendarz.data.storage.LocalStorage
import krystian.kalendarz.permissions.PermissionHelper
import krystian.kalendarz.permissions.PermissionManager
import krystian.kalendarz.util.CalendarHelper
import krystian.kalendarz.util.Loggger
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


@Singleton
class CalendarRepositoryImpl @Inject constructor(
    private val context: Context,
    private val externalDataBase: ExternalDataBase,
    private val permissionManager: PermissionManager,
    private val localStorage: LocalStorage
) : CalendarRepository {

    internal val LOG = Loggger(this.javaClass)

    private val selectedDate = MutableLiveData<Date>()

    private var visibleDate = MutableLiveData<Date>()

    private val liveDataColorsHasMap =
        HashMap<PeriodTimeColorsModel, MutableLiveData<ColorsDiffModel>>()

    private val colorsPerDayHasMap = HashMap<YearMonthDay, List<ColorEvent>>()

    private val colorsPerDayHasMapLiveData = MutableLiveData<YearMonthDay>()

    private val colorsContainer = ActiveLiveDateContainer<ColorsDiffModel>()

    init {

        permissionManager.addPermissionManagerListener(object : PermissionManager.PermissionManagerListener {
            override fun onPermissionChange() {
                initObserveOnTheExternalDatabase()
            }
        })

        selectedDate.observeForever { date -> visibleDate.value = date }
        selectedDate.value = Date()
    }

    private fun initObserveOnTheExternalDatabase() {
        if (PermissionHelper.isCalendarGranted(context)) {
            externalDataBase.event().setExternalEventDataBaseListener(object : ExternalEventDataBaseListener {
                override fun onEventsUpdated() {
                    onExternalEventsUpdated()
                }
            })
        }
    }

    override fun getVisibleDate(): MutableLiveData<Date> {
        return visibleDate
    }

    override fun getSelectedDate(): MutableLiveData<Date> {
        return selectedDate
    }

    override suspend fun getLastChosenCalendar(): CalendarEntity? {
        val lastCalendarId = localStorage.getLong(LocalStorage.KEY_LAST_CHOSEN_CALENDAR_ID, -1)
        return if (lastCalendarId == -1L) {
            null
        } else {
            externalDataBase.calendar().getItem(lastCalendarId, true)
        }
    }

    override fun saveLastChosenCalendar(calendarEntity: CalendarEntity) {
        calendarEntity.id?.let { localStorage.putLong(LocalStorage.KEY_LAST_CHOSEN_CALENDAR_ID, it) }
    }

    override fun getOnlyVisibleCalendarList(): LiveData<List<CalendarEntity>> {
        return externalDataBase.calendar().getList(true)
    }

    override suspend fun getAllCalendar(accountName: String): List<CalendarEntity> {
        return externalDataBase.calendar().getItem(accountName)
    }

    override suspend fun getAllCalendar(calendarId: Long): CalendarEntity? {
        return externalDataBase.calendar().getItem(calendarId, false)
    }

    override suspend fun insertOrUpdateCalendar(calendarEntity: CalendarEntity): Long {
        return externalDataBase.calendar().insertOrUpdate(calendarEntity)
    }

    override suspend fun insertOrUpdateEvent(eventEntity: EventEntity): Long {
        return externalDataBase.event().insertOrUpdate(eventEntity)
    }

    override fun getCalendarList(): LiveData<List<CalendarEntity>> {
        return externalDataBase.calendar().getList(false)
    }

    override suspend fun getCalendarListByCoroutines(): List<CalendarEntity> {
        return externalDataBase.calendar().getListByCoroutines()
    }

    override fun getEventList(): LiveData<List<EventEntity>> {
        return externalDataBase.event().getList()
    }

    override suspend fun getEvent(eventId: Long): EventEntity? {
        return externalDataBase.event().getEvent(eventId)
    }

    override fun getEventListByDescription(description: String): LiveData<List<EventEntity>> {
        return externalDataBase.event().getListByDescription(description)
    }

    override fun fetchEventListRecursiveByLiveDate(fromDate: Date, toDate: Date): LiveData<List<EventEntity>> {
        return externalDataBase.event().fetchEventListRecursiveByLiveDate(fromDate, toDate)
    }

    override suspend fun fetchEventListRecursiveByCoroutines(fromDate: Date, toDate: Date): List<EventEntity> {
        return externalDataBase.event().fetchEventListRecursiveByCoroutines(fromDate, toDate)
    }

    override fun fetchColorGroupedByCalendarIdByLiveDate(
        fromDate: Date,
        toDate: Date,
        limit: Int
    ): LiveData<List<ColorEvent>> {
        return externalDataBase.event().fetchColorGroupedByCalendarIdByLiveDate(fromDate, toDate, limit)
    }

    override suspend fun fetchColorGroupedByCalendarIdByCoroutines(
        fromDate: Date,
        toDate: Date,
        limit: Int
    ): List<ColorEvent> {
        return externalDataBase.event().fetchColorGroupedByCalendarIdByCoroutines(fromDate, toDate, limit)
    }

    override fun getColors(
        from: YearMonthDay, to: YearMonthDay, limit: Int
    ): LiveData<ColorsDiffModel> {
        val periodTime = PeriodTimeColorsModel(from, to, limit)

        var liveData = liveDataColorsHasMap[periodTime]
        if (liveData == null) {
            liveData = colorsContainer.create()
            liveDataColorsHasMap[periodTime] = liveData
            runBlocking(Dispatchers.Main) {
                liveData.activeObserver = object : TrackingLiveDate.ActiveObserver {
                    override fun onActive() {
                        GlobalScope.launch(Dispatchers.IO) {
                            val colorsDiffModel = updateColorHasMap(periodTime)
                            runBlocking(Dispatchers.Main) {
                                liveData.value = colorsDiffModel
                            }
                        }
                    }
                }
            }
        }

        return liveData
    }

    private fun updateColorHasMp(yearMonthDay: YearMonthDay, listColors: List<ColorEvent>): Boolean {
        val listEntry = if (listColors.isEmpty()) null else listColors
        val listFormMap = colorsPerDayHasMap[yearMonthDay]
        return when {
            listFormMap == null && listEntry != null -> {
                colorsPerDayHasMap[yearMonthDay] = listColors
                triggerColorsPerDayHasMapLiveData(yearMonthDay)
                true
            }
            listFormMap != null && listEntry == null -> {
                colorsPerDayHasMap.remove(yearMonthDay)
                triggerColorsPerDayHasMapLiveData(yearMonthDay)
                true
            }
            listFormMap != listEntry -> {
                colorsPerDayHasMap[yearMonthDay] = listColors
                triggerColorsPerDayHasMapLiveData(yearMonthDay)
                true
            }
            else -> {
                false
            }
        }
    }

    private fun triggerColorsPerDayHasMapLiveData(yearMonthDay: YearMonthDay) {

        val date = yearMonthDay.toDate()

        colorsContainer.liveDateActive.forEach { liveDate ->
            liveDate.value?.let { diffModel ->
                val period = diffModel.periodTimeColorsModel
                if ((period.from.toDate().time <= date.time) && (date.time <= period.to.toDate().time)) {
                    liveDataColorsHasMap[period]?.let {
                        runBlocking(Dispatchers.Main) {
                            it.value = ColorsDiffModel(period, arrayListOf(yearMonthDay))
                        }
                    }
                }
            }
        }

        GlobalScope.launch(Dispatchers.Main) {
            colorsPerDayHasMapLiveData.value = yearMonthDay
        }
    }

    private suspend fun updateColorHasMap(
        periodTime: PeriodTimeColorsModel
    ): ColorsDiffModel {
        val updatedDays = ArrayList<YearMonthDay>()

        val fromDate = periodTime.from.toDate()
        val toDate = periodTime.to.toDate()
        if (fromDate.after(toDate)) {
            throw Exception("the fromDate $fromDate should be before toDate $toDate")
        }

        val listColorsForAll = runBlocking {
            externalDataBase.event().fetchColorByCoroutines(
                CalendarHelper.prepareStartDate(periodTime.from.toDate()),
                CalendarHelper.prepareEndDate(periodTime.to.toDate())
            )
        }

        val calendar = CalendarHelper.getInstance()
        calendar.time = fromDate
        val numberOfDay = CalendarHelper.getDeferenceNumberOfDay(toDate, fromDate)
        for (i in 0..numberOfDay) {
            val yearMonthDay = YearMonthDay.build(calendar)
            val day = yearMonthDay.toDate()

            val fromDateTime = CalendarHelper.prepareStartDate(day).time
            val toDateTime = CalendarHelper.prepareEndDate(day).time

            val listColors = ArrayList<ColorEvent>()

            listColorsForAll.forEach { eventColor ->
                val startDate = eventColor.startDate.time
                val endDate = eventColor.endDate.time
                if ((fromDateTime in (startDate + 1)..endDate) ||
                    (toDateTime in startDate..endDate) ||
                    (fromDateTime <= startDate && endDate <= toDateTime)
                ) {
                    listColors.add(eventColor)
                }
            }
            val groupedListColors = ColorEventMapper.groupByCalendarId(listColors, periodTime.limit)


            if (updateColorHasMp(yearMonthDay, groupedListColors)) {
                updatedDays.add(yearMonthDay)
            }

            calendar.add(Calendar.DATE, 1)
        }

        return ColorsDiffModel(periodTime, updatedDays)
    }

    override fun getColorsPerDayHasMap(): HashMap<YearMonthDay, List<ColorEvent>> {
        return colorsPerDayHasMap
    }

    override fun getColorsPerDayHasMapLiveData(): LiveData<YearMonthDay> {
        return colorsPerDayHasMapLiveData
    }

    override suspend fun deleteEvent(id: Long) {
        externalDataBase.event().delete(id)
    }

    override suspend fun deleteEvent(eventEntity: EventEntity) {
        externalDataBase.event().delete(eventEntity)
    }

    override suspend fun deleteCalendar(id: Long) {
        externalDataBase.calendar().delete(id)
    }

    override suspend fun deleteCalendar(calendarEntity: CalendarEntity) {
        externalDataBase.calendar().delete(calendarEntity)
    }

    private fun onExternalEventsUpdated() {
        colorsContainer.liveDateActive.iterator().forEach { liveDate ->
            liveDate.value?.let {
                GlobalScope.launch(Dispatchers.IO) {
                    updateColorHasMap(it.periodTimeColorsModel)
                }
            }
        }
    }

    override suspend fun insertOrUpdateAttendee(attendeeEntity: AttendeeEntity): Long {
        return externalDataBase.attendee().insertOrUpdate(attendeeEntity)
    }

    override fun getAttendeeListLiveDate(eventEntityId: Long): AttendeeLiveDate {
        return externalDataBase.attendee().getListLiveDate(eventEntityId)
    }

    override suspend fun deleteAttendee(attendeeEntity: AttendeeEntity) {
        return externalDataBase.attendee().delete(attendeeEntity)
    }

    override suspend fun deleteAttendeeByEventId(eventId: Long) {
        return externalDataBase.attendee().deleteByEventId(eventId)
    }
}