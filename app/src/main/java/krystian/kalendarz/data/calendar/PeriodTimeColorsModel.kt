package krystian.kalendarz.data.calendar

data class PeriodTimeColorsModel(val from: YearMonthDay, val to: YearMonthDay, val limit: Int) {
}