package krystian.kalendarz.data.contact

import android.net.Uri
import krystian.kalendarz.data.externalcalendar.ExternalDataBase
import krystian.kalendarz.data.externalcalendar.contact.ContactPhone
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ContactRepositoryImpl @Inject constructor(private val externalDataBase: ExternalDataBase) : ContactRepository {
    override suspend fun getContactPhone(uri: Uri): List<ContactPhone> {
        return externalDataBase.contact().getPhoneList(uri)
    }
}