package krystian.kalendarz.data.contact

import android.net.Uri
import krystian.kalendarz.data.externalcalendar.contact.ContactPhone

interface ContactRepository {

    suspend fun getContactPhone(uri: Uri):List<ContactPhone>
}