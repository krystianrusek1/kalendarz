package krystian.kalendarz.data.database.sms

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "Sms")
data class SmsEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int?,
    var phoneNumber: String,
    var text: String,
    var dispatchDate: Date,
    var isPending: Boolean,
    var lastTryDate: Date?,
    var nextDateOfTry: Date,
    var numberOfTry: Int,
    var isSent: Boolean,
    var sentDate: Date?,
    var isDelivered: Boolean,
    var deliveredDate: Date?,
    var lasErrorCode: Int?,
    var lasDateErrorCode: Date?
) : Serializable {
    companion object {
        fun create(phoneNumber: String, text: String, dispatchDate: Date): SmsEntity {
            return SmsEntity(
                null,
                phoneNumber,
                text,
                dispatchDate,
                false,
                null,
                dispatchDate,
                0,
                false,
                null,
                false,
                null,
                null,
                null
            )
        }
    }
}