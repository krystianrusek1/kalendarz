package krystian.kalendarz.data.database.sms

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import java.util.*

@Dao
interface SmsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(smsEntity: SmsEntity)

    @Query("SELECT * FROM Sms")
    fun getList(): LiveData<List<SmsEntity>>

    @Query("SELECT * FROM Sms WHERE isSent = :isSent ORDER BY dispatchDate ")
    fun getListOrderByDispatchDate(isSent: Boolean): LiveData<List<SmsEntity>>

    @Query("SELECT * FROM Sms WHERE nextDateOfTry <= :untilDate AND isSent = 0 AND numberOfTry < :maxNumberOfTry")
    suspend fun getSmsReadyToSendList(untilDate: Date, maxNumberOfTry: Int): List<SmsEntity>

    @Query("SELECT * FROM Sms WHERE nextDateOfTry > :afterDate AND isSent = 0 AND numberOfTry < :maxNumberOfTry")
    suspend fun getSmsToScheduleList(afterDate: Date, maxNumberOfTry: Int): List<SmsEntity>

    @Query("SELECT * FROM Sms WHERE dispatchDate < :untilDate AND numberOfTry < :maxNumberOfTry AND isSent = 0 AND (isPending == 1 OR lasErrorCode IS NOT NULL)")
    suspend fun getErrorSmsOrPending(untilDate: Date, maxNumberOfTry: Int): List<SmsEntity>

    @Query("SELECT * FROM Sms WHERE :id == id")
    suspend fun getSms(id: Int): SmsEntity?
}