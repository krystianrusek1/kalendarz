package krystian.kalendarz.data.database.phone

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PhoneDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdate(phoneEntity: PhoneEntity)

    @Query("SELECT * FROM PhoneEntity WHERE eventId == :eventId")
    fun getList(eventId: Long):LiveData<List<PhoneEntity>>

    @Query("DELETE FROM PhoneEntity WHERE eventId == :eventId")
    suspend fun delete(eventId: Long)
}