package krystian.kalendarz.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import krystian.kalendarz.data.database.phone.PhoneDao
import krystian.kalendarz.data.database.phone.PhoneEntity
import krystian.kalendarz.data.database.sms.SmsDao
import krystian.kalendarz.data.database.sms.SmsEntity

@Database(entities = [SmsEntity::class, PhoneEntity::class], version = 2)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DATA_BASE_NAME = "calendarDatabase"
    }

    abstract fun smsDao(): SmsDao
    abstract fun phoneDao(): PhoneDao
}