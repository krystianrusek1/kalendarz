package krystian.kalendarz.data.database.phone

import androidx.room.Entity
import androidx.room.PrimaryKey
import krystian.kalendarz.data.PhoneNumber

@Entity(tableName = "PhoneEntity")
data class PhoneEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val eventId: Long,
    val phoneNumber: PhoneNumber
) {

}