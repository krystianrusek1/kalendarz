package krystian.kalendarz.data.database

import androidx.room.TypeConverter
import krystian.kalendarz.data.PhoneNumber
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }

    @TypeConverter
    fun fromPhoneNumber(value: String?): PhoneNumber? {
        return value?.let { PhoneNumber(it) }
    }

    @TypeConverter
    fun phoneNumberToString(phoneNumber: PhoneNumber?): String? {
        return phoneNumber?.phoneNumber
    }
}