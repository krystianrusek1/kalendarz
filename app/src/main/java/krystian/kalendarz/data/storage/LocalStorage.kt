package krystian.kalendarz.data.storage

interface LocalStorage {
    companion object {
        const val KEY_LAST_CHOSEN_CALENDAR_ID = "KEY_LAST_CHOSEN_CALENDAR_ID"
    }

    fun putInt(key: String, value: Int)
    fun getInt(key: String, defValue: Int): Int

    fun putLong(key: String, value: Long)
    fun getLong(key: String, defValue: Long): Long
}