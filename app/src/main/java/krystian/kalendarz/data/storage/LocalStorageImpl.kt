package krystian.kalendarz.data.storage

import android.content.Context
import android.content.Context.MODE_PRIVATE
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalStorageImpl @Inject constructor(appContext: Context) : LocalStorage {

    private val pref = appContext.getSharedPreferences("LocalStorage", MODE_PRIVATE)

    override fun putInt(key: String, value: Int) {
        val edit = pref.edit()
        edit.putInt(key, value)
        edit.apply()
    }

    override fun getInt(key: String, defValue: Int): Int {
        return pref.getInt(key, defValue)
    }

    override fun putLong(key: String, value: Long) {
        val edit = pref.edit()
        edit.putLong(key, value)
        edit.apply()
    }

    override fun getLong(key: String, defValue: Long): Long {
        return pref.getLong(key, defValue)
    }

}