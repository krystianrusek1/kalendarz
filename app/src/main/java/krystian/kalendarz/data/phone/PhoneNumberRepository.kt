package krystian.kalendarz.data.phone

import androidx.lifecycle.LiveData
import krystian.kalendarz.data.PhoneNumber

interface PhoneNumberRepository {

    suspend fun updatePhonesList(eventId: Long, phonesList: List<PhoneNumber>)

    fun getPhonesList(eventEntityId: Long): LiveData<List<PhoneNumber>>

}