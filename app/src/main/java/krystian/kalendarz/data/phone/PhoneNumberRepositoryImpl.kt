package krystian.kalendarz.data.phone

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import krystian.kalendarz.data.PhoneNumber
import krystian.kalendarz.data.database.AppDatabase
import krystian.kalendarz.data.database.phone.PhoneEntity
import javax.inject.Inject

class PhoneNumberRepositoryImpl @Inject constructor(
    private val database: AppDatabase
) : PhoneNumberRepository {

    override suspend fun updatePhonesList(eventId: Long, phonesList: List<PhoneNumber>) {
        database.phoneDao().delete(eventId)
        phonesList.forEach { phone ->
            database.phoneDao().insertOrUpdate(PhoneEntity(null, eventId, phone))
        }
    }

    override fun getPhonesList(eventEntityId: Long): LiveData<List<PhoneNumber>> {
        return Transformations.map(database.phoneDao().getList(eventEntityId)) { phoneEntity ->
            val phonesList = ArrayList<PhoneNumber>()
            phoneEntity.forEach { phone ->
                phonesList.add(phone.phoneNumber)
            }
            phonesList
        }
    }
}