package krystian.kalendarz.data.sms

import androidx.lifecycle.LiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import krystian.kalendarz.data.database.AppDatabase
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.util.Loggger
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SendSMSRepositoryImpl @Inject constructor(private val database: AppDatabase) : SendSMSRepository {

    internal val LOG = Loggger(this.javaClass)

    override fun addOrUpdateSms(sms: SmsEntity) {
        GlobalScope.launch {
            database.smsDao().insertOrUpdate(sms)
            LOG.d("insertOrUpdateCalendar: $sms")

        }
    }

    override suspend fun addOrUpdateSmsBlocking(sms: SmsEntity) {
        database.smsDao().insertOrUpdate(sms)
        LOG.d("addOrUpdateSmsBlocking: $sms")
    }

    override suspend fun getSms(id: Int): SmsEntity? {
        return database.smsDao().getSms(id)
    }

    override fun getListOrderByDispatchDate(isSent: Boolean): LiveData<List<SmsEntity>> {
        return database.smsDao().getListOrderByDispatchDate(isSent)
    }

    override fun getSmsList(): LiveData<List<SmsEntity>> {
        return database.smsDao().getList()
    }

    override suspend fun getSmsReadyToSendList(untilDate: Date, maxNumberOfTry: Int): List<SmsEntity> {
        return database.smsDao().getSmsReadyToSendList(untilDate, maxNumberOfTry)
    }

    override suspend fun getSmsToScheduleList(afterDate: Date, maxNumberOfTry: Int): List<SmsEntity> {
        return database.smsDao().getSmsToScheduleList(afterDate, maxNumberOfTry)
    }

    override suspend fun getErrorSmsOrPending(untilDate: Date, maxNumberOfTry: Int): List<SmsEntity> {
        return database.smsDao().getErrorSmsOrPending(untilDate, maxNumberOfTry)
    }

}