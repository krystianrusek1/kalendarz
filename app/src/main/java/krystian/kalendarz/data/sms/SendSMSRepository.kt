package krystian.kalendarz.data.sms

import androidx.lifecycle.LiveData
import krystian.kalendarz.data.database.sms.SmsEntity
import java.util.*

interface SendSMSRepository {

    fun addOrUpdateSms(sms: SmsEntity)

    suspend fun addOrUpdateSmsBlocking(sms: SmsEntity)

    suspend fun getSms(id: Int): SmsEntity?

    fun getListOrderByDispatchDate(isSent: Boolean): LiveData<List<SmsEntity>>

    fun getSmsList(): LiveData<List<SmsEntity>>

    suspend fun getSmsReadyToSendList(untilDate: Date, maxNumberOfTry: Int): List<SmsEntity>

    suspend fun getSmsToScheduleList(afterDate: Date, maxNumberOfTry: Int): List<SmsEntity>

    suspend fun getErrorSmsOrPending(untilDate: Date, maxNumberOfTry: Int): List<SmsEntity>
}