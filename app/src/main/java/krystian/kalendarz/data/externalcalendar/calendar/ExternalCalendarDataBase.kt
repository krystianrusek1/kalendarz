package krystian.kalendarz.data.externalcalendar.calendar

import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import androidx.lifecycle.LiveData
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveDataFactory
import krystian.kalendarz.util.Loggger

class ExternalCalendarDataBase(private val appContext: Context) {

    private val contentResolver = appContext.contentResolver

    private val calendarFactory = object : ContentProviderLiveDataFactory<CalendarLiveDate>() {
        override fun new(selection: String?, sortOrder: String?, limit: Int): CalendarLiveDate {
            return CalendarLiveDate(contentResolver, selection)
        }
    }

    internal val LOG = Loggger(this.javaClass)

    suspend fun insertOrUpdate(calendarEntity: CalendarEntity): Long {
        return if (calendarEntity.id == null) {
            insert(calendarEntity)
        } else {
            update(calendarEntity)
        }
    }

    private suspend fun insert(calendarEntity: CalendarEntity): Long {
        val values = ContentValues().apply {
            put(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL)
            put(CalendarContract.Calendars.ACCOUNT_NAME, calendarEntity.accountName)
            put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, calendarEntity.displayName)
            put(CalendarContract.Calendars.CALENDAR_COLOR, calendarEntity.color)
            put(CalendarContract.Calendars.VISIBLE, if (calendarEntity.visible) 1 else 0)
        }
        val calUri = CalendarMapper.uri.buildUpon()
            .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, calendarEntity.accountName)
            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL)
            .build()

        val uri = contentResolver.insert(calUri, values)
        return if (uri == null) {
            -1
        } else {
            ContentUris.parseId(uri)
        }
    }

    private fun update(calendarEntity: CalendarEntity): Long {
        val values = ContentValues().apply {
            put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, calendarEntity.displayName)
            put(CalendarContract.Calendars.CALENDAR_COLOR, calendarEntity.color)
            put(CalendarContract.Calendars.VISIBLE, if (calendarEntity.visible) 1 else 0)
        }
        val updateUri: Uri = ContentUris.withAppendedId(CalendarMapper.uri, calendarEntity.id!!)
        contentResolver.update(updateUri, values, null, null)

        return calendarEntity.id!!
    }

    suspend fun delete(calendarEntity: CalendarEntity) {
        calendarEntity.id?.let { id ->
            delete(id)
        }
    }

    suspend fun delete(calendarId: Long) {
        val deleteUri: Uri = ContentUris.withAppendedId(CalendarMapper.uri, calendarId)
        val rows: Int = contentResolver.delete(deleteUri, null, null)
        LOG.i("Rows deleted: $rows")
    }

    fun getList(onlyVisible: Boolean): LiveData<List<CalendarEntity>> {
        return calendarFactory.create(
            getDeleteAndVisibleSelection(onlyVisible)
        )
    }

    suspend fun getListByCoroutines(): List<CalendarEntity> {
        return CalendarMapper.getCalendarList(
            contentResolver,
            "${CalendarContract.Calendars.DELETED} == 0"
        )
    }

    suspend fun getItem(accountName: String): List<CalendarEntity> {
        val selection = "${CalendarContract.Calendars.ACCOUNT_NAME} LIKE '$accountName'"

        val cur: Cursor? = contentResolver.query(
            CalendarMapper.uri,
            CalendarMapper.CALENDAR_PROJECTION,
            selection,
            null,
            null
        )
        val list = ArrayList<CalendarEntity>()
        if (cur != null) {
            while (cur.moveToNext()) {
                list.add(CalendarMapper.parseCursor(cur))
            }
            cur.close()
        }
        return list
    }

    suspend fun getItem(calendarId: Long, onlyVisible: Boolean): CalendarEntity? {
        val selection =
            "${CalendarContract.Calendars._ID} LIKE '$calendarId' AND ${getDeleteAndVisibleSelection(onlyVisible)}"

        val cur: Cursor? = contentResolver.query(
            CalendarMapper.uri,
            CalendarMapper.CALENDAR_PROJECTION,
            selection,
            null,
            null
        )
        val list = ArrayList<CalendarEntity>()
        if (cur != null) {
            while (cur.moveToNext()) {
                list.add(CalendarMapper.parseCursor(cur))
            }
            cur.close()
        }
        if (list.isEmpty()) {
            return null
        }
        return list[0]
    }

    private fun getDeleteAndVisibleSelection(onlyVisible: Boolean): String {
        return if (onlyVisible) {
            "${CalendarContract.Calendars.DELETED} == 0 AND ${CalendarContract.Calendars.VISIBLE} == 1"
        } else {
            "${CalendarContract.Calendars.DELETED} == 0"
        }
    }
}
