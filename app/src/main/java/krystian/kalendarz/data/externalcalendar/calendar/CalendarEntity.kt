package krystian.kalendarz.data.externalcalendar.calendar

import android.graphics.Color
import androidx.annotation.ColorInt
import java.io.Serializable

data class CalendarEntity private constructor(
    val id: Long?,
    val displayName: String?,
    val accountName: String,
    @ColorInt val color: Int,
    val visible: Boolean,
    val accountType: AccountType
) : Serializable {

    enum class AccountType {
        LOCAL, OTHER
    }

    companion object {
        val ACCOUNT_LOCAL_NAME = "local"
    }

    fun copyBuilder(): Builder {
        val builder = Builder()
            .withId(id)
            .withDisplayName(displayName)
            .withAccountName(accountName)
            .withColor(color)
            .withVisible(visible)
            .withAccountType(accountType)
        return builder
    }

    class Builder {
        private var id: Long? = null
        private var displayName: String? = null
        private var accountName: String =
            ACCOUNT_LOCAL_NAME
        @ColorInt
        private var color: Int = Color.TRANSPARENT
        private var visible: Boolean = true
        private var accountType: AccountType =
            AccountType.OTHER

        fun withId(id: Long?): Builder {
            apply { this.id = id }
            return this
        }

        fun withDisplayName(displayName: String?): Builder {
            apply { this.displayName = displayName }
            return this
        }

        fun withAccountName(accountName: String): Builder {
            apply { this.accountName = accountName }
            return this
        }

        fun withColor(@ColorInt color: Int): Builder {
            apply { this.color = color }
            return this
        }

        fun withVisible(visible: Boolean): Builder {
            apply { this.visible = visible }
            return this
        }

        fun withAccountType(accountType: AccountType): Builder {
            apply { this.accountType = accountType }
            return this
        }

        fun build(): CalendarEntity {
            return CalendarEntity(
                id,
                displayName,
                accountName,
                color,
                visible,
                accountType
            )
        }
    }

}