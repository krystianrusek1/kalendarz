package krystian.kalendarz.data.externalcalendar.livedate

import android.content.ContentResolver
import android.database.ContentObserver
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import krystian.kalendarz.util.Loggger

abstract class ContentProviderLiveData<T>(
    private val contentResolver: ContentResolver,
    private val uri: Uri
) : MutableLiveData<T>() {
    private lateinit var observer: ContentObserver

    protected val LOG = Loggger(this.javaClass)

    override fun onActive() {
        observer = object : ContentObserver(null) {
            override fun onChange(self: Boolean) {
                triggerContentProvider()
            }
        }

        contentResolver.registerContentObserver(uri, true, observer)
        triggerContentProvider()
        super.onActive()
    }

    override fun onInactive() {
        contentResolver.unregisterContentObserver(observer)
        super.onInactive()
    }

    private fun triggerContentProvider() {
        GlobalScope.launch {
            postValue(getContentProviderValue())
        }
    }

    abstract fun getContentProviderValue(): T
}