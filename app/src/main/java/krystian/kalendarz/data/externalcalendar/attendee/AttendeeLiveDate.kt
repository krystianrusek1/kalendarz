package krystian.kalendarz.data.externalcalendar.attendee

import android.content.ContentResolver
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveData

class AttendeeLiveDate(private val contentResolver: ContentResolver, private val selection: String?) :
    ContentProviderLiveData<List<AttendeeEntity>>(
        contentResolver,
        AttendeeMapper.uri
    ) {

    override fun getContentProviderValue(): List<AttendeeEntity> {
        return AttendeeMapper.getAttendeeList(contentResolver, selection)
    }
}