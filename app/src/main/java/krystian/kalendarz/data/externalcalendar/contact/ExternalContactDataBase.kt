package krystian.kalendarz.data.externalcalendar.contact

import android.content.Context
import android.net.Uri
import krystian.kalendarz.data.externalcalendar.ThreadHelper

class ExternalContactDataBase(appContext: Context) {

    private val contentResolver = appContext.contentResolver

    suspend fun getPhoneList(uri: Uri): List<ContactPhone> {
        ThreadHelper.throwIfThreadIsMain()
        return ContactPhoneNumberMapper.getContactPhoneList(uri, contentResolver, null)
    }
}