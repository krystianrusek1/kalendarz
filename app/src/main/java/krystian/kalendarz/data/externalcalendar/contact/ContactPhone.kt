package krystian.kalendarz.data.externalcalendar.contact

import krystian.kalendarz.data.PhoneNumber

data class ContactPhone(val phoneNumber: PhoneNumber?) {
}