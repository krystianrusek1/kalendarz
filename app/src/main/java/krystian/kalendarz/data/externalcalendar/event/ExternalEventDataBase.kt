package krystian.kalendarz.data.externalcalendar.event

import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import krystian.kalendarz.data.externalcalendar.ThreadHelper
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveDataFactory
import krystian.kalendarz.util.Loggger
import java.util.*

class ExternalEventDataBase(private val appContext: Context) {

    private val contentResolver = appContext.contentResolver

    private var externalEventDataBaseListener: ExternalEventDataBaseListener? = null

    private val observer = object : ContentObserver(null) {
        override fun onChange(self: Boolean) {
            externalEventDataBaseListener?.onEventsUpdated()
        }
    }

    private val colorEventFactory = object : ContentProviderLiveDataFactory<ColorEventLiveDate>() {
        override fun new(selection: String?, sortOrder: String?, limit: Int): ColorEventLiveDate {
            return ColorEventLiveDate(contentResolver, selection, sortOrder, limit)
        }
    }

    private val eventFactory = object : ContentProviderLiveDataFactory<EventLiveDate>() {
        override fun new(selection: String?, sortOrder: String?, limit: Int): EventLiveDate {
            return EventLiveDate(contentResolver, selection, sortOrder)
        }
    }

    internal val LOG = Loggger(this.javaClass)

    suspend fun insertOrUpdate(eventEntity: EventEntity): Long {
        return if (eventEntity.id == null) {
            insert(eventEntity)
        } else {
            update(eventEntity)
        }
    }

    private suspend fun insert(eventEntity: EventEntity): Long {
        ThreadHelper.throwIfThreadIsMain()
        val values = getContentValues(eventEntity)
        val uri: Uri? = contentResolver.insert(EventMapper.uri, values)
        return uri?.lastPathSegment?.toLong() ?: -1
    }

    private suspend fun update(eventEntity: EventEntity): Long {
        ThreadHelper.throwIfThreadIsMain()
        eventEntity.id?.let { id ->
            val values = getContentValues(eventEntity)
            val updateUri: Uri = ContentUris.withAppendedId(EventMapper.uri, id)
            val rows: Int = contentResolver.update(updateUri, values, null, null)
            LOG.i("Rows updated: $rows")
            return id
        }
        return -1L
    }

    private fun getContentValues(eventEntity: EventEntity) = ContentValues().apply {

        var startTime = eventEntity.startDate.time
        var endTime = eventEntity.endDate.time
        var timeZone = eventEntity.timeZone
        if (eventEntity.allDays) {
            startTime = FixingAllDay.prepareStartDateForSave(eventEntity.startDate)
            endTime = FixingAllDay.prepareEndDateForSave(eventEntity.endDate)
            timeZone = TimeZone.getTimeZone("UTC")
        }

        put(CalendarContract.Events.DTSTART, startTime)
        put(CalendarContract.Events.DTEND, endTime)
        put(CalendarContract.Events.TITLE, eventEntity.title)
        put(CalendarContract.Events.DESCRIPTION, eventEntity.description)
        put(CalendarContract.Events.ALL_DAY, if (eventEntity.allDays) 1 else 0)
        put(CalendarContract.Events.CALENDAR_ID, eventEntity.calendarId)
        put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.id)
    }

    suspend fun delete(eventEntity: EventEntity) {
        eventEntity.id?.let {
            delete(it)
        }
    }

    suspend fun delete(eventEntityId: Long) {
        ThreadHelper.throwIfThreadIsMain()
        return withContext(Dispatchers.IO) {
            val deleteUri: Uri = ContentUris.withAppendedId(EventMapper.uri, eventEntityId)
            val rows: Int = contentResolver.delete(deleteUri, null, null)
            LOG.i("Rows deleted: $rows")
        }
    }

    fun getList(): LiveData<List<EventEntity>> {
        return eventFactory.create(
            getDeleteAndVisibleSelection(),
            null
        )
    }

    fun getListByDescription(description: String): LiveData<List<EventEntity>> {
        return eventFactory.create(
            updateDeleteAndVisibleSelection("${CalendarContract.Events.DESCRIPTION} LIKE '$description'"),
            null
        )
    }

    suspend fun getEvent(eventId: Long): EventEntity? {
        ThreadHelper.throwIfThreadIsMain()
        val selection = updateDeleteAndVisibleSelection("${CalendarContract.Events._ID} == $eventId ")

        val cur: Cursor? = appContext.contentResolver.query(
            EventMapper.uri,
            EventMapper.EVENT_PROJECTION,
            selection,
            null,
            null
        )
        if (cur != null) {
            while (cur.moveToNext()) {
                return EventMapper.parseCursor(cur)
            }
            cur.close()
        }
        return null
    }

    fun fetchEventListRecursiveByLiveDate(fromDate: Date, toDate: Date): LiveData<List<EventEntity>> {
        return eventFactory.create(
            createSelectionBetteannStartEnd(fromDate, toDate),
            CalendarContract.Events.DTSTART
        )
    }

    suspend fun fetchEventListRecursiveByCoroutines(fromDate: Date, toDate: Date): List<EventEntity> {
        ThreadHelper.throwIfThreadIsMain()
        return withContext(Dispatchers.IO) {
            return@withContext EventMapper.getEventList(
                contentResolver,
                createSelectionBetteannStartEnd(fromDate, toDate),
                CalendarContract.Events.DTSTART
            )
        }
    }

    private fun createSelectionBetteannStartEnd(fromDate: Date, toDate: Date): String {
        val startDate = CalendarContract.Events.DTSTART
        val endDate = CalendarContract.Events.DTEND

        var fromDateTime = fromDate.time
        var toDateTime = toDate.time

        val selectionWithoutAllDays = "(($startDate<$fromDateTime AND $endDate>=$fromDateTime) " +
                "OR ($startDate<= $toDateTime AND $endDate >= $toDateTime) " +
                "OR ($fromDateTime<= $startDate AND $endDate <= $toDateTime)) " +
                "AND (${CalendarContract.Events.ALL_DAY} == 0 )"

        val fromDateFormatted = FixingAllDay.prepareStartDateToQuery(fromDate)
        val toDateFormatted = FixingAllDay.prepareEndDateToQuery(toDate)

        fromDateTime = fromDateFormatted.time
        toDateTime = toDateFormatted.time

        val selectionWithAllDays = "(($startDate<$fromDateTime AND $endDate>=$fromDateTime) " +
                "OR ($startDate<= $toDateTime AND $endDate >= $toDateTime) " +
                "OR ($fromDateTime<= $startDate AND $endDate <= $toDateTime)) " +
                "AND (${CalendarContract.Events.ALL_DAY} == 1 )"


        return updateDeleteAndVisibleSelection("($selectionWithoutAllDays) OR ($selectionWithAllDays)")
    }

    private fun updateDeleteAndVisibleSelection(selection: String): String {
        return "( $selection ) AND ( ${getDeleteAndVisibleSelection()} )"
    }

    private fun getDeleteAndVisibleSelection(): String {
        return "${CalendarContract.Events.DELETED} == 0 AND ${CalendarContract.Events.VISIBLE} == 1"
    }

    fun fetchColorGroupedByCalendarIdByLiveDate(fromDate: Date, toDate: Date, limit: Int): LiveData<List<ColorEvent>> {
        return colorEventFactory.create(
            createSelectionBetteannStartEnd(fromDate, toDate),
            CalendarContract.Events.DTSTART,
            limit
        )
    }

    suspend fun fetchColorGroupedByCalendarIdByCoroutines(fromDate: Date, toDate: Date, limit: Int): List<ColorEvent> {
        ThreadHelper.throwIfThreadIsMain()
        val colors = ColorEventMapper.getEventList(
            contentResolver,
            createSelectionBetteannStartEnd(fromDate, toDate),
            CalendarContract.Events.DTSTART
        )
        return ColorEventMapper.groupByCalendarId(colors, limit)
    }

    suspend fun fetchColorByCoroutines(fromDate: Date, toDate: Date): List<ColorEvent> {
        ThreadHelper.throwIfThreadIsMain()
        return ColorEventMapper.getEventList(
            contentResolver,
            createSelectionBetteannStartEnd(fromDate, toDate),
            CalendarContract.Events.DTSTART
        )
    }

    fun setExternalEventDataBaseListener(externalEventDataBaseListener: ExternalEventDataBaseListener) {
        this.externalEventDataBaseListener = externalEventDataBaseListener
        contentResolver.registerContentObserver(EventMapper.uri, true, observer)
    }
}