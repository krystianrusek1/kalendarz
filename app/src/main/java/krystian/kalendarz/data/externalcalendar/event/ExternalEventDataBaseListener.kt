package krystian.kalendarz.data.externalcalendar.event

interface ExternalEventDataBaseListener {

    fun onEventsUpdated()
}