package krystian.kalendarz.data.externalcalendar.contact

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import krystian.kalendarz.data.PhoneNumber

object ContactPhoneNumberMapper {

    val uri: Uri = ContactsContract.RawContacts.CONTENT_URI

    internal val CONTACT_PHONE_PROJECTION: Array<String> = arrayOf(
        ContactsContract.Data.MIMETYPE,            // 1
        ContactsContract.CommonDataKinds.Phone.NUMBER  // 2
    )

    private const val PROJECTION_MIMETYPE: Int = 0
    private const val PROJECTION_NUMBER_INDEX: Int = 1

    fun parseCursor(cursor: Cursor): ContactPhone? {
        return if (cursor.getString(PROJECTION_MIMETYPE) == ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE) {
            ContactPhone(PhoneNumber(cursor.getString(PROJECTION_NUMBER_INDEX) ?: ""))
        } else {
            null
        }
    }

    fun getContactPhoneList(contentResolver: ContentResolver, selection: String?): List<ContactPhone> {
        return getContactPhoneList(uri, contentResolver, selection)
    }

    fun getContactPhoneList(uri: Uri, contentResolver: ContentResolver, selection: String?): List<ContactPhone> {
        val list = ArrayList<ContactPhone>()
        val cur: Cursor? = contentResolver.query(
            uri,
            CONTACT_PHONE_PROJECTION,
            selection,
            null,
            null
        )
        if (cur != null) {
            while (cur.moveToNext()) {
                parseCursor(cur)?.let { list.add(it) }
            }
            cur.close()
        }
        return list
    }
}