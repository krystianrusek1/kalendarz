package krystian.kalendarz.data.externalcalendar

import android.content.Context
import krystian.kalendarz.data.externalcalendar.attendee.ExternalAttendeeDataBase
import krystian.kalendarz.data.externalcalendar.calendar.ExternalCalendarDataBase
import krystian.kalendarz.data.externalcalendar.contact.ExternalContactDataBase
import krystian.kalendarz.data.externalcalendar.event.ExternalEventDataBase
import javax.inject.Inject

class ExternalDataBaseImpl
@Inject constructor(
    appContext: Context
) : ExternalDataBase {

    private val externalCalendarDataBase = ExternalCalendarDataBase(appContext)

    private val externalEventDataBase = ExternalEventDataBase(appContext)

    private val externalAttendeeDataBase = ExternalAttendeeDataBase(appContext)

    private val externalContactDataBase = ExternalContactDataBase(appContext)

    override fun calendar(): ExternalCalendarDataBase {
        return externalCalendarDataBase
    }

    override fun event(): ExternalEventDataBase {
        return externalEventDataBase
    }

    override fun attendee(): ExternalAttendeeDataBase {
        return externalAttendeeDataBase
    }

    override fun contact(): ExternalContactDataBase {
        return externalContactDataBase
    }


}