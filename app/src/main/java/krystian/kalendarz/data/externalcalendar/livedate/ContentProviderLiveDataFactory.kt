package krystian.kalendarz.data.externalcalendar.livedate

abstract class ContentProviderLiveDataFactory<T : ContentProviderLiveData<*>> {

    private val liveDateMap = HashMap<String, T>()

    fun create(
        selection: String?
    ): T {
        return create(selection, null, 0)
    }

    fun create(
        selection: String?,
        sortOrder: String?
    ): T {
        return create(selection, sortOrder, 0)
    }

    fun create(
        selection: String?,
        sortOrder: String?,
        limit: Int
    ): T {
        val key = generateKey(selection, sortOrder, limit)

        var live = liveDateMap[key]
        if (live == null) {
            live = new(selection, sortOrder, limit)
            liveDateMap[key] = live
        }
        return live
    }

    private fun generateKey(selection: String?, sortOrder: String?, limit: Int): String {
        return "$selection$sortOrder$limit"
    }

    abstract fun new(
        selection: String?,
        sortOrder: String?,
        limit: Int
    ): T
}