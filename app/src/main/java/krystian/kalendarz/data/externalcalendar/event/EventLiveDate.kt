package krystian.kalendarz.data.externalcalendar.event

import android.content.ContentResolver
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveData

class EventLiveDate(private val contentResolver: ContentResolver, private val selection: String?, val sortOrder: String?) :
    ContentProviderLiveData<List<EventEntity>>(
        contentResolver,
        EventMapper.uri
    ) {

    override fun getContentProviderValue(): List<EventEntity> {
        return EventMapper.getEventList(contentResolver, selection, sortOrder)
    }
}