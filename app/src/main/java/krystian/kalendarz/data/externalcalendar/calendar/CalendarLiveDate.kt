package krystian.kalendarz.data.externalcalendar.calendar

import android.content.ContentResolver
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveData

class CalendarLiveDate(private val contentResolver: ContentResolver, private val selection: String?) :
    ContentProviderLiveData<List<CalendarEntity>>(
        contentResolver,
        CalendarMapper.uri
    ) {

    override fun getContentProviderValue(): List<CalendarEntity> {
        return CalendarMapper.getCalendarList(contentResolver, selection)
    }
}