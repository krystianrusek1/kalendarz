package krystian.kalendarz.data.externalcalendar.event

import android.graphics.Color
import androidx.annotation.ColorInt
import java.io.Serializable
import java.util.*

data class EventEntity private constructor(
    val id: Long?,
    val calendarId: Long,
    val title: String,
    val description: String,
    val startDate: Date,
    val endDate: Date,
    val allDays: Boolean,
    val timeZone: TimeZone,
    val isDelete: Boolean,
    val isVisible: Boolean,
    @ColorInt val color: Int = Color.TRANSPARENT
) : Serializable {

    fun copyBuilder(): Builder {
        return Builder(calendarId)
            .withId(id)
            .withTitle(title)
            .withDescription(description)
            .withStartDate(startDate)
            .withEndDate(endDate)
            .withAllDays(allDays)
            .withTimeZone(timeZone)
            .withIsDelete(isDelete)
            .withIsVisible(isVisible)
    }

    class Builder(var calendarId: Long) {
        var id: Long? = null
        var title: String = ""
        var description: String = ""
        var startDate: Date = Date()
        var endDate: Date = Date()
        var allDays: Boolean = false
        var timeZone: TimeZone = TimeZone.getDefault()
        var isDelete: Boolean = false
        var isVisible: Boolean = true
        @ColorInt
        var color: Int = Color.TRANSPARENT

        fun withId(id: Long?): Builder {
            apply { this.id = id }
            return this
        }

        fun withCalendarId(calendarId: Long): Builder {
            apply { this.calendarId = calendarId }
            return this
        }

        fun withTitle(title: String): Builder {
            apply { this.title = title }
            return this
        }

        fun withDescription(description: String): Builder {
            apply { this.description = description }
            return this
        }

        fun withStartDate(startDate: Date): Builder {
            apply { this.startDate = startDate }
            return this
        }

        fun withEndDate(endDate: Date): Builder {
            apply { this.endDate = endDate }
            return this
        }

        fun withAllDays(allDays: Boolean): Builder {
            apply { this.allDays = allDays }
            return this
        }

        fun withTimeZone(timeZone: TimeZone): Builder {
            apply { this.timeZone = timeZone }
            return this
        }

        fun withColor(@ColorInt color: Int): Builder {
            apply { this.color = color }
            return this
        }

        internal fun withIsVisible(isVisible: Boolean): Builder {
            apply { this.isVisible = isVisible }
            return this
        }

        internal fun withIsDelete(isVisible: Boolean): Builder {
            apply { this.isVisible = isVisible }
            return this
        }

        fun build(): EventEntity {
            return EventEntity(
                id,
                calendarId,
                title,
                description,
                startDate,
                endDate,
                allDays,
                timeZone,
                isDelete,
                isVisible,
                color
            )
        }
    }
}