package krystian.kalendarz.data.externalcalendar.calendar

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract

object CalendarMapper {

    val uri: Uri = CalendarContract.Calendars.CONTENT_URI

    internal val CALENDAR_PROJECTION: Array<String> = arrayOf(
        CalendarContract.Calendars._ID,                     // 0
        CalendarContract.Calendars.ACCOUNT_NAME,            // 1
        CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,  // 2
        CalendarContract.Calendars.CALENDAR_COLOR,           // 3
        CalendarContract.Calendars.VISIBLE,                  //4
        CalendarContract.Calendars.ACCOUNT_TYPE               //5
    )

    private const val PROJECTION_ID_INDEX: Int = 0
    private const val PROJECTION_ACCOUNT_NAME_INDEX: Int = 1
    private const val PROJECTION_DISPLAY_NAME_INDEX: Int = 2
    private const val PROJECTION_COLOR_INDEX: Int = 3
    private const val PROJECTION_VISIBLE_INDEX: Int = 4
    private const val PROJECTION_ACCOUNT_TYPE_INDEX: Int = 5

    fun parseCursor(cursor: Cursor): CalendarEntity {
        var accountName = cursor.getString(PROJECTION_ACCOUNT_NAME_INDEX)
        if (accountName == null) {
            accountName = ""
        }

        return CalendarEntity.Builder()
            .withId(cursor.getLong(PROJECTION_ID_INDEX)).withAccountName(accountName)
            .withDisplayName(cursor.getString(PROJECTION_DISPLAY_NAME_INDEX))
            .withColor(cursor.getInt(PROJECTION_COLOR_INDEX))
            .withVisible(cursor.getInt(PROJECTION_VISIBLE_INDEX) == 1)
            .withAccountType(parseAccountType(cursor.getString(PROJECTION_ACCOUNT_TYPE_INDEX))).build()
    }

    fun getCalendarList(contentResolver: ContentResolver, selection: String?): List<CalendarEntity> {
        val list = ArrayList<CalendarEntity>()
        val cur: Cursor? = contentResolver.query(
            uri,
            CALENDAR_PROJECTION,
            selection,
            null,
            null
        )
        if (cur != null) {
            while (cur.moveToNext()) {
                list.add(parseCursor(cur))
            }
            cur.close()
        }
        return list
    }

    private fun parseAccountType(type: String): CalendarEntity.AccountType {
        if (type.equals(CalendarContract.ACCOUNT_TYPE_LOCAL)) {
            return CalendarEntity.AccountType.LOCAL
        }
        return CalendarEntity.AccountType.OTHER
    }
}