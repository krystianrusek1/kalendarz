package krystian.kalendarz.data.externalcalendar

import android.os.Looper

object ThreadHelper {

    fun throwIfThreadIsMain() {
        if (Thread.currentThread() == Looper.getMainLooper().thread) {
            throw Exception("This is the Main thread. The action has to be executed on another thread")
        }
    }
}