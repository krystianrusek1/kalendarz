package krystian.kalendarz.data.externalcalendar.event

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import java.util.*
import kotlin.collections.ArrayList

object EventMapper {

    val uri: Uri = CalendarContract.Events.CONTENT_URI

    internal val EVENT_PROJECTION: Array<String> = arrayOf(
        CalendarContract.Events._ID,                     // 0
        CalendarContract.Events.CALENDAR_ID,            // 1
        CalendarContract.Events.TITLE,                  // 2
        CalendarContract.Events.DESCRIPTION,         // 3
        CalendarContract.Events.DTSTART,      // 4
        CalendarContract.Events.DTEND,           // 5
        CalendarContract.Events.ALL_DAY,          // 6
        CalendarContract.Events.EVENT_TIMEZONE,           // 7
        CalendarContract.Events.DELETED,
        CalendarContract.Events.VISIBLE,
        CalendarContract.Events.CALENDAR_COLOR
    )

    private const val PROJECTION_ID_INDEX: Int = 0
    private const val PROJECTION_CALENDAR_ID_INDEX: Int = 1
    private const val PROJECTION_TITLE_INDEX: Int = 2
    private const val PROJECTION_DESCRIPTION_INDEX: Int = 3
    private const val PROJECTION_DTSTART_INDEX: Int = 4
    private const val PROJECTION_DTEND_INDEX: Int = 5
    private const val PROJECTION_ALL_DAY_INDEX: Int = 6
    private const val PROJECTION_TIMEZONE_INDEX: Int = 7
    private const val PROJECTION_DELETED_INDEX: Int = 8
    private const val PROJECTION_VISIBLE_INDEX: Int = 9
    private const val PROJECTION_COLOR_INDEX: Int = 10

    fun parseCursor(cursor: Cursor): EventEntity {
        val timeZoneString = cursor.getString(PROJECTION_TIMEZONE_INDEX)
        var startDate = Date(cursor.getLong(PROJECTION_DTSTART_INDEX))
        var endDate = Date(cursor.getLong(PROJECTION_DTEND_INDEX))
        val allDays = cursor.getInt(PROJECTION_ALL_DAY_INDEX) == 1
        if (allDays) {
            startDate = FixingAllDay.prepareStartDateAfterRead(startDate)
            endDate = FixingAllDay.prepareEndDateAfterRead(endDate)
        }
        return EventEntity.Builder(cursor.getLong(PROJECTION_CALENDAR_ID_INDEX))
            .withId(cursor.getLong(PROJECTION_ID_INDEX)).withTitle(cursor.getString(PROJECTION_TITLE_INDEX) ?: "")
            .withTitle(cursor.getString(PROJECTION_TITLE_INDEX) ?: "")
            .withDescription(cursor.getString(PROJECTION_DESCRIPTION_INDEX) ?: "")
            .withStartDate(startDate)
            .withEndDate(endDate)
            .withAllDays(allDays)
            .withTimeZone(TimeZone.getTimeZone(timeZoneString))
            .withIsDelete(cursor.getInt(PROJECTION_DELETED_INDEX) == 1)
            .withIsVisible(cursor.getInt(PROJECTION_VISIBLE_INDEX) == 1)
            .withColor(cursor.getInt(PROJECTION_COLOR_INDEX)).build()

    }

    fun getEventList(cursor: Cursor?): List<EventEntity> {
        val list = ArrayList<EventEntity>()
        if (cursor != null) {
            while (cursor.moveToNext()) {
                list.add(parseCursor(cursor))
            }
        }
        return list
    }

    fun getEventList(contentResolver: ContentResolver, selection: String?, sortOrder: String?): List<EventEntity> {
        val cursor: Cursor? = contentResolver.query(
            uri,
            EVENT_PROJECTION,
            selection,
            null,
            sortOrder
        )
        val list = getEventList(cursor)
        cursor?.close()
        return list
    }
}