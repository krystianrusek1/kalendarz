package krystian.kalendarz.data.externalcalendar.attendee

data class AttendeeEntity
private constructor(
    val id: Long?,
    val eventId: Long,
    val name: String,
    val email: String?,
    val status: Status,
    val relationship: Relationship,
    val type: Type,
    val identity: String?,
    val idNamespace: String?
) {
    enum class Status {
        Accepted, Declined, Invited, None, Tentative
    }

    enum class Relationship {
        Attendee, None, Organizer, Performer, Speaker
    }

    enum class Type {
        None, Optional, Required, Resource
    }

    fun copyBuilder(): Builder {
        return Builder(eventId, name)
            .withEmail(email)
            .withId(id)
            .withStatus(status)
            .withIdNamespace(idNamespace)
            .withIdentity(identity)
            .withType(type)
            .withRelationship(relationship)
    }

    class Builder(
        val eventId: Long,
        var name: String
    ) {

        private var id: Long? = null
        private var email: String? = null
        private var status: Status = Status.None
        private var relationship: Relationship = Relationship.None
        private var type: Type = Type.None
        private var identity: String? = null
        private var idNamespace: String? = null

        fun withName(name: String): Builder {
            this.name = name
            return this
        }

        fun withId(id: Long?): Builder {
            this.id = id
            return this
        }

        fun withEmail(email: String?): Builder {
            this.email = email
            return this
        }

        fun withStatus(status: Status): Builder {
            this.status = status
            return this
        }

        fun withRelationship(relationship: Relationship): Builder {
            this.relationship = relationship
            return this
        }

        fun withType(type: Type): Builder {
            this.type = type
            return this
        }

        fun withIdentity(identity: String?): Builder {
            this.identity = identity
            return this
        }

        fun withIdNamespace(idNamespace: String?): Builder {
            this.idNamespace = idNamespace
            return this
        }

        fun build(): AttendeeEntity {
            return AttendeeEntity(id, eventId, name, email, status, relationship, type, identity, idNamespace)
        }
    }
}