package krystian.kalendarz.data.externalcalendar.event

import krystian.kalendarz.util.CalendarHelper
import java.util.*

object FixingAllDay {

    fun prepareStartDateForSave(startDate: Date): Long {
        val resultDate = CalendarHelper.prepareStartDate(startDate)
        return resultDate.time - (resultDate.timezoneOffset * 60 * 1000)
    }

    fun prepareEndDateForSave(endDate: Date): Long {
        val calendar = CalendarHelper.getInstance()
        calendar.time = Date(endDate.time - (endDate.timezoneOffset * 60 * 1000))
        calendar.add(Calendar.DATE, 1)

        return calendar.time.time
    }

    fun prepareStartDateAfterRead(date: Date): Date {
        val resultDate = Date(date.time + (date.timezoneOffset * 60 * 1000))
        return CalendarHelper.prepareStartDate(resultDate)
    }

    fun prepareEndDateAfterRead(date: Date): Date {
        val resultDate = Date(date.time + (date.timezoneOffset * 60 * 1000) - 2000)
        return CalendarHelper.prepareEndDate(resultDate)
    }

    fun prepareStartDateToQuery(date: Date): Date {
        val resultDate = CalendarHelper.prepareStartDate(date)
        return Date(resultDate.time - (resultDate.timezoneOffset * 60 * 1000) + 2000)
    }

    fun prepareEndDateToQuery(endDate: Date): Date {
        val preparedStartCalendar = CalendarHelper.getInstance()
        preparedStartCalendar.time = endDate
        preparedStartCalendar.set(Calendar.HOUR_OF_DAY, 0)
        preparedStartCalendar.set(Calendar.MINUTE, 0)
        preparedStartCalendar.set(Calendar.SECOND, 0)
        preparedStartCalendar.set(Calendar.MILLISECOND, 0)
        preparedStartCalendar.add(Calendar.DATE, 1)

        val preparedStartDate = preparedStartCalendar.time

        return Date(preparedStartDate.time - (preparedStartDate.timezoneOffset * 60 * 1000) - 2000)
    }
}