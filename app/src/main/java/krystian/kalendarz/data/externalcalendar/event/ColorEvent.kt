package krystian.kalendarz.data.externalcalendar.event

import androidx.annotation.ColorInt
import java.util.*

data class ColorEvent(
    val calendarId: Long,
    val eventId: Long? = null,
    val title: String = "",
    @ColorInt var color: Int,
    val startDate: Date = Date(0),
    val endDate: Date = Date(0),
    val allDay: Boolean = false
)