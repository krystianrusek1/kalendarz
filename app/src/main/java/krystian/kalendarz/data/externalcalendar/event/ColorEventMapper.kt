package krystian.kalendarz.data.externalcalendar.event

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

object ColorEventMapper {

    val uri: Uri = CalendarContract.Events.CONTENT_URI

    private val EVENT_PROJECTION: Array<String> = arrayOf(
        CalendarContract.Events.CALENDAR_ID,
        CalendarContract.Events.TITLE,
        CalendarContract.Events.CALENDAR_COLOR,
        CalendarContract.Events.DTSTART,
        CalendarContract.Events.DTEND,
        CalendarContract.Events.ALL_DAY
    )

    private const val PROJECTION_CALENDAR_ID_INDEX: Int = 0
    private const val PROJECTION_TITLE_INDEX: Int = 1
    private const val PROJECTION_COLOR_INDEX: Int = 2
    private const val PROJECTION_DTSTART_INDEX: Int = 3
    private const val PROJECTION_DTEND_INDEX: Int = 4
    private const val PROJECTION_ALL_DAY_INDEX: Int = 5

    fun parseCursor(cursor: Cursor): ColorEvent {
        var startDate = Date(cursor.getLong(PROJECTION_DTSTART_INDEX))
        var endDate = Date(cursor.getLong(PROJECTION_DTEND_INDEX))
        val allDays = cursor.getInt(PROJECTION_ALL_DAY_INDEX) == 1
        if (allDays) {
            startDate = FixingAllDay.prepareStartDateAfterRead(startDate)
            endDate = FixingAllDay.prepareEndDateAfterRead(endDate)
        }

        return ColorEvent(
            calendarId = cursor.getLong(PROJECTION_CALENDAR_ID_INDEX),
            title = cursor.getString(PROJECTION_TITLE_INDEX)?:"",
            color = cursor.getInt(PROJECTION_COLOR_INDEX),
            startDate = startDate,
            endDate = endDate,
            allDay = allDays
        )
    }

    fun getEventList(cursor: Cursor?): List<ColorEvent> {
        val list = ArrayList<ColorEvent>()
        if (cursor != null) {
            while (cursor.moveToNext()) {
                list.add(parseCursor(cursor))
            }
        }
        return list
    }

    fun getEventList(contentResolver: ContentResolver, selection: String?, sortOrder: String?): List<ColorEvent> {
        val cursor: Cursor? = contentResolver.query(
            uri,
            EVENT_PROJECTION,
            selection,
            null,
            sortOrder
        )
        val list = getEventList(cursor)
        cursor?.close()
        return list
    }

    fun groupByCalendarId(list: List<ColorEvent>, limit: Int): List<ColorEvent> {
        val resultList = ArrayList<ColorEvent>()
        val set = HashSet<Long>()
        list.forEach {
            if (!set.contains(it.calendarId)) {
                set.add(it.calendarId)
                resultList.add(it)
            }
            if (resultList.size >= limit) {
                return resultList
            }
        }
        return resultList
    }
}