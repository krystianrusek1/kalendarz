package krystian.kalendarz.data.externalcalendar.attendee

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract

object AttendeeMapper {

    val uri: Uri = CalendarContract.Attendees.CONTENT_URI

    internal val ATTENDEE_PROJECTION: Array<String> = arrayOf(
        CalendarContract.Attendees._ID,                     // 0
        CalendarContract.Attendees.EVENT_ID,                     // 1
        CalendarContract.Attendees.ATTENDEE_NAME,            // 2
        CalendarContract.Attendees.ATTENDEE_EMAIL,  // 3
        CalendarContract.Attendees.ATTENDEE_RELATIONSHIP,           // 4
        CalendarContract.Attendees.ATTENDEE_TYPE,                  //5
        CalendarContract.Attendees.ATTENDEE_STATUS,               //6
        CalendarContract.Attendees.ATTENDEE_IDENTITY,               //7
        CalendarContract.Attendees.ATTENDEE_ID_NAMESPACE               //8
    )

    private const val PROJECTION_ID_INDEX: Int = 0
    private const val PROJECTION_EVENT_ID_INDEX: Int = 1
    private const val PROJECTION_ATTENDEE_NAME_INDEX: Int = 2
    private const val PROJECTION_ATTENDEE_EMAIL_INDEX: Int = 3
    private const val PROJECTION_ATTENDEE_RELATIONSHIP_INDEX: Int = 4
    private const val PROJECTION_ATTENDEE_TYPE_INDEX: Int = 5
    private const val PROJECTION_ATTENDEE_STATUS_INDEX: Int = 6
    private const val PROJECTION_ATTENDEE_IDENTITY_INDEX: Int = 7
    private const val PROJECTION_ATTENDEE_ID_NAMESPACE_INDEX: Int = 8

    fun parseCursor(cursor: Cursor): AttendeeEntity {
        return AttendeeEntity.Builder(
            cursor.getLong(PROJECTION_EVENT_ID_INDEX),
            cursor.getString(PROJECTION_ATTENDEE_NAME_INDEX)
        )
            .withId(cursor.getLong(PROJECTION_ID_INDEX))
            .withEmail(cursor.getString(PROJECTION_ATTENDEE_EMAIL_INDEX))
            .withRelationship(parseRelationship(cursor.getInt(PROJECTION_ATTENDEE_RELATIONSHIP_INDEX)))
            .withType(parseType(cursor.getInt(PROJECTION_ATTENDEE_TYPE_INDEX)))
            .withStatus(parseStatus(cursor.getInt(PROJECTION_ATTENDEE_STATUS_INDEX)))
            .withIdentity(cursor.getString(PROJECTION_ATTENDEE_IDENTITY_INDEX))
            .withIdNamespace(cursor.getString(PROJECTION_ATTENDEE_ID_NAMESPACE_INDEX))
            .build()
    }

    fun getAttendeeList(contentResolver: ContentResolver, selection: String?): List<AttendeeEntity> {
        val list = ArrayList<AttendeeEntity>()
        val cur: Cursor? = contentResolver.query(
            uri,
            ATTENDEE_PROJECTION,
            selection,
            null,
            null
        )
        if (cur != null) {
            while (cur.moveToNext()) {
                list.add(parseCursor(cur))
            }
            cur.close()
        }
        return list
    }

    fun parseRelationship(intRelationship: Int): AttendeeEntity.Relationship {
        return when (intRelationship) {
            CalendarContract.Attendees.RELATIONSHIP_ATTENDEE -> AttendeeEntity.Relationship.Attendee
            CalendarContract.Attendees.RELATIONSHIP_ORGANIZER -> AttendeeEntity.Relationship.Organizer
            CalendarContract.Attendees.RELATIONSHIP_PERFORMER -> AttendeeEntity.Relationship.Performer
            CalendarContract.Attendees.RELATIONSHIP_SPEAKER -> AttendeeEntity.Relationship.Speaker
            else -> AttendeeEntity.Relationship.None
        }
    }

    fun parseRelationship(relationship: AttendeeEntity.Relationship): Int {
        return when (relationship) {
            AttendeeEntity.Relationship.Attendee -> CalendarContract.Attendees.RELATIONSHIP_ATTENDEE
            AttendeeEntity.Relationship.Organizer -> CalendarContract.Attendees.RELATIONSHIP_ORGANIZER
            AttendeeEntity.Relationship.Performer -> CalendarContract.Attendees.RELATIONSHIP_PERFORMER
            AttendeeEntity.Relationship.Speaker -> CalendarContract.Attendees.RELATIONSHIP_SPEAKER
            else -> CalendarContract.Attendees.RELATIONSHIP_NONE
        }
    }

    fun parseType(intType: Int): AttendeeEntity.Type {
        return when (intType) {
            CalendarContract.Attendees.TYPE_OPTIONAL -> AttendeeEntity.Type.Optional
            CalendarContract.Attendees.TYPE_REQUIRED -> AttendeeEntity.Type.Required
            CalendarContract.Attendees.TYPE_RESOURCE -> AttendeeEntity.Type.Resource
            else -> AttendeeEntity.Type.None
        }
    }

    fun parseType(type: AttendeeEntity.Type): Int {
        return when (type) {
            AttendeeEntity.Type.Optional -> CalendarContract.Attendees.TYPE_OPTIONAL
            AttendeeEntity.Type.Required -> CalendarContract.Attendees.TYPE_REQUIRED
            AttendeeEntity.Type.Resource -> CalendarContract.Attendees.TYPE_RESOURCE
            else -> CalendarContract.Attendees.TYPE_NONE
        }
    }

    fun parseStatus(intStatus: Int): AttendeeEntity.Status {
        return when (intStatus) {
            CalendarContract.Attendees.ATTENDEE_STATUS_ACCEPTED -> AttendeeEntity.Status.Accepted
            CalendarContract.Attendees.ATTENDEE_STATUS_DECLINED -> AttendeeEntity.Status.Declined
            CalendarContract.Attendees.ATTENDEE_STATUS_INVITED -> AttendeeEntity.Status.Invited
            CalendarContract.Attendees.ATTENDEE_STATUS_TENTATIVE -> AttendeeEntity.Status.Tentative
            else -> AttendeeEntity.Status.None
        }
    }

    fun parseStatus(status: AttendeeEntity.Status): Int {
        return when (status) {
            AttendeeEntity.Status.Accepted -> CalendarContract.Attendees.ATTENDEE_STATUS_ACCEPTED
            AttendeeEntity.Status.Declined -> CalendarContract.Attendees.ATTENDEE_STATUS_DECLINED
            AttendeeEntity.Status.Invited -> CalendarContract.Attendees.ATTENDEE_STATUS_INVITED
            AttendeeEntity.Status.Tentative -> CalendarContract.Attendees.ATTENDEE_STATUS_TENTATIVE
            else -> CalendarContract.Attendees.ATTENDEE_STATUS_NONE
        }
    }
}