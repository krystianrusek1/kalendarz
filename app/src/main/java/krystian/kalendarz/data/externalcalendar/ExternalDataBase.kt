package krystian.kalendarz.data.externalcalendar

import krystian.kalendarz.data.externalcalendar.attendee.ExternalAttendeeDataBase
import krystian.kalendarz.data.externalcalendar.calendar.ExternalCalendarDataBase
import krystian.kalendarz.data.externalcalendar.contact.ExternalContactDataBase
import krystian.kalendarz.data.externalcalendar.event.ExternalEventDataBase

interface ExternalDataBase {
    fun calendar(): ExternalCalendarDataBase
    fun event(): ExternalEventDataBase
    fun attendee(): ExternalAttendeeDataBase
    fun contact(): ExternalContactDataBase
}