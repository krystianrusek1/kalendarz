package krystian.kalendarz.data.externalcalendar.event

import android.content.ContentResolver
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveData

class ColorEventLiveDate(
    private val contentResolver: ContentResolver,
    private val selection: String?,
    val sortOrder: String?,
    val limit: Int
) :
    ContentProviderLiveData<List<ColorEvent>>(
        contentResolver,
        EventMapper.uri
    ) {

    override fun getContentProviderValue(): List<ColorEvent> {
        val list = ColorEventMapper.getEventList(contentResolver, selection, sortOrder)
        return ColorEventMapper.groupByCalendarId(list, limit)
    }
}