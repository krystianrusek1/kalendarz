package krystian.kalendarz.data.externalcalendar.attendee

import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.CalendarContract
import krystian.kalendarz.data.externalcalendar.ThreadHelper
import krystian.kalendarz.data.externalcalendar.livedate.ContentProviderLiveDataFactory
import krystian.kalendarz.util.Loggger

class ExternalAttendeeDataBase(private val appContext: Context) {

    internal val LOG = Loggger(this.javaClass)

    private val contentResolver = appContext.contentResolver

    private val attendeeFactory = object : ContentProviderLiveDataFactory<AttendeeLiveDate>() {
        override fun new(selection: String?, sortOrder: String?, limit: Int): AttendeeLiveDate {
            return AttendeeLiveDate(contentResolver, selection)
        }
    }

    suspend fun insertOrUpdate(attendeeEntity: AttendeeEntity): Long {
        return if (attendeeEntity.id == null) {
            insert(attendeeEntity)
        } else {
            update(attendeeEntity)
        }
    }

    private suspend fun insert(attendeeEntity: AttendeeEntity): Long {
        ThreadHelper.throwIfThreadIsMain()
        val values = getContentValues(attendeeEntity)
        val uri: Uri? = contentResolver.insert(AttendeeMapper.uri, values)
        return uri?.lastPathSegment?.toLong() ?: -1
    }

    private suspend fun update(attendeeEntity: AttendeeEntity): Long {
        ThreadHelper.throwIfThreadIsMain()
        attendeeEntity.id?.let { id ->
            val values = getContentValues(attendeeEntity)
            val updateUri: Uri = ContentUris.withAppendedId(AttendeeMapper.uri, id)
            val rows: Int = contentResolver.update(updateUri, values, null, null)
            LOG.i("Rows updated: $rows")
            return id
        }
        return -1L
    }

    private fun getContentValues(attendeeEntity: AttendeeEntity) = ContentValues().apply {
        put(CalendarContract.Attendees.EVENT_ID, attendeeEntity.eventId)
        put(CalendarContract.Attendees.ATTENDEE_NAME, attendeeEntity.name)
        put(CalendarContract.Attendees.ATTENDEE_EMAIL, attendeeEntity.email)
        put(
            CalendarContract.Attendees.ATTENDEE_RELATIONSHIP,
            AttendeeMapper.parseRelationship(attendeeEntity.relationship)
        )
        put(CalendarContract.Attendees.ATTENDEE_TYPE, AttendeeMapper.parseType(attendeeEntity.type))
        put(CalendarContract.Attendees.ATTENDEE_STATUS, AttendeeMapper.parseStatus(attendeeEntity.status))
        put(CalendarContract.Attendees.ATTENDEE_IDENTITY, attendeeEntity.identity)
        put(CalendarContract.Attendees.ATTENDEE_ID_NAMESPACE, attendeeEntity.idNamespace)
    }

    fun getListLiveDate(eventEntityId: Long): AttendeeLiveDate {
        return attendeeFactory.create("${CalendarContract.Attendees.EVENT_ID} = $eventEntityId")
    }

    suspend fun delete(attendeeEntity: AttendeeEntity) {
        ThreadHelper.throwIfThreadIsMain()
        attendeeEntity.id?.let { id ->
            val deleteUri: Uri = ContentUris.withAppendedId(AttendeeMapper.uri, id)
            val rows: Int = contentResolver.delete(
                deleteUri,
                null,
                null
            )
            LOG.i("Rows deleted: $rows")
        }
    }

    suspend fun deleteByEventId(eventId: Long) {
        ThreadHelper.throwIfThreadIsMain()
        val rows: Int = contentResolver.delete(
            AttendeeMapper.uri,
            "${CalendarContract.Attendees.EVENT_ID} == $eventId",
            null
        )
        LOG.i("Rows deleted: $rows")

    }
}