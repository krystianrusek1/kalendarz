package krystian.kalendarz.data

data class PhoneNumber(val phoneNumber: String) {

    fun isEmpty(): Boolean {
        return phoneNumber.isEmpty()
    }

    fun isNotEmpty(): Boolean {
        return phoneNumber.isNotEmpty()
    }

}