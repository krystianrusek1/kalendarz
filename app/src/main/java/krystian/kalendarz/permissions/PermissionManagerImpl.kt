package krystian.kalendarz.permissions

import javax.inject.Inject

class PermissionManagerImpl @Inject constructor() : PermissionManager {

    private val listeners = ArrayList<PermissionManager.PermissionManagerListener>()

    override fun addPermissionManagerListener(listener: PermissionManager.PermissionManagerListener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener)
            listener.onPermissionChange()
        }
    }

    override fun removePermissionManagerListener(listener: PermissionManager.PermissionManagerListener) {
        listeners.remove(listener)
    }

    override fun triggerPermissionChanged() {
        listeners.forEach {
            it.onPermissionChange()
        }
    }

}
