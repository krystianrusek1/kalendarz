package krystian.kalendarz.permissions

interface PermissionManager {

    fun triggerPermissionChanged()

    fun addPermissionManagerListener(listener: PermissionManagerListener)

    fun removePermissionManagerListener(listener: PermissionManagerListener)

    interface PermissionManagerListener {
        fun onPermissionChange()
    }
}