package krystian.kalendarz.permissions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.IntRange
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import krystian.kalendarz.ui.main.activity.MainActivity

object PermissionHelper {

    fun permissionCheck(activity: MainActivity, @IntRange(from = 0) requestCode: Int, vararg permissions: String) {
        val listPermission = ArrayList<String>()
        permissions.forEach { permission ->
            if (!isGranted(activity, permission) && !ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    permission
                )
            ) {
                listPermission.add(permission)
            }
        }

        if (listPermission.size > 0) {
            val array = arrayOfNulls<String>(listPermission.size)
            ActivityCompat.requestPermissions(
                activity,
                listPermission.toArray(array), requestCode
            )
        }
    }


    fun isCalendarGranted(context: Context): Boolean {
        return isGranted(context, Manifest.permission.WRITE_CALENDAR) &&
                isGranted(context, Manifest.permission.READ_CALENDAR)
    }

    fun isGranted(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }
}