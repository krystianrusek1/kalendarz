import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object LiveDataHelper {

    @Throws(InterruptedException::class)
    fun <T> getValue(liveData: LiveData<T>): T {
        return getValue(2, liveData)
    }

    @Throws(InterruptedException::class)
    fun <T> getValue(timeoutSec: Long?, liveData: LiveData<T>): T {
        val data = arrayOfNulls<Any>(1)
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(o: T) {
                data[0] = o
                latch.countDown()
                val observer = this
                GlobalScope.launch {
                    withContext(Dispatchers.Main) {
                        liveData.removeObserver(observer)
                    }
                }
            }
        }
        liveData.observeForever(observer)
        if (timeoutSec == null) {
            latch.await()
        } else {
            latch.await(timeoutSec, TimeUnit.SECONDS)
        }

        return data[0] as T
    }
}