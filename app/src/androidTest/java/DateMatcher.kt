import krystian.kalendarz.util.CalendarHelper
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import java.util.*


class DateMatcher(private val date: Date) : BaseMatcher<Date>() {

    override fun describeTo(description: Description?) {
        description?.appendText("is (")?.appendValue(date)?.appendText(")")
    }

    override fun matches(item: Any?): Boolean {
        val calendar = CalendarHelper.getInstance()
        calendar.time = date

        val calendarItem = CalendarHelper.getInstance()
        calendarItem.time = item as Date

        var isValid = true
        if (calendar[Calendar.YEAR] != calendarItem[Calendar.YEAR]) {
            isValid = false
        }

        if (calendar[Calendar.MONTH] != calendarItem[Calendar.MONTH]) {
            isValid = false
        }

        if (calendar[Calendar.DATE] != calendarItem[Calendar.DATE]) {
            isValid = false
        }

        if (calendar[Calendar.HOUR_OF_DAY] != calendarItem[Calendar.HOUR_OF_DAY]) {
            isValid = false
        }

        if (calendar[Calendar.MINUTE] != calendarItem[Calendar.MINUTE]) {
            isValid = false
        }

        if (calendar[Calendar.SECOND] != calendarItem[Calendar.SECOND]) {
            isValid = false
        }
        return isValid
    }


}
