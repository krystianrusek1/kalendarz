package krystian.kalendarz.util

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import krystian.kalendarz.data.database.AppDatabase
import org.hamcrest.core.Is
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class AppPhoneNumberUtilTest{
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        AppPhoneNumberUtil.init(context)
    }

    @Test
    fun clearPhoneNumber() {
        assertThat(AppPhoneNumberUtil.clearPhoneNumber("+48665  "), Is.`is`("+48665"))
        assertThat(AppPhoneNumberUtil.clearPhoneNumber("+abcdefghaijotkamnloperstwuyz48665#.-"), Is.`is`("+48665#."))
        assertThat(AppPhoneNumberUtil.clearPhoneNumber("0123456789"), Is.`is`("0123456789"))
    }

    @Test
    fun formatNumber() {
        assertThat(AppPhoneNumberUtil.formatNumber("+48665  "), Is.`is`("+48 665"))
        assertThat(AppPhoneNumberUtil.formatNumber("+48669050639"), Is.`is`("+48 669 050 639"))
        Locale.setDefault(Locale("pl","pl"))
        assertThat(AppPhoneNumberUtil.formatNumber("669050639"), Is.`is`("+48 669 050 639"))
        Locale.setDefault(Locale("us","us"))
        assertThat(AppPhoneNumberUtil.formatNumber("669 050 6399"), Is.`is`("+1 669-050-6399"))
    }
}