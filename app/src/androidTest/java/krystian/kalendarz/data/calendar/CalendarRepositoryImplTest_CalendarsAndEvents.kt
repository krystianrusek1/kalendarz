package krystian.kalendarz.data.calendar

import DateMatcher
import LiveDataHelper
import android.content.Context
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.data.externalcalendar.ExternalDataBase
import krystian.kalendarz.data.externalcalendar.ExternalDataBaseImpl
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.data.externalcalendar.event.ColorEvent
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.data.storage.LocalStorage
import krystian.kalendarz.data.storage.LocalStorageImpl
import krystian.kalendarz.permissions.PermissionManager
import krystian.kalendarz.permissions.PermissionManagerImpl
import krystian.kalendarz.util.CalendarHelper
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

/**
 * Tests [CalendarRepositoryImpl]
 **/
@RunWith(AndroidJUnit4::class)
class CalendarRepositoryImplTest_CalendarsAndEvents {

    companion object {
        const val TEST_EMAIL = "TEST_EMAIL1"

        private const val DISPLAY_NAME = "DISPLAY_NAME"

        private const val DISPLAY_NAME2 = "DISPLAY_NAME2"

        private const val COLOR0 = 10
        private const val COLOR1 = 20
        private const val COLOR2 = 30
        private const val COLOR3 = 40

        private const val EVENT1 = "EVENT1"
        private const val EVENT2 = "EVENT2"
        private const val EVENT3 = "EVENT3"
        private const val EVENT4 = "EVENT4"

        private const val DESCRIPTION = "TestDESCRIPTION"
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var externalDataBase: ExternalDataBase

    private lateinit var suit: CalendarRepositoryImpl

    private lateinit var permissionManager: PermissionManager

    private lateinit var localStorageImpl: LocalStorage

    val listCalendarEventId = ArrayList<Long>()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        externalDataBase = ExternalDataBaseImpl(context)
        permissionManager = PermissionManagerImpl()
        localStorageImpl = LocalStorageImpl(context)
        suit = CalendarRepositoryImpl(context, externalDataBase, permissionManager, localStorageImpl)

        runBlocking {
            clearEvents()

            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR0)
                )
            )
            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR1)
                )
            )
            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR2)
                )
            )
            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR3)
                )
            )
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        runBlocking {
            listCalendarEventId.forEach { id ->
                suit.deleteCalendar(id)
            }
            LiveDataHelper.getValue(suit.getCalendarList())
                .forEach { calendarEntity ->
                    if (calendarEntity.accountName == TEST_EMAIL || calendarEntity.displayName == DISPLAY_NAME) {
                        suit.deleteCalendar(calendarEntity)
                    }

                    if (calendarEntity.accountName == TEST_EMAIL || calendarEntity.displayName == DISPLAY_NAME2) {
                        suit.deleteCalendar(calendarEntity)
                    }

                }

            clearEvents()
        }
    }

    private fun clearEvents() {
        runBlocking {
            LiveDataHelper.getValue(suit.getEventListByDescription(DESCRIPTION))
                .forEach { eventEntry ->
                    suit.deleteEvent(eventEntry)
                }
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertOrUpdateCalendar() {
        runBlocking {
            val calendarEntity = createCalendarEntity(
                displayName = DISPLAY_NAME2,
                accountName = TEST_EMAIL
            )

            suit.insertOrUpdateCalendar(calendarEntity)

            val calendarList = LiveDataHelper.getValue(
                suit.getCalendarList()
            )

            var hasCalendarInserted = false
            calendarList.forEach { calendarEntity ->
                if (calendarEntity.accountName == TEST_EMAIL && calendarEntity.displayName == DISPLAY_NAME2) {
                    hasCalendarInserted = true
                }
            }

            Assert.assertThat(hasCalendarInserted, Matchers.equalTo(true))
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteCalendar() {
        runBlocking {
            val calendarEntity = createCalendarEntity(
                displayName = DISPLAY_NAME2,
                accountName = TEST_EMAIL
            )

            val calendarId = suit.insertOrUpdateCalendar(calendarEntity)
            suit.deleteCalendar(calendarId)

            val calendarList = LiveDataHelper.getValue(
                suit.getCalendarList()
            )

            var hasCalendarInserted = false
            calendarList.forEach { calendarEntity ->
                if (calendarEntity.accountName == TEST_EMAIL && calendarEntity.displayName == DISPLAY_NAME2) {
                    hasCalendarInserted = true
                }
            }

            Assert.assertThat(hasCalendarInserted, Matchers.equalTo(false))
        }

    }

    @Test
    @Throws(Exception::class)
    fun getCalendar() {
        runBlocking {
            val calendarEntity = createCalendarEntity(
                displayName = DISPLAY_NAME2,
                accountName = TEST_EMAIL
            )

            val calendarId = suit.insertOrUpdateCalendar(calendarEntity)

            val calendarResultList = suit.getAllCalendar(TEST_EMAIL)
            val calendarResult = calendarResultList[calendarResultList.size - 1]

            Assert.assertThat(calendarResult!!.id, Matchers.equalTo(calendarId))
            Assert.assertThat(calendarResult!!.displayName, Matchers.equalTo(calendarEntity.displayName))
            Assert.assertThat(calendarResult!!.accountName, Matchers.equalTo(calendarEntity.accountName))
        }

    }

    @Test
    @Throws(Exception::class)
    fun getCalendarListByCoroutines() {
        runBlocking {
            val calendarEntity = createCalendarEntity(
                displayName = DISPLAY_NAME2,
                accountName = TEST_EMAIL
            )

            val calendarId = suit.insertOrUpdateCalendar(calendarEntity)

            val calendarResultList = suit.getCalendarListByCoroutines()
            val calendarResult = calendarResultList[calendarResultList.size - 1]

            Assert.assertThat(calendarResult!!.id, Matchers.equalTo(calendarId))
            Assert.assertThat(calendarResult!!.displayName, Matchers.equalTo(calendarEntity.displayName))
            Assert.assertThat(calendarResult!!.accountName, Matchers.equalTo(calendarEntity.accountName))
        }

    }

    @Test
    @Throws(Exception::class)
    fun insertEvent_allDaysFalse() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = false,
                timeZone = TimeZone.getDefault()
            )


            var resultEvent: EventEntity? = null
            suit.insertOrUpdateEvent(eventEntity)
            LiveDataHelper.getValue(suit.getEventList()).forEach { event ->
                if (event.description == DESCRIPTION) {
                    resultEvent = event
                }
            }


            Assert.assertThat(resultEvent!!.title, Matchers.equalTo(eventEntity.title))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(resultEvent!!.startDate, Matchers.equalTo(eventEntity.startDate))
            Assert.assertThat(resultEvent!!.endDate, Matchers.equalTo(eventEntity.endDate))
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(eventEntity.timeZone))
        }
    }

    @Test
    @Throws(Exception::class)
    fun updateEvent() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = false,
                timeZone = TimeZone.getDefault()
            )


            val eventId = suit.insertOrUpdateEvent(eventEntity)

            var resultEvent = suit.getEvent(eventId)
            suit.insertOrUpdateEvent(
                resultEvent!!.copyBuilder()
                    .withTitle("tittle2")
                    .withTimeZone(TimeZone.getTimeZone("UTC"))
                    .build()
            )

            resultEvent = suit.getEvent(eventId)

            Assert.assertThat(resultEvent!!.title, Matchers.equalTo("tittle2"))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(resultEvent!!.startDate, Matchers.equalTo(eventEntity.startDate))
            Assert.assertThat(resultEvent!!.endDate, Matchers.equalTo(eventEntity.endDate))
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(TimeZone.getTimeZone("UTC")))
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteEvent_getEvent() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = false,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            suit.deleteEvent(eventId)

            val resultEvent = suit.getEvent(eventId)

            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteEvent_getEventList() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "getEventList",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = false,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            suit.deleteEvent(eventId)

            var resultEvent: EventEntity? = null
            LiveDataHelper.getValue(suit.getEventList()).forEach { event ->
                if (event.description == DESCRIPTION) {
                    resultEvent = event
                }
            }
            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteEvent_fetchColorRecursiveByLiveDate() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2000, Calendar.SEPTEMBER, 15)
            val endDate = CalendarHelper.prepareDate(2000, Calendar.NOVEMBER, 10)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = false,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            suit.deleteEvent(eventId)
            var resultEvent: ColorEvent? = null
            LiveDataHelper.getValue(suit.fetchColorGroupedByCalendarIdByLiveDate(startDate, endDate, 1))
                .forEach { event ->
                    resultEvent = event
                }
            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteEvent_fetchEventListRecursiveByLiveDate() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2000, Calendar.SEPTEMBER, 15)
            val endDate = CalendarHelper.prepareDate(2000, Calendar.NOVEMBER, 10)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = false,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            suit.deleteEvent(eventId)
            var resultEvent: EventEntity? = null
            LiveDataHelper.getValue(suit.fetchEventListRecursiveByLiveDate(startDate, endDate)).forEach { event ->
                if (event.description == DESCRIPTION) {
                    resultEvent = event
                }
            }
            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertEvent_allDaysTrue_GMTPlus02() {
        runBlocking {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT+02:00"))

            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 10, 0)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            val resultEvent = suit.getEvent(eventId)


            Assert.assertThat(resultEvent!!.title, Matchers.equalTo(eventEntity.title))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(
                resultEvent!!.startDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 0))
            )
            Assert.assertThat(
                resultEvent!!.endDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 23, 59))
            )
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(TimeZone.getTimeZone("UTC")))
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertEvent_allDaysTrue_GMTMinus05() {
        runBlocking {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT+05:00"))

            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 10, 0)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            val resultEvent = suit.getEvent(eventId)


            Assert.assertThat(resultEvent!!.title, Matchers.equalTo(eventEntity.title))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(
                resultEvent!!.startDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 0))
            )
            Assert.assertThat(
                resultEvent!!.endDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 23, 59))
            )
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(TimeZone.getTimeZone("UTC")))
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertEvent_allDaysTrue_GMTPlus05_getEventList() {
        runBlocking {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT+05:00"))

            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 10, 0)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "GMTPlus05_getEventList",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )

            val eventId = suit.insertOrUpdateEvent(eventEntity)
            var resultEvent: EventEntity? = null
            var numberOfElements = 0
            LiveDataHelper.getValue(suit.getEventList()).forEach {
                if (it.title == "GMTPlus05_getEventList") {
                    resultEvent = it
                    numberOfElements++
                }
            }

            Assert.assertThat(numberOfElements, Matchers.equalTo(1))
            Assert.assertThat(resultEvent!!.title, Matchers.equalTo(eventEntity.title))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(
                resultEvent!!.startDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 0))
            )
            Assert.assertThat(
                resultEvent!!.endDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 23, 59))
            )
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(TimeZone.getTimeZone("UTC")))
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertEvent_allDaysTrue_GMTPlus02_fetchEventListRecursiveByLiveDate() {
        runBlocking {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT+02:00"))

            val startDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 0)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 23, 59)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "GMTPlus02",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )

            suit.insertOrUpdateEvent(eventEntity)
            var resultEvent: EventEntity? = null
            var numberOfElements = 0
            LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 0),
                    CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 0)
                )
            ).forEach {
                if (it.title == "GMTPlus02") {
                    resultEvent = it
                    numberOfElements++
                }
            }

            Assert.assertThat(numberOfElements, Matchers.equalTo(1))
            Assert.assertThat(resultEvent!!.title, Matchers.equalTo(eventEntity.title))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(
                resultEvent!!.startDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 0))
            )
            Assert.assertThat(
                resultEvent!!.endDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 23, 59))
            )
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(TimeZone.getTimeZone("UTC")))

            resultEvent = null
            LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 11, 3, 1),
                    CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 12, 0, 0)
                )
            ).forEach {
                if (it.title == "GMTPlus02") {
                    resultEvent = it
                }
            }
            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertEvent_allDaysTrue_GMTMinus05_fetchEventListRecursiveByCoroutines() {
        runBlocking {
            TimeZone.setDefault(TimeZone.getTimeZone("GMT-05:00"))

            val startDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 10, 0)

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[0],
                title = "GMTMinus05_getEventList",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )

            suit.insertOrUpdateEvent(eventEntity)
            var resultEvent: EventEntity? = null

            var numberOfElements = 0
            suit.fetchEventListRecursiveByCoroutines(
                CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 0),
                CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 11, 0, 0)
            ).forEach {
                if (it.title == "GMTMinus05_getEventList") {
                    resultEvent = it
                    numberOfElements++
                }
            }

            Assert.assertThat(numberOfElements, Matchers.equalTo(1))
            Assert.assertThat(resultEvent!!.title, Matchers.equalTo(eventEntity.title))
            Assert.assertThat(resultEvent!!.description, Matchers.equalTo(eventEntity.description))
            Assert.assertThat(
                resultEvent!!.startDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 0, 0))
            )
            Assert.assertThat(
                resultEvent!!.endDate,
                DateMatcher(CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10, 23, 59))
            )
            Assert.assertThat(resultEvent!!.allDays, Matchers.equalTo(eventEntity.allDays))
            Assert.assertThat(resultEvent!!.timeZone, Matchers.equalTo(TimeZone.getTimeZone("UTC")))

            resultEvent = null
            suit.fetchEventListRecursiveByCoroutines(
                CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 11, 0, 0),
                CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 12, 23, 59)
            ).forEach {
                if (it.title == "GMTMinus05_getEventList") {
                    resultEvent = it
                }
            }

            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun invisibleCalendar_getEventList() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR3, visible = false)
                )
            )

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[listCalendarEventId.size - 1],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )


            var resultEvent: EventEntity? = null
            suit.insertOrUpdateEvent(eventEntity)
            LiveDataHelper.getValue(suit.getEventList()).forEach { event ->
                if (event.description == DESCRIPTION) {
                    resultEvent = event
                }
            }


            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun invisibleCalendar_fetchEventListRecursiveByLiveDate() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR3, visible = false)
                )
            )

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[listCalendarEventId.size - 1],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )


            var resultEvent: EventEntity? = null
            suit.insertOrUpdateEvent(eventEntity)
            LiveDataHelper.getValue(
                null,
                suit.fetchEventListRecursiveByLiveDate(
                    CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 0, 0, 59),
                    CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)
                )
            ).forEach { event ->
                if (event.description == DESCRIPTION) {
                    resultEvent = event
                }
            }


            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun invisibleCalendar_fetchEventListRecursiveByCoroutines() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR3, visible = false)
                )
            )

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[listCalendarEventId.size - 1],
                title = "title",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )


            var resultEvent: EventEntity? = null
            suit.insertOrUpdateEvent(eventEntity)

            suit.fetchEventListRecursiveByCoroutines(
                CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 0, 0, 59),
                CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)
            ).forEach { event ->
                if (event.description == DESCRIPTION) {
                    resultEvent = event
                }
            }


            Assert.assertNull(resultEvent)
        }
    }

    @Test
    @Throws(Exception::class)
    fun invisibleCalendar_fetchColorRecursiveByLiveDate() {
        runBlocking {
            val startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15, 0, 59)
            val endDate = CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10)

            listCalendarEventId.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR3, visible = false)
                )
            )

            val eventEntity = createEventEntity(
                calendarId = listCalendarEventId[listCalendarEventId.size - 1],
                title = "invisibleCalendar_fetchColorRecursiveByLiveDate",
                description = DESCRIPTION,
                startDate = startDate,
                endDate = endDate,
                allDays = true,
                timeZone = TimeZone.getDefault()
            )


            var resultEvent: ColorEvent? = null
            suit.insertOrUpdateEvent(eventEntity)
            LiveDataHelper.getValue(
                null,
                suit.fetchColorGroupedByCalendarIdByLiveDate(
                    CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 0, 0, 59),
                    CalendarHelper.prepareDate(2019, Calendar.NOVEMBER, 10),
                    3
                )
            ).forEach { event ->
                if (event.title == "invisibleCalendar_fetchColorRecursiveByLiveDate") {
                    resultEvent = event
                }
            }

            Assert.assertNull(resultEvent)

        }
    }

    @Test
    @Throws(Exception::class)
    fun fetchEventListRecursiveByLiveDate_theTimeInterval() {

        runBlocking {

            val listEventIds = ArrayList<Long>()

            val eventEntityDay10to15 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 10-15",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 0, 0),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 15, 0, 0)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay10to15))

            val eventEntityDay13to20 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 13-20",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 13, 5, 5),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 20, 0, 0)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay13to20))

            val eventEntityDay14to14 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 14-14",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 14, 5, 5),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 14, 5, 5)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay14to14))

            val eventEntityDay21to25 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 21-25",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 21, 5, 5),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 25, 23, 59)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay21to25))

            val emptyEntryList = LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 9),
                    createEndDate(0, Calendar.SEPTEMBER, 9)
                )
            )

            Assert.assertThat(" list is empty", emptyEntryList.size, Matchers.equalTo(0))

            val fullEntryList = LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 14),
                    createEndDate(0, Calendar.SEPTEMBER, 14)
                )
            )
            Assert.assertThat(" list is full", fullEntryList.size, Matchers.equalTo(3))
            Assert.assertNotNull(findEventByName(fullEntryList, "Day 10-15"))
            Assert.assertNotNull(findEventByName(fullEntryList, "Day 13-20"))
            Assert.assertNotNull(findEventByName(fullEntryList, "Day 14-14"))

            val startDateBorderEntryList = LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 13),
                    createEndDate(0, Calendar.SEPTEMBER, 13)
                )
            )

            Assert.assertThat(startDateBorderEntryList.size, Matchers.equalTo(2))
            Assert.assertNotNull(findEventByName(startDateBorderEntryList, "Day 10-15"))
            Assert.assertNotNull(findEventByName(startDateBorderEntryList, "Day 13-20"))

            val endDateBorderEntryList = LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 25),
                    createEndDate(0, Calendar.SEPTEMBER, 25)
                )
            )

            Assert.assertThat(endDateBorderEntryList.size, Matchers.equalTo(1))
            Assert.assertNotNull(findEventByName(endDateBorderEntryList, "Day 21-25"))

            listEventIds.forEach { id ->
                suit.deleteEvent(id)
            }
        }
    }

    @Test
    @Throws(Exception::class)
    fun fetchEventListRecursiveByLiveDate_order() {
        runBlocking {
            val listEventIds = ArrayList<Long>()

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        EVENT1,
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 5, 5),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        EVENT2,
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 0, 0),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        EVENT3,
                        CalendarHelper.prepareDate(0, Calendar.AUGUST, 25, 0, 0),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            val entryList = LiveDataHelper.getValue(
                suit.fetchEventListRecursiveByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 10),
                    createEndDate(0, Calendar.SEPTEMBER, 10)
                )
            )

            Assert.assertThat(entryList[0].title, Matchers.equalTo(EVENT3))
            Assert.assertThat(entryList[1].title, Matchers.equalTo(EVENT2))
            Assert.assertThat(entryList[2].title, Matchers.equalTo(EVENT1))

            listEventIds.forEach { id ->
                suit.deleteEvent(id)
            }
        }
    }

    @Test
    @Throws(Exception::class)
    fun fetchEventListRecursiveByCoroutines_order() {

        runBlocking {

            val listEventIds = ArrayList<Long>()

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        EVENT1,
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 5, 5),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        EVENT2,
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 0, 0),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        EVENT3,
                        CalendarHelper.prepareDate(0, Calendar.AUGUST, 25, 0, 0),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            val entryList = suit.fetchEventListRecursiveByCoroutines(
                createStartDate(0, Calendar.SEPTEMBER, 10),
                createEndDate(0, Calendar.SEPTEMBER, 10)
            )


            Assert.assertThat(entryList[0].title, Matchers.equalTo(EVENT3))
            Assert.assertThat(entryList[1].title, Matchers.equalTo(EVENT2))
            Assert.assertThat(entryList[2].title, Matchers.equalTo(EVENT1))

            listEventIds.forEach { id ->
                suit.deleteEvent(id)
            }

        }
    }

    @Test
    @Throws(Exception::class)
    fun fetchEventListRecursiveByCoroutines() {
        runBlocking {

            val listEventIds = ArrayList<Long>()

            val eventEntityDay10to15 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 10-15",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 0, 0),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 15, 0, 0)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay10to15))

            val eventEntityDay13to20 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 13-20",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 13, 5, 5),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 20, 0, 0)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay13to20))

            val eventEntityDay14to14 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 14-14",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 14, 5, 5),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 14, 5, 5)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay14to14))

            val eventEntityDay21to25 = createEventEntity(
                listCalendarEventId[0],
                title = "Day 21-25",
                startDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 21, 5, 5),
                endDate = CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 25, 23, 59)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay21to25))

            val emptyEntryList =
                suit.fetchEventListRecursiveByCoroutines(
                    createStartDate(0, Calendar.SEPTEMBER, 9),
                    createEndDate(0, Calendar.SEPTEMBER, 9)
                )


            Assert.assertThat(" list is empty", emptyEntryList.size, Matchers.equalTo(0))

            val fullEntryList =
                suit.fetchEventListRecursiveByCoroutines(
                    createStartDate(0, Calendar.SEPTEMBER, 14),
                    createEndDate(0, Calendar.SEPTEMBER, 14)
                )

            Assert.assertThat(" list is full", fullEntryList.size, Matchers.equalTo(3))
            Assert.assertNotNull(findEventByName(fullEntryList, "Day 10-15"))
            Assert.assertNotNull(findEventByName(fullEntryList, "Day 13-20"))
            Assert.assertNotNull(findEventByName(fullEntryList, "Day 14-14"))

            val startDateBorderEntryList =
                suit.fetchEventListRecursiveByCoroutines(
                    createStartDate(0, Calendar.SEPTEMBER, 13),
                    createEndDate(0, Calendar.SEPTEMBER, 13)
                )


            Assert.assertThat(startDateBorderEntryList.size, Matchers.equalTo(2))
            Assert.assertNotNull(findEventByName(startDateBorderEntryList, "Day 10-15"))
            Assert.assertNotNull(findEventByName(startDateBorderEntryList, "Day 13-20"))

            val endDateBorderEntryList =
                suit.fetchEventListRecursiveByCoroutines(
                    createStartDate(0, Calendar.SEPTEMBER, 25),
                    createEndDate(0, Calendar.SEPTEMBER, 25)
                )


            Assert.assertThat(endDateBorderEntryList.size, Matchers.equalTo(1))
            Assert.assertNotNull(findEventByName(endDateBorderEntryList, "Day 21-25"))

            listEventIds.forEach { id ->
                suit.deleteEvent(id)
            }
        }
    }

    @Test
    @Throws(Exception::class)
    fun fetchColorRecursiveByLiveDate_checkColors() {
        runBlocking {
            val listEventIds = ArrayList<Long>()

            val eventEntityDay10to15 = createEventEntity(
                listCalendarEventId[0],
                "Day 10-15",
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 0, 0),
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 15, 0, 0)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay10to15))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay10to15))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay10to15))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay10to15))

            val eventEntityDay13to20 = createEventEntity(
                listCalendarEventId[1],
                "Day 13-20",
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 13, 5, 5),
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 20, 0, 0)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay13to20))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay13to20))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay13to20))

            val eventEntityDay14to14 = createEventEntity(
                listCalendarEventId[2],
                "Day 14-14",
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 14, 5, 5),
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 14, 5, 5)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay14to14))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay14to14))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay14to14))

            val eventEntityDay21to25 = createEventEntity(
                listCalendarEventId[3],
                "Day 21-25",
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 21, 5, 5),
                CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 25, 23, 59)
            )
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay21to25))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay21to25))
            listEventIds.add(suit.insertOrUpdateEvent(eventEntityDay21to25))

            val emptyEntryList = LiveDataHelper.getValue(
                suit.fetchColorGroupedByCalendarIdByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 9),
                    createEndDate(0, Calendar.SEPTEMBER, 9),
                    3
                )
            )

            Assert.assertThat(" list is empty", emptyEntryList.size, Matchers.equalTo(0))

            val fullEntryList = LiveDataHelper.getValue(
                suit.fetchColorGroupedByCalendarIdByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 14),
                    createEndDate(0, Calendar.SEPTEMBER, 14),
                    3
                )
            )
            Assert.assertThat(" list is full", fullEntryList.size, Matchers.equalTo(3))
            Assert.assertThat(fullEntryList[0].color, Matchers.equalTo(COLOR0))
            Assert.assertThat(
                fullEntryList[0].startDate,
                Matchers.equalTo(eventEntityDay10to15.startDate)
            )
            Assert.assertThat(
                fullEntryList[0].endDate,
                Matchers.equalTo(eventEntityDay10to15.endDate)
            )
            Assert.assertThat(fullEntryList[1].color, Matchers.equalTo(COLOR1))
            Assert.assertThat(
                fullEntryList[1].startDate,
                Matchers.equalTo(eventEntityDay13to20.startDate)
            )
            Assert.assertThat(
                fullEntryList[1].endDate,
                Matchers.equalTo(eventEntityDay13to20.endDate)
            )
            Assert.assertThat(fullEntryList[2].color, Matchers.equalTo(COLOR2))

            val startDateBorderEntryList = LiveDataHelper.getValue(
                suit.fetchColorGroupedByCalendarIdByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 13),
                    createEndDate(0, Calendar.SEPTEMBER, 13),
                    3
                )
            )

            Assert.assertThat(startDateBorderEntryList.size, Matchers.equalTo(2))
            Assert.assertThat(startDateBorderEntryList[0].color, Matchers.equalTo(COLOR0))
            Assert.assertThat(
                startDateBorderEntryList[0].startDate,
                Matchers.equalTo(eventEntityDay10to15.startDate)
            )
            Assert.assertThat(
                startDateBorderEntryList[0].endDate,
                Matchers.equalTo(eventEntityDay10to15.endDate)
            )
            Assert.assertThat(startDateBorderEntryList[1].color, Matchers.equalTo(COLOR1))
            Assert.assertThat(
                startDateBorderEntryList[1].startDate,
                Matchers.equalTo(eventEntityDay13to20.startDate)
            )
            Assert.assertThat(
                startDateBorderEntryList[1].endDate,
                Matchers.equalTo(eventEntityDay13to20.endDate)
            )

            val endDateBorderEntryList = LiveDataHelper.getValue(
                suit.fetchColorGroupedByCalendarIdByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 25),
                    createEndDate(0, Calendar.SEPTEMBER, 25),
                    3
                )
            )

            Assert.assertThat(endDateBorderEntryList.size, Matchers.equalTo(1))
            Assert.assertThat(endDateBorderEntryList[0].color, Matchers.equalTo(COLOR3))
            Assert.assertThat(
                endDateBorderEntryList[0].startDate,
                Matchers.equalTo(eventEntityDay21to25.startDate)
            )
            Assert.assertThat(
                endDateBorderEntryList[0].endDate,
                Matchers.equalTo(eventEntityDay21to25.endDate)
            )

            listEventIds.forEach { id ->
                suit.deleteEvent(id)
            }
        }
    }


    @Test
    @Throws(Exception::class)
    fun fetchColorRecursiveByLiveDate_order() {

        runBlocking {
            val listEventIds = ArrayList<Long>()

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        listCalendarEventId[0],
                        EVENT1,
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 5, 5),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        listCalendarEventId[1],
                        EVENT2,
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 10, 0, 0),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    createEventEntity(
                        listCalendarEventId[2],
                        EVENT3,
                        CalendarHelper.prepareDate(0, Calendar.AUGUST, 25, 0, 0),
                        CalendarHelper.prepareDate(0, Calendar.SEPTEMBER, 30, 0, 0)
                    )
                )
            )

            val colorEventList = LiveDataHelper.getValue(
                suit.fetchColorGroupedByCalendarIdByLiveDate(
                    createStartDate(0, Calendar.SEPTEMBER, 10),
                    createEndDate(0, Calendar.SEPTEMBER, 10),
                    3
                )
            )

            Assert.assertThat(colorEventList[0].title, Matchers.equalTo(EVENT3))
            Assert.assertThat(colorEventList[1].title, Matchers.equalTo(EVENT2))
            Assert.assertThat(colorEventList[2].title, Matchers.equalTo(EVENT1))

            listEventIds.forEach { id ->
                suit.deleteEvent(id)
            }
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_OneColor() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )


            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15))

            val colors = LiveDataHelper.getValue(suit.getColors(from, to, 3))
            val list = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    15
                )
            )]

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_TwoColor() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[1],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 14))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16))

            val diffModel = LiveDataHelper.getValue(null, suit.getColors(from, to, 3))
            Assert.assertThat(
                diffModel.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            val list = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    15
                )
            )]

            Assert.assertThat(list!!.size, Matchers.equalTo(2))
            Assert.assertThat(list!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))
            Assert.assertThat(list!![1].calendarId, Matchers.equalTo(listCalendarEventId[1]))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_OneMonth() {
        runBlocking {
            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2000, Calendar.NOVEMBER, 25))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2001, Calendar.JANUARY, 5))
            suit.getColors(from, to, 3).observeForever { }

            var lastChange: YearMonthDay? = null
            suit.getColorsPerDayHasMapLiveData().observeForever { change ->
                lastChange = change
            }

            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2000, Calendar.DECEMBER, 8, 10, 0),
                    endDate = CalendarHelper.prepareDate(2000, Calendar.DECEMBER, 8, 11, 0),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )
            delay(1500)
            Assert.assertThat(
                lastChange,
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2000, Calendar.DECEMBER, 8)))
            )

            val list = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2000,
                    Calendar.DECEMBER,
                    8
                )
            )]

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_TwoColor_DeferenceDay() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[1],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 16),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 16),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 14))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16))

            val diffModel = LiveDataHelper.getValue(null, suit.getColors(from, to, 3))
            Assert.assertThat(
                diffModel.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )
            Assert.assertThat(
                diffModel.listDiff[1],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16)))
            )

            val list15 = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    15
                )
            )]

            Assert.assertThat(list15!!.size, Matchers.equalTo(1))
            Assert.assertThat(list15!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))

            val list16 = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    16
                )
            )]

            Assert.assertThat(list16!!.size, Matchers.equalTo(1))
            Assert.assertThat(list16!![0].calendarId, Matchers.equalTo(listCalendarEventId[1]))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_LongEvent() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 2),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 20),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 10))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15))

            val diffModel = LiveDataHelper.getValue(suit.getColors(from, to, 3))

            Assert.assertThat(
                diffModel.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 10)))
            )
            Assert.assertThat(
                diffModel.listDiff[1],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 11)))
            )
            Assert.assertThat(
                diffModel.listDiff[2],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 12)))
            )
            Assert.assertThat(
                diffModel.listDiff[3],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 13)))
            )
            Assert.assertThat(
                diffModel.listDiff[4],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 14)))
            )
            Assert.assertThat(
                diffModel.listDiff[5],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            Assert.assertThat(
                suit.getColorsPerDayHasMap()[YearMonthDay(2019, Calendar.SEPTEMBER, 10)]!![0].calendarId,
                Matchers.equalTo(listCalendarEventId[0])
            )
            Assert.assertThat(
                suit.getColorsPerDayHasMap()[YearMonthDay(2019, Calendar.SEPTEMBER, 11)]!![0].calendarId,
                Matchers.equalTo(listCalendarEventId[0])
            )
            Assert.assertThat(
                suit.getColorsPerDayHasMap()[YearMonthDay(2019, Calendar.SEPTEMBER, 12)]!![0].calendarId,
                Matchers.equalTo(listCalendarEventId[0])
            )
            Assert.assertThat(
                suit.getColorsPerDayHasMap()[YearMonthDay(2019, Calendar.SEPTEMBER, 13)]!![0].calendarId,
                Matchers.equalTo(listCalendarEventId[0])
            )
            Assert.assertThat(
                suit.getColorsPerDayHasMap()[YearMonthDay(2019, Calendar.SEPTEMBER, 14)]!![0].calendarId,
                Matchers.equalTo(listCalendarEventId[0])
            )
            Assert.assertThat(
                suit.getColorsPerDayHasMap()[YearMonthDay(2019, Calendar.SEPTEMBER, 15)]!![0].calendarId,
                Matchers.equalTo(listCalendarEventId[0])
            )
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_empty() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 2),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 5),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(100, Calendar.SEPTEMBER, 10))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(100, Calendar.SEPTEMBER, 15))

            val diffModel = LiveDataHelper.getValue(suit.getColors(from, to, 3))

            Assert.assertThat(diffModel.listDiff.isEmpty(), Matchers.equalTo(true))

            Assert.assertNull(suit.getColorsPerDayHasMap()[YearMonthDay(100, Calendar.SEPTEMBER, 10)])
            Assert.assertNull(suit.getColorsPerDayHasMap()[YearMonthDay(100, Calendar.SEPTEMBER, 11)])
            Assert.assertNull(suit.getColorsPerDayHasMap()[YearMonthDay(100, Calendar.SEPTEMBER, 12)])
            Assert.assertNull(suit.getColorsPerDayHasMap()[YearMonthDay(100, Calendar.SEPTEMBER, 13)])
            Assert.assertNull(suit.getColorsPerDayHasMap()[YearMonthDay(100, Calendar.SEPTEMBER, 14)])
            Assert.assertNull(suit.getColorsPerDayHasMap()[YearMonthDay(100, Calendar.SEPTEMBER, 15)])
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_update() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 14))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16))
            val liveDate = suit.getColors(from, to, 3)
            liveDate.observeForever { }
            val diffModel = LiveDataHelper.getValue(liveDate)
            Assert.assertThat(
                diffModel.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[1],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 16),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 16),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            delay(300)

            val diffModel2 = LiveDataHelper.getValue(liveDate)
            Assert.assertThat(
                diffModel2.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16)))
            )

            val list15 = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    15
                )
            )]

            Assert.assertThat(list15!!.size, Matchers.equalTo(1))
            Assert.assertThat(list15!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))

            val list16 = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    16
                )
            )]

            Assert.assertThat(list16!!.size, Matchers.equalTo(1))
            Assert.assertThat(list16!![0].calendarId, Matchers.equalTo(listCalendarEventId[1]))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_update_the_same_day() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 14))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16))
            val liveDate = suit.getColors(from, to, 3)
            liveDate.observeForever { }
            val diffModel = LiveDataHelper.getValue(null, liveDate)
            Assert.assertThat(
                diffModel.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[1],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = false,
                    timeZone = TimeZone.getDefault()
                )
            )

            delay(300)

            val diffModel2 = LiveDataHelper.getValue(null, liveDate)
            Assert.assertThat(
                diffModel2.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            val list15 = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    15
                )
            )]

            Assert.assertThat(list15!!.size, Matchers.equalTo(2))
            Assert.assertThat(list15!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))
            Assert.assertThat(list15!![1].calendarId, Matchers.equalTo(listCalendarEventId[1]))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getColors_update_the_same_day_AllDayTrue() {

        runBlocking {
            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[0],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = true,
                    timeZone = TimeZone.getDefault()
                )
            )

            val from = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 14))
            val to = YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 16))
            val liveDate = suit.getColors(from, to, 3)
            liveDate.observeForever { }
            val diffModel = LiveDataHelper.getValue(null, liveDate)
            Assert.assertThat(
                diffModel.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            suit.insertOrUpdateEvent(
                createEventEntity(
                    calendarId = listCalendarEventId[1],
                    title = "title",
                    description = DESCRIPTION,
                    startDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    endDate = CalendarHelper.prepareDate(2019, Calendar.SEPTEMBER, 15),
                    allDays = true,
                    timeZone = TimeZone.getDefault()
                )
            )

            delay(300)

            val diffModel2 = LiveDataHelper.getValue(null, liveDate)
            Assert.assertThat(
                diffModel2.listDiff[0],
                Matchers.equalTo(YearMonthDay.build(CalendarHelper.prepareCalendar(2019, Calendar.SEPTEMBER, 15)))
            )

            val list15 = suit.getColorsPerDayHasMap()[YearMonthDay.build(
                CalendarHelper.prepareCalendar(
                    2019,
                    Calendar.SEPTEMBER,
                    15
                )
            )]

            Assert.assertThat(list15!!.size, Matchers.equalTo(2))
            Assert.assertThat(list15!![0].calendarId, Matchers.equalTo(listCalendarEventId[0]))
            Assert.assertThat(list15!![1].calendarId, Matchers.equalTo(listCalendarEventId[1]))
        }
    }


    fun findEventByName(list: List<ColorEvent>, title: String): ColorEvent? {
        list.forEach {
            if (it.title == title) {
                return it
            }
        }
        return null
    }

    fun findEventByName(list: List<EventEntity>, title: String): EventEntity? {
        list.forEach {
            if (it.title == title) {
                return it
            }
        }
        return null
    }

    fun createCalendarEntity(
        displayName: String? = null,
        accountName: String = CalendarEntity.ACCOUNT_LOCAL_NAME,
        @ColorInt color: Int = Color.TRANSPARENT,
        visible: Boolean = true
    ): CalendarEntity {
        return CalendarEntity.Builder().withDisplayName(displayName)
            .withAccountName(accountName).withColor(color).withVisible(visible)
            .build()
    }

    fun createStartDate(year: Int, month: Int, date: Int): Date {
        val calendar = CalendarHelper.getInstance()
        calendar.time = Date(0)
        calendar.set(year, month, date, 0, 0, 0)
        return CalendarHelper.prepareStartDate(calendar.time)
    }

    fun createEndDate(year: Int, month: Int, date: Int): Date {
        val calendar = CalendarHelper.getInstance()
        calendar.time = Date(0)
        calendar.set(year, month, date, 0, 0, 0)
        return CalendarHelper.prepareEndDate(calendar.time)
    }

    fun createEventEntity(title: String, startDate: Date, endDate: Date): EventEntity {
        return createEventEntity(listCalendarEventId[0], title, startDate, endDate)
    }

    fun createEventEntity(
        calendarId: Long,
        title: String = "",
        startDate: Date,
        endDate: Date,
        description: String = "",
        allDays: Boolean = false,
        timeZone: TimeZone = TimeZone.getDefault()
    ): EventEntity {
        return EventEntity.Builder(calendarId)
            .withTitle(title)
            .withStartDate(startDate)
            .withEndDate(endDate)
            .withAllDays(allDays)
            .withTimeZone(timeZone)
            .withDescription(description)
            .build()
    }
}