package krystian.kalendarz.data.calendar

import LiveDataHelper
import android.content.Context
import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.data.externalcalendar.ExternalDataBase
import krystian.kalendarz.data.externalcalendar.ExternalDataBaseImpl
import krystian.kalendarz.data.externalcalendar.attendee.AttendeeEntity
import krystian.kalendarz.data.externalcalendar.calendar.CalendarEntity
import krystian.kalendarz.data.externalcalendar.event.EventEntity
import krystian.kalendarz.data.storage.LocalStorage
import krystian.kalendarz.data.storage.LocalStorageImpl
import krystian.kalendarz.permissions.PermissionManager
import krystian.kalendarz.permissions.PermissionManagerImpl
import krystian.kalendarz.util.CalendarHelper
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

/**
 * Tests [CalendarRepositoryImpl]
 **/
@RunWith(AndroidJUnit4::class)
class CalendarRepositoryImplTest_Attendee {

    companion object {
        const val TEST_EMAIL = "TEST_EMAIL1"

        private const val DISPLAY_NAME = "DISPLAY_NAME"

        private const val DISPLAY_NAME2 = "DISPLAY_NAME2"

        private const val COLOR0 = 10
        private const val COLOR1 = 20
        private const val COLOR2 = 30
        private const val COLOR3 = 40

        private const val EVENT1 = "EVENT1"
        private const val EVENT2 = "EVENT2"
        private const val EVENT3 = "EVENT3"
        private const val EVENT4 = "EVENT4"

        private const val ATTENDEE_NAME1 = "ATTENDEE_NAME1"
        private const val ATTENDEE_NAME2 = "ATTENDEE_NAME2"

        private const val DESCRIPTION = "TestDESCRIPTION"
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var externalDataBase: ExternalDataBase

    private lateinit var suit: CalendarRepositoryImpl

    private lateinit var permissionManager: PermissionManager

    private lateinit var localStorageImpl: LocalStorage

    val listCalendarEventIds = ArrayList<Long>()

    val listEventIds = ArrayList<Long>()

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        externalDataBase = ExternalDataBaseImpl(context)
        permissionManager = PermissionManagerImpl()
        localStorageImpl = LocalStorageImpl(context)
        suit = CalendarRepositoryImpl(context, externalDataBase, permissionManager, localStorageImpl)

        runBlocking {
            clearEvents()

            listCalendarEventIds.add(
                suit.insertOrUpdateCalendar(
                    createCalendarEntity(accountName = TEST_EMAIL, color = COLOR0)
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    EventEntity.Builder(listCalendarEventIds[0]).withDescription(DESCRIPTION)
                        .withTitle(EVENT1)
                        .withStartDate(CalendarHelper.prepareDate(2000, Calendar.SEPTEMBER, 15))
                        .withEndDate(CalendarHelper.prepareDate(2000, Calendar.SEPTEMBER, 16))
                        .build()
                )
            )

            listEventIds.add(
                suit.insertOrUpdateEvent(
                    EventEntity.Builder(listCalendarEventIds[0]).withDescription(DESCRIPTION)
                        .withTitle(EVENT2)
                        .withStartDate(CalendarHelper.prepareDate(2000, Calendar.OCTOBER, 15))
                        .withEndDate(CalendarHelper.prepareDate(2000, Calendar.OCTOBER, 15))
                        .build()
                )
            )
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        runBlocking {
            listCalendarEventIds.forEach { id ->
                suit.deleteCalendar(id)
            }
            LiveDataHelper.getValue(suit.getCalendarList())
                .forEach { calendarEntity ->
                    if (calendarEntity.accountName == TEST_EMAIL || calendarEntity.displayName == DISPLAY_NAME) {
                        suit.deleteCalendar(calendarEntity)
                    }

                    if (calendarEntity.accountName == TEST_EMAIL || calendarEntity.displayName == DISPLAY_NAME2) {
                        suit.deleteCalendar(calendarEntity)
                    }

                }

            clearEvents()
        }
    }

    private fun clearEvents() {
        runBlocking {
            LiveDataHelper.getValue(suit.getEventListByDescription(DESCRIPTION))
                .forEach { eventEntry ->
                    suit.deleteEvent(eventEntry)
                }
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertAttendee() {
        runBlocking {
            val addedAttendee = AttendeeEntity.Builder(listEventIds[0], ATTENDEE_NAME1)
                .withEmail("email")
                .withIdentity("Identity")
                .withIdNamespace("IdNamespace")
                .withRelationship(AttendeeEntity.Relationship.Speaker)
                .withStatus(AttendeeEntity.Status.Tentative)
                .withType(AttendeeEntity.Type.Required).build()

            suit.insertOrUpdateAttendee(addedAttendee)

            val list = LiveDataHelper.getValue(suit.getAttendeeListLiveDate(listEventIds[0]))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].name, Matchers.equalTo(addedAttendee.name))
            Assert.assertThat(list!![0].email, Matchers.equalTo(addedAttendee.email))
            Assert.assertThat(list!![0].identity, Matchers.equalTo(addedAttendee.identity))
            Assert.assertThat(list!![0].idNamespace, Matchers.equalTo(addedAttendee.idNamespace))
            Assert.assertThat(list!![0].relationship, Matchers.equalTo(addedAttendee.relationship))
            Assert.assertThat(list!![0].type, Matchers.equalTo(addedAttendee.type))
            Assert.assertThat(list!![0].status, Matchers.equalTo(addedAttendee.status))
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertAttendee_minimalParameters() {
        runBlocking {
            val addedAttendee = AttendeeEntity.Builder(listEventIds[0], ATTENDEE_NAME1).build()

            suit.insertOrUpdateAttendee(addedAttendee)

            val list = LiveDataHelper.getValue(suit.getAttendeeListLiveDate(listEventIds[0]))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].name, Matchers.equalTo(addedAttendee.name))
            Assert.assertThat(list!![0].email, Matchers.equalTo(addedAttendee.email))
            Assert.assertNull(list!![0].identity)
            Assert.assertNull(list!![0].idNamespace)
            Assert.assertThat(list!![0].relationship, Matchers.equalTo(AttendeeEntity.Relationship.None))
            Assert.assertThat(list!![0].type, Matchers.equalTo(AttendeeEntity.Type.None))
            Assert.assertThat(list!![0].status, Matchers.equalTo(AttendeeEntity.Status.None))
        }
    }

    @Test
    @Throws(Exception::class)
    fun updateAttendee() {
        runBlocking {
            val addedAttendee = AttendeeEntity.Builder(listEventIds[0], ATTENDEE_NAME1)
                .withEmail("email")
                .withIdentity("Identity")
                .withIdNamespace("IdNamespace")
                .withRelationship(AttendeeEntity.Relationship.Speaker)
                .withStatus(AttendeeEntity.Status.Tentative)
                .withType(AttendeeEntity.Type.Required).build()

            suit.insertOrUpdateAttendee(addedAttendee)

            val resultLiveDate = suit.getAttendeeListLiveDate(listEventIds[0])
            resultLiveDate.observeForever{  }
            delay(50)
            Assert.assertThat(resultLiveDate.value!!.size, Matchers.equalTo(1))

            val copyAttendee = resultLiveDate.value!![0].copyBuilder().withEmail("email2")
                .withName(ATTENDEE_NAME2)
                .build()

            suit.insertOrUpdateAttendee(copyAttendee)
            delay(50)

            Assert.assertThat(resultLiveDate.value!!.size, Matchers.equalTo(1))
            Assert.assertThat(resultLiveDate.value!![0].name, Matchers.equalTo(copyAttendee.name))
            Assert.assertThat(resultLiveDate.value!![0].email, Matchers.equalTo(copyAttendee.email))
            Assert.assertThat(resultLiveDate.value!![0].identity, Matchers.equalTo(copyAttendee.identity))
            Assert.assertThat(resultLiveDate.value!![0].idNamespace, Matchers.equalTo(copyAttendee.idNamespace))
            Assert.assertThat(resultLiveDate.value!![0].relationship, Matchers.equalTo(copyAttendee.relationship))
            Assert.assertThat(resultLiveDate.value!![0].type, Matchers.equalTo(copyAttendee.type))
            Assert.assertThat(resultLiveDate.value!![0].status, Matchers.equalTo(copyAttendee.status))
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteAttendee() {
        runBlocking {
            val addedAttendee = AttendeeEntity.Builder(listEventIds[0], ATTENDEE_NAME1)
                .withEmail("email")
                .withIdentity("Identity")
                .withIdNamespace("IdNamespace")
                .withRelationship(AttendeeEntity.Relationship.Speaker)
                .withStatus(AttendeeEntity.Status.Tentative)
                .withType(AttendeeEntity.Type.Required).build()

            suit.insertOrUpdateAttendee(addedAttendee)
            val resultLiveDate = suit.getAttendeeListLiveDate(listEventIds[0])
            resultLiveDate.observeForever{  }
            delay(50)
            suit.deleteAttendee(resultLiveDate.value!![0])
            delay(50)

            Assert.assertThat(resultLiveDate.value!!.size, Matchers.equalTo(0))
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteAttendee_byEvent() {
        runBlocking {
            suit.insertOrUpdateAttendee(AttendeeEntity.Builder(listEventIds[0], ATTENDEE_NAME1)
                .withEmail("email")
                .withIdentity("Identity")
                .withIdNamespace("IdNamespace")
                .withRelationship(AttendeeEntity.Relationship.Speaker)
                .withStatus(AttendeeEntity.Status.Tentative)
                .withType(AttendeeEntity.Type.Required).build())

            suit.insertOrUpdateAttendee(AttendeeEntity.Builder(listEventIds[0], ATTENDEE_NAME1)
                .withEmail("email2")
                .withIdentity("Identity")
                .withIdNamespace("IdNamespace")
                .withRelationship(AttendeeEntity.Relationship.Speaker)
                .withStatus(AttendeeEntity.Status.Tentative)
                .withType(AttendeeEntity.Type.Required).build())


            val resultLiveDate = suit.getAttendeeListLiveDate(listEventIds[0])
            resultLiveDate.observeForever{  }
            delay(50)
            suit.deleteAttendeeByEventId(listEventIds[0])
            delay(50)

            Assert.assertThat(resultLiveDate.value!!.size, Matchers.equalTo(0))
        }
    }


    fun createCalendarEntity(
        displayName: String? = null,
        accountName: String = CalendarEntity.ACCOUNT_LOCAL_NAME,
        @ColorInt color: Int = Color.TRANSPARENT,
        visible: Boolean = true
    ): CalendarEntity {
        return CalendarEntity.Builder().withDisplayName(displayName)
            .withAccountName(accountName).withColor(color).withVisible(visible)
            .build()
    }
}