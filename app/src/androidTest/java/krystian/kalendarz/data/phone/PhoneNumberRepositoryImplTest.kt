package krystian.kalendarz.data.phone

import LiveDataHelper
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.data.PhoneNumber
import krystian.kalendarz.data.database.AppDatabase
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith
import java.io.IOException

/**
 * Tests [PhoneNumberRepositoryImpl]
 **/
@RunWith(AndroidJUnit4::class)
class PhoneNumberRepositoryImplTest {

    companion object {
        private const val EVENT_ID_1 = 0L
        private const val EVENT_ID_2 = 1L
        private const val EVENT_ID_3 = 2L
        private const val EVENT_ID_4 = 3L
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    lateinit var suit: PhoneNumberRepositoryImpl
    private lateinit var db: AppDatabase

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        suit = PhoneNumberRepositoryImpl(db)
    }

    @After
    @Throws(IOException::class)
    fun after() {
        runBlocking {
            db.phoneDao().delete(EVENT_ID_1)
            db.phoneDao().delete(EVENT_ID_2)
            db.phoneDao().delete(EVENT_ID_3)
            db.phoneDao().delete(EVENT_ID_4)
        }

        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun getPhonesList_valid_simple() {
        runBlocking {
            suit.updatePhonesList(EVENT_ID_1, arrayListOf(PhoneNumber("8888")))

            val list = LiveDataHelper.getValue(suit.getPhonesList(EVENT_ID_1))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].phoneNumber, Matchers.equalTo("8888"))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getPhonesList_valid_international() {
        runBlocking {
            suit.updatePhonesList(EVENT_ID_1, arrayListOf(PhoneNumber("+48 34754 65 ")))

            val list = LiveDataHelper.getValue(suit.getPhonesList(EVENT_ID_1))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].phoneNumber, Matchers.equalTo("+48 34754 65 "))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getPhonesList_no_valid() {
        runBlocking {

            suit.updatePhonesList(EVENT_ID_1, arrayListOf(PhoneNumber("8888fffff")))

            val list = LiveDataHelper.getValue(suit.getPhonesList(EVENT_ID_1))

            Assert.assertThat(list!!.size, Matchers.equalTo(0))
        }
    }

    @Test
    fun updatePhoneNumber_overwrite() {
        runBlocking {
            suit.updatePhonesList(EVENT_ID_1, arrayListOf(PhoneNumber("+48 34754 65 ")))
            suit.updatePhonesList(EVENT_ID_1, arrayListOf(PhoneNumber("+48 34754 66 ")))

            val list = LiveDataHelper.getValue(suit.getPhonesList(EVENT_ID_1))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].phoneNumber, Matchers.equalTo("+48 34754 66 "))
        }
    }

    @Test
    fun updatePhoneNumber() {
        runBlocking {
            suit.updatePhonesList(EVENT_ID_1, arrayListOf(PhoneNumber("+48 34754 65 ")))
            suit.updatePhonesList(EVENT_ID_2, arrayListOf(PhoneNumber("+48 34754 66 ")))

            var list = LiveDataHelper.getValue(suit.getPhonesList(EVENT_ID_1))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].phoneNumber, Matchers.equalTo("+48 34754 65 "))

            list = LiveDataHelper.getValue(suit.getPhonesList(EVENT_ID_2))

            Assert.assertThat(list!!.size, Matchers.equalTo(1))
            Assert.assertThat(list!![0].phoneNumber, Matchers.equalTo("+48 34754 66 "))
        }
    }
}