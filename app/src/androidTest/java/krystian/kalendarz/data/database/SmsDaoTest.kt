package krystian.kalendarz.data.database

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.runBlocking
import krystian.kalendarz.data.database.sms.SmsEntity
import krystian.kalendarz.data.database.sms.SmsDao
import org.hamcrest.Matchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.*

/**
 * Tests [SmsDao]
 **/
@RunWith(AndroidJUnit4::class)
class SmsDaoTest {

    companion object {
        const val MAX_NUMBER_OF_TRY = 5
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var smsDao: SmsDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        smsDao = db.smsDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun getErrorSmsOrPending_Errors() {
        runBlocking {
            val smsError1 = createSms("smsError1", "smsError1", Date())
            smsError1.lasErrorCode = 0
            smsError1.lastTryDate = Date()
            smsError1.lasDateErrorCode = Date()
            smsDao.insertOrUpdate(smsError1)

            val smsError2 = createSms("smsError2", "smsError2", Date())
            smsError2.sentDate = Date()
            smsError2.isSent = true
            smsError2.lasErrorCode = 0
            smsError2.lastTryDate = Date()
            smsError2.lasDateErrorCode = Date()
            smsDao.insertOrUpdate(smsError2)

            val smsError3 = createSms("smsError3", "smsError3", Date())
            smsError3.lasErrorCode = 0
            smsError3.lastTryDate = Date()
            smsError3.lasDateErrorCode = Date()
            smsError3.numberOfTry = MAX_NUMBER_OF_TRY
            smsDao.insertOrUpdate(smsError3)

            val smsError4 = createSms("smsError4", "smsError4", Date())
            smsError4.lasErrorCode = 0
            smsError4.lastTryDate = Date()
            smsError4.lasDateErrorCode = Date()
            smsDao.insertOrUpdate(smsError4)


            var isSmsError1 = false
            var isSmsError2 = false
            var isSmsError3 = false
            var isSmsError4 = false
            smsDao.getErrorSmsOrPending(Date(), MAX_NUMBER_OF_TRY).forEach {
                if (it.phoneNumber == smsError1.phoneNumber) {
                    isSmsError1 = true
                }
                if (it.phoneNumber == smsError2.phoneNumber) {
                    isSmsError2 = true
                }
                if (it.phoneNumber == smsError3.phoneNumber) {
                    isSmsError3 = true
                }
                if (it.phoneNumber == smsError4.phoneNumber) {
                    isSmsError4 = true
                }
            }

            assertThat("Sms has Errors", isSmsError1, equalTo(true))
            assertThat("Sms was sent", isSmsError2, equalTo(false))
            assertThat("Sms has to much the number of try", isSmsError3, equalTo(false))
            assertThat("Sms has Errors ", isSmsError4, equalTo(true))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getErrorSmsOrPending_Pending() {
        runBlocking {
            val smsPending = createSms("smsPending", "smsPending", Date())
            smsPending.isPending = true
            smsDao.insertOrUpdate(smsPending)

            val smsOverTray = createSms("smsOverTray", "smsOverTray", Date())
            smsOverTray.isPending = true
            smsOverTray.numberOfTry = MAX_NUMBER_OF_TRY
            smsDao.insertOrUpdate(smsOverTray)


            var isSmsPending = false
            var isSmsOverTray = false
            smsDao.getErrorSmsOrPending(Date(), MAX_NUMBER_OF_TRY).forEach {
                if (it.phoneNumber == smsPending.phoneNumber) {
                    isSmsPending = true
                }
                if (it.phoneNumber == smsOverTray.phoneNumber) {
                    isSmsOverTray = true
                }
            }

            assertThat("Sms is Pending", isSmsPending, equalTo(true))
            assertThat("Sms has to much the number of try", isSmsOverTray, equalTo(false))
        }

    }

    @Test
    @Throws(Exception::class)
    fun getSmsReadyToSendList() {
        runBlocking {
            val smsReady = createSms("smsReady", "smsReady", Date())
            smsDao.insertOrUpdate(smsReady)

            val smsNotReadyIsPending = createSms("smsNotReadyIsPending", "smsNotReadyIsPending", Date())
            smsNotReadyIsPending.isPending = true
            smsDao.insertOrUpdate(smsNotReadyIsPending)

            val smsNotReadyHasErrors = createSms("smsNotReadyHasErrors", "smsNotReadyHasErrors", Date())
            smsNotReadyHasErrors.lasErrorCode = 0
            smsNotReadyHasErrors.lastTryDate = Date()
            smsNotReadyHasErrors.lasDateErrorCode = Date()
            smsNotReadyHasErrors.numberOfTry = MAX_NUMBER_OF_TRY
            smsDao.insertOrUpdate(smsNotReadyHasErrors)

            val smsWasSent = createSms("smsWasSent", "smsWasSent", Date())
            smsWasSent.isSent = true
            smsWasSent.sentDate = Date()
            smsDao.insertOrUpdate(smsWasSent)


            var isSmsReady = false
            var isSmsNotReadyIsPending = false
            var isSmsNotReadyHasErrors = false
            var isSmsWasSent = false
            smsDao.getSmsReadyToSendList(Date(), MAX_NUMBER_OF_TRY).forEach {
                if (it.phoneNumber == smsReady.phoneNumber) {
                    isSmsReady = true
                }
                if (it.phoneNumber == smsNotReadyIsPending.phoneNumber) {
                    isSmsNotReadyIsPending = true
                }
                if (it.phoneNumber == smsNotReadyHasErrors.phoneNumber) {
                    isSmsNotReadyHasErrors = true
                }
                if (it.phoneNumber == smsWasSent.phoneNumber) {
                    isSmsWasSent = true
                }
            }

            assertThat("Sms is ready to send", isSmsReady, equalTo(true))
            assertThat("Sms is pending", isSmsNotReadyIsPending, equalTo(true))
            assertThat("Sms has errors", isSmsNotReadyHasErrors, equalTo(false))
            assertThat("Sms was sent ", isSmsWasSent, equalTo(false))
        }
    }

    @Test
    @Throws(Exception::class)
    fun getSmsToScheduleList() {
        runBlocking {
            val smsScheduled = createSms("isSmsScheduled", "isSmsScheduled", Date())
            smsScheduled.nextDateOfTry = Date(Date().time + 3000)
            smsDao.insertOrUpdate(smsScheduled)

            val smsNotScheduled = createSms("smsNotScheduled", "smsNotScheduled", Date())
            smsNotScheduled.nextDateOfTry = Date(Date().time - 3000)
            smsDao.insertOrUpdate(smsNotScheduled)

            val smsHasErrors = createSms("smsHasErrors", "smsHasErrors", Date())
            smsHasErrors.nextDateOfTry = Date(Date().time + 3000)
            smsHasErrors.lasErrorCode = 0
            smsHasErrors.lastTryDate = Date()
            smsHasErrors.lasDateErrorCode = Date()
            smsHasErrors.numberOfTry = MAX_NUMBER_OF_TRY -1
            smsDao.insertOrUpdate(smsHasErrors)

            val smsHasErrorsWithMaxOfTry = createSms("smsHasErrorsWithMaxOfTry", "smsHasErrorsWithMaxOfTry", Date())
            smsHasErrorsWithMaxOfTry.nextDateOfTry = Date(Date().time + 3000)
            smsHasErrorsWithMaxOfTry.lasErrorCode = 0
            smsHasErrorsWithMaxOfTry.lastTryDate = Date()
            smsHasErrorsWithMaxOfTry.lasDateErrorCode = Date()
            smsHasErrorsWithMaxOfTry.numberOfTry = MAX_NUMBER_OF_TRY
            smsDao.insertOrUpdate(smsHasErrorsWithMaxOfTry)

            val smsWasSent = createSms("smsWasSent", "smsWasSent", Date())
            smsScheduled.nextDateOfTry = Date(Date().time + 3000)
            smsWasSent.isSent = true
            smsWasSent.sentDate = Date()
            smsDao.insertOrUpdate(smsWasSent)


            var isSmsScheduled = false
            var isSmsNotScheduled= false
            var isSmsHasErrors = false
            var isSmsWasSent = false
            var isSmsHasErrorsWithMaxOfTry = false
            smsDao.getSmsToScheduleList(Date(), MAX_NUMBER_OF_TRY).forEach {
                if (it.phoneNumber == smsScheduled.phoneNumber) {
                    isSmsScheduled = true
                }
                if (it.phoneNumber == smsNotScheduled.phoneNumber) {
                    isSmsNotScheduled = true
                }
                if (it.phoneNumber == smsHasErrors.phoneNumber) {
                    isSmsHasErrors = true
                }
                if (it.phoneNumber == smsWasSent.phoneNumber) {
                    isSmsWasSent = true
                }
                if (it.phoneNumber == smsHasErrorsWithMaxOfTry.phoneNumber) {
                    isSmsHasErrorsWithMaxOfTry = true
                }
            }

            assertThat("Sms is sms scheduled", isSmsScheduled, equalTo(true))
            assertThat("Sms is not scheduled", isSmsNotScheduled, equalTo(false))
            assertThat("Sms has errors", isSmsHasErrors, equalTo(true))
            assertThat("Sms was sent ", isSmsWasSent, equalTo(false))
            assertThat("Sms has errors and max of try ", isSmsHasErrorsWithMaxOfTry, equalTo(false))
        }
    }

    private fun createSms(phoneNumber: String, text: String, date: Date): SmsEntity {
        return SmsEntity(
            null,
            phoneNumber,
            text,
            date,
            false,
            null,
            date,
            0,
            false,
            null,
            false,
            null,
            null,
            null
        )
    }
}